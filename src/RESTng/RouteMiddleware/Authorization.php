<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 12:34 PM
 */

namespace MiamiOH\RESTng\RouteMiddleware;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Apm\Transaction;

class Authorization extends RouteMiddleware
{

    public function call(App $app, array $options): void
    {
        /** @var Transaction $transaction */
        $transaction = $app->getService('ApmTransaction');
        $span = $transaction->startSpan('Authorization', 'middleware');

        /** @var \MiamiOH\RESTng\Util\User $apiUser */
        $apiUser = $app->getService('APIUser');
        $authorized = $apiUser->checkAuthorization($options);

        $span->stop();

        if (!$authorized) {
            # TODO - render a suitable response to get proper formatting

            // Allow domain for CORS so the consumer can handle the response
            if (isset($app->environment['HTTP_ORIGIN'])) {
                $app->response()->header('Access-Control-Allow-Origin', $app->environment['HTTP_ORIGIN']);
            }

            $transaction->setName('RESTng Authorization Middleware Failed');

            $app->halt(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
        }
    }
}
