<?php

namespace MiamiOH\RESTng\Connector;

use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Propel;

/**
 * @codeCoverageIgnore
 *
 * This class interacts directly with Prople static methods, making
 * it difficult to test.
 */
class ORMManager
{

    /**
     * @param string $class
     * @return mixed
     */
    public function newObject($class)
    {

        # TODO: verify that class is in the ORM namespace
        return new $class();

    }

    /**
     * @param $class
     * @param $id
     * @return mixed
     */
    public function newObjectById($class, $id)
    {
        $queryClass = $class. 'Query';
        return $queryClass::create()->findPk($id);
    }

    /**
     * @return \Propel\Runtime\ServiceContainer\ServiceContainerInterface
     */
    public function getServiceContainer()
    {
        return \Propel\Runtime\Propel::getServiceContainer();
    }

    /**
     * @param string $databaseName
     * @return ConnectionInterface A database connection
     */
    public function getReadConnection($databaseName)
    {
        return Propel::getReadConnection($databaseName);
    }

    /**
     * @param string $databaseName
     * @return ConnectionInterface A database connection
     */
    public function getWriteConnection($databaseName)
    {
        return Propel::getWriteConnection($databaseName);
    }

}
