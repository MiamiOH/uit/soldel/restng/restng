<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/8/18
 * Time: 10:44 AM
 */

namespace MiamiOH\RESTng\Observer;


abstract class Observer
{
    private $observations = [];

    public function observations(): array
    {
        return $this->observations;
    }

    public function clear(): void
    {
        $this->observations = [];
    }

    protected function recordObservations(array $observations): void
    {
        foreach ($observations as $key => $fact) {
            $this->recordObservation($key, $fact);
        }
    }

    protected function recordObservation(string $key, string $fact): void
    {
        $this->observations[$key] = $fact;
    }

    abstract public function update(): void;

}