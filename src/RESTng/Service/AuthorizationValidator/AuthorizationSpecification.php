<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/4/18
 * Time: 2:25 PM
 */

namespace MiamiOH\RESTng\Service\AuthorizationValidator;


class AuthorizationSpecification
{

    /**
     * @var string
     */
    private $application;
    /**
     * @var string
     */
    private $module;
    /**
     * @var string
     */
    private $key;

    private function __construct(string $application, string $module, string $key)
    {
        $this->application = $application;
        $this->module = $module;
        $this->key = $key;
    }

    public static function fromValues(string $application, string $module, string $key): self
    {
        return new self($application, $module, $key);
    }

    public function application(): string
    {
        return $this->application;
    }

    public function module(): string
    {
        return $this->module;
    }

    public function key(): string
    {
        return $this->key;
    }
}