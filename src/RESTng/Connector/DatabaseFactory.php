<?php

namespace MiamiOH\RESTng\Connector;

class DatabaseFactory
{

    private $dataSource;

    /**
     * @param $dataSource
     */
    public function setDataSource($dataSource)
    {
        $this->dataSource = $dataSource;
    }

    /**
     * @param $datasourceName
     * @return \MiamiOH\RESTng\Legacy\DB\DBH
     */
    public function getHandle($datasourceName)
    {
        $dbh = $this->dataSource->Factory($datasourceName);

        return $dbh;
    }

}
