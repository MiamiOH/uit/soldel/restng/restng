<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:59 PM
 */

namespace MiamiOH\RESTng\Testing\Data;


use MiamiOH\RESTng\Testing\Data\Database\Sqlite;

/**
 * Class Factory
 * @package MiamiOH\RESTng\Testing\Data
 *
 * @codeCoverageIgnore
 */
class Factory
{
    /** @var DatabaseInterface */
    protected $database;

    private $withTables = [];

    /** @var Table[]  */
    private $tables = [];

    private $tablesAreGenerated = false;
    private $tablesArePopulated = false;

    public function withSqlite(\SQLite3 $db = null): self
    {
        if ($db === null) {
            $db = new \SQLite3(':memory:');
        }

        $this->database = new Database\Sqlite($db);

        return $this;
    }

    public function withTable(string $tableClass, array $options = []): self
    {
        $this->withTables[$tableClass] = [
            'options' => $options,
        ];

        $this->tablesAreGenerated = false;
        $this->tablesArePopulated = false;

        return $this;
    }

    public function generate(): self
    {
        if ($this->tablesAreGenerated) {
            return $this;
        }

        $this->prepareTables();

        foreach ($this->tables as $table) {
            $table->create();
        }

        $this->tablesAreGenerated = true;

        return $this;
    }

    public function populate(): self
    {
        if ($this->tablesArePopulated) {
            return $this;
        }

        $this->generate();

        foreach ($this->tables as $table) {
            $table->populate();
        }

        $this->tablesArePopulated = true;

        return $this;
    }

    private function prepareTables(): void
    {
        if ($this->database === null) {
            $this->withSqlite();
        }

        $this->tables = [];

        foreach ($this->withTables as $tableClass => $tableConfig) {
            $this->tables[] = new $tableClass($this->database, $tableConfig['options']);
        }
    }

}