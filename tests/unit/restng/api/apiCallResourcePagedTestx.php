<?php

class apiCallResourcePagedTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $restngApp;
    private $request;
    private $response;
    private $service;
    private $apiUser;
    private $ormManager;
    private $apiDeferredCallMgr;

    // members to hold run time values for asserting
    private $resourceName = '';
    private $serviceName = '';
    private $config = array();
    private $options = array();

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->resourceName = '';
        $this->serviceName = '';
        $this->config = array();
        $this->options = array();

        $this->restngApp = new \MiamiOH\RESTng\App();

        $this->service = $this->getMockBuilder('MiamiOH\RESTng\Service')
            ->setMethods(array('setRequest', 'getTestObject'))
            ->getMock();

        $this->apiDeferredCallMgr = $this->getMockBuilder('MiamiOH\RESTng\Util\DeferredCallManager')
            ->setMethods(array('addDeferredCall'))
            ->getMock();

        $this->response = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
            ->setMethods(array('getStatus'))
            ->getMock();

        $this->service->method('setRequest')
            ->with($this->callback(array($this, 'setRequestWith')))
            ->willReturn(true);

        $this->service->method('getTestObject')
            ->willReturn($this->response);

        $this->request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'setResourceConfig',
                'setResourceName',
                'setData',
                'setOptions',
                'setResourceParams',
                'setPartialFields',
                'extractRequestOptions',
            ))
            ->getMock();

        $this->request->method('setResourceConfig')
            ->with($this->callback(array($this, 'setResourceConfigWith')))
            ->willReturn(true);

        $this->request->method('setResourceName')
            ->with($this->callback(array($this, 'setResourceNameWith')))
            ->willReturn(true);

        $this->request->method('setData')
            ->with($this->callback(array($this, 'setDataWith')))
            ->willReturn(true);

        $this->request->method('setResourceParams')
            ->with($this->callback(array($this, 'setResourceParamsWith')))
            ->willReturn(true);

        $this->request->method('setPartialFields')
            ->with($this->callback(array($this, 'setPartialFieldsWith')))
            ->willReturn(true);

        $this->request->method('extractRequestOptions')
            ->willReturn(true);

        $requestFactory = $this->getMockBuilder('MiamiOH\RESTng\Util\RequestFactory')
            ->setMethods(array('newRequest'))
            ->getMock();

        $requestFactory->method('newRequest')
            ->willReturn($this->request);

        $this->restngApp->setRequestFactory($requestFactory);

        $this->restngApp->setResponseFactory(new MiamiOH\RESTng\Util\ResponseFactory());

        $app = $this->getMockBuilder('MiamiOH\RESTng\Util\AppInstance')
            ->setMethods(array('getInstance'))
            ->getMock();

        // The Slim class cannot be mocked, possibly due to static methods.
        $slim = $this->getMockBuilder('stdClass')
            ->setMethods(array('loadConfigForResourceName', 'getResourceByName', 'getService'))
            ->getMock();

        $slim->method('loadConfigForResourceName')
            ->with($this->callback(array($this, 'loadConfigForResourceNameWith')))
            ->will($this->returnCallback(array($this, 'loadConfigMock')));

        $slim->method('getResourceByName')
            ->with($this->callback(array($this, 'getResourceByNameWith')))
            ->will($this->returnCallback(array($this, 'loadResourceMock')));

        $slim->method('getService')
            ->will($this->returnCallback(array($this, 'getServiceMock')));

        $app->method('getInstance')->willReturn($slim);

        $this->restngApp->setAppInstance($app);
    }

    public function testCallResourceWithPagedOptions()
    {

        $this->resourceName = 'test.resource';

        $this->options = array(
            'options' => array('offset' => 1, 'limit' => 15),
        );

        /*
         * Replace the main setOptions mock with a mock that expects there to be no
         * limit or offset defined.
         */
        $this->request->method('setOptions')
            ->with($this->callback(array($this, 'setOptionsWithPaged')))
            ->willReturn(true);

        $response = $this->restngApp->callResource($this->resourceName, $this->options);

        $this->assertEquals($this->response, $response);

    }

    public function loadConfigMock($resourceName)
    {
        switch ($resourceName) {
            case 'test.resource':
                return true;
                break;

        }

        return false;
    }

    public function loadResourceMock($resourceName)
    {
        switch ($resourceName) {
            case 'test.resource':
                $this->serviceName = 'TestService';
                $this->config = array(
                    'name' => 'test.resource',
                    'service' => $this->serviceName,
                    'method' => 'getTestObject',
                    'action' => 'read',
                    'isPageable' => false,
                    'isPartialable' => false,
                    'isComposable' => false,
                );
                return $this->config;
                break;

            case 'test.resource.create':
                $this->serviceName = 'TestService';
                $this->config = array(
                    'name' => 'test.resource',
                    'service' => $this->serviceName,
                    'method' => 'getTestObject',
                    'action' => 'create',
                    'isPageable' => false,
                    'isPartialable' => false,
                    'isComposable' => false,
                );
                return $this->config;
                break;

            case 'test.resource.unknown.service':
                $this->serviceName = 'TestServiceUnknown';
                $this->config = array(
                    'name' => 'test.resource',
                    'service' => $this->serviceName
                );
                return $this->config;
                break;

            case 'test.resource.missing.method':
                $this->serviceName = 'TestService';
                $this->config = array(
                    'name' => 'test.resource',
                    'service' => $this->serviceName
                );
                return $this->config;
                break;

            case 'test.resource.unknown.method':
                $this->serviceName = 'TestService';
                $this->config = array(
                    'name' => 'test.resource',
                    'service' => $this->serviceName,
                    'method' => 'unknownMethod',
                );
                return $this->config;
                break;

        }

        return false;
    }

    public function getServiceMock($serviceName)
    {
        switch ($serviceName) {
            case 'TestService':
                return $this->service;
                break;

            case 'APIUser':
                return $this->apiUser;
                break;

            case 'ORMManager':
                return $this->ormManager;
                break;

            case 'APIDeferredCallManager':
                return $this->apiDeferredCallMgr;
                break;

        }

        return null;
    }

    public function setRequestWith($subject)
    {
        $this->assertEquals($this->request, $subject);
        return $this->request === $subject;
    }

    public function setResourceConfigWith($subject)
    {
        $this->assertEquals($this->config, $subject);
        return $this->config === $subject;
    }

    public function setResourceNameWith($subject)
    {
        $this->assertEquals($this->resourceName, $subject);
        return $this->resourceName === $subject;
    }

    public function setDataWith($subject)
    {
        $this->assertEquals($this->options['data'], $subject);
        return $this->options['data'] === $subject;
    }

    public function setOptionsWith($subject)
    {
        $this->assertEquals($this->options['options'], $subject);
        return $this->options['options'] === $subject;
    }

    public function setOptionsWithPaged($subject)
    {
        $this->assertFalse(array_key_exists('limit', $subject), 'Limit is removed from options array');
        $this->assertFalse(array_key_exists('offset', $subject), 'Offset is removed from options array');
        return true;
    }

    public function setResourceParamsWith($subject)
    {
        $this->assertEquals($this->options['params'], $subject);
        return $this->options['params'] === $subject;
    }

    public function setPartialFieldsWith($subject)
    {
        $this->assertEquals($this->options['fields'], $subject);
        return $this->options['fields'] === $subject;
    }

    public function loadConfigForResourceNameWith($subject)
    {
        $this->assertEquals($this->resourceName, $subject);
        return $this->resourceName === $subject;
    }

    public function getResourceByNameWith($subject)
    {
        $this->assertEquals($this->resourceName, $subject);
        return $this->resourceName === $subject;
    }

    public function addDeferredCallMock()
    {
        return $this->response;
    }
}
