<?php


namespace MiamiOH\RESTng\Testing;


use Illuminate\Database\Capsule\Manager;
use MiamiOH\RESTng\Connector\DatabaseCapsule;
use PDO;
use Phinx\Config\Config;
use Phinx\Migration\Manager as PhinxManager;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\NullOutput;

/**
 * Trait RefreshDatabase
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
trait RefreshDatabase
{
    /** @var DatabaseCapsule */
    private $capsule;

    /** @var PhinxManager */
    private $phinxManager;

    protected function refreshDatabase(): void
    {
        if (null !== $this->phinxManager) {
            $this->phinxManager->rollback('testing');
            return;
        }

        $pdo = new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);

        $configArray = [
            'paths' => [
                'migrations' => './db/migrations',
                'seeds' => './db/seeds'
            ],
            'environments' => [
                'default_migration_table' => 'migrations',
                'default_environment' => 'testing',
                'testing' => [
                    'adapter' => 'sqlite',
                    'connection' => $pdo,
                ]
            ],
            'version_order' => 'creation'
        ];
        $config = new Config($configArray);

//        $output = new ConsoleOutput();
        $output = new NullOutput();

        $this->phinxManager = new PhinxManager($config, new StringInput(' '), $output);
        $this->phinxManager->migrate('testing');
        $this->phinxManager->seed('testing');

        $connection = Manager::connection();
        $connection->setPdo($pdo);
    }

    protected function resetDatabase(): void
    {
        if (null !== $this->phinxManager) {
            $this->phinxManager->rollback('testing');
            return;
        }

        $pdo = new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);

        $connection = Manager::connection();
        $connection->setPdo($pdo);
    }
}