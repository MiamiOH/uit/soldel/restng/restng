<?php

class stageTypeTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = new \MiamiOH\RESTng\App();

    }

    public function testSetCurrentStage() {
        $this->assertEquals('development', $this->restngApp->getCurrentStage());
        $this->restngApp->setCurrentStage('test');
        $this->assertEquals('test', $this->restngApp->getCurrentStage());
    }

    public function testSetCurrentStageInvalid() {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Cannot set current stage to testing. Unsupported stage type.');

        $this->restngApp->setCurrentStage('testing');
    }

    public function testIsPriorToTrue() {
        $this->restngApp->setCurrentStage('development');
        $isPrior = $this->restngApp->isStagePriorTo('test');

        $this->assertTrue($isPrior);
    }

    public function testIsPriorToFalse() {
        $this->restngApp->setCurrentStage('production');
        $isPrior = $this->restngApp->isStagePriorTo('test');

        $this->assertFalse($isPrior);
    }

}
