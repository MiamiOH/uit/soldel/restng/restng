<?php

namespace MiamiOH\RESTng\Util;

use Carbon\Carbon;
use Logger;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Service\Apm\TransactionFactory;

class DeferredCallProcessor
{
    public const RETRY_DELAY = 5 * 60;
    public const MAX_FAILURES = 3;

    /** @var Logger  */
    private $logger;

    /** @var \MiamiOH\RESTng\App $app */
    private $app;

    /** @var \MiamiOH\RESTng\Util\User $apiUser */
    private $apiUser;

    /** @var \MiamiOH\RESTng\Util\ResponseFactory */
    private $responseFactory;

    /** @var TransactionFactory */
    private $transactionFactory;

    /** @var Transaction */
    private $transaction;

    private $maxFailures = self::MAX_FAILURES;
    private $retryDelay = self::RETRY_DELAY;

    public function __construct()
    {
        $this->logger = Logger::getLogger('cli');
    }

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setApp($app)
    {
        /** @var \MiamiOH\RESTng\App $app */
        $this->app = $app;
    }

    public function setApiUser($apiUser)
    {
        /** @var \MiamiOH\RESTng\Util\User $apiUser */
        $this->apiUser = $apiUser;
    }

    public function setResponseFactory(ResponseFactory $factory)
    {
        $this->responseFactory = $factory;
    }

    public function setApmTransactionFactory(TransactionFactory $factory): void
    {
        $this->transactionFactory = $factory;
    }

    public function setMaxFailures(int $maxFailures): void
    {
        $this->maxFailures = $maxFailures;
    }

    public function setRetryDelay(int $retryDelay): void
    {
        $this->retryDelay = $retryDelay;
    }

    /**
     * @param DeferredCall $call
     * @param int $maxFailures
     * @param int|float $retryDelay
     * @throws DeferredCallException
     * @throws \Exception
     */
    public function processCall(DeferredCall $call): void
    {
        $this->verify($call);

        $this->prepare($call);

        $response = $this->execute($call);

        $this->update($call, $response);

        $this->finalize($response, $call);
    }

    /**
     * @param DeferredCall $call
     * @throws DeferredCallException
     */
    protected function verify(DeferredCall $call): void
    {
        if ($call->isNotPending()) {
            throw new DeferredCallException(sprintf('Call %s status is not pending: %s', $call->id(), $call->status()));
        }

        if ($call->verifyCallHash()) {
            return;
        }

        $this->logger->warn('Call ID ' . $call->id() . ' hash check failed, cancelling call');
        $call->markCancelled();
        $call->save();

        throw new DeferredCallException(sprintf('Attempt to verify call id %s failed', $call->id()));
    }

    /**
     * @param DeferredCall $call
     * @return array
     */
    protected function prepare(DeferredCall $call): void
    {
        $name = $call->resourceName();
        $params = $call->resourceParams();

        $this->apiUser->assumeUser($call->apiUser());

        $this->transaction = $this->transactionFactory->newTransaction($name);

        $this->logger->debug('Starting job for ' . $name);
        $this->transaction->setUserContext($this->apiUser);
        $this->transaction->setRequestContextFromArray($params);

        $this->app->useService([
            'name' => 'ApmTransaction',
            'object' => $this->transaction,
            'description' => 'An APM transaction',
        ]);
    }

    /**
     * @param DeferredCall $call
     * @param $name
     * @param $params
     * @return Response
     * @throws \Exception
     */
    protected function execute(DeferredCall $call): Response
    {
        $name = $call->resourceName();
        $params = $call->resourceParams();

        try {
            $call->markInProgress();
            $call->save();
            $this->logger->debug('Call ID ' . $call->id() . ' set status to i');

            $response = $this->app->callResource($name, $params);
        } catch (\Exception $e) {
            $this->logger->error('Call ID ' . $call->id() . ' exception', $e);

            $response = $this->responseFactory->newResponse();

            $payload = array(
                'message' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            );

            $response->setStatus(App::API_FAILED);
            $response->setPayload($payload);
        }

        return $response;
    }

    /**
     * @param DeferredCall $call
     * @param Response $response
     */
    protected function update(DeferredCall $call, Response $response): void
    {
        $call->addResponse($response);
        $call->incrementAttemptCount();

        $this->transaction->setMetadata(['type' => 'Deferred Call', 'result' => $response->getStatus()]);
        $this->transaction->send();
    }

    /**
     * @param Response $response
     * @param DeferredCall $call
     */
    protected function finalize(Response $response, DeferredCall $call): void
    {
        if ($response->isSuccess()) {
            $this->logger->info('Call ID ' . $call->id() . ' succeeded');
            $call->markComplete();
        } elseif ($response->getStatus() === App::API_PRECONDITIONREQUIRED) {
            $this->logger->info('Call ID ' . $call->id() . ' returned precondition requested');
            $payload = $response->getPayload();
            $delay = $payload['delay'] ?? $this->retryDelay;

            $call->scheduleAt(Carbon::now()->addSeconds($delay));
            $call->markPending();
            $this->logger->debug('Call ID ' . $call->id() . ' set status to p');
        } else {
            $this->logger->warn('Call ID ' . $call->id() . ' failed');
            $call->incrementErrorCount();
            if ($call->errorCount() >= $this->maxFailures) {
                $call->markFailed();
                $this->logger->debug('Call ID ' . $call->id() . ' set status to f');
            } else {
                $call->scheduleAt(Carbon::now()->addSeconds($this->retryDelay));
                $call->markPending();
                $this->logger->debug('Call ID ' . $call->id() . ' set status to p');
            }
        }

        $call->save();
    }
}
