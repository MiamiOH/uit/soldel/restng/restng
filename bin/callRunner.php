<?php

require_once(__DIR__ . '/../vendor/autoload.php');

require(__DIR__ . '/../loader.php');

$retryDelay = 5 * 60;
$maxFailures = 3;

/** @var \RESTng\Util\API $api */
/** @var \RESTng\Slim $app */

$app = $api->getAppInstance();

$logger = \Logger::getLogger('cli');

/** @var \RESTng\Connector\ORMManager $ormMgr */
$ormMgr = $app->getService('ORMManager');

/** @var \RESTng\Util\User $apiUser */
$apiUser = $app->getService('APIUser');

/** @var \RESTng\Util\ResponseFactory $responseFactory */
$responseFactory = $app->getService('APIServiceResponseFactory');

// TODO: move most of this into the deferred call manager
/** @var \RESTng\Storage\DeferredCallQuery $callQuery */
$callQuery = $ormMgr->newObject('\RESTng\Storage\DeferredCallQuery');

//$con = \Propel\Runtime\Propel::getWriteConnection(\RESTng\Storage\Map\DeferredCallTableMap::DATABASE_NAME);
//$con->useDebug(true);
//echo $con->getLastExecutedQuery();
$nextCallQuery = $callQuery
    ->filterByStatus('p')
    ->where('scheduled_time < current_timestamp') // exclude future dated
    ->orderByScheduledTime('asc') // order by oldest first
    ;

// Use while loop to continue polling for calls
while ($call = $nextCallQuery->findOne()) {

    //echo $con->getLastExecutedQuery();

    if (!$call->verifyCallHash()) {
        $logger->warn('Call ID ' . $call->getId() . ' hash check failed, cancelling call');
        $call->setStatus('x');
        $call->save();
        continue;
    }

    // Set the status to 'i', in progress
    $call->setStatus('i');
    $call->save();
    $logger->debug('Call ID ' . $call->getId() . ' set status to i');

    $name = $call->getResourceName();
    $params = $call->getResourceParams();

    $apiUser->unpack($call->getApiUser());

    try {
        /** @var \RESTng\Util\Response $response */
        $response = $api->callResource($name, $params);
    } catch (\Exception $e) {
        $logger->error('Call ID ' . $call->getId() . ' exception', $e);

        $response = $responseFactory->newResponse();

        $payload = array(
            'message' => $e->getMessage(),
            'line' => $e->getLine(),
            'file' => $e->getFile(),
        );

        // Only show the stack trace in stages prior to testing. The trace will still be in the
        // log and may expose sensitive information.
        if ($app->isStagePriorTo('test')) {
            $payload['trace'] = $e->getTraceAsString();
        }

        $response->setStatus(\RESTng\API_FAILED);
        $response->setPayload($payload);
    }

    $call->setResponse($response);

    $call->incrementAttemptCount();

    // if isSuccess (status between 200 and 299)
    if ($response->isSuccess()) {
        $logger->info('Call ID ' . $call->getId() . ' succeeded');
        $call->markComplete();
    } elseif ($response->getStatus() === \RESTng\API_PRECONDITIONREQUIRED) {
        $logger->info('Call ID ' . $call->getId() . ' returned precondition requested');
        $payload = $response->getPayload();
        $delay = isset($payload['delay']) ? $payload['delay'] : $retryDelay;

        $call->setScheduledTime(time() + $delay);
        $call->setStatus('p');
        $logger->debug('Call ID ' . $call->getId() . ' set status to p');
    } else {
        $logger->warn('Call ID ' . $call->getId() . ' failed');
        $call->incrementErrorCount();
        if ($call->getErrorCount() >= $maxFailures) {
            $call->setStatus('f');
            $logger->debug('Call ID ' . $call->getId() . ' set status to f');
        } else {
            $call->setScheduledTime(time() + $retryDelay);
            $call->setStatus('p');
            $logger->debug('Call ID ' . $call->getId() . ' set status to p');
        }
    }

    $call->save();

}

//echo $con->getLastExecutedQuery();
