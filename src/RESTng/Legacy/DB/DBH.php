<?php

namespace MiamiOH\RESTng\Legacy\DB;

use MiamiOH\RESTng\Service\Apm\Transaction;

define('DB_ERROR', true);
define('DB_NO_ERROR', '');
define('DB_EMPTY_SET', '-1000: EMPTY SET');

class DBH {

    /** @var Transaction */
    protected $transaction;

	protected $user;
	protected $database;
	protected $type;
	protected $connected = false;
	protected $db;

	protected $query;
	protected $auto_commit = true;

	protected $statement_tracking = false;
	protected $_statements = array();

	protected $lastStatement = '';
	protected $lastStatementParsed = '';
	protected $lastParameters = array();

	//Error related members
	protected $error = DB_NO_ERROR;
	protected $error_string;
	protected $error_num;

	/**
	 * @param string $type
	 * @param string $user
	 * @param string $password
	 * @param string $database
	 * @param string $host
	 * @param string $connect_type
	 * @return mixed
     */
	static public function Factory($type = '', $user = '', $password = '', $database = '', $host = '', $connect_type = '')
	{

		$newclassname = 'MiamiOH\\RESTng\\Legacy\\DB\\DBH\\' . $type;

		return new $newclassname($user, $password, $database, $host, $connect_type);
	}

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
	 * @return mixed
     */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return mixed
     */
	public function getErrorNum()
	{
		return $this->error_num;
	}

	/**
	 * @return mixed
     */
	public function getErrorString()
	{
		return $this->error_string;
	}

	/**
	 * @return mixed
     */
	public function getDbConnection()
	{
		return $this->db;
	}

	/**
	 * @throws \Exception
     */
	public function connect()
	{
		throw new \Exception('Method "connect" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @throws \Exception
     */
	public function disconnect()
	{
		throw new \Exception('Method "disconnect" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @return MU_STH
     */
	public function prepare($statement)
	{
		return new MU_STH($statement);
	}

	/**
	 * @param $statement
	 * @return bool
     */
	public function perform($statement)
	{
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		return $this->_run($statement, $parameters);
	}

	/**
	 * @param $statement
	 * @param array $parameters
	 * @return bool
	 * @throws \Exception
     */
	public function _run($statement, $parameters = array())
	{
		$this->lastStatementParsed = $statement;
		$this->lastStatement = $this->_parse_placeholders($statement, $parameters);
		$this->lastParameters = $parameters;

		return false;
	}

	/**
	 * @return string
     */
	public function getLastStatement()
	{
		return $this->lastStatement;
	}

	/**
	 * @return string
     */
	public function getLastStatementParsed()
	{
		return $this->lastStatementParsed;
	}

	/**
	 * @return array
     */
	public function getLastParameters()
	{
		return $this->lastParameters;
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryfirstcolumn($statement)
	{
		throw new \Exception('Method "queryfirstcolumn" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryfirstrow_assoc($statement)
	{
		throw new \Exception('Method "queryfirstrow_assoc" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryfirstrow_array($statement)
	{
		throw new \Exception('Method "queryfirstrow_array" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryall_assoc($statement)
	{
		throw new \Exception('Method "queryall_assoc" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryall_array($statement)
	{
		throw new \Exception('Method "queryall_array" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param $statement
	 * @throws \Exception
     */
	public function queryall_list($statement)
	{
		throw new \Exception('Method "queryall_list" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param bool $value
	 * @return bool
	 * @throws \Exception
     */
	public function auto_commit($value = true)
	{
		if ($value === true || $value === false) {
			$this->auto_commit = $value;
		} else {
			throw new \Exception('Value provided for auto_commit must be boolean true or false');
		}

		return $this->auto_commit;
	}

	/**
	 * @return bool
     */
	public function getAutoCommit()
	{
		return $this->auto_commit;
	}

	/**
	 * @throws \Exception
     */
	public function commit()
	{
		throw new \Exception('Method "commit" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @throws \Exception
     */
	public function rollback()
	{
		throw new \Exception('Method "rollback" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @throws \Exception
     */
	public function error()
	{
		throw new \Exception('Method "error" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @param string $string
	 * @return mixed
     */
	public function escape($string = '')
	{
		return str_replace('\'', '\'\'', $string);
	}

	/**
	 * @param $list
	 * @return string
     */
	public function quote_list($list)
	{
		$quoted_list = implode(', ', array_map('quote_argument', $list));

		return $quoted_list;
	}

	/**
	 * @param $statement
	 * @param $parameters
	 * @return mixed
	 * @throws \Exception
     */
	public function _parse_placeholders($statement, $parameters)
	{
		$p_query = $statement;
		$placeholder = '?';

		$ph_count = substr_count($statement, $placeholder);

		if ($ph_count != count($parameters)) {
			throw new \Exception('Placeholder/argument count mismatch: (' . $ph_count . '/' . count($parameters) . ')');
		}

		$offset = strpos($statement, $placeholder) - 1; //Starting offset

		for ($i = 0; $i < count($parameters); $i++) {
			$value = null;

			if (is_int($parameters[$i]) || is_float($parameters[$i])) {
				$value = $this->escape($parameters[$i]);
			} elseif (is_string($parameters[$i])) {
				$value = '\'' . $this->escape($parameters[$i]) . '\'';
			} elseif (is_null($parameters[$i])) {
				$value = 'NULL';
			} elseif (is_bool($parameters[$i])) {
				$value = $parameters[$i] ? 1 : 0;
			} else {
				throw new \Exception('Type "' . gettype($parameters[$i]) . '" is not supported for placeholder replacement');
			}

			$statement = substr_replace($statement, $value, strpos($statement, $placeholder, $offset),
				strlen($placeholder));

			$offset = strpos($statement, $placeholder, $offset + strlen($value)) - 1;
		}

		return $statement;
	}

	/**
	 * @param $conversion
	 * @param $target
	 * @param string $format
	 * @throws \Exception
     */
	public function convert($conversion, $target, $format = '')
	{
		throw new \Exception('Method "convert" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @throws \Exception
     */
	protected function _error()
	{
		throw new \Exception('Method "_error" is not supported by the ' . $this->type . ' driver.');
	}

	/**
	 * @throws \Exception
     */
	protected function _error_num()
	{
		throw new \Exception('Method "_error_num" is not supported by the ' . $this->type . ' driver.');
	}

}

/**
 * @param $argument
 * @return string
 */
function quote_argument ($argument) {
	return "'$argument'";
}
