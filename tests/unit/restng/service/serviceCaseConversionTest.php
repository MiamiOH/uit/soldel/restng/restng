<?php

class serviceCaseConversionTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $service;

  public function setUp(): void
  {
    parent::setUp();

    $this->service = new \MiamiOH\RESTng\Service();

  }

  public function testCamelCaseKeysRequiresArray() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Argument to camelCaseKeys must be an array');

      $arrayOut = $this->service->camelCaseKeys('');
  }

  public function testCamelCaseKeys() {

    $arrayIn = array(
        'test_element1' => 'test value 1',
        'test_element2' => 'test value 2',
        'test_1element3' => 'test value 3',
        'test_element4' => array('test value 4'),
        'test_element5' => array('id_num' => 'test value 5')
      );

    $arrayOut = $this->service->camelCaseKeys($arrayIn);

    $this->assertTrue(is_array($arrayOut));
    $this->assertEquals(5, count(array_keys($arrayOut)));
    $this->assertTrue(array_key_exists('testElement1', $arrayOut));
    $this->assertTrue(array_key_exists('testElement2', $arrayOut));
    $this->assertTrue(array_key_exists('test1element3', $arrayOut));
    $this->assertTrue(array_key_exists('testElement4', $arrayOut));
    $this->assertTrue(array_key_exists('testElement5', $arrayOut));
    $this->assertTrue(is_array($arrayOut['testElement4']));
    $this->assertEquals(1, count($arrayOut['testElement4']));
    $this->assertEquals($arrayIn['test_element4'][0], $arrayOut['testElement4'][0]);
    $this->assertTrue(is_array($arrayOut['testElement5']));
    $this->assertEquals(1, count($arrayOut['testElement5']));
    $this->assertEquals($arrayIn['test_element5']['id_num'], $arrayOut['testElement5']['id_num']);

  }

  public function testSnakeCaseKeysRequiresArray() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Argument to snakeCaseKeys must be an array');

      $arrayOut = $this->service->snakeCaseKeys('');
  }

  public function testSnakeCaseKeys() {

    $arrayIn = array(
        'testElement1' => 'test value 1',
        'testElement2' => 'test value 2',
        'test1element3' => 'test value 3',
        'testElement4' => array('test value 4'),
        'testElement5' => array('id_num' => 'test value 5')
      );

    $arrayOut = $this->service->snakeCaseKeys($arrayIn);

    $this->assertTrue(is_array($arrayOut));
    $this->assertEquals(5, count(array_keys($arrayOut)));
    $this->assertTrue(array_key_exists('test_element1', $arrayOut));
    $this->assertTrue(array_key_exists('test_element2', $arrayOut));
    $this->assertTrue(array_key_exists('test1element3', $arrayOut));
    $this->assertTrue(array_key_exists('test_element4', $arrayOut));
    $this->assertTrue(array_key_exists('test_element5', $arrayOut));
    $this->assertTrue(is_array($arrayOut['test_element4']));
    $this->assertEquals(1, count($arrayOut['test_element4']));
    $this->assertEquals($arrayIn['testElement4'][0], $arrayOut['test_element4'][0]);
    $this->assertTrue(is_array($arrayOut['test_element5']));
    $this->assertEquals(1, count($arrayOut['test_element5']));
    $this->assertEquals($arrayIn['testElement5']['id_num'], $arrayOut['test_element5']['id_num']);

  }

  public function testSnakeCaseValuesRequiresArray() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Argument to snakeCaseValues must be an array');

      $arrayOut = $this->service->snakeCaseValues('');
  }

  public function testSnakeCaseValuesNumericArray() {

    $arrayIn = array(
        'testElement1',
        'testElement2',
        'test1element3',
        'testElement4',
        'testElement5',
      );

    $arrayOut = $this->service->snakeCaseValues($arrayIn);

    $this->assertTrue(is_array($arrayOut));
    $this->assertEquals(5, count($arrayOut));
    $this->assertTrue(in_array('test_element1', $arrayOut));
    $this->assertTrue(in_array('test_element2', $arrayOut));
    $this->assertTrue(in_array('test1element3', $arrayOut));
    $this->assertTrue(in_array('test_element4', $arrayOut));
    $this->assertTrue(in_array('test_element5', $arrayOut));

  }

  public function testSnakeCaseValuesAssocArray() {

    $arrayIn = array(
        'key1' => 'testElement1',
        'key2' => 'testElement2',
        'key3' => 'test1element3',
        'key4' => 'testElement4',
        'key5' => array('keyElement1', 'keyElement2'),
      );

    $arrayOut = $this->service->snakeCaseValues($arrayIn);

    $this->assertTrue(is_array($arrayOut));
    $this->assertEquals(5, count($arrayOut));
    $this->assertEquals('test_element1', $arrayOut['key1']);
    $this->assertEquals('test_element2', $arrayOut['key2']);
    $this->assertEquals('test1element3', $arrayOut['key3']);
    $this->assertEquals('test_element4', $arrayOut['key4']);
    $this->assertTrue(is_array($arrayOut['key5']));
    $this->assertEquals(2, count($arrayOut['key5']));
    $this->assertTrue(in_array('key_element1', $arrayOut['key5']));
    $this->assertTrue(in_array('key_element2', $arrayOut['key5']));

  }

  public function testCamelCapsKeysRequiresArray() {
      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Argument to camelCapsKeys must be an array');

      $arrayOut = $this->service->camelCapsKeys('');
  }

    public function testCamelCapsKeys() {

        $arrayIn = array(
            'test_element1' => 'test value 1',
            'test_element2' => 'test value 2',
            'test_1element3' => 'test value 3',
            'test_element4' => array('test value 4'),
            'test_element5' => array('id_num' => 'test value 5')
        );

        $arrayOut = $this->service->camelCapsKeys($arrayIn);

        $this->assertTrue(is_array($arrayOut));
        $this->assertEquals(5, count(array_keys($arrayOut)));
        $this->assertTrue(array_key_exists('TestElement1', $arrayOut));
        $this->assertTrue(array_key_exists('TestElement2', $arrayOut));
        $this->assertTrue(array_key_exists('Test1element3', $arrayOut));
        $this->assertTrue(array_key_exists('TestElement4', $arrayOut));
        $this->assertTrue(array_key_exists('TestElement5', $arrayOut));
        $this->assertTrue(is_array($arrayOut['TestElement4']));
        $this->assertEquals(1, count($arrayOut['TestElement4']));
        $this->assertEquals($arrayIn['test_element4'][0], $arrayOut['TestElement4'][0]);
        $this->assertTrue(is_array($arrayOut['TestElement5']));
        $this->assertEquals(1, count($arrayOut['TestElement5']));
        $this->assertEquals($arrayIn['test_element5']['id_num'], $arrayOut['TestElement5']['id_num']);

    }


}
