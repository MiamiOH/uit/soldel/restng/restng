%define      debug_package %{nil}

Name:        miamioh-restng
Version:     2.10.1
Summary:     REST framework
URL:         https://gitlab.com/MiamiOH/uit/soldel/restng/restng

License:     Freely redistributable without restriction
Release:     1%{?dist}
Source0:     %{name}-%{_version}.tar.gz
BuildRoot:   %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:   x86_64

Requires: php

%description
RESTng (REST Next Generation) is continuation of a move to RESTful APIs within Solution Delivery.

%prep
%setup -q -c

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT

mkdir -p -m0755 $RPM_BUILD_ROOT/var
mkdir -p -m0755 $RPM_BUILD_ROOT/var/www
mkdir -p -m0755 $RPM_BUILD_ROOT/var/www/restng
mkdir -p -m0755 $RPM_BUILD_ROOT/var/www/restng/bin
mkdir -p -m0755 $RPM_BUILD_ROOT/var/www/restng/config

cp -p bootstrap.php $RPM_BUILD_ROOT/var/www/restng/bootstrap.php
cp -p console $RPM_BUILD_ROOT/var/www/restng/console
cp -p phinx.php $RPM_BUILD_ROOT/var/www/restng/phinx.php
cp -p composer.json $RPM_BUILD_ROOT/var/www/restng/composer.json
cp -p composer.lock $RPM_BUILD_ROOT/var/www/restng/composer.lock

cp -rp www $RPM_BUILD_ROOT/var/www/restng/www
cp -rp db $RPM_BUILD_ROOT/var/www/restng/db
cp -rp src $RPM_BUILD_ROOT/var/www/restng/src
cp -rp vendor $RPM_BUILD_ROOT/var/www/restng/vendor

touch $RPM_BUILD_ROOT/var/www/restng/.env

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root, -)

/var/www/restng/bootstrap.php
/var/www/restng/console
/var/www/restng/phinx.php
/var/www/restng/composer.json
/var/www/restng/composer.lock

/var/www/restng/www
/var/www/restng/db
/var/www/restng/src
/var/www/restng/vendor

%config(noreplace) /var/www/restng/config

%config(noreplace) /var/www/restng/.env

%changelog
