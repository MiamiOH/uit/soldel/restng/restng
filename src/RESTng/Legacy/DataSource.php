<?php

namespace MiamiOH\RESTng\Legacy;

use MiamiOH\RESTng\Connector\EncryptionInterface;
use MiamiOH\RESTng\Service\Apm\Transaction;

class DataSource
{
    /** @var EncryptionInterface */
    private $encryption;

    /** @var Transaction */
    private $transaction;

    private $dataSourceFileName = 'datasources.yaml';

    private $datasources;

    private $error = '';

    /**
     * @param $filename
     */
    public function setDataSourceFileName($filename)
    {
        $this->dataSourceFileName = $filename;
    }

    public function setEncryption(EncryptionInterface $encryption)
    {
        $this->encryption = $encryption;
    }

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @return string
     */
    public function getDataSourceFileName()
    {
        return $this->dataSourceFileName;
    }

    /**
     * @param $dsn
     * @param string $requested_connect_type
     * @return mixed
     * @throws \Exception
     */
    public function Factory($dsn, $requested_connect_type = '')
    {

        if (empty($dsn)) {
            throw new \Exception('No data source name given');
        }

        if (!$this->hasDataSource($dsn)) {
            throw new \Exception('No defined MU_DataSource: ' . $dsn);
        }

        $ds = $this->getDataSource($dsn);

        $ds = $this->ensureDatasourceStruct($ds);

        $type = $ds['type'];
        $user = $ds['user'];
        $password = $ds['password'];
        $host = $ds['host'];
        $database = $ds['database'];
        $port = $ds['port'];

        /*
          The ds connectType can be one of:

            '' (empty string) - allows new or persistent as requested
            'persistent'      - only allows persistent connections
            'new'             - only allows new connections

          If there is either a requested connect type or a connect type given in the
          data source, we will make sure they line up and use that in the factory call.
          otherwise, we can let the default behavior (new) happen.
        */

        // Normalize case
        $requested_connect_type = strtolower($requested_connect_type);
        $ds['connect_type'] = strtolower($ds['connect_type']);

        // Determine the effective connect type.
        $effectiveConnectType = '';
        if ($requested_connect_type || $ds['connect_type']) {
            // If they are equal, we just use the ds value.
            if ($requested_connect_type == $ds['connect_type']) {
                $effectiveConnectType = $ds['connect_type'];

                // If there is a requested type, but no ds type value, the ds is saying
                // the app can use either, so just use the requested type.
            } elseif ($requested_connect_type && !$ds['connect_type']) {
                $effectiveConnectType = $requested_connect_type;

                // Otherwise, we will use the value given by the ds. This would be
                // the case if there is a ds type, but no requested type or if the
                // requested type does not equal the ds type.
            } else {
                $effectiveConnectType = $ds['connect_type'];
            }
        }

        switch ($type) {
            case 'MySQL':
            case 'OCI8':
            case 'PostgreSQL':
            case 'MSSQL':
            case 'SQLite3':
                $dbh = \MiamiOH\RESTng\Legacy\DB\DBH::Factory($type, $user, $password, $database, $host, $effectiveConnectType);
                $dbh->setApmTransaction($this->transaction);
                return $dbh;

            case 'LDAP':
                $ldap = new LDAP($host, $port);
                if ($ldap->bind($user, $password) === false) {
                    throw new \Exception('LDAP connection failed: ' . $ldap->error_num . ' ' . $ldap->error);
                }
                return $ldap;

            case 'LDAPng':
                $ldap = new \Dreamscapes\Ldap\Core\Ldap($host);
                return $ldap->bind($user, $password);

            case 'Other':
                return array(
                    'username' => $user,
                    'password' => $password,
                    'host' => $host,
                );

            default:
                throw new \Exception('Unsupported data source type "' . $type . '" for ' . $dsn . '.');
        }
    }

    public function hasDataSource($name): bool
    {
        $sources = $this->loadDataSources();

        return isset($sources[$name]);
    }

    public function getDataSource($name)
    {
        $sources = $this->loadDataSources();

        return $sources[$name];
    }

    public function loadDataSources()
    {
        if ($this->datasources !== null) {
            return $this->datasources;
        }

        // Using yaml_parse_file here did not work with mocking the file system with vfsStream
        $this->datasources = yaml_parse(file_get_contents($this->dataSourceFileName));

        return $this->datasources;
    }

    private function ensureDatasourceStruct(array $ds): array
    {
        $expectedKeys = ['type', 'user', 'password', 'host', 'database', 'port', 'connect_type'];

        foreach ($expectedKeys as $key) {
            if (array_key_exists($key, $ds)) {
                continue;
            }

            $ds[$key] = '';
        }

        return $ds;
    }
}

?>
