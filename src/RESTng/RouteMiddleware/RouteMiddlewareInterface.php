<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 12:31 PM
 */

namespace MiamiOH\RESTng\RouteMiddleware;


use MiamiOH\RESTng\App;

interface RouteMiddlewareInterface
{
    public function call(App $app, array $options): void;
}