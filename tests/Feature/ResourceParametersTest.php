<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use Tests\FeatureTestCase;

class ResourceParametersTest extends FeatureTestCase
{

    public function testParameterIsAvailableToTheService(): void
    {
        $response = $this->getJson('/citest/reflect/parameter/1234');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'id' => '1234'
            ]
        ], true);
    }

    public function testMultipleParametersAreAvailableToTheService(): void
    {
        $response = $this->getJson('/citest/reflect/parameters/mygroup/1234');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'group' => 'mygroup',
                'id' => '1234'
            ]
        ], true);
    }

    public function testAlternateKeysWorkWithNoExplicitKeyGiven(): void
    {
        $response = $this->getJson('/citest/reflect/parameter_key/1234');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'id' => '1234'
            ]
        ], true);
    }

    public function testAlternateKeyRequestedInPatternIsAvailable(): void
    {
        $response = $this->getJson('/citest/reflect/parameter_key/uid=1234');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'id' => '1234',
                'key' => 'uid'
            ]
        ], true);
    }

    public function testUnknownAlternateKeyCausesError(): void
    {
        $response = $this->getJson('/citest/reflect/parameter_key/gid=1234');
        $response->assertStatus(App::API_FAILED);
    }

}
