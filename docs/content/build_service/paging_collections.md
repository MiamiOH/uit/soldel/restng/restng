+++
title = "Paging Collections"
weight = 80
+++

Reading collections often results in large datasets that are impractical to return in a single response. RESTng provides features to enable a consistent implementation of paged requests and responses. Consider supported paged responses if your collection contains large numbers of records.

The resource interface must adhere to the following pattern.  The actual implementation of paging within the service is up to you.

A paged REST request will take the form of:

    /course/section/201510?offset=1&limit=100

This is a request for 100 records starting with record 1.

You can determine if a request is paged through the request object:

    $isPaged = $request->isPaged();

If a request is paged, you can then get the offset and limit from the request object:

    $offset = $request->getOffset();
    $limit = $request->getLimit();

In order to properly fulfill a paged request, you must set the total records in the collection:

    $response->setTotalObjects($totalRecords);

You set the response payload as normal, adding only the records from offset to limit.  The framework is responsible for adding paging information to the response, including first, previous, next and last resource locations.
