<?php

class slimResourceTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $registeredResources = array();

    public function setUp(): void
    {
        parent::setUp();

        // The \MiamiOH\RESTng\Testing\TestCase::setUp method creates and bootstraps
        // a new \MiamiOH\RESTng\App instance we can use as our subject.

        $this->registeredResources = array();
    }

    public function testAddResourceDefaults()
    {

        $resourceConfig = array(
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('/test/service');

        $this->assertEquals('read', $resource['action']);
        $this->assertFalse($resource['isPageable']);
        $this->assertFalse($resource['isPartialable']);
        $this->assertFalse($resource['isComposable']);
        $this->assertEquals('array', $resource['dataRepresentation']);

        $this->assertTrue(is_array($resource['params']));
        $this->assertEquals(0, count($resource['params']));
        $this->assertTrue(is_array($resource['conditions']));
        $this->assertEquals(0, count($resource['conditions']));
        $this->assertTrue(is_array($resource['middleware']));
        $this->assertEquals(0, count($resource['middleware']));
        $this->assertTrue(is_array($resource['options']));
        $this->assertEquals(0, count($resource['options']));

    }

    public function testAddResourceRequiresArray()
    {

        $resourceConfig = '';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Resource service config must be an array');

        $this->app->addResource($resourceConfig);
    }

    public function testAddResourceRequiresMethod()
    {

        $resourceConfig = array(
            'service' => 'TestService',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Resource method required');
        $this->app->addResource($resourceConfig);
    }

    public function testAddResourceRequiresPattern()
    {

        $resourceConfig = array(
            'service' => 'TestService',
            'method' => 'getSomeTestData',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Resource pattern required');


        $this->app->addResource($resourceConfig);
    }

    public function testAddResourceRequiresDescription()
    {

        $resourceConfig = array(
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Resource description required for /test/service');


        $this->app->addResource($resourceConfig);
    }

    public function testGetResourceWithName()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('test.service', $resource['name']);

    }

    public function testAddResourceUsesPatternAsName()
    {

        $resourceConfig = array(
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

    }

    public function testAddResourceParamsRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => '',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Config option "params" must be an array for addResource');


        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParamsMustBeDeclared()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(),
        );

        $this->expectException(\Exception::class);

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParamMustHaveDescription()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array()
            ),
        );

        $this->expectException(\Exception::class);

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParams()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test')
            ),
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParamsMultiple()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:type/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test'),
                'type' => array('description' => 'Type of thing'),
            ),
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParamsNoExtra()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test'),
                'type' => array('description' => 'Type of thing'),
            ),
        );

        $this->expectException(\Exception::class);

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);
    }

    public function testAddResourceParamsWithCondition()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test', 'condition' => '\d+'),
            ),
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service.id');

        $this->assertEquals('test.service.id', $resource['name']);
        $this->assertEquals('\d+', $resource['conditions']['id']);

    }

    public function testAddResourceParamsWithAlternateKeys()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test', 'alternateKeys' => ['nid', 'uid']),
            ),
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service.id');

        $this->assertEquals('test.service.id', $resource['name']);
        $this->assertEquals('nid', $resource['params']['id']['alternateKeys'][0]);

    }

    public function testAddResourceParamsWithAlternateKeysRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'An id for the test', 'alternateKeys' => 'nid'),
            ),
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The alternateKeys value must be an array');

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service.id');

        $this->assertEquals('test.service.id', $resource['name']);
        $this->assertEquals('nid', $resource['params']['id']['alternateKeys'][0]);

    }

    public function testAddResourceActionDefault()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('read', $resource['action']);

    }

    public function testAddResourceActionRead()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('read', $resource['action']);

    }

    public function testAddResourceActionCreate()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'create',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('create', $resource['action']);

    }

    public function testAddResourceActionUpdate()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'update',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('update', $resource['action']);

    }

    public function testAddResourceActionDelete()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'delete',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('delete', $resource['action']);

    }

    public function testAddResourceActionUnknown()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'invalid',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalid action (invalid) Must be one of: create, read, update, delete, patch');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceMiddlewareRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'middleware' => ''
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('addResource "middleware" must be an array');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => ''
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('addResource "options" must be an array');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsKeyRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => '',
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('addResource options key must be an array');

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

    }

    public function testAddResourceOptionsRequiresDescription()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => array(),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Description is required for test.service option color');

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

    }

    public function testAddResourceOptionsTypeDefault()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => array('description' => 'Color of thing to get'),
            )
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('single', $resource['options']['color']['type']);
    }

    public function testAddResourceOptionsTypeSingle()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => array('type' => 'single', 'description' => 'Color of thing to get'),
            )
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('single', $resource['options']['color']['type']);
    }

    public function testAddResourceOptionsTypeList()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => array('type' => 'list', 'description' => 'Color of thing to get'),
            )
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('list', $resource['options']['color']['type']);
    }

    public function testAddResourceOptionsTypeUnknown()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'color' => array('type' => 'invalid', 'description' => 'Color of thing to get'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Type value for resource option must be "single" or "list"');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsReservedFields()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'fields' => array('type' => 'list', 'description' => 'A list of fields'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Reserved option name: fields');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsReservedOffset()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'offset' => array('type' => 'single', 'description' => 'A offset'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Reserved option name: offset');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsReservedLimit()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'limit' => array('type' => 'single', 'description' => 'A limit'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Reserved option name: limit');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceOptionsReservedToken()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'options' => array(
                'token' => array('type' => 'single', 'description' => 'A test'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Reserved option name: token');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceXOrgSecRequiresArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'xOrgSec' => '',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('addResource xOrgSec must be an array');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceXOrgSecDefaultAllowOrigin()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('any', $resource['xOrgSec']['allow-origin']);
    }

    public function testAddResourceXOrgSecDefaultAllowCredentials()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertTrue($resource['xOrgSec']['allow-credentials']);
    }

    public function testAddResourceXOrgSecDefaultMaxAge()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals(3600, $resource['xOrgSec']['max-age']);
    }

    public function testAddResourceXOrgSecDefaultAllowMethods()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals(5, count($resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('GET', $resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('POST', $resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('PUT', $resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('DELETE', $resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('OPTIONS', $resource['xOrgSec']['allow-methods']));
    }

    public function testAddResourceXOrgSecDefaultAllowHeaders()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertNull($resource['xOrgSec']['allow-headers']);
    }

    public function testAddResourceXOrgSecSetValues()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'xOrgSec' => array(
                'allow-origin' => 'www.example.com',
                'allow-credentials' => false,
                'max-age' => 360,
                'allow-methods' => array('GET'),
                'allow-headers' => array('x-auth-custom'),
            )
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('www.example.com', $resource['xOrgSec']['allow-origin']);
        $this->assertFalse($resource['xOrgSec']['allow-credentials']);
        $this->assertEquals(360, $resource['xOrgSec']['max-age']);
        $this->assertEquals(1, count($resource['xOrgSec']['allow-methods']));
        $this->assertTrue(in_array('GET', $resource['xOrgSec']['allow-methods']));
        $this->assertEquals(1, count($resource['xOrgSec']['allow-headers']));
        $this->assertTrue(in_array('x-auth-custom', $resource['xOrgSec']['allow-headers']));
    }

    public function testAddResourceAuthnRequiresAuthz()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'middleware' => array(
                'authorize' => array('application' => 'TEST'),
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The authorize middleware cannot be used without authenticate');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testSetDefaultPageLimitNumeric()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'defaultPageLimit' => 'a',
            'isPageable' => true,
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('defaultPageLimit must be an integer greater than or equal to 1');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testSetMaxPageLimitNumeric()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'maxPageLimit' => 'a',
            'isPageable' => true,
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('maxPageLimit must be an integer greater than or equal to 1');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceAuthnAndAuthz()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array('$this->>$this->application' => 'TEST'),
            )
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

    }

    public function testAddResourceNoDuplicates()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData2',
            'pattern' => '/test/service2',
            'description' => 'This is another test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Resource with id test.service is already defined');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testGetResources()
    {
        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resourceConfig = array(
            'name' => 'test.service2',
            'service' => 'TestService',
            'method' => 'getSomeTestData2',
            'pattern' => '/test/service2',
            'description' => 'This is another test service',
            'returnType' => 'model',
            'action' => 'read',
        );

        $response = $this->app->addResource($resourceConfig);

        $resources = $this->app->getResources();

        $this->assertTrue(array_key_exists('test.service', $resources));
        $this->assertTrue(array_key_exists('test.service2', $resources));
        $this->assertEquals(2, count($resources));
    }

    public function testAddResourceIsPartialable()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'isPartialable' => true,
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertTrue($resource['isPartialable']);

    }

    public function testAddResourceIsComposable()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'action' => 'read',
            'isComposable' => true,
        );

        $response = $this->app->addResource($resourceConfig);

        $this->assertTrue($response);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertTrue($resource['isComposable']);

    }

    public function testAddResourceDataRepresentationInvalid()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'action' => 'create',
            'dataRepresentation' => 'bubba',
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalid dataRepresentation: bubba');

        $response = $this->app->addResource($resourceConfig);

    }

    public function testAddResourceDataRepresentationArray()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'action' => 'create',
            'dataRepresentation' => 'array',
        );

        $response = $this->app->addResource($resourceConfig);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('array', $resource['dataRepresentation']);

    }

    public function testAddResourceDataRepresentationFile()
    {

        $resourceConfig = array(
            'name' => 'test.service',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service',
            'description' => 'This is a test service',
            'action' => 'create',
            'dataRepresentation' => 'file',
        );

        $response = $this->app->addResource($resourceConfig);

        $resource = $this->app->getResourceByName('test.service');

        $this->assertEquals('file', $resource['dataRepresentation']);

    }

}
