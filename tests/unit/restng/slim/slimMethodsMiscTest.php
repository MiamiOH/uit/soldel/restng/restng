<?php

class slimMethodsMiscTest extends \MiamiOH\RESTng\Testing\TestCase
{

  public function testRemoveRequestOption() {
    $options = array(
        'type' => 'new',
        'color' => 'red',
      );

    $this->app->environment->offsetSet('slim.request.query_hash', $options);

    $response = $this->app->removeRequestOption('type');

    $optionsIn = $this->app->environment->offsetGet('slim.request.query_hash');

    $this->assertTrue($response);
    $this->assertFalse(array_key_exists('type', $optionsIn));
    $this->assertTrue(array_key_exists('color', $optionsIn));
  }

  public function testRemoveRequestOptionNoWarn() {
    $options = array(
        'type' => 'new',
        'color' => 'red',
      );

    $this->app->environment->offsetSet('slim.request.query_hash', $options);

    $response = $this->app->removeRequestOption('notHere');

    $optionsIn = $this->app->environment->offsetGet('slim.request.query_hash');

    $this->assertTrue($response);
    $this->assertTrue(array_key_exists('type', $optionsIn));
    $this->assertTrue(array_key_exists('color', $optionsIn));
  }

  public function testLocationFor() {
    $apiRequest = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
          ->setMethods(array('getUrl', 'getRootUri'))
          ->getMock();

    $apiRequest->method('getUrl')->willReturn('http://example.com');
    $apiRequest->method('getRootUri')->willReturn('/api');

    $this->app->request = $apiRequest;

    $router = $this->getMockBuilder('\Slim\Router')
          ->setMethods(array('urlFor'))
          ->getMock();

    $router->method('urlFor')->willReturn('/test/service');
    $this->app->router = $router;

    $location = $this->app->urlFor('test.service');

    $this->assertEquals('http://example.com/api/test/service', $location);

  }

}
