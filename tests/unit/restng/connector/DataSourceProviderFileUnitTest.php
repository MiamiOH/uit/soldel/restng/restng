<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/7/18
 * Time: 8:05 PM
 */

namespace Tests\unit\restng\connector;

use MiamiOH\RESTng\Connector\DataSourceProviderFile;
use MiamiOH\RESTng\Exception\DataSourceNotFound;
use org\bovigo\vfs\vfsStream;
use Tests\UnitTestCase;

class DataSourceProviderFileUnitTest extends UnitTestCase
{
    /** @var DataSourceProviderFile */
    private $provider;

    /** @var vfsStream */
    private static $vfs;

    public function setUp(): void
    {
        parent::setUp();

        $datasources = [
            'test_connection' => ['type' => 'Other', 'user' => 'bob']
        ];

        $fileStructure = [
            'datasources.yaml' => yaml_emit($datasources),
        ];

        self::$vfs = vfsStream::setup('RESTng', null, $fileStructure);

        $this->provider = new DataSourceProviderFile(vfsStream::url('RESTng/datasources.yaml'));
    }

    public function testValidatesHasDataSourceIfNameExists(): void
    {
        $this->assertTrue($this->provider->hasDataSource('test_connection'));
    }

    public function testDoesNotValidatesHasDataSourceIfNameDoesNotExists(): void
    {
        $this->assertFalse($this->provider->hasDataSource('another_name'));
    }

    public function testReturnsDataSourceObjectForRequestedName(): void
    {
        $dataSource = $this->provider->getDataSource('test_connection');

        $this->assertEquals('bob', $dataSource->getUser());
    }

    public function testReturnsArrayOfDataSources(): void
    {
        $dataSources = $this->provider->getDataSources();

        $this->assertCount(1, $dataSources);
    }

    public function testThrowsExceptionWhenDataSourceNotFound(): void
    {
        $this->expectException(DataSourceNotFound::class);

        $this->provider->getDataSource('test_not_found');
    }
}
