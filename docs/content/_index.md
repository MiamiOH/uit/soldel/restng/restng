+++
title = "RESTng Platform Documentation"
+++

Note: This documentation pertains to RESTng 1.2 and later. The set up of RESTng <1.2 is substantially different.

RESTng (REST Next Generation) is continuation of a move to RESTful APIs within Solution Delivery. An RESTful API is an <a href="http://en.wikipedia.org/wiki/Api">Application Programming Interface</a> which allows consumers to manipulate data objects using standard HTTP methods.

The primary design principle driving RESTng is to allow developers to focus on high value business functions. RESTng supports this principle by:

* handling all HTTP requests and responses;
* encouraging documentation through configuration;
* implementing authentication and authorization services;
* providing simple methods to call other resources; and
* facilitating both unit and integration testing.
