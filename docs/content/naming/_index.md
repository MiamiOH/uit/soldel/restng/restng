+++
title = "Naming Conventions"
weight = 40
+++

RESTng is a platform for deploying REST services. Your services and resources may appear to be isolated to your project, but they actual reside in a much larger environment.

## PHP Namespaces

Starting with RESTng 1.2, all service projects will utilize standard composer autoloaders and have no special namespace or class naming requirements. You should use a namespace appropriate for your project. You are encouraged to begin your namespace with `\MiamiOH` in order to prevent conflicts with external projects.
