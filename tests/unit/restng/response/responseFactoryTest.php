<?php

class responseFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $responseFactory;

  public function setUp(): void
  {
    parent::setUp();

    $this->responseFactory = new \MiamiOH\RESTng\Util\ResponseFactory();

  }

  public function testNewResponse() {

    $response = $this->responseFactory->newResponse();

    $this->assertEquals('MiamiOH\RESTng\Util\Response', get_class($response));

  }

}
