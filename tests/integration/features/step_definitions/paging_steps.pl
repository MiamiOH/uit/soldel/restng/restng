#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use JSON;

use lib 'lib';
use StepConfig;

# Our CI sample data uses numeric ids which are in order. Use this for
# validating that the expected records are included.
Then qr/the response contains records (\d+) to (\d+)/, sub {
	my $expectedFirstRecordNum = $1;
	my $expectedLastRecordNum = $2;

    ok(defined(S->{'subject'}) && defined(S->{'subject'}[0]), "A subject has been set");
	my $data = S->{'subject'}[0]->{'data'};

	my $firstRecordNum = 0;
	my $lastRecordNum = 0;

	for (my $i = 0; $i < scalar(@{$data}); $i++) {
		if (!$firstRecordNum || $data->[$i]{'id'} < $firstRecordNum) {
			$firstRecordNum = $data->[$i]{'id'}
		}
		if (!$lastRecordNum || $data->[$i]{'id'} > $lastRecordNum) {
			$lastRecordNum = $data->[$i]{'id'}
		}
	}
	
	ok($firstRecordNum == $expectedFirstRecordNum, "First record $firstRecordNum equals expected $expectedFirstRecordNum");
	ok($lastRecordNum == $expectedLastRecordNum, "Last record $lastRecordNum equals expected $expectedLastRecordNum");
};
