<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use Carbon\Carbon;
use MiamiOH\RESTng\Util\DeferredCallManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeferredCallsCheckFailuresCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsCheckFailuresCommand extends Command
{
    protected static $defaultName = 'deferred-calls:check-failures';

    /** @var DeferredCallManager */
    private $callManager;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
    }

    protected function configure(): void
    {
        $this->addOption(
            'within',
            null,
            InputOption::VALUE_OPTIONAL,
            'Within timeframe (ex "2 hours")',
            '12 hours'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $since = Carbon::now()->subtract($input->getOption('within'));

        $calls = $this->callManager->getFailedCalls($since);

        if ($calls->isNotEmpty()) {
            $output->writeln(sprintf('<error>Found %s failed jobs</error>', $calls->count()));
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
