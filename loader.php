<?php

namespace RESTng;

$loadHostSpecific = false;

$app = require('bootstrap.php');

if (file_exists('../config/autoloaders.yaml')) {
    $autoloaders = yaml_parse_file('../config/autoloaders.yaml');
    if (!isset($autoloaders['autoload'])) {
        throw new \Exception('Missing "autoload" key in autoloaders.yaml');
    }
    foreach ($autoloaders['autoload'] as $autoloader) {
        require($autoloader);
    }
}

// The RESTng config will enable some error reporting that is higher than
// we want on our PHP apps. We'll turn it back down for now. Turning
// display_errors off messes with RESTng error handling, but prevents
// issues with production PHP apps. The developer may need to turn
// display_errors back on per app for debugging.
ini_set( "display_errors", "off" );
error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING );

$api = $app->getService('API');

function getRESTngApi() {
  return $GLOBALS['api'];
}
