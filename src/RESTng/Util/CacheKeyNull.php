<?php

namespace MiamiOH\RESTng\Util;

class CacheKeyNull implements CacheKey
{
    public function isValid(): bool
    {
        return false;
    }

    public function isNotValid(): bool
    {
        return true;
    }

    public function format(): string
    {
        return '';
    }

    public function replacements(): array
    {
        return [];
    }

    public function key(): string
    {
        return '';
    }
}
