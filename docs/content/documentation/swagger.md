+++
title = "Swagger"
+++

RESTng uses Swagger (http://swagger.io/) to create functional documentation of RESTful API resources. RESTng can generate a valid Swagger Spec file from the REST configurations provided by developers. We have included an lightly customized version of Swagger-UI (http://swagger.io/swagger-ui/) to allow easy browsing of the resources.

You can view the Swagger Spec file by visiting your api deployment /swagger/swagger.php and you will find the Swagger UI instance at /swagger-ui. 