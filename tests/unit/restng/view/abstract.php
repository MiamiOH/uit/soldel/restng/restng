<?php

abstract class viewAbstract extends \MiamiOH\RESTng\Testing\TestCase
{

  protected $view;
  protected $response;
  protected $app;

  protected $baseUrl = 'http://example.com/api';

  protected $limit = 0;
  protected $offset = 1;
  protected $totalObjects = 0;
  protected $objects = array();
  protected $objectName = 'data';
  protected $resourceName = '';
  protected $status = 0;
  protected $resourceOptions = array();
  protected $resourceParams = array();
  protected $paged = false;
  protected $partial = false;

  public function setUp(): void
  {
    parent::setUp();

    $this->limit = 0;
    $this->offset = 1;
    $this->totalObjects = 0;
    $this->objects = array();
    $this->objectName = 'data';
    $this->resourceName = '';
    $this->status = 0;
    $this->resourceOptions = array();
    $this->resourceParams = array();
    $this->paged = false;
    $this->partial = false;

    $this->app = $this->getMockBuilder('MiamiOH\RESTng\Util\AppInstance')
          ->setMethods(array('getInstance'))
          ->getMock();

    $transaction = $this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class);
    /*
        The Slim class cannot be mocked due to static method calls.
        However, the View class depends on it. We will create a mock
        and inject some mocked methods and objects as well as some
        real objects.
    */
    $slim = $this->getMockBuilder('MiamiOH\RESTng\Slim\Mock')
          ->setMethods(array('getService', 'getInstance', 'response', 'urlFor'))
          ->getMock();

    $slim->method('getService')->willReturn($transaction);

    $slim->response = new \Slim\Http\Response();

    $slim->method('response')->willReturn($slim->response);

    $slim->method('urlFor')
        ->with($this->callback(array($this, 'urlForResourceNameWith')),
          $this->callback(array($this, 'urlForResourceParamsWith')))
        ->will($this->returnCallback(array($this, 'urlForMock')));

    $this->app->method('getInstance')->willReturn($slim);

    $this->response = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
          ->setMethods(array('getStatus', 'isPaged', 'getResourceOptions',
            'getResourceName', 'getResponseLimit', 'getResponseOffset',
            'getTotalObjects', 'getResourceParams', 'getResponseCallback',
              'isPartial', 'getPartialFieldsSpec'))
          ->getMock();

  }

  public function urlForResourceNameWith($subject) {
    return $subject === $this->resourceName;
  }

  public function urlForResourceParamsWith($subject) {
    return is_array($subject);
  }

  public function urlForMock() {
    switch ($this->resourceName) {

      case 'test.resource.owner':
        return $this->baseUrl . '/test/resource/' . $this->resourceParams['owner'];

      default:
        return $this->baseUrl . '/test/resource';

    }
  }

}
