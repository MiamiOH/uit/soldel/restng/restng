+++
title = "Partial Request"
weight = 30
+++

## Partial Model

The consumer asks for a partial model by calling the resource with a fields option giving a list of attribute names. For example:

    https://ws.miamioh.edu/api/person?fields=first,last,phone,address

The default person model likely contains many more properties, but by asking for only the ones needed for the current context, the consumer can reduce the data and possibly the processing required to fulfill the request.

Only attributes of the resource's model may be specified in the fields list.

## Composed Model

The consumer asks for a composed resources using the compose option. For example:

    https://ws.miamioh.edu/api/class?compose=instructor,location

The default class model is not required to contain the subobjects. This arrangement allows the consumer to determine when the higher overhead of fetching subobjects is reasonable.

You may specify a fields list for the composed objects (assuming that your resource knows how to support the fields capability for the objects it composes):

    https://ws.miamioh.edu/api/class?compose=instructor(first,last,email),location

## Using Partial and Composed Models

You are free to combine the use of partial and composed models:

    https://ws.miamioh.edu/api/class?fields=first,last,phone,address&compose=instructor(first,last,email),location
