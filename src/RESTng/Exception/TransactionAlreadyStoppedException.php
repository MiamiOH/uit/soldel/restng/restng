<?php

namespace MiamiOH\RESTng\Exception;

class TransactionAlreadyStoppedException extends ApmException
{
}
