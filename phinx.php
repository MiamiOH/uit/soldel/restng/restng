<?php

use Illuminate\Database\Capsule\Manager;

/*
 * This configuration applies to phinx for running migrations.
 */
/** @var \MiamiOH\RESTng\App $app */
$app = require(__DIR__ . '/bootstrap.php');

$connection = Manager::connection('restng');
$config = $connection->getConfig();

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'migrations',
        'default_environment' => 'restng',
        'restng' => [
            'adapter' => 'mysql',
            'name' => $config['database'],
            'connection' => $connection->getPdo(),
            'charset' => 'utf8',
        ],
    ],
    'version_order' => 'creation'
];
