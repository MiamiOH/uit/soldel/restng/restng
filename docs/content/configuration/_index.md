+++
title = "Configuration"
weight = 37
+++

RESTng 2 uses several files to store configuration settings. Puppet manages these files during test and production deployment, but developers will need to edit them for local development needs.

RESTng loads configuration files from the `config` directory relative to install root. The install root will be `/var/www/restng` for package deployments. The appropriate location will be specified when using containers or alternate VMs for development.
 