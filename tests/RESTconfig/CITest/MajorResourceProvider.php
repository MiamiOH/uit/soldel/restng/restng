<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class MajorResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Major',
            'type' => 'object',
            'properties' => array(
                'majorId' => array(
                    'type' => 'string',
                ),
                'majorDept' => array(
                    'type' => 'string',
                ),
                'advisor' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Major.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Major'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestMajor',
            'class' => 'Tests\Service\CITest\Major',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.major.all',
            'description' => 'Pageable collection of major records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/major',
            'service' => 'CITestMajor',
            'method' => 'getMajor',
            'isPageable' => true,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of majors',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Major.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.major.majorId',
            'description' => 'A single test major.',
            'tags' => array('CITest'),
            'pattern' => '/citest/major/:majorId',
            'service' => 'CITestMajor',
            'method' => 'getMajor',
            'params' => array(
                'majorId' => array('description' => 'A test majorId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A major object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Major',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Major Id not found'
                )
            )
        ));
    }
}