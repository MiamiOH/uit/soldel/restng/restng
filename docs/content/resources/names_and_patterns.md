+++
title = "Names & Patterns"
weight = 20
+++

The name and pattern given for a resource are used by RESTng to identify and execute a resource. We will assume the following resource configuration for this discussion:

{{< highlight php "linenos=table" >}}
<?php
...
return [
    'resources' => [
        'course' => [
            \MiamiOH\RESTng\CourseResourceProvider::class,
        ],
    ]
];
{{< / highlight >}} 

Resources have both **names** and **patterns** which are used to route requests. Names are used when requests are made via the callResource method between RESTng objects. Patterns are used by the Slim routing service to map an HTTP request to a resource. RESTng matches names and patterns based on their first component matched against resource configurations. Name components are separated by a '.' and path components are separated by a '/'.

Patterns inherently carry the HTTP verb as a differentiater for Slim to route on. Names do not have a corresponding indicator and shoudl therefore include some type of action where appropriate.

All of the following examples would resolve to the 'course' resource defined above:

Name | Pattern
-----|--------
course|GET /course
course.section|GET /course/section
course.instructor|GET /course/instructor
course.create|POST /course
course.update.id|PUT /course/:id
course.delete.id|DELETE /course/:id
