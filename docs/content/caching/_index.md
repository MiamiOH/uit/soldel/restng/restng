+++
title = "Caching"
weight = 75
+++

RESTng includes a cache service which can be used store results from expensive transactions for an appropriate amount of time. Using the cache can dramatically improve the overall responsiveness of a resource and the applications which use on it. However, great care is required to ensure cached results are returned for an exactly matching request.

There are three points the caching service may be used:

* internal authorization
* resource responses
* service implementation

The cache service is configured via the RESTng `.env` file:

```shell
CACHE_DRIVER=database
CACHE_FILE_PATH=/tmp
CACHE_CONNECTION=restng
CACHE_TABLE=cache
CACHE_TTL=900
```

Valid cache drivers are `null`, `array`, `file` and `database`. The default driver is `null` which prevents any cache behaviors. The `array` driver may be used for testing if you want to observe cache behaviors per request. The `file` driver will store cache entries in the given path (defaults to `/tmp`). The `database` driver will use the given connection and table as the cache.

The TTL (time to live) is in seconds. The application of the TTL varies by use and is **not** a global value.

## Caching During Internal Authorization

The RESTng Authorization middleware automatically caches all authorizations of type `authMan`. Individual resources have no control over this behavior. This caching greatly improves the response time when multiple calls are made to the same resource. However, the consequence is that authorization changes may not be applied until cached items expire.

## Caching Resource Responses

In very controlled circumstances, it is possible to have RESTng cache an entire response and return it on subsequent requests. While this approach can yield dramatic performance gains, you must be extremely careful to ensure cached responses are returned only for an exactly matching request.

Resource response caching is disabled by default and may be enabled and configured when defining a resource, as shown here:

{{< highlight php "linenos=table" >}}
<?php
...
        $this->addResource(array(
            'action' => 'read',
            'name' => 'authorization.v1.key',
            'service' => 'Authorization',
            'method' => 'getAuthorizations',
            ...
            'cache' => [
                'enabled' => true,
                'ttl' => env('CACHE_TTL', 300),
                'keyMethod' => 'getAuthorizationsCacheKey',
                'cacheResponses' => [
                    \MiamiOH\RESTng\App::API_OK,
                ],
            ],
            ...
        ));
{{< / highlight >}} 

You are free to use the configured `CACHE_TTL` or disregard it completely and define your own TTL. The `keyMethod` and `cacheResponses` options give you control over how responses are keyed for caching and what types of responses should be cached.

### Cache Key Method

Cache items are referenced by a `key`. The key value must uniquely identify a cache entry, in this case a resource response, to the degree required to differentiate it from any other entry. The `keyMethod` configuration option gives the name of PHP class method which MUST be declared on the resource's service class (just like the resource's `method`). This method will be called when RESTng evaluates the viability of caching the response.

The key method must evaluate the request and construct a suitable `CacheKey` object to return. Remember that the purpose of the `CacheKey` is to distinguish responses to the ensure a cached response is only returned for the correct request. The relevant attributes will vary by resource. The typical process is:

1. Construct a suitable key format (this can used to search for cache hits/misses in Elastic)
2. Determine the request attributes to use in the key
3. Determine if the current request can be cached
4. Create and return an appropriate `CacheKey` object

The following is a complete example.

{{< highlight php "linenos=table" >}}
<?php
...
    public function getAuthorizationsCacheKey(): CacheKey
    {
        $request = $this->getRequest();

        $application = $request->getResourceParam('application');
        $module = $request->getResourceParam('module');

        $options = $request->getOptions();

        if (empty($options['username'])) {
            return new CacheKeyNull();
        }

        $keys = $options['key'] ?? [];

        $username = $options['username'];

        return new CacheKeyString(
            'authorization:list:{username}:{application}:{module}:{keyList}',
            [
                'username' => $username,
                'application' => $application,
                'module' => $module,
                'keyList' => implode(',', $keys),
            ]
        );
    }
{{< / highlight >}} 

The key format, while arbitrary, should adhere to some general guidelines:

* Elements should be separated by colons
* The first element(s) should provide a namespace
* Value placeholders are best ordered by importance
* The total resulting string value must be less than 512 characters

The value array must be an associative array using placeholder names as the key and the relevant value from the request as a `string`.

If the values required to uniquely identify a request may result in a key greater than 512 characters, consider hashing long value strings to reduce the length. The cache key length limit 512 characters and any cache key exceeding that limit will be considered invalid. If the key is invalid, the response will not be cached. For example:

{{< highlight php "linenos=table" >}}
    'keyList' => hash('sha256', implode(',', $keys))
{{< / highlight >}} 

If the request is missing a value that is required to create a suitable key, but the request may otherwise be valid (remember that the key is evaluated **before** the service method is called), you should return an instance of `CacheKeyNull`. A null key object is invalid and no response will be cached.

### Response Statuses

Not all resource responses should be cached. If the resource returns some type of server error, caching the response would not be appropriate. The `cacheResponses` configuration item MUST be an array of response codes that can be safely cached. In general, the most likely code to cache is a 200 Ok. Depending on the purpose of the resource, caching a 404 Not Found may be appropriate. Think carefully before caching a response. Caching something like 201 would be potentially create some serious problems for example.

## Caching in Service Implementations

Service classes must extend `\MiamiOH\RESTng\Service` which provides a protected `cache` attribute. The `cache` attribute will contain a `\Illuminate\Cache\Repository` object (version 6 at this time).

Refer to the [Laravel Cache](https://laravel.com/docs/6.x/cache) documentation for information on using the cache object.
