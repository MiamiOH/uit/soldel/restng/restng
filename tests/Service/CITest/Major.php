<?php

namespace Tests\Service\CITest;

class Major extends \MiamiOH\RESTng\Service {

  private $database = '';
  private $dbh = '';

  public function setDatabase ($database) {
    $this->database = $database;

    $this->dbh = $this->database->getHandle('authorize');
    $this->dbh->mu_trigger_error = false;
  }

  public function getMajor () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $majorId = $request->getResourceParam('majorId');

    if ($majorId) {

      $record = $this->dbh->queryfirstrow_assoc('
          select major_id, major_dept, advisor
            from major
            where major_id = ?
        ', $majorId);

      if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      } else {
        $record = $this->camelCaseKeys($record);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);
      }
    } else {

      $query = '
            select major_id, major_dept, advisor
              from major
              order by major_id
          ';
      $sth = '';

      $records = array();

      if ($request->isPaged()) {
        $minRowToFetch = $request->getOffset();
        $maxRowToFetch = $minRowToFetch + $request->getLimit() - 1;

        // paged queries should include the total number of records
        $totalRecords = $this->dbh->queryfirstcolumn('
            select count(*)
              from major
          ');

        $response->setTotalObjects($totalRecords);

        if ($this->dbh->getType() == 'MySQL') {
          $query .= ' limit ' . ($request->getOffset() - 1) . ', ' . $request->getLimit();
          $sth = $this->dbh->prepare($query);
        } else {
          $query = "
            select major_id, major_dept, advisor
              from ( select /*+ FIRST_ROWS(n) */
                a.*, ROWNUM rnum
                  from ( $query ) a
                  where ROWNUM <= :MAX_ROW_TO_FETCH )
            where rnum  >= :MIN_ROW_TO_FETCH";

          $sth = $this->dbh->prepare($query);

          $sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
          $sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
        }

      } else {
        $sth = $this->dbh->prepare($query);
      }

      $sth->execute();

      while ($record = $sth->fetchrow_assoc()) {
        $record = $this->camelCaseKeys($record);
        $record['apiLocation'] = $this->locationFor('citest.major.majorId',
          array('majorId' => $record['majorId']));
        $records[] = $record;
      }

      $response->setStatus(\MiamiOH\RESTng\App::API_OK);
      $response->setPayload($records);
    }

    return $response;
  }

}
