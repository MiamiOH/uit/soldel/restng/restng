<?php

namespace Tests\unit\restng\Util;

use Carbon\Carbon;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Exception\InvalidArgumentException;
use MiamiOH\RESTng\Util\DeferredCall;
use MiamiOH\RESTng\Util\DeferredCallModel;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Testing\RefreshDatabase;
use Tests\UnitTestCase;

class DeferredCallModelTest extends UnitTestCase
{
    use RefreshDatabase;

    protected $databaseConnections = ['restng'];

    public function setUp(): void
    {
        parent::setUp();

        putenv('HASH_KEY=bob');
    }

    public function tearDown(): void
    {
        parent::tearDown();

        putenv('HASH_KEY');
        Carbon::setTestNow();
    }

    public function testCanBeCreatedWithInitialData(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals('test-key', $call->key());
        $this->assertEquals('my-resource', $call->resourceName());
    }

    public function testRequiresKeyToBeCreated(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $call = new DeferredCallModel([
            'resource_name' => 'my-resource',
        ]);

        $call->save();
    }

    public function testCannotChangeKeyAfterCreation(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->key = 'new-key';
    }

    public function testRequiresResourceNameToBeCreated(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $call = new DeferredCallModel([
            'key' => 'test-key',
        ]);

        $call->save();
    }

    public function testCannotChangeResourceNameAfterCreation(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->resource_name = 'new-resource';
    }

    public function testCanProvideResourceParamsWhenCreated(): void
    {
        $params = ['category' => 'open'];

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'resource_params' => $params,
        ]);

        $this->assertEquals($params, $call->resourceParams());
    }

    public function testResourceParamsMustBeArray(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'resource_params' => 'open',
        ]);
    }

    public function testCannotChangeResourceParamsAfterCreation(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'resource_params' => ['category' => 'open'],
        ]);

        $this->expectException(DeferredCallException::class);

        $call->resource_params = ['category' => 'closed'];
    }

    public function testCanProvideApiUserWhenCreated(): void
    {
        $user = $this->makeApiUser();

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'api_user' => $user,
        ]);

        $this->assertEquals($user->getUsername(), $call->apiUser()->getUsername());
    }

    public function testApiUserMustBeOfUserClass(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'api_user' => new \stdClass(),
        ]);
    }

    public function testCanAssertHasApiUser(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'api_user' => $this->makeApiUser(),
        ]);

        $this->assertTrue($call->hasApiUser());
    }

    public function testCanAssertDoesNotHaveApiUser(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertFalse($call->hasApiUser());
    }

    public function testThrowsExceptionAccessingApiUserWhenNoneIsPresent(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->apiUser();
    }

    public function testCannotChangeApiUserAfterCreation(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'api_user' => $this->makeApiUser(),
        ]);

        $this->expectException(DeferredCallException::class);

        $call->api_user = $this->makeApiUser();
    }

    public function testIsCreatedWithStatusPending(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());
    }

    public function testIsCreatedWithDefaultScheduledTime(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals($now, $call->scheduledTime());
    }

    public function testIsCreatedWithSpecifiedScheduledTime(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');
        $scheduledTime = $now->addDays(3);

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
            'scheduled_time' => $scheduledTime
        ]);

        $this->assertEquals($scheduledTime, $call->scheduledTime());
    }

    public function testCanUpdateScheduledTime(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals($now, $call->scheduledTime());

        $scheduledTime = $now->addDays(3);

        $call->scheduleAt($scheduledTime);

        $this->assertEquals($scheduledTime, $call->scheduledTime());
    }

    public function testCanIncrementAttemptCount(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(0, $call->attemptCount());

        $call->incrementAttemptCount();

        $this->assertEquals(1, $call->attemptCount());
    }

    public function testCanIncrementErrorCount(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(0, $call->errorCount());

        $call->incrementErrorCount();

        $this->assertEquals(1, $call->errorCount());
    }

    public function testIsCreatedWithCreateAtNow(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals($now, $call->createdAt());
    }

    public function testIsCreatedWithUpdatedAtNow(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals($now, $call->updatedAt());
    }

    public function testUpdatedAtIsSetWhenSaving(): void
    {
        $now = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($now);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals($now, $call->updatedAt());

        $now = Carbon::parse('2020-05-16 11:04:10');

        Carbon::setTestNow($now);

        $call->incrementAttemptCount();
        $call->save();

        $this->assertEquals($now, $call->updatedAt());
    }

    public function testAssertsIsPendingStatus(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertTrue($call->isPending());
        $this->assertFalse($call->isNotPending());
    }

    public function testAssertsIsNotPendingStatus(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->markComplete();

        $this->assertFalse($call->isPending());
        $this->assertTrue($call->isNotPending());
    }

    public function testGeneratesHashForCall(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertNotEmpty($call->hash());
    }

    public function testVerifiesHashForCall(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertTrue($call->verifyCallHash());
    }

    public function testCanAddResponseToCall(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertFalse($call->hasResponse());

        $call->addResponse(new Response());

        $this->assertTrue($call->hasResponse());
    }

    public function testCanUpdateResponseOnCall(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->addResponse(new Response());

        $this->assertEquals(200, $call->responseStatus());

        $newResponse = new Response();
        $newResponse->setStatus(201);
        $call->addResponse($newResponse);

        $this->assertEquals(201, $call->responseStatus());
    }

    public function testCanGetResponseStatus(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->addResponse(new Response());

        $this->assertEquals(200, $call->responseStatus());
    }

    public function testCannotSetResponseStatusDirectly(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->response_status = 200;
    }

    public function testThrowsExceptionAccessingResponseStatusWhenNoneIsPresent(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->responseStatus();
    }

    public function testCanGetResponseFromCall(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->addResponse(new Response());

        $response = $call->response();

        $this->assertEquals(200, $response->getStatus());
    }

    public function testThrowsExceptionAccessingResponseWhenNoneIsPresent(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->expectException(DeferredCallException::class);

        $call->response();
    }

    public function testCanMarkCallInProgress(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());

        $call->markInProgress();

        $this->assertEquals(DeferredCall::STATUS_IN_PROGRESS, $call->status());
    }

    public function testCanMarkCallPending(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->markInProgress();
        $this->assertEquals(DeferredCall::STATUS_IN_PROGRESS, $call->status());

        $call->markPending();

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());
    }

    public function testCanMarkCallCancelled(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());

        $call->markCancelled();

        $this->assertEquals(DeferredCall::STATUS_CANCELLED, $call->status());
    }

    public function testCanMarkCallComplete(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());

        $call->markComplete();

        $this->assertEquals(DeferredCall::STATUS_COMPLETE, $call->status());
    }

    public function testCanMarkCallFailed(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertEquals(DeferredCall::STATUS_PENDING, $call->status());

        $call->markFailed();

        $this->assertEquals(DeferredCall::STATUS_FAILED, $call->status());
    }

    public function testAssertsCallIsFailed(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $call->markFailed();

        $this->assertTrue($call->isFailed());
    }

    public function testAssertsCallIsNotFailed(): void
    {
        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        $this->assertFalse($call->isFailed());
    }

    public function testCanBeReset(): void
    {
        $scheduledAt = Carbon::parse('2020-05-15 11:04:10');

        Carbon::setTestNow($scheduledAt);

        $call = new DeferredCallModel([
            'key' => 'test-key',
            'resource_name' => 'my-resource',
        ]);

        Carbon::setTestNow($scheduledAt->addHours(4));

        $call->incrementAttemptCount();
        $call->incrementErrorCount();
        $call->markFailed();

        $call->reset();

        $this->assertEquals(DeferredCallModel::STATUS_PENDING, $call->status());
        $this->assertEquals(0, $call->attemptCount());
        $this->assertEquals(0, $call->errorCount());
        $this->assertEquals(Carbon::now(), $call->scheduledTime());
    }

    private function makeApiUser(string $username = 'doej'): User
    {
        $validator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);
        $validator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => $username,
        ]);

        $user = new \MiamiOH\RESTng\Util\User();
        $user->setCredentialValidator($validator);
        $user->addUserByToken('abc123');

        return $user;
    }
}
