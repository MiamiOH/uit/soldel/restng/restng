<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:57 PM
 */

namespace Tests\Data\Table;


use MiamiOH\RESTng\Testing\Data\Table;

class Student extends Table
{

    private $generatedIds = [];

    public function create(): void
    {
        $this->drop();

        $this->database->exec('
            create table if not exists student (
                student_id int primary key,
                unique_id text not null,
                last_name text,
                first_name text,
                gender text,
                birthday date,
                entry_year int,
                us_resident int,
                state text,
                major_id text
            )
        ');

        $this->database->exec('create index student_student_id on student (student_id)');
        $this->database->exec('create index student_unique_id on student (unique_id)');
        $this->database->exec('create index student_gender on student (gender)');
        $this->database->exec('create index student_entry_year on student (entry_year)');
        $this->database->exec('create index student_us_resident on student (us_resident)');
        $this->database->exec('create index student_state on student (state)');
        $this->database->exec('create index student_major_id on student (major_id)');
    }

    public function drop(): void
    {
        $this->database->exec('drop table if exists student');
    }

    public function populate(): void
    {
        $this->clearGeneratedIds();

        foreach ($this->testRecords() as $student) {
            $uniqueId = $this->generateUniqueId($student);

            $insert = sprintf(
                "insert into student (unique_id, student_id, last_name, first_name, gender, birthday, entry_year, us_resident, state, major_id) values ('%s', '%s', '%s', '%s', '%s', date('%s'), '%s', '%s', '%s', '%s')",
                $uniqueId, ...$student
            );

            $this->database->exec($insert);
        }
    }

    private function testRecords(): array
    {
        return [
            [100574, 'Kanaga', 'Giles', 'M', '1/14/1988', 2007, 1, 'IN', 'ACC'],
        ];
    }

    private function clearGeneratedIds(): void
    {
        $this->generatedIds = [];
    }

    private function generateUniqueId(array $student): string
    {
        $id = strtolower(substr($student[1], 0, 6) . $student[2][0]);

        while (in_array($id, $this->generatedIds)) {
            $id = $this->incrementUniqueId($id);
        }

        $this->generatedIds[] = $id;

        return $id;
    }

    private function incrementUniqueId(string $uniqueId): string
    {
        $currentIncrement = 0;
        if (preg_match('/(\d+)$/', $uniqueId,$matches)) {
            $currentIncrement = (int) $matches[1];
        }

        $currentIncrement++;

        $base = preg_replace('/(\d+)$/', '', $uniqueId);

        return substr($base, 0, 8 - strlen((string) $currentIncrement)) . (string) $currentIncrement;
    }
}