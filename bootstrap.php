<?php

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use MiamiOH\RESTng\Service\Apm\TransactionFactory;
use MiamiOH\RESTng\Util\ResourceLoader;
use Psr\Http\Message\RequestInterface;

foreach (['.env.' . getenv('RESTNG_ENVIRONMENT'), '.env'] as $fileName) {
    $dotEnv = Dotenv::create([__DIR__, getcwd()], $fileName);
    try {
        $dotEnv->load();
    } catch (\Dotenv\Exception\InvalidPathException $e) {
        // Not found, continue to next candidate
        continue;
    }

    // Successfully loaded so break out of the loop
    break;
}

$baseDir = __DIR__;
$confDir = $baseDir . '/config';

// Load the log4php config if we have one.
if (file_exists( $confDir . '/log4php.conf.php')) {
    require $confDir . '/log4php.conf.php';
} else {
    Logger::configure(array(
        'rootLogger' => array(
            'level' => 'warn',
        )
    ));
}

$logger = Logger::getLogger('api.bootstrap');

$agent = (new \Nipwaayoni\AgentBuilder())
    ->withEnvData(['REMOTE_ADDR'])
    ->withHttpClient(new \Http\Adapter\Guzzle6\Client(new Client(['verify' => env('APM_VERIFYCERT', false)])))
    ->build();

$transaction = new \MiamiOH\RESTng\Service\Apm\Transaction($agent);

$span = $transaction->startSpan('Bootstrap');

$app = new MiamiOH\RESTng\App();
if (env('TESTING', false)) {
    $app->router = new \MiamiOH\RESTng\Testing\TestRouter();
}

/*
	Set up Slim application.

	Many settings may be stage dependent.  These should come from an external
	config so we can manage them independently.
*/

// Debug should be off, otherwise the default error handler returns HTML.
// Since we are doing REST exclusively, the response should match the
// consumer expectation.
$app->config('debug', false);

$app->setCurrentStage(env('RESTNG_ENVIRONMENT', 'development'));

$app->addRegistry(require($baseDir . '/src/registry.php'));

$app->addService([
    'name' => 'APIApp',
    'object' => $app,
    'description' => 'The API application object',
]);

$app->addService([
    'name' => 'ApmAgent',
    'object' => $agent,
    'description' => 'The APM Agent',
]);

$app->addService([
    'name' => 'ApmTransactionFactory',
    'object' => new TransactionFactory($agent),
    'description' => 'The APM Agent',
]);

$app->addService([
    'name' => 'ApmTransaction',
    'object' => $transaction,
    'description' => 'An APM transaction',
]);

// The API resources are bundled with RESTng
$app->addResourceLoader(ResourceLoader::fromArray([
    'resources' => [
        'api' => [
            \MiamiOH\RESTng\Service\API\ApiResourceProvider::class,
        ],
    ]
]));

$dataSourceFile = env('DATASOURCE_FILE', 'config/datasources.yaml');
if (strpos($dataSourceFile, '/') !== 0) {
    $dataSourceFile = $baseDir . '/' . $dataSourceFile;
}
$dataSourceFactory = $app->makeDataSourceFactory($dataSourceFile);

$app->makeEventHandler();
// There should be a better method to register listeners, but this will work for now
$queryListener = new \MiamiOH\RESTng\Service\Apm\QueryListener($app);
$queryListener->register();

$capsule = $app->makeDatabaseCapsule();
if (!empty($baseDir) && file_exists( $baseDir . '/db/connections.php')) {
    $databaseConfig = require($baseDir . '/db/connections.php');
    $capsule->register($databaseConfig);
}

$app->makeCache(
    env('CACHE_DRIVER'),
    [
        'path' => env('CACHE_FILE_PATH', $baseDir . '/cache'),
        'connection' => env('CACHE_CONNECTION', 'restng'),
        'table' => env('CACHE_TABLE', 'cache'),
    ]
);

$providerFile = env('RESOURCE_PROVIDER_CONFIG', $confDir . '/providers.yaml');
if ($providerFile && !file_exists($providerFile) && strpos($providerFile, '/') !== 0) {
    $providerFile = $baseDir . '/' . $providerFile;
}
if (file_exists($providerFile)) {
    $providers = yaml_parse_file($providerFile);
    if (isset($providers['resources'])) {
        foreach ($providers['resources'] as $provider) {
            // Resolve a relative path using the realpath from the provider file
            $providerBase = dirname(realpath($providerFile));
            if (strpos($provider, '/') !== 0) {
                $provider = $providerBase . '/' . $provider;
            }
            if (file_exists($provider)) {
                $app->addResourceLoader(ResourceLoader::fromArray(require($provider)));
            }
        }
    }
}

$services = [];

// The built in service definitions always exist
$apiServices = yaml_parse_file($baseDir . '/src/api-services.yaml');
// Implementation specific service definitions are added if found
$localServicesFile = env('SERVICE_FILE', $confDir . '/services.yaml');
// Resolve a relative path using the baseDir
if ($localServicesFile && strpos($localServicesFile, '/') !== 0) {
    $localServicesFile = $baseDir . '/' . $localServicesFile;
}
if (file_exists($localServicesFile)) {
    $localServices = yaml_parse_file($localServicesFile);
}

$services = array_merge($apiServices['services'], ($localServices['services'] ?? []));

foreach ($services as $name => $serviceConfig) {
    if (isset($serviceConfig['set'])) {
        foreach (array_keys($serviceConfig['set']) as $setKey) {
            if (isset($serviceConfig['set'][$setKey]['value']) && strpos($serviceConfig['set'][$setKey]['value'], 'return ') === 0) {
                try {
                    $serviceConfig['set'][$setKey]['value'] = eval($serviceConfig['set'][$setKey]['value']);
                } catch (\ParseError $e) {
                    throw new InvalidArgumentException("Invalid PHP fragment for $name => $setKey");
                }
            }
        }
    }

    $app->addService(array_merge($serviceConfig, ['name' => $name]));
}

/*
  Set up error handling now that we're mostly set up. For best results, set the following
  (or a variation of) in the api.config.php file.

    ini_set( "display_errors", "on" );
    error_reporting( E_ALL );

  If display_errors is off, the framework will not catch errors.

  The error_reporting setting can be dialed back, but best practice is to be as strict
  as you can be.

*/
$returnExceptions = env('TESTING', false);
$app->error(function (\Exception $e) use ($app, $returnExceptions) {
    $app->handleException($e, $returnExceptions);
});

$span->stop();

return $app;
