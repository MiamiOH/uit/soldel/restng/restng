<?php

namespace Tests\Service\CITest;

class Student extends \MiamiOH\RESTng\Service {

  private $database = '';
  private $dbh = '';

  public function setDatabase ($database) {
    $this->database = $database;

    $this->dbh = $this->database->getHandle('authorize');
    $this->dbh->mu_trigger_error = false;
  }

  public function getStudent () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $studentId = $request->getResourceParam('studentId');
    $keyField = 'student_id';
    switch ($request->getResourceParamKey('studentId')) {
      case 'sid':
            $keyField = 'student_id';
            break;
      case 'uid':
            $keyField = 'unique_id';
            break;
    }

    $options = $request->getOptions();

    $fieldList = '';
    if ($request->isPartial()) {
      $fieldList = implode(', ', $this->snakeCaseValues($request->getPartialRequestFields()));
    } else {
      $fieldList = 'unique_id, student_id, last_name, first_name, gender, birthday, entry_year, us_resident, state, major_id';
    }

    if ($studentId) {

      $record = $this->dbh->queryfirstrow_assoc("
          select $fieldList
            from student
            where $keyField = ?
        ", $studentId);

      if ($record === false) {
        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      } else {
        $record = $this->camelCaseKeys($record);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);
      }
    } else {

      $whereConditions = array();
      if (isset($options['entryYear']) && $options['entryYear']) {
        $whereConditions[] = 'entry_year in (' . implode(', ',
            array_map(function($s) { return "'$s'"; }, $options['entryYear'])) . ')';
      }

      if (isset($options['majorId']) && $options['majorId']) {
        $whereConditions[] = 'major_id in (' . implode(', ',
            array_map(function($s) { return "'$s'"; }, $options['majorId'])) . ')';
      }

      if (isset($options['state']) && $options['state']) {
        $whereConditions[] = 'state in (' . implode(', ',
            array_map(function($s) { return "'$s'"; }, $options['state'])) . ')';
      }

      if (isset($options['usResident'])) {
        $whereConditions[] = 'us_resident = ' . $options['usResident'];
      }

      $whereClause = $whereConditions ? ' and ' . implode(' and ', $whereConditions) : '';

      $query = "
            select $fieldList
              from student
              where 1 = 1 $whereClause
              order by student_id
          ";
      $sth = '';

      $records = array();

      if ($request->isPaged()) {
        $minRowToFetch = $request->getOffset();
        $maxRowToFetch = $minRowToFetch + $request->getLimit() - 1;

        // paged queries should include the total number of records
        $totalRecords = $this->dbh->queryfirstcolumn("
            select count(*)
              from student
              where 1 = 1 $whereClause
          ");

        $response->setTotalObjects($totalRecords);

        if ($this->dbh->type == 'MySQL') {
          $query .= ' limit ' . ($request->getOffset() - 1) . ', ' . $request->getLimit();
          $sth = $this->dbh->prepare($query);
        } else {
          $query = "
            select $fieldList
              from ( select /*+ FIRST_ROWS(n) */
                a.*, ROWNUM rnum
                  from ( $query ) a
                  where ROWNUM <= :MAX_ROW_TO_FETCH )
            where rnum  >= :MIN_ROW_TO_FETCH";

          $sth = $this->dbh->prepare($query);

          $sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
          $sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
        }

      } else {
        $sth = $this->dbh->prepare($query);
      }

      $sth->execute();

      while ($record = $sth->fetchrow_assoc()) {
        $record = $this->camelCaseKeys($record);
        $record['apiLocation'] = $this->locationFor('citest.student.studentId',
          array('studentId' => $record['studentId']));
        $records[] = $record;
      }

      $response->setStatus(\MiamiOH\RESTng\App::API_OK);
      $response->setPayload($records);
    }

    return $response;
  }

  public function createStudent () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $data = $request->getData();

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($data);

    return $response;
  }

  public function getStudentAuths () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $user = $this->getApiUser();

    $records = array();

    if ($user->isAuthenticated()) {
      $records['authorized'] = $user->isAuthorized('RESTng Test App', 'User', 'view');
      $records['self'] = $user->isSelfAccess('studentId', 'strtolower');
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($records);

    return $response;
  }

}
