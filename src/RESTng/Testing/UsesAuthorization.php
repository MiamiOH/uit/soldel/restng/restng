<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 10:06 AM
 */

namespace MiamiOH\RESTng\Testing;


use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface;

/**
 * Trait UsesAuthorization
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
trait UsesAuthorization
{
    /** @var AuthorizationValidatorInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $authorizationValidator;

    public function willNotAuthorizeUser(): void
    {
        $this->installAuthorizationValidator();

        $this->authorizationValidator->method('validateAuthorizationForKey')
            ->willReturn(false);
    }

    public function willAuthorizeUser(): void
    {
        $this->installAuthorizationValidator();

        $this->authorizationValidator->method('validateAuthorizationForKey')
            ->willReturn(true);
    }

    private function installAuthorizationValidator(): void
    {
        $this->authorizationValidator =
            $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        $this->app->useService([
            'name' => 'AuthorizationValidator',
            'object' => $this->authorizationValidator,
            'description' => 'Mock authorization validator',
        ]);
    }

}