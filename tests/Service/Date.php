<?php

namespace Tests\Service;

use Carbon\Carbon;

class Date extends \MiamiOH\RESTng\Service
{

    public function getDate()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $now = Carbon::now();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload([
          'date_string' => $now->toDateString()
        ]);

        return $response;
    }
}
