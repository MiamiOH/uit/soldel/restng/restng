<?php

class AddBookValidatedTest extends \MiamiOH\RESTng\Testing\TestCase
{

  protected $bookObj;

  public function setUp(): void
  {
    parent::setUp();

    /*
        The API service handles a lot of the coordination between the consumer
        interface layer and the business service layer. We mock that here
        so that we have complete control of what happens.
    */
    $app = $this->getMockBuilder('\MiamiOH\RESTng\App')
          ->setMethods(array('newResponse', 'callResource', 'locationFor'))
          ->getMock();

    /*
        We will use the real Response class in this case. If we wanted to enforce
        specific behaviors of the response, we could mock it as well.
    */
    $app->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

    /*
        Create a mock database handle. This will be used to respond to any queries
        coming from the subject under test.
    */
    $dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
        ->setMethods(array('perform'))
        ->getMock();

    $dbh->method('perform')->willReturn(true);

    /*
        Create a mock database service. The database service is normally injected
        by the framework when a service asks for it. We will inject the mock so
        we can return our mock database handle.
    */
    $db = $this->getMockBuilder('MiamiOH\RESTng\Connector\DatabaseFactory')
        ->setMethods(array('getHandle'))
        ->getMock();

    $db->method('getHandle')->willReturn($dbh);

    /*
        Create a real instance of our subject under test.
    */
    $this->bookObj = new Tests\Service\CITest\Book();

    /*
        Inject the mock API service using the RESTng\Service::setApi method. Since
        all business services are supposed to inherit from that class, this method
        should exist.
    */
    $this->bookObj->setApp($app);

    /*
        Inject the mock Database service. The subject under test declares the setter
        method in this case.
    */
    $this->bookObj->setDatabase($db);

  }

  public function testAddBookValidated() {

    /*
        In normal operation, the framework constructs and provides the Request
        object. For our test, we want to mock the Request so we can apply very
        specific values.
    */
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

    $request->expects($this->once())->method('getData')->willReturn(array(
        'id' => 1,
        'publishDate' => '2016-06-06',
        'title' => 'My Book',
        'author' => 'Jane Doe'
    ));

    /*
     * Create mock validator factory and validator which will pass all tests.
     */
    $validator = $this->getMockBuilder('\Respect\Validation\Validator')
        ->setMethods(array('intVal', 'min', 'date'))
        ->getMock();

    $validator->method('intVal')->will($this->returnSelf());
    $validator->method('min')->will($this->returnSelf());
    $validator->method('date')->will($this->returnSelf());

    $validatorFactory = $this->getMockBuilder('\MiamiOH\RESTng\Util\ValidationFactory')
        ->setMethods(array('newValidator'))
        ->getMock();

    $validatorFactory->method('newValidator')->willReturn($validator);

    $this->bookObj->setValidator($validatorFactory);

    $this->bookObj->setRequest($request);

    $resp = $this->bookObj->addBookValidated();

    $payload = $resp->getPayload();

  }

}
