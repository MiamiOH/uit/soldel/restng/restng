<?php

class apiHashTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\App */
    private $restngApp;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = new \MiamiOH\RESTng\App();

    }

    public function testHash()
    {
        $expectedHash = 'bGabpz9bGzfmG1lQpXr14UozS6u4hODvQ4LInIAsc5A=';

        $hash = $response = $this->restngApp->hash('my secret');

        $this->assertEquals($expectedHash, $hash);

    }

}
