+++
title = "Deferred Call Processes"
weight = 30
+++

The following processes should be in place to support the execution of deferred calls. Only the basic commands are shown here.  See the [command](commands.md) documentation for all options.

## Deferred Call Worker

```shell
php console deferred-calls:work
```

This command would typically be started by systemd or otherwise run continually in the background.

## Deferred Call Purge

```shell
php console deferred-calls:purge --before "7 days"
```

This command should be run daily to purge completed and cancelled jobs older than 1 week.

## Health Check

```shell
php console deferred-calls:check-failures --within "5 minutes"
```

This should run as part of routine monitoring where the "within" time is the interval between checks.
