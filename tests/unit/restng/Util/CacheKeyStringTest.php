<?php

namespace Tests\unit\restng\Util;

use MiamiOH\RESTng\Util\CacheKeyString;
use PHPUnit\Framework\TestCase;

class CacheKeyStringTest extends TestCase
{
    /**
     * @var string
     */
    private $format;
    /**
     * @var array
     */
    private $values;

    public function setUp(): void
    {
        parent::setUp();

        $this->format = 'authorization:{username}:{application}:{module}:{key}';
        $this->values = [
            'username' => 'doej',
            'application' => 'My Application',
            'module' => 'Random',
            'key' => 'view',
        ];
    }
    public function testReturnsFormatString(): void
    {
        $key = new CacheKeyString($this->format, $this->values);

        $this->assertEquals($this->format, $key->format());
    }

    public function testReturnsReplacementsArray(): void
    {
        $key = new CacheKeyString($this->format, $this->values);

        $this->assertEquals($this->values, $key->replacements());
    }

    public function testReturnsRenderedKey(): void
    {
        $key = new CacheKeyString($this->format, $this->values);

        $this->assertEquals('authorization:doej:My Application:Random:view', $key->key());
    }

    /**
     * @dataProvider keyValidityChecks
     */
    public function testAssertsValidityForKeyLength(string $format, array $values, bool $valid): void
    {
        $key = new CacheKeyString($format, $values);

        $this->assertEquals($valid, $key->isValid());
        $this->assertEquals(!$valid, $key->isNotValid());
    }

    public function keyValidityChecks(): array
    {
        return [
            'valid key'=> ['{key}', ['key' => 'abc123'], true],
            'not valid, empty'=> ['{key}', ['key' => ''], false],
            'not valid, length'=> ['{key}', ['key' => str_repeat('abc123', 100)], false],
        ];
    }
}
