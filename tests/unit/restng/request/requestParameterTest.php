<?php

class requestParameterTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\RequestParameter $param */
    private $param;

    public function setUp(): void
    {
        parent::setUp();

        $this->param = new \MiamiOH\RESTng\Util\RequestParameter();

    }

    public function testParameterCreate()
    {

        $this->assertFalse(is_null($this->param));

    }

    public function testParameterOptions()
    {
        $options = [
            'alternateKeys' => ['nid', 'uid'],
        ];

        $this->param = new \MiamiOH\RESTng\Util\RequestParameter($options);

        $altKeys = $this->param->getAlternateKeys();

        $this->assertTrue(is_array($altKeys));
        $this->assertEquals('nid', $altKeys[0]);
        $this->assertEquals('uid', $altKeys[1]);

    }

    public function testParameterSetParameterNoAlternates()
    {
        $this->param = new \MiamiOH\RESTng\Util\RequestParameter();

        $this->param->setParameter('testName', '123');

        $this->assertEquals('123', $this->param->getValue());
    }

    public function testParameterSetParameterAlternatesDefault()
    {
        $options = [
            'alternateKeys' => ['nid', 'uid'],
        ];

        $this->param = new \MiamiOH\RESTng\Util\RequestParameter($options);

        $this->param->setParameter('testName', '123');

        $this->assertEquals('123', $this->param->getValue());
        $this->assertEquals('nid', $this->param->getKey());
    }

    public function testParameterSetParameterAlternate()
    {
        $options = [
            'alternateKeys' => ['nid', 'uid'],
        ];

        $this->param = new \MiamiOH\RESTng\Util\RequestParameter($options);

        $this->param->setParameter('testName', 'uid=bob');

        $this->assertEquals('bob', $this->param->getValue());
        $this->assertEquals('uid', $this->param->getKey());
    }

    public function testParameterSetParameterAlternateNotFound()
    {
        $options = [
            'alternateKeys' => ['nid', 'uid'],
        ];

        $this->param = new \MiamiOH\RESTng\Util\RequestParameter($options);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Provided key gid is not in the alternate key list: nid, uid');

        $this->param->setParameter('testName', 'gid=bob');

    }

    public function testParameterSerialize()
    {
        $testName = 'testName';
        $testValue = 'testValue';
        $testKey = 'testKey';

        $this->param->setName($testName);
        $this->param->setValue($testValue);
        $this->param->setKey($testKey);

        $serializedParam = $this->param->serialize();

        $newParam = new \MiamiOH\RESTng\Util\RequestParameter();
        $newParam->unserialize($serializedParam);

        $this->assertEquals($testName, $newParam->getName());
        $this->assertEquals($testValue, $newParam->getValue());
        $this->assertEquals($testKey, $newParam->getKey());
    }

    // The toString magic method is called in limited contexts which renders
    // it less useful than hoped.
    public function xtestParameterToString()
    {
        $testName = 'testName';
        $testValue = 'testValue';

        $this->param->setName($testName);
        $this->param->setValue($testValue);

        $this->assertEquals($testValue, (string)$this->param);
    }
}
