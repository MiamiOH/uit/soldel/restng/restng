<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-02-11
 * Time: 20:28
 */

namespace MiamiOH\RESTng\Service\Apm;


use Nipwaayoni\Agent;
use Nipwaayoni\Events\EventBean;

class Span
{
    /** @var \Nipwaayoni\Events\Span  */
    protected $span;

    protected $type = 'request';
    protected $subType;

    public function __construct(string $name, \Nipwaayoni\Events\Transaction $parent)
    {
        $this->span = new \Nipwaayoni\Events\Span($name, $parent);
        $this->span->start();
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function setSubType(string $subType): void
    {
        $this->subType = $subType;
    }

    public function name(): string
    {
        return $this->span->getName();
    }

    public function stop(): void
    {
        $this->span->stop();
    }

    public function event(): EventBean
    {
        $this->span->setType($this->type);

        return $this->span;
    }
}
