+++
title = "RESTng Documentation"
weight = 20
+++

The RESTng documentation is the collection of documents you are currently browsing. It is managed is part of the project in docs directory in text files using markdown syntax.

The markdown files are converted to static html using hugo (http://gohugo.io/). Hugo is not provided as part of the RESTng distribution and you will need to install it on your own if you need to generate the docs.
 