<?php

class mwContentFormatHttpHeaderTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testContentSetupAcceptAny() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => '*/*',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupAcceptJsonPlainMultiSpace() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json, text/plain, */*',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupAcceptJsonPlainMultiNoSpace() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json,text/plain,*/*',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupAcceptJsonPlainComplex() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupContentTypeTextCharset() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'text/html;charset=UTF-8',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unsupported content-type: text/html;charset=UTF-8 (text/html)');

        $this->mw->call();

    }

}
