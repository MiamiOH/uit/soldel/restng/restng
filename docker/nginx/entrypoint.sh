#!/bin/sh

if [ "$1" = 'sh' ]; then
  exec "$@"
  exit 0
fi

nginx -g 'daemon off;'
