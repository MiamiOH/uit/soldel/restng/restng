<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/17/18
 * Time: 9:21 PM
 */

namespace MiamiOH\RESTng\Connector;


interface EncryptionInterface
{
    public function decrypt(string $encryptedString): string;
    public function encrypt(string $plainTextString): string;
}