<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\Data\Table\Student;
use Tests\FeatureTestCase;
use MiamiOH\RESTng\Testing\TestResponse;

class ResourceAuthorizationTest extends FeatureTestCase
{
    use UsesDatasource;
    use UsesTestData;
    use UsesAuthentication;
    use UsesAuthorization;

    public function setUp(): void
    {
        parent::setUp();

        $db = $this->createSqlite3DatabaseHandle();

        $this->useData()
            ->withSqlite($db->getDbConnection())
            ->withTable(Student::class)
            ->populate();

        $this->installDatasourceService($db);

        $this->withToken('8TgYQQlZKY')->willAuthenticateUser();

        $this->showExceptions();
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/citest/student/all/100574');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $this->willAuthorizeUser();

        $response = $this->getJson('/citest/student/all/100574');

        $this->assertRequestIsAuthorized($response);
    }

    public function assertRequestIsAuthorized(TestResponse $response): void
    {
        $response->assertSuccessful();
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}
