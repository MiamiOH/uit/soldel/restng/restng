<?php

namespace Tests\Service\CITest;

class CITestValidation extends \MiamiOH\RESTng\Service
{

    /** @var \MiamiOH\RESTng\Util\ValidationFactory $validator */
    private $validator = '';

    public function setValidator($validator)
    {
        /** @var \MiamiOH\RESTng\Util\ValidationFactory $validator */
        $this->validator = $validator;
    }

    public function getValidatedResult()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $rule = $this->validator->newValidator()->length(4,10);

        $payload['color'] = $options['color'];
        $payload['passes'] = $rule->validate($options['color']);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

}