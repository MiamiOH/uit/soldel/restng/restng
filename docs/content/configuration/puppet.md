+++
title = "Puppet"
weight = 10
+++

RESTng 2 is installed and configured using Puppet on our development, test and production servers.

## Installation

Most of the heavy lifting is done by a module built specifically to manage RESTng:

https://gitlab.com/MiamiOH/uit/operations/puppet-restng

The primary purpose of this module is to manage the configuration files described in the following sections.

The module does ensure the latest miamioh-restng RPM package is installed, along with any specified addo-ns RPM. (Add-on packages contain the resources RESTng will make available.)

## Configuration

Configuration data is kept in the Puppet HostGroup files which are applied to our main RESTng servers, `Server/PHP/Admin/REST`.

All servers:

https://gitlab.com/MiamiOH/uit/operations/puppet/-/blob/master/data/hostgroup/all/Server/PHP/Admin/REST.yaml

Test servers:

https://gitlab.com/MiamiOH/uit/operations/puppet/-/blob/master/data/hostgroup/test/Server/PHP/Admin/REST.yaml

Production servers:

https://gitlab.com/MiamiOH/uit/operations/puppet/-/blob/master/data/hostgroup/production/Server/PHP/Admin/REST.yaml
