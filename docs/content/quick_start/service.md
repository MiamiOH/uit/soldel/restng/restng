+++
title = "Create a Service"
weight = 40
+++

Now that we have defined our resource, we need to create the business service to handle the request. Your class must extend `\MiamiOH\RESTng\Service`. Any method being called by a resource will implement the pattern shown below. The request and response objects will be provided by RESTng. Your service will read from the request, update the response and then return the response.

{{< highlight php "linenos=table" >}}
<?php
...
    public function getById () {
        $request = $this->getRequest();
        $response = $this->getResponse();
        
        $response->setStatus(\MiamiOH\RESTng\API_OK);
        $response->setPayload( new MyObject($request->getResourceParam('id') );
        
        return $response;
    }
...
{{< / highlight >}}
