<?php

class responseTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $response;

    public function setUp(): void
    {
        parent::setUp();

        $this->response = new \MiamiOH\RESTng\Util\Response();

    }

    public function testSetRequest()
    {

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->getMock();

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($request, $this->response->getRequest());

    }

    public function testIsSuccessOk()
    {
        $this->response->setStatus(\MiamiOH\RESTng\App::API_OK);

        $this->assertTrue($this->response->isSuccess());

    }

    public function testIsSuccessCreated()
    {
        $this->response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        $this->assertTrue($this->response->isSuccess());

    }

    public function testIsSuccessFailed()
    {
        $this->response->setStatus(\MiamiOH\RESTng\App::API_FAILED);

        $this->assertFalse($this->response->isSuccess());

    }

    public function testIsSuccessUnauthorized()
    {
        $this->response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);

        $this->assertFalse($this->response->isSuccess());

    }

    public function testSetPayload()
    {

        $payload = array('id' => 1, 'name' => 'Test name');

        $this->response->setPayload($payload);

        $this->assertEquals($payload, $this->response->getPayload());

    }

    public function testSetPayloadRequiresArray()
    {

        $payload = 'test';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Argument to MiamiOH\RESTng\Util\Response::setPayload() must be an array');

        $this->response->setPayload($payload);

        $this->assertEquals($payload, $this->response->getPayload());

    }

    public function testSetStatus()
    {

        $status = 200;

        $this->response->setStatus($status);

        $this->assertEquals($status, $this->response->getStatus());

    }

    public function testSetObjectName()
    {

        $objectName = 'dataModel';

        $this->response->setObjectName($objectName);

        $this->assertEquals($objectName, $this->response->getObjectName());

    }

    public function testSetTotalObjects()
    {

        $totalObjects = 15;

        $this->response->setTotalObjects($totalObjects);

        $this->assertEquals($totalObjects, $this->response->getTotalObjects());

    }

    public function testIsPaged()
    {

        $isPaged = true;

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('isPaged'))
            ->getMock();

        $request->method('isPaged')->willReturn($isPaged);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($isPaged, $this->response->isPaged());

    }

    public function testGetResourceParams()
    {

        $params = array('id' => 1);

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParams'))
            ->getMock();

        $request->method('getResourceParams')->willReturn($params);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($params, $this->response->getResourceParams());

    }

    public function testGetResourceOptions()
    {

        $options = array('type' => 'new');

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->method('getOptions')->willReturn($options);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($options, $this->response->getResourceOptions());

    }

    public function testGetResourceName()
    {

        $resourceName = 'test.resource';

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceName'))
            ->getMock();

        $request->method('getResourceName')->willReturn($resourceName);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($resourceName, $this->response->getResourceName());

    }

    public function testGetResponseLimit()
    {

        $responseLimit = 10;

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getLimit'))
            ->getMock();

        $request->method('getLimit')->willReturn($responseLimit);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($responseLimit, $this->response->getResponseLimit());

    }

    public function testGetResponseOffset()
    {

        $responseOffset = 10;

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOffset'))
            ->getMock();

        $request->method('getOffset')->willReturn($responseOffset);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($responseOffset, $this->response->getResponseOffset());

    }

    public function testGetResponseCallback()
    {

        $responseCallback = 'myCallback';

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getCallback'))
            ->getMock();

        $request->method('getCallback')->willReturn($responseCallback);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($responseCallback, $this->response->getResponseCallback());

    }

    public function testGetResponseFieldSpec()
    {

        $responseFieldSpec = 'id,name,description';

        $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getPartialFieldsSpec'))
            ->getMock();

        $request->method('getPartialFieldsSpec')->willReturn($responseFieldSpec);

        $setResp = $this->response->setRequest($request);

        $this->assertEquals($responseFieldSpec, $this->response->getPartialFieldsSpec());

    }

}
