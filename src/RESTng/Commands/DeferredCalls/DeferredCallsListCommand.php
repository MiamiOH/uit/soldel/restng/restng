<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use Carbon\Carbon;
use MiamiOH\RESTng\Util\DeferredCall;
use MiamiOH\RESTng\Util\DeferredCallManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DeferredCallsListCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsListCommand extends Command
{
    protected static $defaultName = 'deferred-calls:list';

    /** @var DeferredCallManager */
    private $callManager;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
    }

    protected function configure(): void
    {
        $this->addOption(
            'status',
            null,
            InputOption::VALUE_OPTIONAL,
            'List only status (p, c, f)'
        );

        $this->addOption(
            'within',
            null,
            InputOption::VALUE_OPTIONAL,
            'Within timeframe (ex "2 hours")',
            '12 hours'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $headers = ['ID', 'Key', 'Status', 'Scheduled', 'Attempts', 'Updated'];
        $data = [];

        $status = $input->getOption('status');
        $within = $input->getOption('within');
        $since = Carbon::now()->subtract($within);

        $calls = $this->callManager->getCalls($since, $status);

        $calls->each(function (DeferredCall $call) use (&$data) {
            $data[] = [
                $call->id(),
                $call->key(),
                $call->status(),
                $call->scheduledTime(),
                $call->attemptCount() . '/' . $call->errorCount(),
                $call->updatedAt(),
            ];
        });

        $output->writeln(sprintf('<info>Deferred calls in the previous %s</info>', $within));
        $outputStyle->table($headers, $data);

        return Command::SUCCESS;
    }
}
