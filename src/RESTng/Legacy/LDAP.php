<?php

namespace MiamiOH\RESTng\Legacy;

class LDAP
{

    var $server = '';
    var $port = '';
    var $bind_dn = '';
    var $ds;
    var $sr;
    var $default_container = 'ou=people,dc=muohio,dc=edu';
    var $default_attributes = array(
        'uid',
        'cn',
        'mailalternateAddress',
        'ou',
        'sn',
        'givenname',
    );
    var $dn_start = 'uid';
    var $current_entry_count = 0;
    var $current_entry = false;

    var $connected = false;

    var $error_num = 0;
    var $error = '';

    /**
     * @param string $server
     * @param string $port
     */
    function __construct($server = '', $port = '')
    {
        if ($server) {
            $this->connect($server, $port);
        }
    }

    /**
     * @param $server
     * @param string $port
     * @return bool
     */
    function connect($server, $port = '389')
    {
        // If an empty port is given, this hangs.
        if ($port) {
            $this->ds = ldap_connect($server, $port);
        } else {
            $this->ds = ldap_connect($server);
        }
        $this->server = $server;
        $this->port = $port;

        $this->connected = $this->ds === false ? false : true;

        return $this->connected;
    }

    /**
     * @param string $bind_dn
     * @param string $bind_pw
     * @return bool
     */
    function bind($bind_dn = '', $bind_pw = '')
    {
        $success = @ldap_bind($this->ds, $bind_dn, $bind_pw);
        if (!$this->check_error($success)) {
            $this->bind_dn = $bind_dn;
        }

        return $success;
    }

    /**
     * @param string $bind_dn
     * @param string $bind_pw
     * @return bool|int
     */
    function authenticate($bind_dn = '', $bind_pw = '')
    {
        $success = false;

        if ($bind_dn && $bind_pw) {
            if ($this->search($bind_dn) == 1) {
                $entry = $this->next_entry();
                $success = @ldap_bind($this->ds, $entry->dn, $bind_pw);
                $this->check_error($success);
            } else {
                $success = 0;
                $this->error_num = -3;
                $this->error = 'ID not found';
            }
        } else {
            $success = -1;
            $this->error_num = -2;
            $this->error = 'Missing credentials';
        }

        return $success;
    }

    /**
     * @param $dn
     * @param $attribute
     * @param $value
     * @return bool|int|mixed
     */
    function compare($dn, $attribute, $value)
    {
        $success = false;

        if (substr_count($dn, '=') > 0) {
            $success = @ldap_compare($this->ds, $dn, $attribute, $value);
            $this->check_error($success);
        } else {
            $success = -1;
            $this->error_num = -1;
            $this->error = 'Compare requires a full DN';
        }

        return $success;
    }

    /**
     * @param $filter
     * @param string $attributes
     * @param string $start
     * @return bool|int
     */
    function search($filter, $attributes = '', $start = '')
    {
        if (substr_count($filter, '=') == 0) {
            $filter = 'uid=' . $filter;
        }

        if (!$start) {
            $start = $this->default_container;
        }

        if (!is_array($attributes)) {
            $attributes = $this->default_attributes;
        }

        $result = false;
        $this->current_entry = '';
        $this->current_entry_count = 0;
        $this->sr = @ldap_search($this->ds, $start, $filter, $attributes, 0, 0, 30);
        if ($this->sr !== false) {
            $result = $this->count = ldap_count_entries($this->ds, $this->sr);
            // errno=4 is size limit exceeded. this information is still interesting upon success
            $this->error_num = ldap_errno($this->ds);
            $this->error = ldap_error($this->ds);
        } else {
            $this->error_num = ldap_errno($this->ds);
            $this->error = ldap_error($this->ds);
        }

        return $result;
    }

    /**
     * @return bool|LDAP\Entry
     */
    function next_entry()
    {
        if ($this->sr) {
            if ($this->current_entry_count == 0) {
                $this->current_entry = ldap_first_entry($this->ds, $this->sr);
            } else {
                $this->current_entry = ldap_next_entry($this->ds, $this->current_entry);
            }

            if ($this->current_entry !== false) {
                $entry = new LDAP\Entry($this->ds, $this->current_entry);
            } else {
                $entry = false;
            }

            $this->current_entry_count++;
        } else {
            mu_trigger_error('No search results to process');
            exit;
        }

        return $entry;
    }

    /**
     * @return bool
     */
    function disconnect()
    {
        return ldap_unbind($this->ds);
    }

    /**
     * @param $result
     * @return bool
     */
    function check_error($result)
    {
        $had_error = false;

        if ($result === false || $result === -1) {
            $this->error_num = ldap_errno($this->ds);
            $this->error = ldap_error($this->ds);
            $had_error = true;
        } else {
            $this->error_num = 0;
            $this->error = '';
        }

        return $had_error;
    }

}
