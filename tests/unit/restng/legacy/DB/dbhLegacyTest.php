<?php

class dbhLegacyTest extends \PHPUnit\Framework\TestCase {

  private $dbh;

  public function setUp(): void
  {
      parent::setUp();

      $this->dbh = new \MiamiOH\RESTng\Legacy\DB\DBH();
  }

  public function testDbh() {

    $this->assertTrue($this->dbh !== null);

  }

  public function testDbhPerform() {

    $query = 'select count(*) from some_table';
    $result = $this->dbh->perform($query);

    $this->assertEquals($query, $this->dbh->getLastStatementParsed());

  }

  public function testDbhPerformWithParamList() {

    $query = 'select count(*) from some_table where id > ? and type = ?';
    $param1 = 10;
    $param2 = 'blue';

    $result = $this->dbh->perform($query, $param1, $param2);

    $this->assertEquals($query, $this->dbh->getLastStatementParsed());

    $lastParams = $this->dbh->getLastParameters();

    $this->assertEquals(2, count($lastParams));
    $this->assertEquals($param1, $lastParams[0]);
    $this->assertEquals($param2, $lastParams[1]);

  }

  public function testDbhPerformWithParamArray() {

    $query = 'select count(*) from some_table where id > ? and type = ?';
    $params = array(10, 'blue');

    $result = $this->dbh->perform($query, $params);

    $this->assertEquals($query, $this->dbh->getLastStatementParsed());

    $lastParams = $this->dbh->getLastParameters();

    $this->assertEquals(count($params), count($lastParams));
    $this->assertEquals($params[0], $lastParams[0]);
    $this->assertEquals($params[1], $lastParams[1]);

  }

  public function testDbhAutoCommit() {

    $result = $this->dbh->auto_commit(false);

    $this->assertFalse($result);

  }

  public function testDbhAutoCommitReset() {

    $result = $this->dbh->auto_commit(false);

    $this->assertFalse($result);

    $result = $this->dbh->auto_commit(true);

    $this->assertTrue($result);

  }

  public function testDbhAutoCommitInvalid() {

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Value provided for auto_commit must be boolean true or false');

      $result = $this->dbh->auto_commit('false');

    $this->assertFalse($result);

  }

  public function testDbhPlaceHolders() {

    $query = 'select count(*) from some_table where id > ? and type = ?';
    $params = array(10, 'blue');

    $queryExpect = 'select count(*) from some_table where id > 10 and type = \'blue\'';
    $result = $this->dbh->perform($query, $params);

    $this->assertEquals($queryExpect, $this->dbh->getLastStatement());

  }

  public function testDbhPlaceHoldersExtraParams() {

    $query = 'select count(*) from some_table where id > ? and type = ?';
    $params = array(10, 'blue', 'bob');

    $queryExpect = 'select count(*) from some_table where id > 10 and type = \'blue\'';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Placeholder/argument count mismatch: (2/3)');

      $result = $this->dbh->perform($query, $params);

    $this->assertEquals($queryExpect, $this->dbh->getLastStatement());

  }

  public function testDbhPlaceHoldersMissingParams() {

    $query = 'select count(*) from some_table where id > ? and type = ?';
    $params = array(10);

    $queryExpect = 'select count(*) from some_table where id > 10 and type = \'blue\'';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Placeholder/argument count mismatch: (2/1)');

      $result = $this->dbh->perform($query, $params);

    $this->assertEquals($queryExpect, $this->dbh->getLastStatement());

  }

  public function testDbhPlaceHolderTypes() {

    $query = 'select count(*) from some_table where id > ? and type = ? and status = ? and available = ?';
    $params = array(10, 'blue', null, true);

    $queryExpect = 'select count(*) from some_table where id > 10 and type = \'blue\' and status = NULL and available = 1';
    $result = $this->dbh->perform($query, $params);

    $this->assertEquals($queryExpect, $this->dbh->getLastStatement());

  }

}
