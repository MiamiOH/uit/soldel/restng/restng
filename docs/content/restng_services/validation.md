+++
title = "Validation"
weight = 40
+++

Data validation is an important part of any API. Rather than rolling our own validation library, I think it's best to make use of an existing community project. After evaluating the current offers, I'm opting to implement the Respect\Validation library. Documentation can be found at:

https://github.com/Respect/Validation 

The documentation provides an overview of the features and a list of the provided validators. We can also create our validators specific to a project or shared through RESTng.

RESTng provides a factory service which can be injected into your service in order to generate and execute validation rules.

In your resource configuration, define the service with a setter or param dependency of 'APIValidationFactory':

{{< highlight php "linenos=table" >}}
<?php
...
$app->addService(array(
   'name' => 'ServiceName',
   'class' => 'MiamiOH\RESTng\Service\Service\ServiceName',
   'set' => array(
      'validation' => array('name' => 'APIValidationFactory'),
    ));
{{< / highlight >}} 

Within your business service, capture the injected object for later use:

{{< highlight php "linenos=table" >}}
<?php
...
public function setValidation($validation) {
    $this->validation = $validation;
}
{{< / highlight >}} 

The validator factory can be used to create a new instance of a Respect\Validation object for validation. 

{{< highlight php "linenos=table" >}}
<?php
...
$idRule = $this->validator->newValidator()->intVal()->min(1);
$publishDateRule = $this->validator->newValidator()->date('Y-m-d');

$errors = [];

try {
  $idRule->assert($data['id']);
} catch (NestedValidationException $exception) {
  $errors['id'] = $exception->getMessages();
}

try {
  $publishDateRule->assert($data['publishDate']);
} catch (NestedValidationException $exception) {
  $errors['publishDate'] = $exception->getMessages();
}
{{< / highlight >}} 

The above example demonstrates a basic use of validation. You can choose to implement this within the REST resource or within a service class, but you should consider how your service will be used when making this choice.
