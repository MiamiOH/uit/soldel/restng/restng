<?php

class requestTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\Request $request */
    private $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->request = new \MiamiOH\RESTng\Util\Request();

    }

    public function testSetResourceName()
    {

        $this->request->setResourceName('test.resource');

        $this->assertEquals('test.resource', $this->request->getResourceName());

    }

    public function testSetResourceConfig()
    {

        $config = array('name' => 'test.resource', 'action' => 'create');

        $this->request->setResourceConfig($config);

        $this->assertEquals($config['name'], $this->request->getResourceName());
        $this->assertEquals($config, $this->request->getResourceConfig());

    }

    public function testSetResourceConfigRequiresArray()
    {

        $config = '';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Argument for MiamiOH\RESTng\Util\Request::setResourceConfig must be an array');

        $this->request->setResourceConfig($config);

    }

    public function testSetResourceConfigRequiresName()
    {

        $config = array('action' => 'create');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Config argument for MiamiOH\RESTng\Util\Request::setResourceConfig must contain a name element');

        $this->request->setResourceConfig($config);

    }

    public function testSetResourceParams()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'color' => array('description' => 'An color for the test')
            ),
        );

        $params = array('color' => 'blue');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals($params, $this->request->getResourceParams());

    }

    public function testSetResourceParamsRequiresArray()
    {

        $params = '';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Argument for MiamiOH\RESTng\Util\Request::setRouteParams must be an array');

        $this->request->setResourceParams($params);

    }

    public function testSetResourceParam()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'color' => array('description' => 'An color for the test')
            ),
        );

        $params = array('color' => 'blue');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals('blue', $this->request->getResourceParam('color'));

    }

    public function testSetResourceParamMissing()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'color' => array('description' => 'An color for the test')
            ),
        );

        $this->request->setResourceConfig($resourceConfig);
        $params = array('color' => 'blue');

        $this->request->setResourceParams($params);

        $this->assertNull($this->request->getResourceParam('type'));

    }

    public function testSetResourceParamDefault()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'color' => array('description' => 'An color for the test')
            ),
        );

        $params = array('color' => 'blue');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals('new', $this->request->getResourceParam('type', 'new'));

    }

    public function testSetResourceParamAlternateKeyDefault()
    {
        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('alternateKeys' => ['nid', 'uid'], 'description' => 'An id for the test')
            ),
        );

        $params = array('id' => '123');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals($resourceConfig['name'], $this->request->getResourceName());

        $this->assertEquals('123', $this->request->getResourceParam('id'));
        $this->assertEquals('nid', $this->request->getResourceParamKey('id'));

    }

    public function testSetResourceParamAlternateKeyFirst()
    {
        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('alternateKeys' => ['nid', 'uid'], 'description' => 'An id for the test')
            ),
        );

        $params = array('id' => 'nid=123');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals($resourceConfig['name'], $this->request->getResourceName());

        $this->assertEquals('123', $this->request->getResourceParam('id'));
        $this->assertEquals('nid', $this->request->getResourceParamKey('id'));

    }

    public function testSetResourceParamAlternateKeySecond()
    {
        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'returnType' => 'model',
            'params' => array(
                'id' => array('alternateKeys' => ['nid', 'uid'], 'description' => 'An id for the test')
            ),
        );

        $params = array('id' => 'uid=123');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setResourceParams($params);

        $this->assertEquals($resourceConfig['name'], $this->request->getResourceName());

        $this->assertEquals('123', $this->request->getResourceParam('id'));
        $this->assertEquals('uid', $this->request->getResourceParamKey('id'));

    }

    public function testSetDataEmptyScalar()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'dataRepresentation' => 'array',
        );

        $data = '';

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setDataAsArray($data);

        $this->assertTrue(is_array($this->request->getData()));

    }

    public function testSetDataNonEmptyScalar()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'dataRepresentation' => 'array',
        );

        $data = 'test';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Argument for MiamiOH\RESTng\Util\Request::setDataAsArray must be an array');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setDataAsArray($data);
    }

    public function testSetDataArray()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'dataRepresentation' => 'array',
        );

        $data = array('color' => 'blue');

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setDataAsArray($data);

        $this->assertEquals($data, $this->request->getData());

    }

    public function testSetDataArrayEmpty()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'dataRepresentation' => 'array',
        );

        $data = array();

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setDataAsArray($data);

        $this->assertEquals($data, $this->request->getData());

    }

    public function testSetDataFile()
    {

        $resourceConfig = array(
            'name' => 'test.service.id',
            'service' => 'TestService',
            'method' => 'getSomeTestData',
            'pattern' => '/test/service/:id',
            'description' => 'This is a test service',
            'dataRepresentation' => 'file',
        );

        $data = 'This is file content';

        $this->request->setResourceConfig($resourceConfig);
        $this->request->setDataAsFile($data);

        $this->assertEquals($data, $this->request->getDataFile());

    }

    public function testSetCallback()
    {

        $callback = 'myfunction';

        $this->request->setCallback($callback);

        $this->assertEquals($callback, $this->request->getCallback());

    }

}
