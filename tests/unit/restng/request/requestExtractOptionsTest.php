<?php

class requestExtractOptionsTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $request;

  public function setUp(): void
  {
    parent::setUp();

    $this->request = new \MiamiOH\RESTng\Util\Request();

  }

  public function testExtractRequestOptionsReserved() {

    $options = array(
        'type' => 'new',
        'color' => 'blue',
        'limit' => '10',
        'offset' => '20',
        'callback' => 'functionName',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => false,
        'isComposable' => false,
        'options' => array(
            'color' => array(
                'type' => 'single',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();

    $this->assertTrue(!isset($setOptions['limit']));
    $this->assertTrue(!isset($setOptions['offset']));
    $this->assertTrue(!isset($setOptions['callback']));
    $this->assertEquals($options['type'], $setOptions['type']);
    $this->assertEquals($options['color'], $setOptions['color']);
    $this->assertEquals($options['limit'], $this->request->getLimit());
    $this->assertEquals($options['offset'], $this->request->getOffset());
    $this->assertEquals($options['callback'], $this->request->getCallback());

    $this->assertFalse($this->request->isPartial());
    $this->assertTrue($this->request->isPaged());

  }

  public function testExtractRequestOptionsFields() {

    $options = array(
        'type' => 'new',
        'color' => 'blue',
        'fields' => 'name,type,color',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => true,
        'isComposable' => false,
        'options' => array(
            'color' => array(
                'type' => 'single',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          ),
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setFields));
    $this->assertEquals(3, count($setFields));
    $this->assertTrue(in_array('name', $setFields));
    $this->assertTrue(in_array('type', $setFields));
    $this->assertTrue(in_array('color', $setFields));
    $this->assertEquals($options['type'], $setOptions['type']);
    $this->assertEquals($options['color'], $setOptions['color']);

    $this->assertTrue($this->request->isPartial());
    $this->assertFalse($this->request->isPaged());
    $this->assertEquals($options['fields'], $this->request->getPartialFieldsSpec());

  }

  public function testExtractRequestOptionsSubObject() {

    $options = array(
        'type' => 'new',
        'color' => 'blue',
        'fields' => 'owner(id,firstname,lastname)',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => true,
        'isComposable' => false,
        'options' => array(
            'color' => array(
                'type' => 'single',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          ),
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setFields));
    $this->assertEquals(0, count($setFields));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(1, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));
    $this->assertEquals(3, count($setSubObjects['owner']));
    $this->assertTrue(in_array('id', $setSubObjects['owner']));
    $this->assertTrue(in_array('firstname', $setSubObjects['owner']));
    $this->assertTrue(in_array('lastname', $setSubObjects['owner']));
    $this->assertEquals($options['type'], $setOptions['type']);
    $this->assertEquals($options['color'], $setOptions['color']);

    $this->assertFalse($this->request->isPartial());
    $this->assertFalse($this->request->isPaged());
    $this->assertEquals($options['fields'], $this->request->getPartialFieldsSpec());

  }

  public function testExtractRequestOptionsComposed() {

    $options = array(
        'compose' => 'owner',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => false,
        'isComposable' => true,
      );

    $this->request->setResourceConfig($config);

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(1, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));

  }

  public function testExtractRequestOptionsComposedWithFields() {

    $options = array(
        'compose' => 'owner(id,firstname,lastname)',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => false,
        'isComposable' => true,
      );

    $this->request->setResourceConfig($config);

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(1, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));
    $this->assertEquals(3, count($setSubObjects['owner']));
    $this->assertTrue(in_array('id', $setSubObjects['owner']));
    $this->assertTrue(in_array('firstname', $setSubObjects['owner']));
    $this->assertTrue(in_array('lastname', $setSubObjects['owner']));

  }

  public function testExtractRequestOptionsComposedMultiple() {

    $options = array(
        'compose' => 'owner(id,firstname,lastname),location,agent(id,name,phone)',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => false,
        'isComposable' => true,
      );

    $this->request->setResourceConfig($config);

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(3, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));
    $this->assertEquals(3, count($setSubObjects['owner']));
    $this->assertTrue(in_array('id', $setSubObjects['owner']));
    $this->assertTrue(in_array('firstname', $setSubObjects['owner']));
    $this->assertTrue(in_array('lastname', $setSubObjects['owner']));
    $this->assertTrue(array_key_exists('location', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['location']));
    $this->assertTrue(array_key_exists('agent', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['agent']));
    $this->assertEquals(3, count($setSubObjects['agent']));
    $this->assertTrue(in_array('id', $setSubObjects['agent']));
    $this->assertTrue(in_array('name', $setSubObjects['agent']));
    $this->assertTrue(in_array('phone', $setSubObjects['agent']));

  }

  public function testExtractRequestOptionsComposedAndSubObjectsFails() {

    $options = array(
        'compose' => 'owner(id,firstname,lastname),location,agent(id,name,phone)',
        'fields' => 'owner(id,firstname,lastname)',
    );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => true,
        'isComposable' => true,
    );

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Requesting subobjects via fields and compose concurrently is not allowed');

      $this->request->setResourceConfig($config);

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(3, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));
    $this->assertEquals(3, count($setSubObjects['owner']));
    $this->assertTrue(in_array('id', $setSubObjects['owner']));
    $this->assertTrue(in_array('firstname', $setSubObjects['owner']));
    $this->assertTrue(in_array('lastname', $setSubObjects['owner']));
    $this->assertTrue(array_key_exists('location', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['location']));
    $this->assertTrue(array_key_exists('agent', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['agent']));
    $this->assertEquals(3, count($setSubObjects['agent']));
    $this->assertTrue(in_array('id', $setSubObjects['agent']));
    $this->assertTrue(in_array('name', $setSubObjects['agent']));
    $this->assertTrue(in_array('phone', $setSubObjects['agent']));

  }

  public function testExtractRequestOptionsFieldsAndSubObjects() {

    $options = array(
        'type' => 'new',
        'color' => 'blue',
        'fields' => 'name,type,color,owner(id,firstname,lastname),cost,location(building,floor,slot),age',
      );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => false,
        'isPartialable' => true,
        'isComposable' => false,
        'options' => array(
            'color' => array(
                'type' => 'single',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          ),
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $setOptions = $this->request->getOptions();
    $setFields = $this->request->getPartialRequestFields();
    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(!isset($setOptions['fields']));
    $this->assertTrue(is_array($setFields));
    $this->assertEquals(5, count($setFields));
    $this->assertTrue(in_array('name', $setFields));
    $this->assertTrue(in_array('type', $setFields));
    $this->assertTrue(in_array('color', $setFields));
    $this->assertTrue(in_array('cost', $setFields));
    $this->assertTrue(in_array('age', $setFields));
    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(2, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists('owner', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['owner']));
    $this->assertEquals(3, count($setSubObjects['owner']));
    $this->assertTrue(in_array('id', $setSubObjects['owner']));
    $this->assertTrue(in_array('firstname', $setSubObjects['owner']));
    $this->assertTrue(in_array('lastname', $setSubObjects['owner']));
    $this->assertTrue(array_key_exists('location', $setSubObjects));
    $this->assertTrue(is_array($setSubObjects['location']));
    $this->assertEquals(3, count($setSubObjects['location']));
    $this->assertTrue(in_array('building', $setSubObjects['location']));
    $this->assertTrue(in_array('floor', $setSubObjects['location']));
    $this->assertTrue(in_array('slot', $setSubObjects['location']));
    $this->assertEquals($options['type'], $setOptions['type']);
    $this->assertEquals($options['color'], $setOptions['color']);

    $this->assertTrue($this->request->isPartial());
    $this->assertFalse($this->request->isPaged());
    $this->assertEquals($options['fields'], $this->request->getPartialFieldsSpec());

  }

  public function testSetPartialFieldsRequiresArray() {

    $fields = '';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Fields must be provided as an array when adding a subObject to a request');

      $this->request->setPartialFields($fields);
  }

  public function testAddSubObject() {

    $name = 'owner';
    $fields = array('id', 'firstname', 'lastname');

    $this->request->addSubObject($name, $fields);

    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(1, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists($name, $setSubObjects));
    $this->assertTrue(is_array($setSubObjects[$name]));
    $this->assertEquals(3, count($setSubObjects[$name]));
    $this->assertTrue(in_array('id', $setSubObjects[$name]));
    $this->assertTrue(in_array('firstname', $setSubObjects[$name]));
    $this->assertTrue(in_array('lastname', $setSubObjects[$name]));

  }

  public function testAddSubObjects() {

    $name1 = 'owner';
    $fields = array('id', 'firstname', 'lastname');

    $name2 = 'location';
    $fields2 = array('building', 'floor', 'slot');

    $this->request->addSubObject($name1, $fields);
    $this->request->addSubObject($name2, $fields2);

    $setSubObjects = $this->request->getSubObjects();

    $this->assertTrue(is_array($setSubObjects));
    $this->assertEquals(2, count(array_keys($setSubObjects)));
    $this->assertTrue(array_key_exists($name1, $setSubObjects));
    $this->assertTrue(is_array($setSubObjects[$name1]));
    $this->assertEquals(3, count($setSubObjects[$name1]));
    $this->assertTrue(in_array('id', $setSubObjects[$name1]));
    $this->assertTrue(in_array('firstname', $setSubObjects[$name1]));
    $this->assertTrue(in_array('lastname', $setSubObjects[$name1]));
    $this->assertTrue(array_key_exists($name2, $setSubObjects));
    $this->assertTrue(is_array($setSubObjects[$name2]));
    $this->assertEquals(3, count($setSubObjects[$name2]));
    $this->assertTrue(in_array('building', $setSubObjects[$name2]));
    $this->assertTrue(in_array('floor', $setSubObjects[$name2]));
    $this->assertTrue(in_array('slot', $setSubObjects[$name2]));

  }

  public function testAddSubObjectRequiresName() {

    $name = '';
    $fields = array('id', 'firstname', 'lastname');

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Name is required to add a subObject to a request');

      $this->request->addSubObject($name, $fields);

  }

  public function testAddSubObjectRequiresArray() {

    $name = 'owner';
    $fields = '';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Fields must be provided as an array when adding a subObject to a request');

      $this->request->addSubObject($name, $fields);

  }

  public function testSetLimitRequiresNumeric() {

    $limit = 'a';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Limit argument must be a numeric value');

      $this->request->setLimit($limit);

  }

  public function testSetLimitRequiresInteger() {

    $limit = '1.5';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Limit argument must be an integer value 1.5');

      $this->request->setLimit($limit);

  }

  public function testSetLimitRequiresGreaterThan0() {

    $limit = '-1';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Limit argument must be 1 or higher');

      $this->request->setLimit($limit);

  }



  public function testSetOffsetRequiresNumeric() {

    $offset = 'a';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Offset argument must be a numeric value');

      $this->request->setOffset($offset);

  }

  public function testSetOffsetRequiresInteger() {

    $offset = '1.5';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Offset argument must be an integer');

      $this->request->setOffset($offset);

  }

  public function testSetOffsetRequiresGreaterThan0() {

    $offset = '-1';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Offset argument must be 1 or higher');

      $this->request->setOffset($offset);

  }

  public function testExtractRequestOptionsPageableNoDefaultLimit() {

    $options = array();

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => true,
        'isPartialable' => false,
        'isComposable' => false,
    );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $this->assertEquals(\MiamiOH\RESTng\App::API_DEFAULTPAGELIMIT, $this->request->getLimit());

    $this->assertFalse($this->request->isPaged());

  }

  public function testExtractRequestOptionsPageableDefaultLimit() {

    $options = array();

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => true,
        'isPartialable' => false,
        'isComposable' => false,
        'defaultPageLimit' => 20,
    );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

    $this->request->extractRequestOptions($options);

    $this->assertEquals($config['defaultPageLimit'], $this->request->getLimit());

    $this->assertTrue($this->request->isPaged());

  }

  public function testExtractRequestOptionsPageableMaxLimit() {

    $options = array(
      'limit' => 1000,
    );

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'isPageable' => true,
        'isPartialable' => false,
        'isComposable' => false,
        'maxPageLimit' => 100,
        'defaultPageLimit' => 20,
    );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $this->request->setResourceName('test.resource');

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Max page limit for resource is 100');

      $this->request->extractRequestOptions($options);

  }

}
