<?php

header('Content-Type', 'application/json');
header("Access-Control-Allow-Origin: *");

if (file_exists('../../config/autoloaders.yaml')) {
    $autoloaders = yaml_parse_file('../../config/autoloaders.yaml');
    if (!isset($autoloaders['autoload'])) {
        throw new \Exception('Missing "autoload" key in autoloaders.yaml');
    }
    foreach ($autoloaders['autoload'] as $autoloader) {
        require($autoloader);
    }
}

require('../../vendor/autoload.php');

$app = require('../../bootstrap.php');

ini_set( "display_errors", "on" );
error_reporting( E_ALL | E_STRICT );

$swagger = [];

$swagger['info'] = [
    'contact' => [
        'email' => 'tepeds@miamioh.edu'
    ],
    'description' => 'RESTng 2 installed resources',
    'version' => '0.0.1',
];

$swagger['consumes'] = [ 'application/json' ];
$swagger['provides'] = [ 'application/json' ];
$swagger['swagger'] = '2.0';
$swagger['basePath'] = str_replace('/swagger/swagger.php', '', $_SERVER['REQUEST_URI']);
$swagger['host'] = $_SERVER['HTTP_HOST'];
$swagger['schemes'] = ['http', 'https'];

$swagger['securityDefinitions'] = [
    'ws_token' => [
        'in' => 'header',
        'type' => 'apiKey',
        'name' => 'token',
        #'description' => 'Miami Web Services authentication token'
    ],
    'restng_oauth' => [
        'authorizationUrl' => 'http://somewhere.com',
        'flow' => 'implicit',
        'scopes' => [
            'read:data' => 'read some data',
            'write:data' => 'write some data',
        ],
        'type' => 'oauth2'
    ]
];

//$swagger['responses'] = [
//    '200' => [
//        'description' => 'The operation was successful',
//    ],
//];

$response = $app->callResource('api.tag');
$tags = $response->getPayload();

$swagger['tags'] = [];
foreach ($tags as $tagName => $tag)
{
    $swagger['tags'][] = [
        'name' => $tag['name'],
        'description' => $tag['description'],
    ];
}

$response = $app->callResource('api.definition');
$definitions = $response->getPayload();

$swagger['definitions'] = [];
foreach ($definitions as $definitionName => $definition)
{
    $swagger['definitions'][$definitionName] = $definition;
}

$response = $app->callResource('api.resource');
$resources = $response->getPayload();

$methodMap = [
    'read' => 'get',
    'create' => 'post',
    'update' => 'put',
    'delete' => 'delete',
];

$paths = [];

foreach ($resources as $resource) {
    $name = $resource['pattern'];
//    if (strpos($name, ':') !== false) {
//        print "<pre>";
//        print_r($resource);
//        exit;
//    }

    $name = preg_replace('/:(\w+)/', '{$1}', $name);

    if (!isset($paths[$name])) {
        $paths[$name] = [];
    }

    $method = $methodMap[$resource['action']];

    $path = [];
    $path['description'] = $resource['description'];
    if (isset($resource['name'])) {
        $path['description'] .= "\n\n**Name: " . $resource['name'] . "**";
    }
    $path['summary'] = isset($resource['summary']) ? $resource['summary'] : $resource['description'];

    // Tag the path with the first part of the resource name
    if (isset($resource['tags']))
    {
        $path['tags'] = $resource['tags'];
    }
    else
    {
        $tag = substr_replace($resource['name'], '', strpos($resource['name'], '.'));
        $path['tags'] = [$tag];
    }

    $path['parameters'] = [];
    foreach (array_keys($resource['params']) as $paramName)
    {
        $param = [];
        $param['in'] = 'path';
        $param['required'] = 'true';
        $param['type'] = 'string';
        $param['name'] = $paramName;
        $param['description'] = $resource['params'][$paramName]['description'];

        if (isset($resource['params'][$paramName]['alternateKeys'])) {
            $param['description'] .= "\n\n" . 'Default key: ' . $resource['params'][$paramName]['alternateKeys'][0];
            $param['description'] .= "\n\n" . 'Allowed alternate keys: ' .
                implode(', ', $resource['params'][$paramName]['alternateKeys']);
        }

        $path['parameters'][] = $param;
    }

    foreach (array_keys($resource['options']) as $paramName)
    {
        $param = [];
        $param['in'] = 'query';
        $param['required'] = $resource['options'][$paramName]['required'] ? true : false;
        $param['type'] = $resource['options'][$paramName]['type'] === 'list' ? 'array' : 'string';
        $param['name'] = $paramName;
        $param['description'] = $resource['options'][$paramName]['description'];

        if (isset($resource['options'][$paramName]['default']))
        {
            $param['default'] = $resource['options'][$paramName]['default'];
        }

        if (isset($resource['options'][$paramName]['enum']))
        {
            $param['enum'] = $resource['options'][$paramName]['enum'];
        }

        if ($param['type'] === 'array')
        {
            $param['items'] = ['type' => 'string'];
        }

        $path['parameters'][] = $param;
    }

    if (isset($resource['body']))
    {
        $param = [
            'in' => 'body',
            'name' => 'body',
        ];

        $param['description'] = $resource['body']['description'];
        $param['required'] = $resource['body']['required'];
        $param['schema'] = $resource['body']['schema'];

        $path['parameters'][] = $param;

    }

    if ($resource['isPageable'])
    {

        $limitDescription = 'The number of objects to return';

        if (isset($resource['defaultPageLimit'])) {
            $limitDescription .= "\n\nDefault Page Limit: " . $resource['defaultPageLimit'];
        }

        if (isset($resource['maxPageLimit'])) {
            $limitDescription .= "\n\nMaximum Page Limit: " . $resource['maxPageLimit'];
        }

        $path['parameters'][] = [
            'name' => 'limit',
            'in' => 'query',
            'description' => $limitDescription,
            'type' => 'integer'
        ];

        $path['parameters'][] = [
            'name' => 'offset',
            'in' => 'query',
            'description' => 'The number of the first object to return',
            'type' => 'integer'
        ];

    }

    if ($resource['isPartialable'])
    {
        $fields = [];

        if (isset($resource['responses']) && isset($resource['responses'][\MiamiOH\RESTng\App::API_OK])
            && isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']))
        {
            $object = [];

            if (isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']))
            {
                if (isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['type'])
                    && $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['type'] === 'model')
                {
                    $ref = str_replace('#/definitions/', '', $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']);
                    if (isset($definitions[$ref]['properties'])) {
                        $object = $definitions[$ref]['properties'];
                    }
                }
                else
                {
                    $ref = str_replace('#/definitions/', '', $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']);
                    $ref = str_replace('#/definitions/', '', $definitions[$ref]['items']['$ref']);
                    if (isset($definitions[$ref]['properties'])) {
                        $object = $definitions[$ref]['properties'];
                    }
                }
            }
            else
            {
                $object = $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['properties'];
            }

            foreach (array_keys($object) as $field)
            {
                if (isset($object[$field]['$ref']))
                {
                    $fields[] = $field . '()';
                }
                else
                {
                    $fields[] = $field;
                }
            }
        }

        $path['parameters'][] = [
            'name' => 'fields',
            'in' => 'query',
            'description' => 'List of object fields and/or subobjects to include',
            'type' => 'array',
            'items' => array(
                'type' => 'string',
            ),
            'enum' => $fields,
        ];
    }

    if ($resource['isComposable'])
    {
        $subObjects = [];

        if (isset($resource['responses']) && isset($resource['responses'][\MiamiOH\RESTng\App::API_OK])
            && isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']))
        {
            $object = [];

            if (isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']))
            {
                if (isset($resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['type'])
                    && $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['type'] === 'model')
                {
                    $ref = str_replace('#/definitions/', '', $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']);
                    if (isset($definitions[$ref]['properties'])) {
                        $object = $definitions[$ref]['properties'];
                    }
                }
                else
                {
                    $ref = str_replace('#/definitions/', '', $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['$ref']);
                    $ref = str_replace('#/definitions/', '', $definitions[$ref]['items']['$ref']);
                    if (isset($definitions[$ref]['properties'])) {
                        $object = $definitions[$ref]['properties'];
                    }
                }
            }
            else
            {
                $object = $resource['responses'][\MiamiOH\RESTng\App::API_OK]['returns']['properties'];
            }

            foreach (array_keys($object) as $subObject)
            {
                if (isset($object[$subObject]['$ref']))
                {
                    $subObjects[] = $subObject;
                }
            }
        }

        $path['parameters'][] = [
            'name' => 'compose',
            'in' => 'query',
            'description' => 'List of subobjects to include',
            'type' => 'array',
            'items' => array(
                'type' => 'string',
            ),
            'enum' => $subObjects,
        ];
    }

    if (isset($resource['middleware']['authenticate']))
    {
        $path['security'] = [];
        $path['security'][] = [
            'ws_token' => [],
        ];
        
        /*
         * Swagger-UI does not currently display apikey auth info. For now
         * we can expose the token query param to allow testing.
         */
        $path['parameters'][] = [
            'name' => 'token',
            'in' => 'query',
            'description' => 'A Miami WS token',
            'type' => 'string'
        ];
    }

    if (isset($resource['responses']))
    {
        //$path['responses'] = $resource['responses'];
        $path['responses'] = [];
        foreach ($resource['responses'] as $status => $config)
        {
            $path['responses'][$status] = [
                'description' => $config['description'],
                'schema' => getRESTngResponseSchema(isset($config['returns']) ? $config['returns'] : null),
                ];
        }
    }

    $paths[$name][$method] = $path;

}

$swagger['paths'] = $paths;

#print "<pre>";
#print_r($swagger);
#exit;

print json_encode($swagger, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

function getRESTngResponseSchema($data) {
    $restngResponseTemplate = [
        'type' => 'object',
        'properties' => [
            'status' => [
                'type' => 'integer',
            ],
            'error' => [
                'type' => 'string',
            ],
        ]
    ];

    if (!is_null($data) && is_array($data))
    {
        $restngResponseTemplate['properties']['data'] = $data;
    }

    return $restngResponseTemplate;
}
