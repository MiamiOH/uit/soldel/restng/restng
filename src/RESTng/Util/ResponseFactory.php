<?php

namespace MiamiOH\RESTng\Util;

class ResponseFactory
{
    /**
     * @return Response
     */
    public function newResponse()
    {
        return new Response();
    }
}
