<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-02-11
 * Time: 20:28
 */

namespace Tests\unit\restng\service\Apm;

use MiamiOH\RESTng\Service\Apm\Span;
use Nipwaayoni\Agent;
use Nipwaayoni\Events\Transaction;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\UnitTestCase;

class SpanUnitTest extends UnitTestCase
{

    /** @var Transaction|MockObject  */
    private $parent;

    public function setUp(): void
    {
        parent::setUp();

        $this->parent = $this->createMock(Transaction::class);
    }

    public function testCanBeCreatedWithName(): void
    {
        $this->parent->expects($this->once())->method('getId')
            ->willReturn('123');
        $this->parent->expects($this->once())->method('getTraceId')
            ->willReturn('abc');

        $span = new Span('Span name', $this->parent);

        $this->assertEquals('Span name', $span->name());
    }

}
