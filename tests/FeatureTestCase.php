<?php
namespace Tests;

use Helmich\JsonAssert\JsonAssertions;

abstract class FeatureTestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    use JsonAssertions;
}
