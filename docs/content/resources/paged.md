+++
title = "Paged"
weight = 90
+++

Collections can often contain far more models than a consumer wishes to process at one time. Using paging allows the consumer to request records in defined groups from the resource. This has the added advantage of allowing a well designed service to return the desired sub-set of records much faster.

You declare your resource as being pageable using the **isPageable** configuration:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'name' => 'citest.record.all',
    'description' => 'A pageable collection of test records. No params or options.',
    'tags' => array('CITest'),
    'pattern' => '/citest/record',
    'service' => 'CITest',
    'method' => 'getExample',
    'isPageable' => true,
));
{{< / highlight >}} 

When isPageable is true, RESTng will look for an incoming offset and limit in the options and make the values available to your in the request object. RESTng will also reflect this capability in the generated Swagger Spec file.

It is up to your service to correctly implement the fetching of the correct models for the requested page. Please see the documentation for developing a business service for details.

## Forcing a Resource to be Paged

Simply declaring a resource as pageable is not enough to alter the RESTng response. If the consumer does not provide a limit or offset, the response will be a normal response with no page metadata. If the use of your resource implies some practical limits on the amount of data to be returned, you may force the paging behavior by including a 'defaultPageLimit' configuration attribute:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    ...
    'isPageable' => true,
    'defaultPageLimit' => 200,
));
{{< / highlight >}} 

In this example, if the consumer does not supply an explicit limit, a limit of 200 will be used and the response will be a normal RESTng paged response.

## Setting a Maximum Page Limit

Resources capable of returning very large or compute intensive collections can choose to set a maximum limit. RESTng will throw a \MiamiOH\RESTng\Exceptions\BadRequest exception if the requested limit exceeds the maximum.

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    ...
    'isPageable' => true,
    'defaultPageLimit' => 200,
    'maxPageLimit' => 1000,
));
{{< / highlight >}} 
