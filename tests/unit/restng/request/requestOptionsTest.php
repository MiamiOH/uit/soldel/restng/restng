<?php

class requestOptionsTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $request;

  public function setUp(): void
  {
    parent::setUp();

    $this->request = new \MiamiOH\RESTng\Util\Request();

  }

  public function testSetOptionsRequiresArray() {

    $options = '';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Argument for MiamiOH\RESTng\Util\Request::setOptions must be an array');

      $this->request->setOptions($options);

  }

  public function testSetOptions() {

    $options = array();

    $this->request->setOptions($options);

    $this->assertEquals($options, $this->request->getOptions());

  }

  public function testSetOptionsReservedOffset() {

    $options = array('offset' => 10);

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Attempt to use reserved option key offset for resource');

      $this->request->setOptions($options);

  }

  public function testSetOptionsReservedLimit() {

    $options = array('limit' => 10);

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Attempt to use reserved option key limit for resource');

      $this->request->setOptions($options);

  }

  public function testSetOptionsReservedFields() {

    $options = array('fields' => array('test1', 'test2'));

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Attempt to use reserved option key fields for resource');

      $this->request->setOptions($options);

  }

  public function testSetOptionsUnknownKey() {

    $options = array('color' => 'blue');

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('No defined option color for resource');

      $this->request->setOptions($options);

  }

  public function testSetOptionsWithResource() {

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'options' => array(
            'color' => array(
                'type' => 'single',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => false,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $options = array('color' => 'blue', 'type' => 'new');

    $this->request->setOptions($options);

    $optionResp = $this->request->getOptions();

    $this->assertEquals(2, count($optionResp));
    $this->assertEquals('blue', $optionResp['color']);
    $this->assertEquals('new', $optionResp['type']);

  }

  public function testSetOptionsWithResourceList() {

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'options' => array(
            'color' => array(
                'type' => 'list',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => false,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $options = array('color' => 'blue,red', 'type' => 'new');

    $this->request->setOptions($options);

    $optionResp = $this->request->getOptions();

    $this->assertEquals(2, count($optionResp));
    $this->assertTrue(is_array($optionResp['color']));
    $this->assertEquals(2, count($optionResp['color']));
    $this->assertTrue(in_array('blue', $optionResp['color']));
    $this->assertTrue(in_array('red', $optionResp['color']));
    $this->assertEquals('new', $optionResp['type']);

  }

  public function testSetOptionsWithResourceListOneItem() {

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'options' => array(
            'color' => array(
                'type' => 'list',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => false,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $options = array('color' => 'blue', 'type' => 'new');

    $this->request->setOptions($options);

    $optionResp = $this->request->getOptions();

    $this->assertEquals(2, count($optionResp));
    $this->assertTrue(is_array($optionResp['color']));
    $this->assertEquals(1, count($optionResp['color']));
    $this->assertTrue(in_array('blue', $optionResp['color']));
    $this->assertEquals('new', $optionResp['type']);

  }

  public function testSetOptionsWithResourceRequired() {

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'options' => array(
            'color' => array(
                'type' => 'list',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $options = array('color' => 'blue', 'type' => 'new');

    $this->request->setOptions($options);

    $optionResp = $this->request->getOptions();

    $this->assertEquals(2, count($optionResp));
    $this->assertTrue(is_array($optionResp['color']));
    $this->assertEquals(1, count($optionResp['color']));
    $this->assertTrue(in_array('blue', $optionResp['color']));
    $this->assertEquals('new', $optionResp['type']);

  }

  public function testSetOptionsWithResourceRequiredMissing() {

    $config = array(
        'name' => 'test.resource',
        'action' => 'read',
        'options' => array(
            'color' => array(
                'type' => 'list',
                'required' => false,
              ),
            'type' => array(
                'type' => 'single',
                'required' => true,
              ),
          )
      );

    $this->request->setResourceConfig($config);

    $this->assertEquals($config['name'], $this->request->getResourceName());
    $this->assertEquals($config, $this->request->getResourceConfig());

    $options = array('color' => 'blue');

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('Missing required option type for resource test.resource');

      $this->request->setOptions($options);

  }

}
