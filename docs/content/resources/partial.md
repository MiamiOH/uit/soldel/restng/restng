+++
title = "Partial & Composed"
weight = 80
+++

Some models may have more properties than are needed in every response or some may have relationships with other models of interest to the consumer. RESTng provides a mechanism for a consumer to request two types of transformation of the returned data model. The consumer may specify a **partial** model with only the listed properties or may ask that the model include, or be **composed** of, other related objects. Your resource may combine partial and composed.

Your resource must correctly implement the logic to support partial or composed models. Please see the documentation for developing a business service for details.

## Partial Responses

You can declare that the resource may return a partial model by setting the isPartialable config option:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'description' => 'A specific weekday.',
    'name' => 'WeekDay.day',
    'pattern' => '/WeekDay/day',
    'service' => 'WeekDayService',
    'method' => 'getWeekDay',
    'isPartialable' => true
));
{{< / highlight >}} 

Setting isPartialable to true will cause RESTng to process the fields option and make the field list available in the request object. RESTng will also reflect this capability in the Swagger Spec file produced for resources.

Only attributes on main model are subject to the partial capability.

## Composed Responses

You can declare that the resource may compose other models by setting the isComposable config option:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'description' => 'A specific weekday.',
    'name' => 'WeekDay.day',
    'pattern' => '/WeekDay/day',
    'service' => 'WeekDayService',
    'method' => 'getWeekDay',
    'isComposable' => true
));
{{< / highlight >}} 

Setting isComposable to true will cause RESTng to process the compose option and make the subobject list available in the request object. RESTng will also reflect this capability in the Swagger Spec file produced for resources.
