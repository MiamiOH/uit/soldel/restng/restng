+++
title = "CORS"
weight = 120
+++

CORS, or Cross-Origin Resource Sharing, is a standard method for browser based clients (JavaScript/XHR) to access resources in domains other than the domain they are served from (cross-origin).  CORS has been incorporated into RESTng and can be configured during the resource registration step.

The default security posture is open.  RESTng will allow a consumer from any domain to consume services using any of the normal REST HTTP methods.  You must apply restrictions as necessary.

You supply CORS configuration values using the ‘xOrgSec’ key in the configuration array of the addResource method.  For example:

{{< highlight php "linenos=table" >}}
<?php
...
'xOrgSec' => array(
    'allow-origin' => 'any', 
        // 'any', or array of domains, default is any
    'allow-credentials' => true, 
        // true or false, default is true
    'max-age' => 1, 
        // int, seconds to cache pre-flight, 1 hour by default
    'allow-methods' => array('GET', 'POST', 'PUT', 
                               'DELETE', 'OPTIONS'), 
        // array of HTTP methods, 
        // empty array means none, 
        // default is common REST
    'allow-headers' => null, 
        // array of header names, empty array means none, 
        // null means any requested by client, default is null
)
{{< / highlight >}} 

Refer to CORS configuration help available on the internet for details if necessary.