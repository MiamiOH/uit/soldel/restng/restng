<?php

namespace MiamiOH\RESTng\Exception;

class TransactionSpanExistsException extends ApmException
{
}
