<?php

namespace Tests\Service\CITest;

class CITest extends \MiamiOH\RESTng\Service
{

    public function getReflectedResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($options);

        return $response;
    }

    public function getReflectedParameterResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $parameters = $request->getResourceParams();

        switch ($request->getResourceParamKey('id')) {
            case 'sid':
                $parameters['key'] = 'sid';
                break;
            case 'uid':
                $parameters['key'] = 'uid';
                break;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($parameters);

        return $response;
    }

    public function getReflectedResponseBody()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($data);

        return $response;
    }

    public function getReflectedPartialResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = array();
        $data['options'] = $request->getOptions();
        $data['isPartial'] = $request->isPartial();
        $data['partialRequestFields'] = $request->getPartialRequestFields();
        $data['subObjects'] = $request->getSubObjects();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($data);

        return $response;
    }

    public function getReflectedComposeResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = array();
        $data['options'] = $request->getOptions();
        $data['isPartial'] = $request->isPartial();
        $data['partialRequestFields'] = $request->getPartialRequestFields();
        $data['isComposed'] = $request->isComposed();
        $data['composedObjects'] = $request->getSubObjects();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($data);

        return $response;
    }

    public function getReflectedSubObjectResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getSubObjects();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($data);

        return $response;
    }

    public function getReflectedPartialCombinedResponse()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getSubObjects();
        $data['partialRequestFields'] = $request->getPartialRequestFields();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($data);

        return $response;
    }

    public function addDeferredCall()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $params = array(
            'params' => ['id' => 1]
        );

        $defer = $this->callResource('citest.deferred.create.record', $params, ['deferred' => true]);

        $response->setStatus($defer->getStatus());
        $response->setPayload($defer->getPayload());

        return $response;
    }

    // This exists to be called by addDeferredCall()
    public function runDeferredCall()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        return $response;
    }

    public function getLocationForResource()
    {
      $request = $this->getRequest();
      $response = $this->getResponse();

      $options = $request->getOptions();

      $resources = $options['resource'] ?? ['time.get'];

      $data = [];

      foreach ($resources as $resource) {
        $data[$resource] = $this->locationFor($resource);
      }

      $response->setStatus(\MiamiOH\RESTng\App::API_OK);
      $response->setPayload($data);

      return $response;
    }
}
