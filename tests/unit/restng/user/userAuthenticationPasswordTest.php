<?php

class userAuthenticationPasswordTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\User */
    private $user;

    /** @var \MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->credentialValidator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->user = new \MiamiOH\RESTng\Util\User();

        $this->user->setCredentialValidator($this->credentialValidator);

    }

    public function testCanAddUserByCredentials() {
        $this->credentialValidator->method('validateUsernamePassword')
            ->willReturn(['valid' => true, 'token' => 'abc123', 'username' => 'doej']);

        $credentials = array(
            'username' => 'doej',
            'password' => 'secret',
        );

        $result = $this->user->addUserByCredentials($credentials);

        // result is false on failure or user key on success
        $this->assertTrue($result !== false);
        $this->assertTrue($this->user->isAuthenticated());
        $this->assertEquals($credentials['username'], $this->user->getUsername());

    }

    public function testDoesNotAddUserWhenCredentialsFail() {
        $this->credentialValidator->method('validateUsernamePassword')
            ->willReturn(['valid' => false, 'token' => null, 'username' => null]);

        $credentials = array(
            'username' => 'doej',
            'password' => 'secret',
        );

        $result = $this->user->addUserByCredentials($credentials);

        $this->assertFalse($result);
        $this->assertFalse($this->user->isAuthenticated());
        $this->assertEmpty($this->user->getUsername());

    }

}
