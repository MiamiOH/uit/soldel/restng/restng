+++
title = "Datasources"
weight = 40
+++

Credentials and other connection information is given to RESTng through datasources. Similar to Laravel's `.env` file, the contained credentials are stored in plain text. Therefore, the datasource file must be treated with due caution.

The datasource configuration is found at:

```bash
config/datasources.yaml
```

The contents of the file are a hash where the key is the name of the datasource and the value is a hash of the required keys. All datasource entries have the same keys, though their specific use may be determined by the `type`.

```yaml
---
MyDatabase:
  type: OCI8
  user: my_db_user
  password: sekret
  database: MYDB
  port:
  connect_type:
  host:
```
