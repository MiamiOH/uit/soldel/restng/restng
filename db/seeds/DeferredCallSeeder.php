<?php


use Carbon\Carbon;
use MiamiOH\RESTng\Util\DeferredCall;
use Phinx\Seed\AbstractSeed;

class DeferredCallSeeder extends AbstractSeed
{
    private $nextId = 1;

    public function run()
    {
        $data = $this->makeCallRecordData();

        $table = $this->table('deferred_calls');

        $table->insert($data)->saveData();
    }

    protected function makeCallRecordData(): array
    {
        $futureTime = Carbon::now()->addMinutes(5);
        $oldestTime = Carbon::now()->subHours(3);
        $midTime = Carbon::now()->subHours(2);
        $recentTime = Carbon::now()->subHour();

        $data = [];

        // This should be record id 1
        $data[] = $this->makeCallRecord([
            'key' => 'fourth',
            'resource_name' => 'test.resource.1',
            'scheduled_time' => $futureTime,
        ]);

        $data[] = $this->makeCallRecord(['key' => 'third', 'scheduled_time' => $recentTime, 'updated_at' => $recentTime]);
        $data[] = $this->makeCallRecord(['key' => 'first', 'scheduled_time' => $oldestTime, 'updated_at' => $oldestTime]);
        $data[] = $this->makeCallRecord(['key' => 'second', 'scheduled_time' => $midTime, 'updated_at' => $midTime]);

        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_COMPLETE, 'updated_at' => $oldestTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_COMPLETE, 'updated_at' => $recentTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_IN_PROGRESS, 'updated_at' => $oldestTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_IN_PROGRESS, 'updated_at' => $recentTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_FAILED, 'attempt_count' => 3, 'error_count' => 3, 'updated_at' => $oldestTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_FAILED, 'attempt_count' => 3, 'error_count' => 3, 'updated_at' => $recentTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_CANCELLED, 'updated_at' => $oldestTime]);
        $data[] = $this->makeCallRecord(['status' => DeferredCall::STATUS_CANCELLED, 'updated_at' => $recentTime]);

        return $data;
    }

    private function makeCallRecord(array $overrides = []): array
    {
        $id = $this->nextId++;

        $data = array_merge([
            'id' => $id,
            'key' => uniqid('', true),
            'status' => DeferredCall::STATUS_PENDING,
            'hash' => null,
            'scheduled_time' => Carbon::now(),
            'attempt_count' => 0,
            'error_count' => 0,
            'api_user' => null,
            'resource_name' => 'test.resource',
            'resource_params' => [],
            'response_status' => null,
            'response' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ], $overrides);

        $data['scheduled_time'] = Carbon::parse($data['scheduled_time'])->toDateTimeString();
        $data['created_at'] = Carbon::parse($data['created_at'])->toDateTimeString();
        $data['updated_at'] = Carbon::parse($data['updated_at'])->toDateTimeString();

        $data['resource_params'] = serialize($data['resource_params']);

        $data['hash'] = $this->generateHash($data);

        return $data;
    }

    private function generateHash(array $data): string
    {
        $string = implode('', [
            $data['resource_name'] ?? '',
            $data['resource_params'] ?? '',
            $data['api_user'] ?? '',
        ]);

        return base64_encode(hash_hmac('sha256', $string, env('HASH_KEY'), true));
    }
}