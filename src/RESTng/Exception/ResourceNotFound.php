<?php

namespace MiamiOH\RESTng\Exception;

class ResourceNotFound extends \Exception
{
}
