+++
title = "Resource Versions"
weight = 30
+++

The REST resource represents an implementation of business data and logic available to external consumers. While these services should remain consistent, there will come a time that a new version must be made which breaks previous compatibility. Therefore, resources must include a version specification prior to any external use.

The version must be reflected in both the resource name and pattern. Versions should reflect only major releases and must not include a dot or other separator. There is no specified location in the name or pattern for a version spec to appear, except that it cannot be the first part. The developer should determine where to place the version spec in relation to logical grouping of associated resources.

Example resource names with version spec:

	course.v1
	course.v1.section
	course.section.v1

Example resource paths with version spec:

	/course/v1
	/course/v1/section
	/course/section/v1

The actual implementation of new versions is up to the developer (create a new class, create new methods in the same class, etc).

