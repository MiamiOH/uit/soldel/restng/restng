<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-02-11
 * Time: 20:28
 */

namespace MiamiOH\RESTng\Service\Apm;


use MiamiOH\RESTng\Exception\ApmException;
use Nipwaayoni\Events\EventBean;

class QuerySpan extends Span
{
    use CreatesDatabaseQuerySpans;

    private $typeFormat = 'db.%s.%s';
    private $action = 'query';

    /** @var string  */
    private $queryType;
    /** @var string  */
    private $statement;
    /** @var string  */
    private $instance;
    /** @var string  */
    private $user;

    public function __construct(string $name, \Nipwaayoni\Events\Transaction $parent, string $queryType = 'sql')
    {
        parent::__construct($name, $parent);

        $this->queryType = $queryType;

        $this->subType = 'mysql';

        $this->updateType();
    }

    public function setType(string $type): void
    {
        throw new ApmException('Span type cannot be set on QuerySpan objects');
    }

    public function setSubType(string $subType): void
    {
        parent::setSubType($subType);

        $this->updateType();
    }

    public function setInstance(string $instance): void
    {
        $this->instance = $instance;
    }

    public function instance(): string
    {
        return $this->instance;
    }

    public function setStatement(string $statement): void
    {
        $this->statement = $statement;
    }

    public function statement(): string
    {
        return $this->statement;
    }

    public function setUser(string $user): void
    {
        $this->user = $user;
    }

    public function user(): string
    {
        return $this->user;
    }

    public function event(): EventBean
    {
        $this->span->setType(sprintf('db.%s.query', $this->type));
        $this->span->setAction($this->action);
        $this->span->setCustomContext($this->makeContext());

        return parent::event();
    }

    private function makeContext(): array
    {
        return [
            'db' => [
                'instance' => $this->instance,
                'statement' => $this->statement,
                'type' => $this->queryType,
                'user' => $this->user,
            ]
        ];
    }

    private function updateType(): void
    {
        $this->type = sprintf($this->typeFormat, $this->subType, $this->action);
    }
}
