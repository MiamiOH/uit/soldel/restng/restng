<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/8/18
 * Time: 10:52 AM
 */

namespace MiamiOH\RESTng\Observer;


use MiamiOH\RESTng\Util\ResourceRunner;

class ResourceRunnerObserver extends Observer
{

    /**
     * @var ResourceRunner
     */
    private $runner;

    public function __construct(ResourceRunner $runner)
    {
        $this->runner = $runner;
    }

    public function update(): void
    {
        $resourceDetails = $this->runner->lastResource();

        $context = [
            'class' => get_class($this->runner),
        ];

        $this->recordObservations(array_merge($context, $resourceDetails));
    }
}