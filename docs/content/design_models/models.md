+++
title = "Models"
weight = 10
+++

Models represent a single instance of a data object. Models have properties which themselves have attributes describing them. A clearly defined model helps API developers and consumers understand the data object and how to use it.

We often find it useful to document a model in a simple table as part of the design process. We used weekdays in our RESTng tutorial. A design for that model may look something like this.

Property     | Type    | Example | Description
-------------|---------|---------|------------
id           | integer | 1       | A unique identifier for the weekday.
name         | string  | Monday  | The name of the weekday.
abbreviation | string  | Mon     | The common three character shortend form of the name.
dayOfWeek    | integer | 2       | The numeric order of appearance within the week. (min 1; max 7)
type         | string  | weekday | One of 'weekday' or 'weekend'

There is not proscribed form for your model design. Use a format that makes sense to you and provides an appropriate level of detail for your team to work with. This format should facility translation into the model definition in the RESTng configuration.

## RESTng Model Definition

You add model definitions to RESTng using the ResourceProvider::addDefinition method in your provider file. Here's an example of a simple model:

{{< highlight php "linenos=table" >}}
<?php
...
    $this->addDefinition(array(
        'name' => 'WeekDay',
        'type' => 'object',
        'properties' => array(
            'id' => array(
                'type' => 'integer',
            ),
            'name' => array(
                'type' => 'string',
            ),
            'abbreviation' => array(
                'type' => 'string',
            ),
            'dayOfWeek' => array(
                'type' => 'integer',
                'minimum' => 0,
                'maximum' => 6
            ),
            'type' => array(
                'type' => 'string',
                'enum' => ['weekday', 'weekend']
            ),
        )
    ));
{{< / highlight >}} 

The configuration array given to the addDefinition method adheres to the Swagger Schema Object specification (http://swagger.io/specification/#schemaObject). Not all features of the specification may be implemented in the RESTng configuration process, but we hope to improve the support over time. 

**Note:** Currently, it is entirely up to your service to enforce any data validation based on the schema. 

## Defining Composed Models

A model may be composed of it's own properties and other models. The following example shows a model composed of both another model and a collection of other models. This technique facilities reduction of REST calls by the consumer, but should be used with care.

{{< highlight php "linenos=table" >}}
<?php
...
    $this->addDefinition(array(
        'name' => 'CITest.Class',
        'type' => 'object',
        'properties' => array(
            'classId' => array(
                'type' => 'string',
            ),
            'courseId' => array(
                'type' => 'string',
            ),
            'semester' => array(
                'type' => 'string',
            ),
            'section' => array(
                'type' => 'string',
            ),
            'instructor' => array(
                'type' => 'string',
            ),
            'location' => array(
                'type' => 'string',
            ),
            'meetingTime' => array(
                'type' => 'string',
            ),
            'maxEnrollment' => array(
                'type' => 'integer',
                'format' => 'int64',
            ),
            'course' => array(
                'type' => 'object',
                '$ref' => '#/definitions/CITest.Course',
            ),
            'enrollments' => array(
                'type' => 'object',
                '$ref' => '#/definitions/CITest.Enrollment.Collection',
            )
        )
    ));
{{< / highlight >}} 

The definitions being referred to must be declared somewhere in the RESTng configuration space, but it is not required that they be in your service. When your service calls the other resource, the required configuration will be autoloaded. Likewise, the Swagger UI interface reads all of the RESTng resources when constructing the Swagger Spec file.