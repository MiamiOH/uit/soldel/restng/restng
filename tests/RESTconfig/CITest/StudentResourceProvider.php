<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class StudentResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Student',
            'type' => 'object',
            'properties' => array(
                'studentId' => array(
                    'type' => 'string',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'gender' => array(
                    'type' => 'string',
                    'enum' => ['M', 'F'],
                ),
                'birthday' => array(
                    'type' => 'date',
                ),
                'entryYear' => array(
                    'type' => 'integer',
                    'format' => 'int64',
                ),
                'usResident' => array(
                    'type' => 'boolean',
                ),
                'state' => array(
                    'type' => 'string',
                ),
                'majorId' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Student.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Student'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestStudent',
            'class' => 'Tests\Service\CITest\Student',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.all',
            'description' => 'Pageable collection of student records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student',
            'service' => 'CITestStudent',
            'method' => 'getStudent',
            'isPageable' => true,
            'isPartialable' => true,
            'options' => array(
                'entryYear' => array('type' => 'list', 'description' => 'A test student entry year'),
                'state' => array('type' => 'list', 'description' => 'A test student state (two letter abbreviation)'),
                'usResident' => array('description' => 'A test student usResident indicator (0 or 1)'),
                'majorId' => array('type' => 'list', 'description' => 'A test student major ID'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of students',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Student.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.public.studentId',
            'description' => 'A single test student.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/public/:studentId',
            'service' => 'CITestStudent',
            'method' => 'getStudent',
            'isPartialable' => true,
            'params' => array(
                'studentId' => array('description' => 'A test studentId', 'alternateKeys' => ['sid', 'uid']),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.studentId',
            'description' => 'A single test student.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/:studentId',
            'service' => 'CITestStudent',
            'method' => 'getStudent',
            'isPartialable' => true,
            'params' => array(
                'studentId' => array('description' => 'A test studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'RESTng Test App',
                        'module' => 'User',
                        'key' => 'view'),
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.all.studentId',
            'description' => 'A single test student for any authenticated user.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/all/:studentId',
            'service' => 'CITestStudent',
            'method' => 'getStudent',
            'isPartialable' => true,
            'params' => array(
                'studentId' => array('description' => 'A test studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    'application' => 'Student Info',
                    'module' => 'All Students',
                    'key' => 'read'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.self.studentId',
            'description' => 'A single test student for authorized user or self.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/self/:studentId',
            'service' => 'CITestStudent',
            'method' => 'getStudent',
            'isPartialable' => true,
            'params' => array(
                'studentId' => array('description' => 'A test studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'studentId',
                    ),
                    array(
                        'type' => 'authMan',
                        'application' => 'RESTng Test App',
                        'module' => 'Student',
                        'key' => 'view'),
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.student.public.studentId.create',
            'description' => 'A single test student.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/public',
            'service' => 'CITestStudent',
            'method' => 'createStudent',
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.student.studentId.create',
            'description' => 'A single test student.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student',
            'service' => 'CITestStudent',
            'method' => 'createStudent',
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    'application' => 'RESTng Test App',
                    'module' => 'User',
                    'key' => 'view'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.student.mixed.studentId',
            'description' => 'A single test student with mixed authorization.',
            'tags' => array('CITest'),
            'pattern' => '/citest/student/mixed/:studentId',
            'service' => 'CITestStudent',
            'method' => 'getStudentAuths',
            'isPartialable' => true,
            'params' => array(
                'studentId' => array('description' => 'A test studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A student object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Student',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'anonymous'
                    ),
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array('type' => 'anonymous'),
                    array(
                        'type' => 'self',
                        'param' => 'studentId',
                    ),
                    array(
                        'type' => 'authMan',
                        'application' => 'RESTng Test App',
                        'module' => 'User',
                        'key' => 'view'),
                ),
            ),
        ));
    }
}