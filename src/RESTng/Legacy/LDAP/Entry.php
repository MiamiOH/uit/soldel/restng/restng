<?php

namespace MiamiOH\RESTng\Legacy\LDAP;

class Entry
{
    var $ds;
    var $dn;
    var $entry;
    var $attributes;
    var $ber_identifier;
    var $changes;

    function __construct(&$ds, &$entry)
    {
        $this->ds = $ds;
        $this->entry = $entry;
        $this->dn = ldap_get_dn($this->ds, $this->entry);

        $attribute = ldap_first_attribute($this->ds, $this->entry, $this->ber_identifier);
        while ($attribute) {
            $this->_populate_attributes(strtolower($attribute));
            $attribute = ldap_next_attribute($this->ds, $this->entry, $this->ber_identifier);
        }

        $this->changes = array();
    }

    function _populate_attributes($attribute)
    {
        $this->attributes[$attribute] = ldap_get_values($this->ds, $this->entry, $attribute);
        unset($this->attributes[$attribute]['count']);
    }

    function attribute_list()
    {
        return array_keys($this->attributes);
    }

    function attribute_values($attribute)
    {
        $attribute = strtolower($attribute);

        if (!array_key_exists($attribute, $this->attributes)) {
            return [];
        }

        return $this->attributes[$attribute];
    }

    function attribute_value($attribute)
    {
        $attribute = strtolower($attribute);

        if (!array_key_exists($attribute, $this->attributes)) {
            return '';
        }

        return $this->attributes[$attribute][0];
    }

}

