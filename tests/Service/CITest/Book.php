<?php

namespace Tests\Service\CITest;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use Respect\Validation\Exceptions\NestedValidationException;

class Book extends \MiamiOH\RESTng\Service
{

    /** @var DBH\Sqlite3 */
    private $dbh;

    /** @var \MiamiOH\RESTng\Util\ValidationFactory $validator */
    private $validator = '';

    public function setValidator($validator)
    {
        /** @var \MiamiOH\RESTng\Util\ValidationFactory $validator */
        $this->validator = $validator;
    }

    public function setDatabase(DatabaseFactory $database)
    {
        $this->dbh = $database->getHandle('bookstore');
    }

    public function getBook()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $options = $request->getOptions();

        if ($id) {

            $record = $this->dbh->queryfirstrow_assoc('
                  select id, title
                    from book
                    where id = ?
                ', $id);

            if ($record === false) {
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            } else {
                $record = $this->camelCaseKeys($record);
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $response->setPayload($record);
            }
        } else {
            $records = $this->dbh->queryall_array('select id, title from book order by id');

            if ($request->isPaged()) {
                $offset = $request->getOffset() - 1;
                $limit = $request->getLimit();

                $response->setTotalObjects(count($records));

                $records = array_slice(
                    $records,
                    $offset,
                    $limit
                );
            }

            for ($i = 0, $iMax = count($records); $i < $iMax; $i++) {
                $records[$i]['api_location'] = $this->locationFor('citest.book.id', array('id' => $records[$i]['id']));
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($records);
        }

        return $response;
    }

    public function addBook()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        if (empty($data['id'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing id for new book');
        }

        $this->dbh->perform('
             insert into book (id, title, publish_date, author)
               values (?, ?, ?, ?)
             ', $data['id'], $data['title'], $data['publishDate'], $data['author']);

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($data);

        return $response;
    }

    public function updateBook()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $data = $request->getData();

        $this->dbh->perform('
      update book set title = ?, publish_date = to_date(?, \'YYYY-MM-DD\'), author = ?
        where id = ?
      ', $data['title'], $data['publishDate'], $data['author'], $id);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    public function deleteBook()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $this->dbh->perform('
      delete from book
        where id = ?
      ', $id);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    public function deleteBooks()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $bookList = $request->getData();

        if (!is_array($bookList)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Request body must contain a collection of books to delete');
        }

        // In a real resource, we would wrap the delete in a try/catch and be sure to
        // process each object, recording the status and returning a collection to the
        // consumer. For our CI test, we just need to process the collection.
        foreach ($bookList as $book) {
            $this->dbh->perform('
        delete from book
          where id = ?
        ', $book['id']);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    public function addBookValidated()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        $idRule = $this->validator->newValidator()->intVal()->min(1);
        $publishDateRule = $this->validator->newValidator()->date('Y-m-d');

        $errors = [];

        try {
            $idRule->assert($data['id']);
        } catch (NestedValidationException $exception) {
            $errors['id'] = $exception->getMessages();
        }

        try {
            $publishDateRule->assert($data['publishDate']);
        } catch (NestedValidationException $exception) {
            $errors['publishDate'] = $exception->getMessages();
        }

        if (!$errors) {
            $this->dbh->perform('
       insert into book (id, title, publish_date, author)
         values (?, ?, to_date(?, \'YYYY-MM-DD\'), ?)
       ', $data['id'], $data['title'], $data['publishDate'], $data['author']);

            $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
            $response->setPayload($data);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            $response->setPayload($errors);
        }

        return $response;
    }

}
