<?php

namespace MiamiOH\RESTng\Util;

use Illuminate\Cache\Repository;
use Logger;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;
use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;

class User implements \Serializable
{

    /** @var \MiamiOH\RESTng\App $api */
    protected $app;

    /** @var CredentialValidatorInterface */
    protected $credentialValiator;

    /** @var AuthorizationValidatorInterface */
    protected $authorizationValidator;

    /** @var Repository */
    private $cache;

    /** @var Transaction */
    private $transaction;

    /** @var Logger */
    protected $logger;

    private $users = array();
    private $currentUser = '';
    private $defaultUser = '';

    /**
     * @param $app
     */
    public function setApp(App $app)
    {
        /** @var \MiamiOH\RESTng\App $api */
        $this->app = $app;

        $this->logger = \Logger::getLogger(__CLASS__);
    }

    public function setCredentialValidator(CredentialValidatorInterface $validator)
    {
        $this->credentialValiator = $validator;
    }

    public function setAuthorizationValidator(AuthorizationValidatorInterface $validator)
    {
        $this->authorizationValidator = $validator;
    }

    public function setCache(Repository $cache): void
    {
        $this->cache = $cache;
    }

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    public function serialize()
    {
        return serialize($this->pack());
    }

    public function unserialize($serialized)
    {
        $this->unpack(unserialize($serialized, ['allowed_classes' => false]));
    }

    public function pack()
    {
        return array(
            'users' => $this->users,
            'currentUser' => $this->currentUser,
            'defaultUser' => $this->defaultUser
        );
    }

    public function unpack($data)
    {
        $this->users = $data['users'];
        $this->currentUser = $data['currentUser'];
        $this->defaultUser = $data['defaultUser'];
    }

    public function assumeUser(?User $user): void
    {
        if (null === $user) {
            $this->users = [];
            $this->currentUser = '';
            $this->defaultUser = '';

            return;
        }

        $this->unpack($user->pack());
    }

    /**
     * @param $token
     * @param bool|true $makeCurrent
     * @return string
     */
    public function addUserByToken($token, $makeCurrent = true)
    {
        $tokenResult = $this->credentialValiator->validateToken($token);

        if ($tokenResult['valid']) {
            $userData = array(
                'username' => $tokenResult['username'],
                'token' => $token,
                'authenticated' => true,
                'authorizations' => array(),
            );

            return $this->addUser($userData, $makeCurrent);
        }

        return false;
    }

    /**
     * @param $credentials
     * @param bool|true $makeCurrent
     * @return bool|string
     * @throws \Exception
     */
    public function addUserByCredentials($credentials, $makeCurrent = true)
    {
        if (!is_array($credentials))
        {
            throw new \Exception('Argument to ' . __CLASS__ . '::addUserByCredentials must be an array');
        }

        if (!(isset($credentials['username']) && isset($credentials['password'])))
        {
            throw new \Exception('Both username and password are required for ' . __CLASS__ . '::addUserByCredentials');
        }

        $tokenResult = $this->credentialValiator->validateUsernamePassword($credentials['username'], $credentials['password']);

        if ($tokenResult['valid']) {
            $userData = array(
                'username' => $tokenResult['username'],
                'token' => $tokenResult['token'],
                'authenticated' => true,
                'authorizations' => array(),
            );

            return $this->addUser($userData, $makeCurrent);
        }

        return false;
    }

    /**
     * @param $userKey
     * @throws \Exception
     */
    public function setCurrentUser($userKey)
    {
        if (!isset($this->users[$userKey]))
        {
            throw new \Exception('Unable to select user with key ' . $userKey . '. No such user.');
        }

        $this->currentUser = $userKey;
    }

    /**
     * @param $userKey
     * @throws \Exception
     */
    public function removeUser($userKey)
    {
        if (!isset($this->users[$userKey]))
        {
            throw new \Exception('Unable to remove user with key ' . $userKey . '. No such user.');
        }

        unset($this->users[$userKey]);

        if ($this->currentUser === $userKey)
        {
            $this->setCurrentUserToDefault();
        }
    }

    /**
     *
     */
    private function setCurrentUserToDefault()
    {
        $this->currentUser = $this->defaultUser;
    }

    /**
     * @param $username
     * @param $userData
     * @param bool|true $makeCurrent
     */
    private function addUser($userData, $makeCurrent = true)
    {
        /*
         * Generate a unique key to identify the user. This key is returned only
         * in the context of adding a user and is otherwise not exposed. The intent
         * is that only the consumer who adds a user (other than the default) can
         * remove it.
         */
        $key = uniqid();

        $this->users[$key] = $userData;

        // The first user becomes the default
        if (!$this->defaultUser)
        {
            $this->defaultUser = $key;
        }

        if ($makeCurrent) {
            $this->setCurrentUser($key);
        }

        return $key;
    }

    private function addAuthorizationForCurrentUser($application, $module, $key, $auth)
    {
        $this->users[$this->currentUser]['authorizations'][$application][$module][$key] = $auth;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        if (isset($this->users[$this->currentUser]) && $this->users[$this->currentUser]['username']) {
            return $this->users[$this->currentUser]['username'];
        } else {
            return '';
        }
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        if (isset($this->users[$this->currentUser]) && $this->users[$this->currentUser]['authenticated']) {
            return $this->users[$this->currentUser]['authenticated'];
        } else {
            return false;
        }
    }

    public function checkAuthorization($authConfig)
    {
        $authorized = false;

        if (!is_array($authConfig))
        {
            throw new \Exception('Argument to ' . __CLASS__ . '::checkAuthorization must be an array');
        }

        /*
         * Determine what type of authorization we need to perform.
         *
         * It would have been nice if I'd thought ahead about the need for multiple means
         * of authorization. Since many resources already declare an associative array
         * of AuthMan oriented config values, we have to look for those first and construct
         * a suitable array to proceed with.
         *
         * If the array does not look to be set up for AuthMan, then we can assume we have a
         * a numeric array and process each entry.
         */
        $authList = [];
        if (isset($authConfig['type']) || isset($authConfig['application']))
        {
            // push our param array in as the single element for our auth list
            $authList[] = $authConfig;
        }
        else
        {
            // otherwise assume we have a list of auths
            $authList = $authConfig;
        }

        // Loop over each provided auth and execute it, stopping on the first positive match
        foreach ($authList as $auth) {
            $authType = (isset($auth['type']) && $auth['type']) ? $auth['type'] : 'authMan';

            switch ($authType)
            {
                case 'authMan':
                    $authorized = $this->isAuthorized($auth['application'], $auth['module'], $auth['key']);
                    break;

                case 'self':
                    $authorized = $this->isSelfAccess($auth['param'], 'strtolower');
                    break;

                case 'anonymous':
                    $authorized = !$this->isAuthenticated();
                    break;
            }

            if ($authorized)
            {
                break;
            }
        }

        return $authorized;
    }

    public function isSelfAccess($paramName, $callable = '')
    {
        $currentUsername = $this->getUsername();

        if (!$currentUsername)
        {
            return false;
        }

        $routeParams = $this->app->router()->getCurrentRoute()->getParams();

        if (!isset($routeParams[$paramName]))
        {
            return false;
        }

        $paramValue = $routeParams[$paramName];

        if ($callable)
        {
            if (!is_callable($callable))
            {
                throw new \Exception('Declared callable ' . $callable . ' cannot be called');
            }

            $currentUsername = $callable($currentUsername);
            $paramValue = $callable($paramValue);

        }

        return $currentUsername === $paramValue;

    }

    /**
     *
     * Deprecated!
     *
     * @param $application
     * @param $module
     * @param $keyList
     * @return bool
     * @throws \Exception
     */
    public function isAuthorized($application, $module, $keyList)
    {
        if (!$application) {
            throw new \Exception('RESTng\Util\User::isAuthorized requires a valid application value');
        }

        if (!$module) {
            throw new \Exception('RESTng\Util\User::isAuthorized requires a valid module value');
        }

        if (!$keyList) {
            throw new \Exception('RESTng\Util\User::isAuthorized requires a valid key value');
        }

        // Normalize the key to an array for consistent handling
        if (!is_array($keyList)) {
            $keyList = [ $keyList ];
        }

        foreach ($keyList as $key)
        {
            if (!(isset($this->users[$this->currentUser]['authorizations'][$application]) &&
                isset($this->users[$this->currentUser]['authorizations'][$application][$module]) &&
                isset($this->users[$this->currentUser]['authorizations'][$application][$module][$key]))
            ) {

                $cacheKey = new CacheKeyString(
                    'authorization:{username}:{application}:{module}:{key}',
                    [
                        'username' => $this->getUsername(),
                        'application' => $application,
                        'module' => $module,
                        'key' => $key,
                    ]
                );

                if ($this->cache->has($cacheKey->key())) {
                    $this->logger->debug(sprintf('Cache hit for %s (%s)', $cacheKey->format(), $cacheKey->key()));

                    $auth = $this->cache->get($cacheKey->key());
                } else {
                    $this->logger->debug(sprintf('Cache miss for %s (%s)', $cacheKey->format(), $cacheKey->key()));

                    $span = $this->transaction->startSpan('Validating authorization for key ' . $key);
                    $auth = $this->authorizationValidator->validateAuthorizationForKey(
                        AuthorizationSpecification::fromValues($application, $module, $key),
                        $this->getUsername()
                    );
                    $span->stop();

                    $this->cache->put($cacheKey->key(), $auth, env('CACHE_TTL', 15 * 60));
                }

                $this->addAuthorizationForCurrentUser($application, $module, $key, $auth);

            }

            // If the authorization was resolved and is true, return
            // otherwise keep trying
            if (isset($this->users[$this->currentUser]['authorizations'][$application][$module][$key])
                && $this->users[$this->currentUser]['authorizations'][$application][$module][$key])
            {
                return $this->users[$this->currentUser]['authorizations'][$application][$module][$key];
            }
        }

        return false;
    }
}
