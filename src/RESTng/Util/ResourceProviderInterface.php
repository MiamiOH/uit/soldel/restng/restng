<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:35 AM
 */

namespace MiamiOH\RESTng\Util;


interface ResourceProviderInterface
{
    public function registerDefinitions(): void;
    public function registerServices(): void;
    public function registerResources(): void;
}