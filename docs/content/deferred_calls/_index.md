+++
title = "Deferred Calls"
weight = 99
+++

RESTng provides a mechanism to defer the execution of resource calls. Deferring a call can be useful when your resource will be called by a web front end, but the execution will take longer than appropriate for web user experience. Deferring a call is done through the callResource method by passing true for the deferred indicator in the options array (the third parameter):

{{< highlight php "linenos=table" >}}
<?php
...
class Entity ...
...
public function createEntity() {
    $request = $this->getRequest();
    ...
    // When creating an entity, the Google account takes a long time.
    // Defer it until later.
    $deferredResponse = $this->callResource(
         'idVault.account.google.create', ['data' => $accountData], ['deferred' => true]);
    ...
{{< / highlight >}} 

The response object is a standard response. If the call was successfully saved, the status will be \MiamiOH\RESTng\API_ACCEPTED (HTTP 202). A separate process will execute the deferred call.

**Note:** Deferred calls use a hash to validate the request prior to execution. The hash is generated using a key provided during the RESTng installation. You must add the \MiamiOH\RESTng\HASH_KEY constant to your api.conf.php file:

    const HASH_KEY = 'random';

This hash key must be the same on all installations of RESTng which will be executing calls from the same store.

## Resource Authorization

Authorization is performed as normal in the callResource method prior to acceptance of the deferred call. The current API user object is captured and saved as part of the deferred call as well. When the deferred call is executed, the API user object is restored and authorization is performed again.

## Accessing Deferred Calls

There are cases in which you will want to get the status of deferred calls. For example, a web front end may need to reflect the status of deferred calls. The response returned from a deferred callResource will contain an id of the deferred call. However, it is not always practical to store the returned id, so the optoins array of the callResource method can include a 'key' index whose value can be used as a key to identify the call later. (Note that keys must be unique.)

To access deferred calls, inject the APIDeferredCall into your service:

    'deferredCall' => array('type' => 'service', 'name' => 'APIDeferredCall')
    
Then ask the service to fetch a deferred call response using the key you provided:

    $response = $this->deferredCall->getCallByKey($key);
