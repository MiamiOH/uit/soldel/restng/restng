+++
title = "Current User"
weight = 90
+++

RESTng will always create a single user object which will persist through out a transaction. If authentication is enabled for the requested resource, the user object will contain a representation of the user that authenticated to the the REST layer. The user object is also capable of performing authorization checks and will cache the results of the authorization for the current transaction.

You can get the current user object in your service:

    $user = $this->getApiUser();

Once you have the user object, you can check it's authenticated state:

    $isAuthenticated = $user->isAuthenticated();

And get the current username:

    $username = $user->getUsername();

## Checking Authorization

The user object is responsible for managing authorizations. The most common use is through middleware configured for a resource, but you can use authorization checks within your own service as well.

You can use AuthMan for authorizations and have the user object check them by calling the checkAuthorization method. This method takes the same array argument used to configure the authorization middleware in your resource configuration (the middleware actually calls this method with the configuration array). For example:

```
$isAuthorized = $user->checkAuthorization(
    array( 'type' => 'authMan',
         'application' => 'Person Service', 
         ‘module’ => 'Read Attributes', 
         'key' => array('view', 'edit')
    ),
);
```

Any authorization type which can be configured for the middleware service can be utilized directly through the user object.

**Note:** Checking authorizations within your service rather than through the resource configuration does hide the authorization requirement from the Swagger spec generator. This can make it harder for developers to understand the authorizations required to consume your service.

## Adding Users

```
$user = $this->getApiUser();
$localUserKey = $user->addUserByCredentials(array(
   'username' => $username,
   'password' => $password
));
```

If you do not get a user key back, the authentication failed and you should take appropriate action.

If successful, the newly added user will be set as the current user. You may want to remove your user after completing your resource transaction:

    $user->removeUser($localUserKey);
