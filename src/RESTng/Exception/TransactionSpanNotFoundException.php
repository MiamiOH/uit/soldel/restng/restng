<?php

namespace MiamiOH\RESTng\Exception;

class TransactionSpanNotFoundException extends ApmException
{
}
