<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\FeatureTestCase;

class ResourceOptionsTest extends FeatureTestCase
{
    use UsesDatasource;

    public function setUp(): void
    {
        parent::setUp();

        $this->installDatasourceService();
    }

    public function testMissingRequiredOptionCausesStatusServerError(): void
    {
        $this->assertHttpStatusWithJsonMessage(
            $this->getJson('/citest/filter'),
            App::API_FAILED,
            'Missing required option type for resource citest.filter'
        );
    }

    public function testUnkownOptionCausesStatusServerError(): void
    {
        $this->assertHttpStatusWithJsonMessage(
            $this->getJson('/citest/filter?size=big'),
            App::API_FAILED,
            'No defined option size for resource citest.filter'
        );
    }

    public function testOptionCanBeDeclaredAsList(): void
    {
        $response = $this->getJson('/citest/reflect/options?color=blue,yellow,green');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'color' => [
                    'blue',
                    'yellow',
                    'green',
                ]
            ]
        ], true);
    }

    public function testListOptionIsTreatedAsArrayWithOnlyOneValue(): void
    {
        $response = $this->getJson('/citest/reflect/options?color=blue');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'color' => [
                    'blue',
                ]
            ]
        ], true);
    }

    public function testScalarOptionIsStringEvenIfValueContainsListSeparator(): void
    {
        $response = $this->getJson('/citest/reflect/options?type=under,over');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'type' => 'under,over'
            ]
        ], true);
    }

    public function testMultipleOptionsAreProvidedAsIndividualElements(): void
    {
        $response = $this->getJson('/citest/reflect/options?color=blue,red&type=outside');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'color' => [
                    'blue',
                    'red',
                ],
                'type' => 'outside'
            ]
        ], true);
    }

}
