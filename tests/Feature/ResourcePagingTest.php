<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/26/18
 * Time: 11:01 AM
 */

namespace Tests\Feature;


use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\Data\Table\Book;
use Tests\FeatureTestCase;

class ResourcePagingTest extends FeatureTestCase
{

    use UsesDatasource;
    use UsesTestData;

    /** @var \MiamiOH\RESTng\Legacy\DB\DBH\Sqlite3 */
    private static $handle;

    private $totalRecords = 83;

    public function setUp(): void
    {
        parent::setUp();

        if (self::$handle === null) {
            self::$handle = $this->createSqlite3DatabaseHandle();
        }

        $this->useData()
            ->withSqlite(self::$handle->getDbConnection())
            ->withTable(Book::class, ['totalRecords' => $this->totalRecords])
            ->populate();

        $this->installDatasourceService(self::$handle);

        $this->showExceptions();
    }

    /**
     * @dataProvider pagedQueryProvider
     */
    public function testCanProvideOffsetAndLimit($offset, $limit, $nextOffset, $lastOffset): void
    {
        $response = $this->getJson('/citest/book?offset=' . $offset . '&limit=' . $limit);

        $response->assertStatus(200);

        $payload = json_decode($response->getContent(), true);

        $this->assertEquals($payload['total'], $this->totalRecords);
        $this->assertCount($limit, $payload['data']);
        $this->assertStringContainsString('limit=' . $limit, $payload['currentUrl']);
        $this->assertStringContainsString('offset=' . $offset, $payload['currentUrl']);
        $this->assertStringContainsString('limit=' . $limit, $payload['firstUrl']);
        $this->assertStringContainsString('offset=1', $payload['firstUrl']);
        $this->assertStringContainsString('limit=' . $limit, $payload['nextUrl']);
        $this->assertStringContainsString('offset=' . $nextOffset, $payload['nextUrl']);
        $this->assertStringContainsString('limit=' . $limit, $payload['lastUrl']);
        $this->assertStringContainsString('offset=' . $lastOffset, $payload['lastUrl']);
    }

    public function pagedQueryProvider(): array
    {
        return [
            [1, 10, 11, 74],
            [11, 10, 21, 74],
            [35, 20, 55, 64],
            [25, 50, 75, 34],
        ];
    }

    public function testPageableResourceIsNotPagedByDefault(): void
    {
        $response = $this->getJson('/citest/book');

        $response->assertStatus(200);

        $payload = json_decode($response->getContent(), true);

        $this->assertArrayNotHasKey('total', $payload);
        $this->assertArrayNotHasKey('currentUrl', $payload);
    }

    public function testResourceWithDefaultPageLimitIsPagedByDefault(): void
    {
        $response = $this->getJson('/citest/bookPaged');

        $response->assertStatus(200);

        $payload = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('total', $payload);
        $this->assertArrayHasKey('currentUrl', $payload);
    }
}
