<?php

class mwContentFormatPutTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testContentSetupPutDefault() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionJsonAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupPutExtensionXmlAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'PUT',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

}
