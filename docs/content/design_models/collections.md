+++
title = "Collections"
weight = 20
+++

Collections are simply a numerically indexed array containing models. The type of model should be consistent, do not return a mixture of model types in a single collection.

For RESTng resources, we expect that a collection will only return previously defined models and not define a new model as part of the collection definition.

Collections are defined using addDefinition and, as with models, follow the Swagger Schema Object specification (http://swagger.io/specification/#schemaObject).

{{< highlight php "linenos=table" >}}
<?php
...
$this->addDefinition(array(
    'name' => 'WeekDay.Collection',
    'type' => 'array',
    'items' => array(
        '$ref' => '#/definitions/WeekDay'
    )
));
{{< / highlight >}} 
