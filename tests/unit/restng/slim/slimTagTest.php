<?php

class slimTagTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = new \MiamiOH\RESTng\App();

    }

    public function testAddTagRequiresArray() {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Tag config must be an array for addTag');

        $tagConfig = '';

        $this->restngApp->addTag($tagConfig);
    }

    public function testAddTagRequiresName() {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Name is required when adding a tag');

        $tagConfig = array();

        $this->restngApp->addTag($tagConfig);
    }

}
