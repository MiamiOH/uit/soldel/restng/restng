<?php

include_once('abstract.php');

class viewTest extends viewAbstract
{

    public function setUp(): void
    {

        parent::setUp();

        $this->view = new \MiamiOH\RESTng\View\Base();

        \MiamiOH\RESTng\View\Base::$stopAfterRender = false;

        $this->view->setAppInstance($this->app);

    }

    public function testGetAppInstance()
    {

        $this->assertNotEquals('', $this->view->getAppInstance());

    }

    public function testView()
    {

        $this->response->method('getStatus')->willReturn(\MiamiOH\RESTng\App::API_OK);
        $this->response->method('isPaged')->willReturn(false);

        $this->view->replace(array('data' => array('id' => 1, 'name' => 'test name')));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(3, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(is_array($body['data']));
        $this->assertEquals(2, count(array_keys($body['data'])));
        $this->assertTrue(array_key_exists('id', $body['data']));
        $this->assertTrue(array_key_exists('name', $body['data']));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);

    }

    public function testViewWithFlashMessage()
    {

        $this->response->method('getStatus')->willReturn(\MiamiOH\RESTng\App::API_OK);
        $this->response->method('isPaged')->willReturn(false);

        $this->view->set('flash', new \Slim\Middleware\Flash());
        $this->view->replace(array('data' => array('id' => 1, 'name' => 'test name')));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(3, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(is_array($body['data']));
        $this->assertEquals(2, count(array_keys($body['data'])));
        $this->assertTrue(array_key_exists('id', $body['data']));
        $this->assertTrue(array_key_exists('name', $body['data']));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);

    }

    public function testViewWithError()
    {

        $this->response->method('getStatus')->willReturn(\MiamiOH\RESTng\App::API_FAILED);
        $this->response->method('isPaged')->willReturn(false);

        $this->view->replace(array('data' => array('id' => 1, 'name' => 'test name')));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(3, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(is_array($body['data']));
        $this->assertEquals(2, count(array_keys($body['data'])));
        $this->assertTrue(array_key_exists('id', $body['data']));
        $this->assertTrue(array_key_exists('name', $body['data']));
        $this->assertEquals(\MiamiOH\RESTng\App::API_FAILED, $body['status']);
        $this->assertTrue($body['error']);

    }

    public function testViewPagedFirstPage()
    {

        $this->limit = 10;
        $this->offset = 1;
        $this->totalObjects = 120;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('nextUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . $this->offset, $body['currentUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['firstUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->totalObjects - $this->limit + 1), $body['lastUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->offset + $this->limit), $body['nextUrl']);

    }

    public function testViewPagedLastPage()
    {

        $this->limit = 10;
        $this->offset = 111;
        $this->totalObjects = 120;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('prevUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . $this->offset, $body['currentUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['firstUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->totalObjects - $this->limit + 1), $body['lastUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->offset - $this->limit), $body['prevUrl']);

    }

    public function testViewPagedLimitGtRecords()
    {

        $this->limit = 15;
        $this->offset = 1;
        $this->totalObjects = 10;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(7, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertFalse(array_key_exists('prevUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . $this->offset, $body['currentUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['firstUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['lastUrl']);

    }

    public function testViewPagedOffsetAndLimitGtRecords()
    {

        $this->limit = 20;
        $this->offset = 15;
        $this->totalObjects = 10;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('prevUrl', $body));
        $this->assertFalse(array_key_exists('nextUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . $this->offset, $body['currentUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['firstUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['lastUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['prevUrl']);

    }

    public function testViewPagedNextUrlOrphan()
    {

        $this->limit = 9;
        $this->offset = 1;
        $this->totalObjects = 10;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertFalse(array_key_exists('prevUrl', $body));
        $this->assertTrue(array_key_exists('nextUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . $this->offset, $body['currentUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . 1, $body['firstUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->totalObjects - $this->limit + 1), $body['lastUrl']);
        $this->assertEquals($this->baseUrl . '/test/resource?limit=' . $this->limit
            . '&offset=' . ($this->offset + $this->limit), $body['nextUrl']);

    }

    public function testViewPagedWithOptions()
    {

        $this->limit = 10;
        $this->offset = 1;
        $this->totalObjects = 120;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource.owner';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;
        $this->resourceOptions = array('color' => 'blue', 'type' => 'new');
        $this->resourceParams = array('owner' => 'bob');

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('nextUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);

        $expectedOptions = array_merge(array('limit' => $this->limit, 'offset' => $this->offset), $this->resourceOptions);

        foreach ($expectedOptions as $key => $value) {
            $expected = $key . '=' . urlencode($value);

            $this->assertTrue(strpos($body['currentUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['firstUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['lastUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['nextUrl'], $expected) !== false);

        }

        $this->assertTrue(strpos($body['currentUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['firstUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['lastUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['nextUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);

    }

    public function testViewPagedWithOptionsList()
    {

        $this->limit = 10;
        $this->offset = 1;
        $this->totalObjects = 120;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource.owner';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;
        $this->resourceOptions = array('color' => ['red', 'blue'], 'type' => 'new');
        $this->resourceParams = array('owner' => 'bob');

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(8, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('nextUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);

        $expectedOptions = array_merge(array('limit' => $this->limit, 'offset' => $this->offset), $this->resourceOptions);

        foreach ($expectedOptions as $key => $value) {
            $expected = $key . '=' . urlencode(is_array($value) ? implode(',', $value) : $value);

            $this->assertTrue(strpos($body['currentUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['firstUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['lastUrl'], $expected) !== false);
            $this->assertTrue(strpos($body['nextUrl'], $expected) !== false);

        }

        $this->assertTrue(strpos($body['currentUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['firstUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['lastUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);
        $this->assertTrue(strpos($body['nextUrl'], '/test/resource/' . $this->resourceParams['owner']
            . '?') !== false);

    }

    public function testViewPagedFields()
    {

        $this->limit = 10;
        $this->offset = 51;
        $this->totalObjects = 120;
        $this->objects = array(
            array('id' => 1, 'name' => 'test name'),
            array('id' => 2, 'name' => 'test name'),
            array('id' => 3, 'name' => 'test name'),
            array('id' => 4, 'name' => 'test name'),
            array('id' => 5, 'name' => 'test name'),
            array('id' => 6, 'name' => 'test name'),
            array('id' => 7, 'name' => 'test name'),
            array('id' => 8, 'name' => 'test name'),
            array('id' => 9, 'name' => 'test name'),
            array('id' => 10, 'name' => 'test name'),
        );
        $this->resourceName = 'test.resource';
        $this->status = \MiamiOH\RESTng\App::API_OK;
        $this->paged = true;
        $this->partial = true;
        $this->fieldSpec = 'id,name,description,type(id,description)';

        $expectedFieldSpec = urlencode($this->fieldSpec);

        $this->response->method('getStatus')->willReturn($this->status);
        $this->response->method('isPaged')->willReturn($this->paged);
        $this->response->method('isPartial')->willReturn($this->partial);
        $this->response->method('getPartialFieldsSpec')->willReturn($this->fieldSpec);
        $this->response->method('getResourceOptions')->willReturn($this->resourceOptions);
        $this->response->method('getResourceName')->willReturn($this->resourceName);
        $this->response->method('getResponseLimit')->willReturn($this->limit);
        $this->response->method('getResponseOffset')->willReturn($this->offset);
        $this->response->method('getTotalObjects')->willReturn($this->totalObjects);
        $this->response->method('getResourceParams')->willReturn($this->resourceParams);

        $this->view->replace(array($this->objectName => $this->objects));

        $this->view->apiRender($this->response);

        $body = $this->view->getResponseBody();

        $this->assertTrue(is_array($body));
        $this->assertEquals(9, count(array_keys($body)));
        $this->assertTrue(array_key_exists('data', $body));
        $this->assertTrue(array_key_exists('status', $body));
        $this->assertTrue(array_key_exists('error', $body));
        $this->assertTrue(array_key_exists('total', $body));
        $this->assertTrue(array_key_exists('currentUrl', $body));
        $this->assertTrue(array_key_exists('firstUrl', $body));
        $this->assertTrue(array_key_exists('lastUrl', $body));
        $this->assertTrue(array_key_exists('nextUrl', $body));
        $this->assertTrue(array_key_exists('prevUrl', $body));
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $body['status']);
        $this->assertFalse($body['error']);
        $this->assertEquals($this->totalObjects, $body['total']);

        $this->assertTrue(strpos($body['currentUrl'], 'fields=' . $expectedFieldSpec) !== false);
        $this->assertTrue(strpos($body['firstUrl'], 'fields=' . $expectedFieldSpec) !== false);
        $this->assertTrue(strpos($body['lastUrl'], 'fields=' . $expectedFieldSpec) !== false);
        $this->assertTrue(strpos($body['nextUrl'], 'fields=' . $expectedFieldSpec) !== false);
        $this->assertTrue(strpos($body['prevUrl'], 'fields=' . $expectedFieldSpec) !== false);

    }

}
