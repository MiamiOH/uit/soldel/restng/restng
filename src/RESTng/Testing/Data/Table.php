<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:56 PM
 */

namespace MiamiOH\RESTng\Testing\Data;

/**
 * Class Table
 * @package MiamiOH\RESTng\Testing\Data
 *
 * @codeCoverageIgnore
 */
 abstract class Table
{

    /**
     * @var DatabaseInterface
     */
    protected $database;

    protected $options = [];

    public function __construct(DatabaseInterface $database, array $options = [])
    {
        $this->database = $database;
        $this->options = $options;
    }

    abstract public function create(): void;
    abstract public function drop(): void;
    abstract public function populate(): void;
}