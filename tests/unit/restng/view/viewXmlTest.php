<?php

include_once('abstract.php');

class viewXmlTest extends viewAbstract {

  public function setUp(): void
  {

    parent::setUp();

    $this->view = new \MiamiOH\RESTng\View\XML();

    \MiamiOH\RESTng\View\XML::$stopAfterRender = false;

    $this->view->setAppInstance($this->app);

    $this->objects = array(
        array('id' => 1, 'name' => 'test name'),
        array('id' => 2, 'name' => 'test name'),
        array('id' => 3, 'name' => 'test name'),
        array('id' => 4, 'name' => 'test name'),
        array('id' => 5, 'name' => 'test name'),
        array('id' => 6, 'name' => 'test name'),
        array('id' => 7, 'name' => 'test name'),
        array('id' => 8, 'name' => 'test name'),
        array('id' => 9, 'name' => 'test name'),
        array('id' => 10, 'name' => 'test name'),
      );
    $this->resourceName = 'test.resource';
    $this->status = \MiamiOH\RESTng\App::API_OK;

  }

  public function testViewXml() {

    $this->callback = '';

    $this->response->method('getStatus')->willReturn($this->status);
    $this->response->method('getResponseCallback')->willReturn($this->callback);

    $this->view->replace(array($this->objectName => $this->objects));

    $body = $this->view->apiRender($this->response);

    $backup = libxml_disable_entity_loader(true);
    $bodyObj = json_decode((string)json_encode(simplexml_load_string($body)), TRUE);
    libxml_disable_entity_loader($backup);

    $this->assertTrue(is_array($bodyObj));

  }

}
