<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/2/18
 * Time: 4:00 PM
 */

namespace MiamiOH\RESTng\Testing;


use Slim\Router;

/**
 * The only purpose of this class to alter the behavior of the base Slim object
 * during testing. The Slim object creates a Router singleton which remembers
 * resolved routes and will not resolve new routes. There is no obvious way
 * to force a reload externally. The bootstrap script sets this class as the
 * Router during testing before any routes are loaded.
 *
 * @codeCoverageIgnore
 */
class TestRouter extends Router
{
    public function getMatchedRoutes($httpMethod, $resourceUri, $reload = true)
    {
        return parent::getMatchedRoutes($httpMethod, $resourceUri, $reload);
    }
}
