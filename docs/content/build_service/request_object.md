+++
title = "Request Object"
weight = 30
+++

The request object provided to a service method will be constructed by RESTng.  The request will be an object of class \MiamiOH\RESTng\Util\Request or a sub-class.  This will likely change to be enforced through an interface. There should be very little reason to extend the Request class, though.

The service method must access request data only through the request object and should not retain any state from previous requests.

You get the request in your resource using:

    $request = $this->getRequest();

## Resource Parameters

Resource parameters are defined in the resource pattern and are provided by name in the request object.  Given the pattern ‘/course/section/:term/:crn’, the request will contain the resource parameters ‘term’ and ‘crn’.

You may retrieve individual parameters using the method:

    $value = $request->getResourceParam(‘name’[, ‘default’]);

The second parameter is optional and will be returned as the value if the parameter was not provided.  This may be useful in the event that you have two resources that are implemented with the same method but different parameters.

You may retrieve an array of all parameters using the method:

    $params = $request->getResourceParams();

## Request Options

Request options are provided by the consumer in addition to resource parameters.  In RESTng, request options are the key/value pairs of the query string.  For example:

	/people?department=eco

The request will provide the department value as an option, found in the options array returned by:

	$options = $request->getOptions();

You **must** check for the existence of an option key prior to access it. If an option is not defined as required, the consumer may leave it out of the request. Attempting to access the option would result in an undefined index exception.

## Request data

Request data is the representation of the object provided during a create or update action from the consumer. RESTng will have converted the incoming data format to an associative array.

[NOTE - RESTng may be enhanced to allow creating and updating of multiple objects in a single call, which will have a direct impact on how service methods treat the data.]

This data may be retrieved using:

    $data = $request->getData();

The request object will also provide information about framework features, such as paged results, partial results and embedded objects.  These features are covered in later sections.
