<?php

namespace MiamiOH\RESTng\Connector;

use MiamiOH\RESTng\Exception\DataSourceNotFound;

class DataSourceFactory
{

    /** @var DataSourceProviderInterface[] */
    private $providers = [];

    public function hasProvider(): bool
    {
        return !empty($this->providers);
    }

    public function addProvider(DataSourceProviderInterface $provider): void
    {
        $this->providers[] = $provider;
    }

    public function getDataSources(): array
    {
        $dataSources = [];

        if (empty($this->providers)) {
            return $dataSources;
        }

        foreach ($this->providers as $provider) {
            $dataSources[] = $provider->getDataSources();
        }

        return array_merge(...$dataSources);
    }

    public function getDataSource(string $name): DataSource
    {
        $datasource = null;
        foreach ($this->providers as $provider) {
            if (!$provider->hasDataSource($name)) {
                continue;
            }

            $datasource = $provider->getDataSource($name);
            break;
        }

        if ($datasource === null) {
            throw new DataSourceNotFound("No data source named $name could be found");
        }

        return $datasource;
    }

    public function hasDataSource(string $name): bool
    {
        foreach ($this->providers as $provider) {
            if ($provider->hasDataSource($name)) {
                return true;
            }
        }

        return false;
    }
}
