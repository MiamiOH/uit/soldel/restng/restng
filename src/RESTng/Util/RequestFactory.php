<?php

namespace MiamiOH\RESTng\Util;

class RequestFactory
{
    /**
     * @return Request
     */
    public function newRequest()
    {
        return new Request();
    }
}
