<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/17/18
 * Time: 9:24 PM
 */

namespace Tests\unit\restng\connector;

use MiamiOH\RESTng\Connector\EncryptionSimple;
use Tests\UnitTestCase;

class EncryptionSimpleUnitTest extends UnitTestCase
{

    public function testCanCorrectlyDecryptValue(): void
    {
        $encryptor = new EncryptionSimple();

        $this->assertEquals('Hello123', $encryptor->decrypt(str_rot13('Hello123')));
    }

    public function testCanCorrectlyEncryptValue(): void
    {
        $encryptor = new EncryptionSimple();

        $this->assertEquals(str_rot13('Hello123'), $encryptor->encrypt('Hello123'));
    }

}
