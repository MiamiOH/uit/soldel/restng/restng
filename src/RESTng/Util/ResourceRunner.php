<?php

namespace MiamiOH\RESTng\Util;

use Illuminate\Cache\Repository;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Observer\ObservableInterface;
use MiamiOH\RESTng\Observer\Observer;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Service\Apm\Transaction;

class ResourceRunner implements ObservableInterface
{

    static public $underTest = false;

    /** @var App */
    private $app;

    /** @var \Logger */
    private $logger;

    /** @var Repository */
    private $cache;

    /** @var Observer[] */
    private $observers = [];

    private $lastResource = [];

    /**
     * @param $app
     */
    public function setAppInstance($app)
    {
        $this->app = $app;

        $this->logger = \Logger::getLogger(__CLASS__);
    }

    public function getAppInstance()
    {
        return $this->app;
    }

    public function setCache(Repository $cache): void
    {
        $this->cache = $cache;
    }

    public function attach(Observer $observer): void
    {
        $this->observers[] = $observer;
    }

    private function notifyObservers(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update();
        }
    }

    /**
     * @param $config
     * @throws \Exception
     */
    public function run($config)
    {
        /** @var Transaction $transaction */
        $transaction = $this->app->getService('ApmTransaction');
        $transaction->setName($config['action'] . ' ' . $config['pattern']);

        $prepareSpan = $transaction->startSpan('Prepare');

        $this->handleCors($config);

        $this->resetLastResource();

        /*
            Use the route and request objects to build a consistent options object
            to pass into the resource method.
        */
        try {
            /** @var Service $r */
            $r = $this->app->getService($config['service']);
        } catch (\Aura\Di\Exception\ReflectionFailure $e) {
            throw new \Exception('Tried to inject service class ' . $e->getMessage() .
                ' but it looks like we could not find it. Is that the right class name for ' .
                $config['service'] . '?');
        } catch (\Exception $e) {
            throw new \Exception('Error loading service ' . $config['service'] .
                '. The object could not be instanciated (' . $e->getMessage() . ')');
        }

        if (!is_a($r, 'MiamiOH\RESTng\Service')) {
            throw new \Exception('Service object for ' . $config['service'] . ' must be a descendent of "MiamiOH\RESTng\Service');
        }

        $servReq = $this->app->newRequest($config['name']);
        $servReq->setResourceConfig($config);

        $currentRoute = $this->app->router()->getCurrentRoute();

        // Set our resource params (from the URL pattern)
        $servReq->setResourceParams($currentRoute->getParams());

        // Set our options from GET data
        $servReq->extractRequestOptions($this->app->request->params());

        $servReq->setResourceName($currentRoute->getName());

        /*
         * If the action is one that expects a body, determine the data representation
         * and call the appropriate setter on the request.
         */
        if (in_array($config['action'], array('create', 'update', 'delete'))) {
            /*
             * The dataRepresentation should be set in the resource config and never be empty by the
             * time we get here.
             */
            if (empty($config['dataRepresentation'])) {
                throw new \Exception('Missing dataRepresentation');
            }

            switch ($config['dataRepresentation']) {
                case 'array':
                    $servReq->setDataAsArray($this->app->request->getBody());
                    break;

                case 'file':
                    // TODO if the dataRepresentation is file, add meta data as well
                    $servReq->setDataAsFile($this->app->request->getBody());
                    break;

                default:
                    throw new \Exception('Unknown dataRepresentation: ' . $this->resourceConfig['dataRepresentation']);
            }
        }

        $method = $config['method'];

        if (!method_exists($r, $method)) {
            throw new \Exception('Service ' . get_class($r) . ' does not have a method ' . $method);
        }

        $r->setApp($this->app);
        $r->setResponse($this->app->newResponse());
        $r->setRequest($servReq);
        $r->setCache($this->cache);
        $r->setApmTransaction($transaction);


        /** @var \MiamiOH\RESTng\Util\User $apiUser */
        $apiUser = $this->app->getService('APIUser');
        $transaction->setUserContext($apiUser);
        $transaction->setRequestContext($servReq);

        $prepareSpan->stop();

        $resourceSpan = $transaction->startSpan('Resource');

        $servResp = $this->runResourceMethod($r, $method, $this->cacheOptions($config));

        $resourceSpan->stop();

        // We likely will not regain control after apiRender, so notify observers now
        $this->updateLastResource('action', $config['action']);
        $this->updateLastResource('serviceClass', get_class($r));
        $this->updateLastResource('method', $method);
        $this->updateLastResource('routeName', $currentRoute->getName());
        $this->updateLastResource('routePattern', $currentRoute->getPattern());
        $this->notifyObservers();

        $this->app->apiRender($servResp);

    }

    private function runResourceMethod(Service $service, string $method, array $options): Response
    {
        // Return immediately if the cache is not enabled
        if (!$options['enabled']) {
            return $service->$method();
        }

        $keyMethod = $options['keyMethod'];
        /** @var CacheKey $cacheKey */
        $cacheKey = $service->$keyMethod();

        // If the service declines to provide a key for the request, return without caching
        if ($cacheKey->isNotValid()) {
            return $service->$method();
        }

        if ($this->cache->has($cacheKey->key())) {
            $this->logger->info(sprintf('Cache hit for %s (%s)', $cacheKey->format(), $cacheKey->key()));

            return $this->cache->get($cacheKey->key());
        } else {
            $this->logger->info(sprintf('Cache miss for %s (%s)', $cacheKey->format(), $cacheKey->key()));

            $response = $service->$method();

            if (in_array($response->getStatus(), $options['cacheResponses'])) {
                $this->cache->put($cacheKey->key(), $response, $options['ttl']);
            }

            return $response;
        }
    }

    private function cacheOptions(array $options): array
    {
        return array_merge([
            'enabled' => false,
            'ttl' => env('CACHE_TTL', 900),
            'keyMethod' => null,
            'cacheResponses' => []
        ], $options['cache'] ?? []);
    }

    private function resetLastResource(): void
    {
        $this->lastResource = [];
    }

    private function updateLastResource(string $observed, $value): void
    {
        $this->lastResource[$observed] = $value;
    }

    public function lastResource(): array
    {
        return $this->lastResource;
    }

    /**
     * @param $config
     * @return bool
     * @throws \Exception
     */
    public function handleCors($config)
    {

        $logger = \Logger::getLogger(__CLASS__);

        // The xOrgSec index should always be part of the config. If it's missing
        // throw an exception.
        if (!(isset($config['xOrgSec']) && is_array($config['xOrgSec']))) {
            throw new \Exception('Missing or non-array xOrgSec (CORS) value for resource ' . $config['name']);
        }

        $env = $this->app->environment;

        if (isset($env['HTTP_ORIGIN'])) {
            if (!$config['xOrgSec']['allow-origin'] || $config['xOrgSec']['allow-origin'] === 'any'
                || in_array(strtolower($env['HTTP_ORIGIN']), $config['xOrgSec']['allow-origin'])
            ) {

                $this->app->response()->header('Access-Control-Allow-Origin', $env['HTTP_ORIGIN']);
                $logger->debug('Allow origin: ' . $env['HTTP_ORIGIN']);

                if ($config['xOrgSec']['allow-credentials']) {
                    $this->app->response()->header('Access-Control-Allow-Credentials', 'true');
                    $logger->debug('Allow credentials: true');
                }

                $this->app->response()->header('Access-Control-Max-Age', $config['xOrgSec']['max-age']);
                $logger->debug('Max age: ' . $config['xOrgSec']['max-age']);

            }

        }

        // Handle CORS preflight for the resource
        if ($this->app->request->isOptions()) {
            $logger->debug('Responding to CORS OPTIONS request');

            if (isset($env['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                $allowMethods = '';
                if (isset($config['xOrgSec']['allow-methods']) && is_array($config['xOrgSec']['allow-methods'])) {
                    $allowMethods = implode(', ', $config['xOrgSec']['allow-methods']);
                }

                if ($allowMethods) {
                    $this->app->response()->header('Access-Control-Allow-Methods', $allowMethods);
                    $logger->debug('Allow methods: ' . $allowMethods);
                }
            }

            if (isset($env['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                $allowHeaders = '';
                if (isset($config['xOrgSec']['allow-headers']) && is_array($config['xOrgSec']['allow-headers'])) {
                    $allowHeaders = implode(', ', $config['xOrgSec']['allow-headers']);
                } else {
                    $allowHeaders = $env['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'];
                }

                if ($allowHeaders) {
                    $this->app->response()->header('Access-Control-Allow-Headers', $allowHeaders);
                    $logger->debug('Allow headers: ' . $allowHeaders);
                }
            }

            if (self::$underTest) {
                return true;
            } else {
                $this->app->halt(200);
            }
        }

    }

}
