<?php

class mwContentFormatGetTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testContentSetupGetDefault() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionJsonAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupGetExtensionXmlAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

}
