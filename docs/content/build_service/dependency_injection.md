+++
title = "Dependency Injection"
weight = 40
+++

RESTng is a platform that is expected to host many API resources. In order to manage those effectively, RESTng lazy loads only configurations necessery to fulfill the current request. As services are registered, they are added to the RESTng dependency injection container. Dependency injection (DI) is a means by which a class asks for dependencies and the outer controlling framework provides them. DI shields your service from needing to know the implementation of service, making your code much easier to maintain and much more testable.
 
For example, RESTng provides a service, APIDatabaseFactory, which is used to get a new database handle. If you need to connect to a database in your business service, you can ask that the APIDatabaseFactory service be injected into your class. By using Dependency Injection, you can also write easily maintained, independent unit tests by providing a mocked implementation of APIDatabaseFactory to your class during testing, something that is not easily done if you instantiate an object yourself.

**Caution** Dependency injection should be limited to framework provided utility services and services shared within a common package. Publicly documented interfaces should be observed and serve as a contract for the consumption of services. DI should not be used to gain access to API resources. Please see the section “Consuming Other Resources” to see how you can make requests to registered resources.

## Configuring Injection

There are two methods your service can use to declare and accept dependencies.  The first method is through constructor parameters and the second is through the use of setter methods. The two methods can be used together in the same service. Both methods are equally effective, but keep in mind that setters are called after an object is created, so if you need dependencies early in your object's life, such as during contructions, you should inject them with a constructor parameter.

Constructor parameters are specified using the params key in the addService configuration array.  Each key in the value array must be a parameter listed for your class constructor.  The DI container uses introspection to identify the dependencies by name and inject the proper object.

Dependencies for setter methods are specified using the set key in the addService configuration array.  Each key in the value array will be mapped to a setter method by uppercasing the key and appending ‘set’ to generate the method name.

Here’s an example of both constructor and setter dependency declarations for injecting the APIDatabaseFactory service:

{{< highlight php "linenos=table" >}}
<?php
...
$app->addService(array(
   'name' => 'ServiceName',
   'class' => 'RESTng\Service\Service\ServiceName',
   'params' => array(
      'database' => array('name' => 'APIDatabaseFactory')),
   'set' => array(
      'database' => array('name' => 'APIDatabaseFactory'),
    ));
{{< / highlight >}} 

The methods in your class would be written as:

{{< highlight php "linenos=table" >}}
<?php
...
public function __construct($database) {
    $this->database = $database;
}

public function setDatabase($database) {
    $this->database = $database;
}
{{< / highlight >}} 

Please remember that services are singleton objects.  This has implications both for creating and consuming services.  The consumer should only interact with the service through a public interface and the service should use appropriate scoping to enforce this access.  The service must not store state information unless the role of the service includes stateful storage for the framework, such as the APIUser service. Services must expect to be called multiple times in a single consumer request.
