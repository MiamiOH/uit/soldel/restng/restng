<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/7/18
 * Time: 7:08 AM
 */

namespace MiamiOH\RESTng\Connector;


use MiamiOH\RESTng\Exception\InvalidArgumentException;

class DataSource
{
    public const NEW_CONNECTION_TYPE = 'new';
    public const PERSISTENT_CONNECTION_TYPE = 'persistent';

    private const TYPES = [
        'MySQL',
        'OCI8',
        'PostgreSQL',
        'MSSQL',
        'SQLite3',
        'LDAP',
        'LDAPng',
        'Other',
    ];

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $user;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $database;
    /**
     * @var string
     */
    private $connectType;
    /**
     * @var array
     */
    private $options;
    /**
     * @var int
     */
    private $port;

    private function __construct(
        string $name,
        string $type,
        string $user,
        string $password,
        string $host,
        string $database,
        string $connectType,
        array $options,
        int $port = null
    )
    {
        $this->ensureName($name);
        $this->ensureType($type);
        $this->ensureConnectType($connectType);

        $this->name = $name;
        $this->type = $type;
        $this->user = $user;
        $this->password = $password;
        $this->host = $host;
        $this->database = $database;
        $this->connectType = $connectType;
        $this->options = $options;
        $this->port = $port;
    }

    public static function fromArray(array $data): self
    {
        $arguments = [];
        $arguments[] = $data['name'] ?? '';
        $arguments[] = $data['type'] ?? '';
        $arguments[] = $data['user'] ?? '';
        $arguments[] = $data['password'] ?? '';
        $arguments[] = $data['host'] ?? '';
        $arguments[] = $data['database'] ?? '';
        $arguments[] = $data['connectType'] ?? self::NEW_CONNECTION_TYPE;
        $arguments[] = $data['options'] ?? [];
        if (array_key_exists('port', $data)) {
            $arguments[] = $data['port'];
        }

        return new self(...$arguments);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getDatabase(): string
    {
        return $this->database;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function getConnectType(): string
    {
        return $this->connectType;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    private function ensureName(string $name): void
    {
        if (empty($name)) {
            throw new InvalidArgumentException('DataSource name is required');
        }
    }

    private function ensureType(string $type): void
    {
        if (empty($type)) {
            throw new InvalidArgumentException('DataSource type is required');
        }

        if (!in_array($type, self::TYPES)) {
            throw new InvalidArgumentException('DataSource type ' . $type . ' is not supported');
        }
    }

    private function ensureConnectType(string $connectType): void
    {
        if (!in_array($connectType, [self::NEW_CONNECTION_TYPE, self::PERSISTENT_CONNECTION_TYPE])) {
            throw new InvalidArgumentException(sprintf('Unsupported connect type: %s', $connectType));
        }
    }
}