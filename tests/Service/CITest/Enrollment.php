<?php

namespace Tests\Service\CITest;

class Enrollment extends \MiamiOH\RESTng\Service {

  private $database = '';
  private $dbh = '';

  public function setDatabase ($database) {
    $this->database = $database;

    $this->dbh = $this->database->getHandle('authorize');
    $this->dbh->mu_trigger_error = false;
  }

  public function getEnrollment () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $options = $request->getOptions();

    $whereConditions = array();
    if (isset($options['classId']) && $options['classId']) {
      $whereConditions[] = 'class_id in (' . implode(', ',
          array_map(function($s) { return "'$s'"; }, $options['classId'])) . ')';
    }

    if (isset($options['studentId']) && $options['studentId']) {
      $whereConditions[] = 'student_id in (' . implode(', ', $options['studentId']) . ')';
    }

    $whereClause = $whereConditions ? ' and ' . implode(' and ', $whereConditions) : '';

    $query = "
          select enrollment_id, class_id, student_id, mid_term_grade,
              final_grade
            from enrollment
            where 1 = 1 $whereClause
        ";
    $sth = '';

    $records = array();

    if ($request->isPaged()) {
      $minRowToFetch = $request->getOffset();
      $maxRowToFetch = $minRowToFetch + $request->getLimit() - 1;

      // paged queries should include the total number of records
      $totalRecords = $this->dbh->queryfirstcolumn("
          select count(*)
            from enrollment
            where 1 = 1 $whereClause
        ");

      $response->setTotalObjects($totalRecords);

      if ($this->dbh->getType() == 'MySQL') {
        $query .= ' limit ' . ($request->getOffset() - 1) . ', ' . $request->getLimit();
        $sth = $this->dbh->prepare($query);
      } else {
        $query = "
          select enrollment_id, class_id, student_id, mid_term_grade,
              final_grade
            from ( select /*+ FIRST_ROWS(n) */
              a.*, ROWNUM rnum
                from ( $query ) a
                where ROWNUM <= :MAX_ROW_TO_FETCH )
          where rnum  >= :MIN_ROW_TO_FETCH";

        $sth = $this->dbh->prepare($query);

        $sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
        $sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
      }

    } else {
      $sth = $this->dbh->prepare($query);
    }

    $sth->execute();

    while ($record = $sth->fetchrow_assoc()) {
      $record = $this->camelCaseKeys($record);

      $studentResponse = $this->callResource('citest.student.public.studentId', array(
        'params' => array('studentId' => 'sid=' . $record['studentId']),
        ));

      $record['student'] = $studentResponse->getPayload();

      $records[] = $record;
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($records);

    return $response;
  }

}
