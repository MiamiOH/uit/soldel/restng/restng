+++
title = "Deploy to RESTng"
weight = 50
+++

At this point, your resource should be operational, but is not yet available through RESTng. For our Quick Start, we will assume that you have a running instance of RESTng (on a local VM for example). We will assume RESTng is installed to it's default location of `/var/www/restng`. The deployment to a test or production server is somewhat different and will be covered later.

## Add Your AutoLoader

RESTng relies on the composer autoloader files to find your classes. PHP allows the use of multiple autoloaders and you can register them with RESTng. Edit the file `/var/www/restng/config/autoloaders.yaml` and add your autoloader:

{{< highlight yaml "linenos=table" >}}
---
autoload:
  - /path/to/project/vendor/autoload.php
{{< / highlight >}} 

## Add Your Resource Configuration

RESTng will use your resource configuration to match incoming requests to your ResourceProvider classes. You tell RESTng how to find your resource configurations by adding them to the `/var/www/restng/config/providers.yaml` file:

{{< highlight yaml "linenos=table" >}}
---
resources:
  - /path/to/project/resources.php
{{< / highlight >}} 

## Validate with Swagger

After adding the autoloader and resource provider to your working instance of RESTng, you should be able to visit the corresponding Swagger UI page to see and test your resources.