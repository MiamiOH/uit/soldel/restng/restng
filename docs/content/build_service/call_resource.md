+++
title = "Calling Resources"
weight = 50
+++

Any service should represent the smallest possible object and provide publicly available resources.  Small, independent objects result in highly reusable, loosely coupled code.  The resources can be requested by other services and aggregated to produce a more complex object.  For example, a course section object may contain a course, term, instructor, location, schedule and even a collection of students.

The power of service oriented design is the ability to reuse services with minimal developer effort. Once a resource is registered, other developers can browse the documentation to learn how to consume it within their own services. The \MiamiOH\RESTng\Service class provides a method named callResource to facilitate resource consumption. The callResource method takes the desired resource name and an array of arguments to use when making the request.

For example, our CourseSection service needs an instructor, which is provided by the ‘course.instructor.term.crn’ resource:

{{< highlight php "linenos=table" >}}
<?php
...
class CourseSection ...
...
public function getCourseSection() {
    $request = $this->getRequest();
    $termId = $request->getResourceParam('term');
    $crn = $request->getResourceParam('crn');
    ...
    $instructors = $this->callResource(
         'course.instructor.crn.term', array(
           'params' => array('crn' => $crn, 'term' => $termId)));

    $courses[$crn]['instructors'] = $instructors->getPayload();
    ...
{{< / highlight >}} 

The returned payload should follow the same specification as your service and may be directly included in your response or used for your own business purpose.

## Using callResource

The array sent using callResource is made up of three parts (all are optional). The first is the **params** which correlate to the parameters in the URL. The next is **options** which correlates to options given after the question mark in the URL. The last is **data** which corresponds to the body of the request to be sent. An example of a call resource with all parameters is as follows:

{{< highlight php "linenos=table" >}}
<?php
...
$results = $this->callResource(
     'NAME_OF_RESOURCE', array(
       'params'  => array(...),
       'options' => array(...), 
       'data'    => $body)
   );
{{< / highlight >}} 

## Resource Authorization

Please note that any authorization configuration defined for the resource being called will be enforced when using callResource. The authorization will be attempted using the current API user object. In some cases, you may need to consume another resource using a different user. In this case, you can push another user into the API user object and use it when calling the other service. For example:

{{< highlight php "linenos=table" >}}
<?php
...
$user = $this->getApiUser();
$localUserKey = $user->addUserByCredentials(array(
   'username' => $username,
   'password' => $password
));
{{< / highlight >}} 

If you do not get a user key back, the authentication failed and you should take appropriate action.

If successful, the newly added user will be set as the current user. You may want to remove your user after completing your resource transaction:

    $user->removeUser($localUserKey);
