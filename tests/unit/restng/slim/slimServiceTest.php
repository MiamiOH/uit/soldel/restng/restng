<?php

class slimServiceTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $restngApp;
  private $diObj;
  private $registeredServices = array();

  public function setUp(): void
  {
    parent::setUp();

    $this->restngApp = new \MiamiOH\RESTng\App();

    $this->diObj = $this->getMockBuilder('\Aura\Di')
          ->setMethods(array('get', 'set', 'lazyNew', 'lazyGet'))
          ->getMock();

    /*
        We will use the real Response class in this case. If we wanted to enforce
        specific behaviors of the response, we could mock it as well.
    */
    $this->diObj->method('get')->will($this->returnCallback(array($this, 'getService')));
    $this->diObj->method('set')->will($this->returnCallback(array($this, 'setService')));
    $this->diObj->method('lazyNew')->will($this->returnCallback(array($this, 'lazyNewObject')));
    $this->diObj->method('lazyGet')->will($this->returnCallback(array($this, 'lazyGetObject')));

    $this->registeredServices = array();
  }

  public function testSetDiContainer() {
    $this->restngApp->setDiContainer($this->diObj);
    $this->assertEquals($this->diObj, $this->restngApp->getDiContainer());
  }

  // This raises the question of what getService should return. Right now, it returns
  // whatever the Aura/DI object returns. Maybe we should be more explicit.
  public function testGetServiceNotFound() {
    $this->restngApp->setDiContainer($this->diObj);

    $response = $this->restngApp->getService('TestServiceNotThere');

    $this->assertFalse($response);
  }

  public function testGetObjectFromDiContainer() {
    $this->restngApp->setDiContainer($this->diObj);

    $object = $this->restngApp->getService('TestServiceThere');

    $this->assertTrue(is_object($object));
    $this->assertTrue(get_class($object) === 'stdClass');
  }

  public function testAddServiceRequiresArray() {
    $this->restngApp->setDiContainer($this->diObj);

    $this->expectException(\Exception::class);

    $this->restngApp->addService('');
  }

  public function testAddServiceRequiresName() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array();

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);
  }

  public function testAddServiceRequiresClass() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);
  }

  public function testAddServiceRequiresDescription() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);
  }

  public function testAddServiceSimple() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
      );

    $this->restngApp->addService($serviceConfig);

    $object = $this->restngApp->getService('TestService');

    $this->assertTrue(is_object($object));
    $this->assertTrue(get_class($object) === 'stdClass');
  }

  public function testAddServiceSetOptionIsAnArray() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => '',
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceSetOptionUnknownType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'database' => array('type' => 'invalid'),
          ),
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceSetOptionMissingName() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'database' => array('class' => 'Database'),
          ),
      );

    $this->expectException(\Exception::class);

    $response = $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceSetOptionDefaultType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'database' => array('name' => 'Database'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceSetOptionServiceType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'database' => array('type' => 'service', 'name' => 'Database'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceSetOptionScalarTypeNoValue() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'scalarThing' => array('type' => 'scalar'),
          ),
      );

    $this->expectException(\Exception::class);

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceSetOptionScalarType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'scalarThing' => array('type' => 'scalar', 'value' => 'bob'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceSetOptionArrayType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'set' => array(
            'arrayThing' => array('type' => 'array', 'value' => array()),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceParamsOptionIsAnArray() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => '',
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceParamsOptionUnknownType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'database' => array('type' => 'invalid'),
          ),
      );

    $this->expectException(\Exception::class);

    $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceParamsOptionMissingName() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'database' => array('class' => 'Database'),
          ),
      );

    $this->expectException(\Exception::class);

    $response = $this->restngApp->addService($serviceConfig);

  }

  public function testAddServiceParamsOptionDefaultType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'database' => array('name' => 'Database'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceParamsOptionServiceType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'database' => array('type' => 'service', 'name' => 'Database'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceParamsOptionScalarTypeNoValue() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'scalarThing' => array('type' => 'scalar'),
          ),
      );

    $this->expectException(\Exception::class);

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceParamsOptionScalarType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'scalarThing' => array('type' => 'scalar', 'value' => 'bob'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testAddServiceParamsOptionArrayType() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
        'params' => array(
            'arrayThing' => array('type' => 'array'),
          ),
      );

    $response = $this->restngApp->addService($serviceConfig);

    $this->assertTrue($response);

  }

  public function testGetServices() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
      );

    $response = $this->restngApp->addService($serviceConfig);

    $serviceConfig = array(
        'name' => 'TestService2',
        'description' => 'This is another test service',
        'class' => 'stdClass',
      );

    $response = $this->restngApp->addService($serviceConfig);

    $services = $this->restngApp->getServices();

    $this->assertTrue(array_key_exists('TestService', $services));
    $this->assertTrue(array_key_exists('TestService2', $services));
    $this->assertEquals(2, count($services));
  }

  public function testGetServiceByName() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => 'stdClass',
      );

    $response = $this->restngApp->addService($serviceConfig);

    $serviceConfig = array(
        'name' => 'TestService2',
        'description' => 'This is another test service',
        'class' => 'stdClass',
      );

    $response = $this->restngApp->addService($serviceConfig);

    $service = $this->restngApp->getServiceByName('TestService2');

    $this->assertTrue(array_key_exists('name', $service));
    $this->assertEquals('TestService2', $service['name']);
  }

  public function testGetUseServiceAsReplacement() {
    $this->restngApp->setDiContainer($this->diObj);

    $serviceConfig1 = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => '\Tests\Service\CITest\Book',
      );

    $serviceConfig2 = array(
        'name' => 'TestService',
        'description' => 'This is a test service',
        'class' => '\Tests\Service\CITest\Course',
      );

    $this->restngApp->addService($serviceConfig1);
    $service = $this->restngApp->getServiceByName('TestService');
    $this->assertEquals('\Tests\Service\CITest\Book', $service['class']);

    $this->restngApp->useService($serviceConfig2);
    $service = $this->restngApp->getServiceByName('TestService');
    $this->assertEquals('\Tests\Service\CITest\Course', $service['class']);
  }

  // Callback methods to respond to mocked DI calls

  public function setService($name, $classObject) {
    $this->registeredServices[$name] = $classObject;
  }

  public function getService($name) {
    if ($name === 'TestServiceThere') {
      return new stdClass();
    }

    if (isset($this->registeredServices[$name])) {
      return $this->registeredServices[$name];
    }

    return false;
  }

  public function lazyNewObject($className) {
    return new $className();
  }

  public function lazyGetObject($className) {
    return function() { new $className(); };
  }

}
