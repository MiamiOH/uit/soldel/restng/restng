+++
title = "Define a Resource"
weight = 30
+++

## Add a Service

Services provide the methods to handle requests and produce responses. (We'll go over the actual class implementation later.) A service is registered with the RESTng container and will be lazy created when needed. Register your service in your ResourceProvider as follows:

{{< highlight php "linenos=table" >}}
<?php
...
    public function registerServices(): void
    {
        $this->addService([
            'name' => 'MyService',
            'class' => RESTng\RESTng\MyProject\MyService::class,
            'description' => 'Provides weekday data.'
        ]);
    }
...
{{< / highlight >}} 

The *name* can be any value, but should be unique within RESTng. A descriptive name related to your project will help prevent future name collisions. 

The *class* is the full namespaced path to your class and allows RESTng to locate and load your class.

## Add Resources

Our next step will be to define the REST endpoints (aka resources) which will be exposed via HTTP. The following resource is a simple GET for a single data model

{{< highlight php "linenos=table" >}}
<?php
...
    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'A simple data model from my project.',
            'name' => 'myresource.get.id',
            'pattern' => '/myresource/:id',
            'service' => 'MyService',
            'method' => 'getById',
        ]);
    }
...
{{< / highlight >}} 

The **action** for your service must be one of 'read', 'create', 'update' or 'delete', each of which maps to one of the HTTP methods. Your service should also have a meaningful **description**, which may contain GFM Syntax (https://help.github.com/articles/github-flavored-markdown/).

The resource definition will provide RESTng the information necessary to match a request to your resource. This is done via the **name** and **pattern** attributes. The first component of each value must be the configuration name you used when setting up resource configuration.

The **service** attribute must refer to the service name you registered in your provider. The **method** must be a valid method within the service class.
