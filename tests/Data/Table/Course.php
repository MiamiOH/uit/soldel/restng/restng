<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:57 PM
 */

namespace Tests\Data\Table;


use MiamiOH\RESTng\Testing\Data\Table;

class Course extends Table
{

    public function create(): void
    {
        $this->drop();

        $this->database->exec('
            create table if not exists course (
                course_id int primary key,
                course_subject text,
                course_number text,
                description text,
                credits int,
                mp_category text,
                technical int
            )
        ');

        $this->database->exec('create index course_course_subject on course (course_subject)');
        $this->database->exec('create index course_mp_category on course (mp_category)');
        $this->database->exec('create index course_technical on course (technical)');
    }

    public function drop(): void
    {
        $this->database->exec('drop table if exists course');
    }

    public function populate(): void
    {
    }
}