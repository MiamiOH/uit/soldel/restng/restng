<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:58 PM
 */

namespace MiamiOH\RESTng\Testing\Data\Database;


use MiamiOH\RESTng\Testing\Data\DatabaseInterface;
use SQLite3;

/**
 * Class Sqlite
 * @package MiamiOH\RESTng\Testing\Data\Database
 *
 * @codeCoverageIgnore
 */
class Sqlite implements DatabaseInterface
{
    /** @var SQLite3 */
    private $handle;

    public function __construct(SQLite3 $db)
    {
        $this->handle = $db;
        $this->handle->enableExceptions(true);
    }

    public function query(string $query)
    {
        return $this->handle->query($query);
    }

    public function exec(string $query): bool
    {
        return $this->handle->exec($query);
    }
}