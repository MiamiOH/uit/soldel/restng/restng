<?php

/*
 * Each entry point (index.php, 'something like artisan', phpunit) is responsible
 * for setting up the environment (like the autoloader) and calling bootstrap.
 * Everything that is common is done in bootstrap.
 */

if (file_exists('../config/autoloaders.yaml')) {
    $autoloaders = yaml_parse_file('../config/autoloaders.yaml');
    if (!isset($autoloaders['autoload'])) {
        throw new \Exception('Missing "autoload" key in autoloaders.yaml');
    }
    foreach ($autoloaders['autoload'] as $autoloader) {
        require($autoloader);
    }
}

require_once('../vendor/autoload.php');

/** @var \MiamiOH\RESTng\App $app */
$app = require('../bootstrap.php');

/** @var \MiamiOH\RESTng\Service\Apm\Transaction $transaction */
$transaction = $app->getService('ApmTransaction');
$span = $transaction->startSpan('Http setup', 'RESTng');

$app->addRouteMiddleware('authenticate', new \MiamiOH\RESTng\RouteMiddleware\Authentication(), 1);
$app->addRouteMiddleware('authorize', new \MiamiOH\RESTng\RouteMiddleware\Authorization(), 2);

// The Content Types middleware handles incoming content format, parsing
// xml or json body content. Middleware is executed in reverse, so the last
// added is the first called.
$app->add(new MiamiOH\RESTng\Middleware\ContentBody());
$app->add(new MiamiOH\RESTng\Middleware\ContentFormat());

try {
    $app->loadConfigForResourcePattern($app->environment['PATH_INFO']);
} catch (\Exception $e) {
    $app->handleException($e);
}

$span->stop();

// Run the Slim app within it's own try block.
try {
    $app->run();
} catch (\MiamiOH\RESTng\Exception\ApmException $e) {
    $apmLogger = Logger::getLogger('api.bootstrap');
    $apmLogger->error($e);
} catch (\Exception $e) {
    $app->handleException($e);
}
