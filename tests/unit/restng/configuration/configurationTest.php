<?php

class configurationTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $configuration;

  private $config = array();

  public function setUp(): void
  {
    parent::setUp();

    $this->config = array();

    $this->configuration = new \MiamiOH\RESTng\Util\Configuration();

  }

  public function testSetDatabase() {

    $database = $this->getMockBuilder('MiamiOH\RESTng\Connector\Database')
          ->getMock();

    $this->assertEquals('', $this->configuration->getDatabase());

    $setResp = $this->configuration->setDatabase($database);

    $this->assertEquals($database, $this->configuration->getDatabase());

  }

  public function testGetConfigurationMissingApplication() {

    $dbh = $this->getMockBuilder('\RESTng\Connector\Database\DBH')
            ->getMock();

    $database = $this->getMockBuilder('MiamiOH\RESTng\Connector\Database')
          ->setMethods(array('getHandle'))
          ->getMock();

    $database->method('getHandle')->willReturn($dbh);

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('getConfiguration requires a valid application');

      $setResp = $this->configuration->setDatabase($database);

    $config = $this->configuration->getConfiguration('');
  }

  public function testGetConfiguration() {

    $dbh = $this->getMockBuilder('\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_list'))
            ->getMock();

    $dbh->method('queryall_list')->will($this->returnCallback(array($this, 'getConfigQuery')));

    $database = $this->getMockBuilder('MiamiOH\RESTng\Connector\Database')
          ->setMethods(array('getHandle'))
          ->getMock();

    $database->method('getHandle')->willReturn($dbh);

    $setResp = $this->configuration->setDatabase($database);

    $config = $this->configuration->getConfiguration('testapp');

    $this->assertEquals($this->config, $config);

  }

  public function testGetConfigurationWithCategory() {

    $dbh = $this->getMockBuilder('\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_list'))
            ->getMock();

    $dbh->method('queryall_list')->will($this->returnCallback(array($this, 'getConfigQuery')));

    $database = $this->getMockBuilder('MiamiOH\RESTng\Connector\Database')
          ->setMethods(array('getHandle'))
          ->getMock();

    $database->method('getHandle')->willReturn($dbh);

    $setResp = $this->configuration->setDatabase($database);

    $config = $this->configuration->getConfiguration('testapp', 'testcategory');

    $this->assertEquals($this->config, $config);

  }

  public function getConfigQuery($query, $params) {

    if (count($params) === 1) {
      $this->config = array('testapp.string.1' => 'This is a test string');
    } elseif (count($params) === 2) {
      $this->config = array('testapp.category.string.1' => 'This is a test string in a category');
    } else {
      $this->config = array();
    }

    return $this->config;
  }
}
