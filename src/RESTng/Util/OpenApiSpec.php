<?php

namespace MiamiOH\RESTng\Util;

use MiamiOH\RESTng\App;

class OpenApiSpec
{
    /** @var App $api */
    protected $app;

    private $paths;
    private $components;
    private $tags;

    private $methodMap = [
        'read' => 'get',
        'create' => 'post',
        'update' => 'put',
        'delete' => 'delete',
    ];

    private $securityScheme = 'api_key';

    /** @var array */
    private $path;

    /**
     * @param $app
     */
    public function setApp(App $app)
    {
        $this->app = $app;
    }

    public function makeOpenApiSpec(): string
    {
        $this->initialize();
        $this->processTags();
        $this->processDefinitions();
        $this->processResources();

        $openapi = [
            'openapi' => '3.0.0',
            'info' => [
                'title' => 'RESTng 2 installed resources',
                'description' => 'RESTng 2 installed resources',
                'contact' => [
                    'email' => 'tepeds@miamioh.edu',
                ],
                'version' => '2.0.0',
            ],
            'servers' => [
                [
                    'url' => $this->baseServerUrl(),
                ],
            ],
            'paths' => $this->paths,
            'components' => $this->components,
            'tags' => $this->prepareTags(),
        ];

        return yaml_emit($openapi);
    }

    private function baseServerUrl(): string
    {
        $url = sprintf('https://%s', $_SERVER['HTTP_HOST']);
        if ($this->app->isStagePriorTo('development')) {
            return $url;
        }
        return $url . '/api';
    }

    private function initialize(): void
    {
        $this->paths = [];
        $this->components = [
            'schemas' => [],
            'responses' => [],
            'parameters' => [],
            'requestBodies' => [],
            'securitySchemes' => [
                'API Key' => [
                    'type' => 'apiKey',
                    'description' => 'Miami authentication token',
                    'name' => $this->securityScheme,
                    'in' => 'header',
                ],
            ],
        ];
        $this->tags = [];
    }

    private function processTags(): void
    {
        $response = $this->app->callResource('api.tag');
        $this->tags = array_reduce($response->getPayload(), function (array $c, array $tagData) {
            $tagData['name'] = ucfirst($tagData['name']);
            $c[] = $tagData;
            return $c;
        }, []);
    }

    private function processDefinitions(): void
    {
        $response = $this->app->callResource('api.definition');
        $definitions = $response->getPayload();

        // Convert paths from Swagger 2 to OpenAPI 3
        // TODO don't put paths in definitions
        array_walk_recursive($definitions, function (&$item, $key) {
            if ($key === '$ref') {
                $item = str_replace('definitions', 'components/schemas', $item);
            }
        });

        // Add each definition as both the RESTng response and the bare object. This allows
        // displaying the proper RESTng response while also referencing the data object by name.
        $this->components['schemas'] = array_reduce(array_keys($definitions), function (array $c, string $key) use ($definitions) {
            $c[$key . '.response'] = $this->getRESTngResponseSchema($definitions[$key]);
            $c[$key] = $definitions[$key];
            return $c;
        }, []);
    }

    private function processResources(): void
    {
        $response = $this->app->callResource('api.resource');
        $resources = $response->getPayload();

        foreach ($resources as $resource) {
            $path = $this->makePathData($resource);

            $name = preg_replace('/:(\w+)/', '{$1}', $resource['pattern']);

            $method = $this->methodMap[$resource['action']];

            $this->paths[$name][$method] = $path;
        }
    }

    private function addTags(array $tags): void
    {
        foreach ($tags as $tag) {
            if ($this->hasTagName($tag)) {
                continue;
            }

            $this->tags[] = [
                'name' => $tag,
            ];
        }
    }

    private function hasTagName(string $name): bool
    {
        foreach ($this->tags as $tag) {
            if ($tag['name'] === $name) {
                return true;
            }
        }

        return false;
    }

    private function prepareTags(): array
    {
        usort($this->tags, function ($a, $b) {
            if ($a['name'] == $b['name']) {
                return 0;
            }
            return ($a['name'] < $b['name']) ? -1 : 1;
        });

        return $this->tags;
    }

    /**
     * @param $resource
     * @return array
     */
    private function makePathData(array $resource): array
    {
        $this->path = [];

        $this->extractTags($resource);

        $this->extractInfo($resource);

        $this->extractParameters($resource);

        $this->extractAuthentication($resource);

        $this->extractPageable($resource);

        $this->extractPartialable($resource);

        $this->extractComposable($resource);

        $this->extractRequestBody($resource);

        $this->extractRequestResponses($resource);

        return $this->path;
    }

    private function makeMediaType(array $config): array
    {
        if (empty($config)) {
            return [];
        }

        if (!empty($config['$ref'])) {
            $ref = $config['$ref'];
        } elseif (!empty($config['schema']['$ref'])) {
            $ref = $config['schema']['$ref'];
        } else {
            return [];
        }

        $ref = str_replace('definitions', 'components/schemas', $ref) . '.response';

        return [
            'application/json' => [
                'schema' => ['$ref' => $ref],
            ],
        ];
    }

    private function getRESTngResponseSchema(?array $data): array
    {
        $response = [
            'type' => 'object',
            'properties' => [
                'status' => [
                    'type' => 'integer',
                ],
                'error' => [
                    'type' => 'string',
                ],
            ],
        ];

        if ($data !== null) {
            $response['properties'] = array_merge(['data' => $data], $response['properties']);
        }

        return $response;
    }

    private function extractTags(array $resource): void
    {
        // Tag the path with the first part of the resource name
        if (isset($resource['tags'])) {
            $tags = $resource['tags'];
        } else {
            $tags = [
                substr_replace($resource['name'], '', strpos($resource['name'], '.')),
            ];
        }

        $this->path['tags'] = array_map('ucfirst', $tags);

        $this->addTags($this->path['tags']);
    }

    private function extractInfo(array $resource): void
    {
        $this->path['description'] = $resource['description'];

        if (isset($resource['name'])) {
            $this->path['description'] .= "\n\n**Name: " . $resource['name'] . "**";
        }

        $this->path['summary'] = isset($resource['summary']) ? $resource['summary'] : $resource['description'];
    }

    private function extractParameters(array $resource): void
    {
        $this->path['parameters'] = [];

        $this->extractParameterArguments($resource);

        $this->extractParameterOptions($resource);
    }

    private function extractParameterArguments(array $resource): void
    {
        foreach (array_keys($resource['params']) as $paramName) {
            $param = [];
            $param['name'] = $paramName;
            $param['in'] = 'path';
            $param['description'] = $resource['params'][$paramName]['description'];
            $param['required'] = 'true';

            if (isset($resource['params'][$paramName]['alternateKeys'])) {
                $param['description'] .= "\n\n" . 'Default key: ' . $resource['params'][$paramName]['alternateKeys'][0];
                $param['description'] .= "\n\n" . 'Allowed alternate keys: ' .
                    implode(', ', $resource['params'][$paramName]['alternateKeys']);
            }

            $this->path['parameters'][] = $param;
        }
    }

    private function extractParameterOptions(array $resource): void
    {
        foreach (array_keys($resource['options']) as $paramName) {
            $param = [];
            $param['name'] = $paramName;
            $param['in'] = 'query';
            $param['description'] = $resource['options'][$paramName]['description'];
            $param['required'] = $resource['options'][$paramName]['required'] ? true : false;

            if (isset($resource['options'][$paramName]['default'])) {
                $param['default'] = $resource['options'][$paramName]['default'];
            }

            if (isset($resource['options'][$paramName]['enum'])) {
                $param['enum'] = $resource['options'][$paramName]['enum'];
            }

            $this->path['parameters'][] = $param;
        }
    }

    private function extractAuthentication(array $resource): void
    {
        if (isset($resource['middleware']['authenticate'])) {
            $this->path['security'] = [
                $this->securityScheme => [],
            ];
        }
    }

    private function extractPageable(array $resource): void
    {
        if (empty($resource['isPageable']) || !$resource['isPageable']) {
            return;
        }

        $limitDescription = 'The number of objects to return';

        if (isset($resource['defaultPageLimit'])) {
            $limitDescription .= "\n\nDefault Page Limit: " . $resource['defaultPageLimit'];
        }

        if (isset($resource['maxPageLimit'])) {
            $limitDescription .= "\n\nMaximum Page Limit: " . $resource['maxPageLimit'];
        }

        $this->path['parameters'][] = [
            'name' => 'limit',
            'in' => 'query',
            'description' => $limitDescription,
            'type' => 'integer',
        ];

        $this->path['parameters'][] = [
            'name' => 'offset',
            'in' => 'query',
            'description' => 'The number of the first object to return',
            'type' => 'integer',
        ];
    }

    private function extractPartialable(array $resource): void
    {
        if (empty($resource['isPartialable']) || !$resource['isPartialable']) {
            return;
        }

        $fields = $this->extractPartialableFields($resource);

        $this->path['parameters'][] = [
            'name' => 'fields',
            'in' => 'query',
            'description' => 'List of object fields and/or subobjects to include',
            'type' => 'array',
            'items' => [
                'type' => 'string',
            ],
            'enum' => $fields,
        ];
    }

    private function extractPartialableFields(array $resource): array
    {
        $fields = [];

        if (empty($resource['responses']) || empty($resource['responses'][App::API_OK])
            || empty($resource['responses'][App::API_OK]['returns'])) {
            return $fields;
        }

        $object = $this->extractResponseObject($resource);

        foreach (array_keys($object) as $field) {
            if (isset($object[$field]['$ref'])) {
                $fields[] = $field . '()';
            } else {
                $fields[] = $field;
            }
        }

        return $fields;
    }

    private function extractResponseObject(array $resource): array
    {
        $object = [];

        if (isset($resource['responses'][App::API_OK]['returns']['$ref'])) {
            if (isset($resource['responses'][App::API_OK]['returns']['type'])
                && $resource['responses'][App::API_OK]['returns']['type'] === 'model') {
                $ref = str_replace('#/definitions/', '', $resource['responses'][App::API_OK]['returns']['$ref']);
                if (isset($this->components['schemas'][$ref]['properties'])) {
                    $object = $this->components['schemas'][$ref]['properties'];
                }
            } else {
                $ref = str_replace('#/definitions/', '', $resource['responses'][App::API_OK]['returns']['$ref']);
                $ref = str_replace('#/definitions/', '', $this->components['schemas'][$ref]['items']['$ref']);
                if (isset($this->components['schemas'][$ref]['properties'])) {
                    $object = $this->components['schemas'][$ref]['properties'];
                }
            }
        } else {
            $object = $resource['responses'][App::API_OK]['returns']['properties'];
        }

        return $object;
    }

    private function extractComposable(array $resource): void
    {
        if (empty($resource['isComposable']) || !$resource['isComposable']) {
            return;
        }

        $subObjects = $this->extractComposableObjects($resource);

        $this->path['parameters'][] = [
            'name' => 'compose',
            'in' => 'query',
            'description' => 'List of subobjects to include',
            'type' => 'array',
            'items' => [
                'type' => 'string',
            ],
            'enum' => $subObjects,
        ];
    }

    private function extractComposableObjects(array $resource): array
    {
        $subObjects = [];

        if (empty($resource['responses']) || empty($resource['responses'][App::API_OK])
            || empty($resource['responses'][App::API_OK]['returns'])) {
            return $subObjects;
        }

        $object = $this->extractResponseObject($resource);

        foreach (array_keys($object) as $subObject) {
            if (isset($object[$subObject]['$ref'])) {
                $subObjects[] = $subObject;
            }
        }

        return $subObjects;
    }

    private function extractRequestBody(array $resource): void
    {
        if (empty($resource['body'])) {
            $this->path['requestBody'] = [];
            return;
        }

        $this->path['requestBody'] = [
            'content' => $this->makeMediaType($resource['body']['schema']),
        ];
    }

    private function extractRequestResponses(array $resource): void
    {
        if (empty($resource['responses'])) {
            $this->path['responses'] =  [];
            return;
        }

        $this->path['responses'] = [];

        foreach ($resource['responses'] as $status => $config) {
            if (empty($config['returns'])) {
                continue;
            }

            // TODO if returns is empty, use $status to get default

            $this->path['responses'][$status] = [
                'description' => $config['description'],
                'content' => $this->makeMediaType($config['returns']),
            ];
        }
    }
}
