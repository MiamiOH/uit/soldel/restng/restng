<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 9:34 PM
 */

namespace MiamiOH\RESTng\Service\API;


class ApiResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'RESTngAPI',
            'description' => 'Get information about RESTng configured resources'
        ));

        $this->addDefinition(array(
            'name' => 'RESTng.Message.PreconditionRequired',
            'type' => 'object',
            'properties' => array(
                'reason' => array('type' => 'string', 'description' => 'Reason that the precondition check failed'),
                'delay' => array('type' => 'integer', 'description' => 'Number of seconds to delay before retry'),
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Resource',
            'class' => 'MiamiOH\RESTng\Service\API\Resource',
            'description' => 'Provides access to the API resource and service configuration for documentation.',
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'api',
            'description' => 'Returns the entire API configuration.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api',
            'service' => 'Resource',
            'method' => 'getAPI',
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.resource',
            'description' => 'Returns all resource configurations from the API.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/resource',
            'service' => 'Resource',
            'method' => 'getResources',
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.service',
            'description' => 'Returns all service configurations from the API.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/service',
            'service' => 'Resource',
            'method' => 'getServices',
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.resource.name',
            'description' => 'Returns detailed information about the given API resource.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/resource/:name',
            'service' => 'Resource',
            'method' => 'getResource',
            'params' => array(
                'name' => array('description' => 'A resource name'),
            ),
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.service.name',
            'description' => 'Returns detailed information about the given API service.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/service/:name',
            'service' => 'Resource',
            'method' => 'getService',
            'params' => array(
                'name' => array('description' => 'A service name'),
            ),
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.tag',
            'description' => 'Returns all tags from the API.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/tag',
            'service' => 'Resource',
            'method' => 'getTags',
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'api.definition',
            'description' => 'Returns all definitions from the API.',
            'tags' => array('RESTngAPI'),
            'pattern' => '/api/definition',
            'service' => 'Resource',
            'method' => 'getDefinitions',
            'middleware' => array(),
        ));
    }
}