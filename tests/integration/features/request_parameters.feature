# Test that request options are correctly supported
Feature: Request parameters provide identifiers via the path
 As a developer using the RESTng API
 I want to provide request parameters
 In order to provide identifiers to the resource

 Scenario: Get a student record using the default key student id
   Given a REST client
   When I make a GET request for /citest/student/public/101402
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject has an element studentId equal to "101402"
   And the subject has an element uniqueId equal to "wat_wile"

Scenario: Get a student record using student uniqueid as an alternate key
  Given a REST client
  When I make a GET request for /citest/student/public/uid=wat_wile
  Then the HTTP status code is 200
  And the response can be parsed
  And I get the data element from the response object
  And the subject has an element studentId equal to "101402"
  And the subject has an element uniqueId equal to "wat_wile"

Scenario: Get a student record explicitly using the key student id
  Given a REST client
  When I make a GET request for /citest/student/public/sid=101402
  Then the HTTP status code is 200
  And the response can be parsed
  And I get the data element from the response object
  And the subject has an element studentId equal to "101402"
  And the subject has an element uniqueId equal to "wat_wile"

Scenario: Unknown alternate keys should cause an error
  Given a REST client
  When I make a GET request for /citest/student/public/gid=101402
  Then the HTTP status code is 500

