+++
title = "RESTng Development"
weight = 10
+++

You can use Docker for local development of RESTng. The project includes a `docker-compose.yml` file which will launch the necessary containers. We use a separate containers for the web server (nginx) and the PHP runtime (php-fpm) which replicates the likely deployment scenario. The RESTng source is mounted in both the nginx and the PHP-FPM containers so changes are immediately available.

Use `docker-compose` to start the containers:

```bash
$ docker-compose up -d
```

Docker will need to build the nginx container on the first run.

Once started, you should be able to access RESTng from `localhost` using the following URLs:

* https://localhost/api/docs/
* https://localhost/api/swagger-ui/
* https://localhost/api/path/to/resource/
