<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 9:21 PM
 */

namespace MiamiOH\RESTng\Service\CredentialValidator;


class File implements CredentialValidatorInterface
{
    private $credentials = ['tokens' => [], 'users' => []];

    public function setCredentialFile(string $filename): void
    {
        if (file_exists($filename)) {
            $this->setCredentials(file_get_contents($filename));
        }
    }

    public function setCredentials(string $credentialData): void
    {
        $data = yaml_parse($credentialData);

        $this->ensureData($data);

        $this->credentials = $data;
    }

    public function validateToken(string $token): array
    {
        if (!$this->hasToken($token)) {
            return [
                'valid' => false,
                'username' => null,
                'token' => null,
            ];
        }

        $tokenInfo = $this->getInfoForToken($token);

        return [
            'valid' => true,
            'username' => $tokenInfo['username'],
            'token' => $token,
        ];
    }

    public function validateUsernamePassword(string $username, string $password): array
    {
        if (!$this->credentialsAreValid($username, $password)) {
            return [
                'valid' => false,
                'username' => null,
                'token' => null,
            ];
        }

        $tokenInfo = $this->getTokenForUser($username);

        return [
            'valid' => true,
            'username' => $tokenInfo['username'],
            'token' => $tokenInfo['token'],
        ];
    }

    private function ensureData(array $data): void
    {
        if (!array_key_exists('tokens', $data)) {
            throw new \InvalidArgumentException('Credential data must contain "tokens"');
        }

        if (!array_key_exists('users', $data)) {
            throw new \InvalidArgumentException('Credential data must contain "users"');
        }

        if (!is_array($data['tokens'])) {
            throw new \InvalidArgumentException('Credential tokens must be an array');
        }

        if (!is_array($data['users'])) {
            throw new \InvalidArgumentException('Credential users must be an array');
        }
    }

    private function hasToken(string $token): bool
    {
        $found = $this->getInfoForToken($token);

        return $found !== null;
    }

    private function getInfoForToken(string $token): ?array
    {
        $found = array_filter(
            $this->credentials['tokens'],
            function ($tokenConfig) use ($token) {
                return $tokenConfig['token'] === $token;
            }
        );

        if (count($found) > 1) {
            throw new \Exception('Too many tokens found');
        }

        return array_shift($found);
    }

    private function credentialsAreValid(string $username, string $password): bool
    {
        $found = array_filter(
            $this->credentials['users'],
            function ($userConfig) use ($username, $password) {
                return $userConfig['username'] === $username && $userConfig['password'] === $password;
            }
        );

        return !empty($found);
    }

    private function getTokenForUser(string $username): array
    {
        $found = array_filter(
            $this->credentials['tokens'],
            function ($tokenConfig) use ($username) {
                return $tokenConfig['username'] === $username;
            }
        );

        if (count($found) > 1) {
            throw new \Exception('Too many users found');
        }

        return array_shift($found);
    }
}
