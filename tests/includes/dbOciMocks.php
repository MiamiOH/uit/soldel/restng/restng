<?php

namespace MiamiOH\RESTng\Legacy\DB\DBH;

include_once('stOciMocks.php');

class dbOciMockData {
  static private $mockData = array();

  static public function reset() {
    self::$mockData = array();

  }

  static public function set($key, $value) {
    self::$mockData[$key] = $value;

    // Expect arrays for some mock data. Make a copy in our local scope
    // so that function mocks can consume them in a loop.
    switch ($key) {
      case 'mockAttributeList':
        if (!is_array($value)) {
          throw new \Exception('Mock value for attributeList must be an array');
        }
        self::$attributeList = $value;
        break;

    }

  }

  static public function keyExists($key) {
    return array_key_exists($key, self::$mockData);
  }

  static public function get($key) {
    if (!isset(self::$mockData[$key])) {
      throw new \Exception('Mock data for ' . $key . ' not found');
    }

    return self::$mockData[$key];
  }
}

function oci_new_connect ($user, $password, $database) {

  dbOciMockData::set('connectType', 'new');

  dbOciMockData::set('user', $user);
  dbOciMockData::set('password', $password);
  dbOciMockData::set('database', $database);

  return dbOciMockData::get('mockConnectResult');
}

function oci_pconnect ($user, $password, $database) {

  dbOciMockData::set('connectType', 'persistent');

  dbOciMockData::set('user', $user);
  dbOciMockData::set('password', $password);
  dbOciMockData::set('database', $database);

  return dbOciMockData::get('mockConnectResult');
}

function oci_close($r) {
  dbOciMockData::set('closeResource', $r);
  return dbOciMockData::get('mockDisconnectResult');
}

function oci_error ($resource = '') {
  return dbOciMockData::get('mockOciError');
}

function oci_parse($db, $statement) {
  dbOciMockData::set('prepareStatement', $statement);
  return dbOciMockData::get('mockParseResult');
}

function oci_execute($statement, $mode = 0) {
  dbOciMockData::set('executeStatement', $statement);
  dbOciMockData::set('executeMode', $mode);
  return dbOciMockData::get('mockExecuteResult');
}

function oci_num_fields($r) {
  return dbOciMockData::get('mockNumFields');
}

