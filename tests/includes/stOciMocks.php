<?php

namespace MiamiOH\RESTng\Legacy\DB\STH;

/*
  These functions support classes in different namespaces, but we
  don't want to repeat all of the mocking. Simply call the higher
  level mocks and return the value.
*/

function oci_parse($db, $query) {
  return \MiamiOH\RESTng\Legacy\DB\DBH\oci_parse($db, $query);
}

function oci_execute($statement, $mode = 0) {
  return \MiamiOH\RESTng\Legacy\DB\DBH\oci_execute($statement, $mode);
}

function oci_num_fields($r) {
  return \MiamiOH\RESTng\Legacy\DB\DBH\oci_num_fields($r);
}
