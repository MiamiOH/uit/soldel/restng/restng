# RESTng

Please see the [docs](./docs) for RESTng information.

## Database Migration

RESTng does need a database for some operational work. We use [Phinx](https://phinx.org/) to manage migrations.

The only supported database is MariaDB at this time.

Migrations must be run manually using the phinx command:

```shell
vendor/bin/phinx migrate
```

The RPM packaging tends to replace the symlink above with a copy of phinx. This causes phinx to fail. The full command to run for test or production is:

```shell
sudo -u phpapps php vendor/robmorgan/phinx/bin/phinx migrate
```

Refer to the [migrate command docs](https://book.cakephp.org/phinx/0/en/commands.html#the-migrate-command) for more details.

Other [commands](https://book.cakephp.org/phinx/0/en/commands.html) are available.

The phinx configuration uses the `RESTng` datasource defined in `config/datasources.yaml`.
