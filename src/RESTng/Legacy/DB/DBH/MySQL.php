<?php

class MU_DBH_MySQL extends MU_DBH {

	var $default_date_format_db = 'YYYY-MM-DD HH:MM:SS';
	var $default_date_format_parse = '%Y-%m-%d %H:%M:%S';

	function MU_DBH_MySQL ($user = '', $password = '', $database = '', $host = '') {
		$this->type = 'MySQL';
		$this->database = $database;

		if ($user && $password) {
			$this->connect($user, $password, $host);
		}
	}

	function connect ($user = '', $password = '', $host = '', $database = '') {
		$this->connected = false;
		if ($database) {
			$this->database = $database;
		}

		if($this->db = mysql_pconnect($host, $user, $password) !== false) {
			$this->connected = true;
			$this->user = $user;
			$this->host = $host;
			if (!mysql_select_db($this->database)) {
				mu_trigger_error('Could not select database ' . $this->database );
				exit;
			}
		} else {
			mu_trigger_error('Connect to ' . $host . ' as ' . $user  . ' failed');
			exit;
		}

		$this->error = $this->connected ? DB_NO_ERROR : DB_ERROR;

		return $this->connected;
	}

	function disconnect () {
		$disconnected = false;
		if ($this->connected) {
			$disconnected = mysql_close($this->db);
		} else {
			$disconnected = true;
		}

		$this->error = $disconnected ? DB_NO_ERROR : DB_ERROR;

		return $disconnected;
	}

	function prepare ($statement = '') {
		if (!$statement) {
			mu_trigger_error('No SQL statement to prepare');
			exit;
		}

		$this->error = DB_NO_ERROR;

		return new MU_STH_MySQL($this, $statement);
	}

	function _run ($statement, $parameters = '') {

		if (!$statement) {
			mu_trigger_error('No SQL statement to run');
			exit;
		}

		$this->query = $this->_parse_placeholders($statement, $parameters);

		$this->result = mysql_query($this->query, $this->db);

		if ($this->result === false) {
			$success = false;
			$this->error = DB_ERROR;
			$this->catch_error();
		} else {
			$success = true;
			$this->error = DB_NO_ERROR;
		}

		$this->_track_statement($this->query);

		return $success;
	}

	function queryfirstrow_array ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$result_row = false;

		if ($this->_run($statement, $parameters)) {
			$result_row = mysql_fetch_row($this->result);
			if ($result_row === false) {
				$result_row = DB_EMPTY_SET;
			}
			$this->error = DB_NO_ERROR;
		}

		return $result_row;
	}

	function queryfirstrow_assoc ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$result_row = false;

		if ($this->_run($statement, $parameters)) {
			$result_row = mysql_fetch_assoc($this->result);
			if ($result_row === false) {
				$result_row = DB_EMPTY_SET;
			}
			$this->error = DB_NO_ERROR;
		}

		return is_array($result_row) ? array_change_key_case($result_row, CASE_LOWER) : $result_row;
	}

	function queryfirstcolumn ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$result_row = false;

		if ($this->_run($statement, $parameters)) {
			$result_row = mysql_fetch_row($this->result);
			if ($result_row === false) {
				$result_row = DB_EMPTY_SET;
			}
			$this->error = DB_NO_ERROR;
		}

		return is_array($result_row) ? $result_row[0] : $result_row;
	}

	function queryall_array ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}
		$result_row = false;
		$records = false;

		if ($this->_run($statement, $parameters)) {
			$records = array();
			$row_num = 0;
			while ($row = mysql_fetch_assoc($this->result)) {
				$row = array_change_key_case($row, CASE_LOWER);
				$records[$row_num] = array();
				foreach ($row as $name => $value) {
					$records[$row_num][$name] = $value;
				}
				$row_num++;
			}
			$this->error = DB_NO_ERROR;
		}

		return $records;
	}

	function queryall_assoc ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$records = false;

		if ($this->_run($statement, $parameters)) {
			$records = array();
			while ($row = mysql_fetch_assoc($this->result)) {
				$row = array_change_key_case($row, CASE_LOWER);
				$key = array_shift($row);
				$records[$key] = array();
				foreach ($row as $name => $value) {
					$records[$key][$name] = $value;
				}
			}
			$this->error = DB_NO_ERROR;
		}

		return $records;
	}

	function queryall_list ($statement) {
		$param_count = func_num_args();
		$parameters = array();
		for ($i = 1; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$records = false;

		if ($this->_run($statement, $parameters)) {
			$field_count = mysql_num_fields($this->result);

			switch ($field_count) {
				case (1):
					$list_type = 'numeric';
					break;
				case (2):
					$list_type = 'assoc';
					break;
				default:
					mu_trigger_error('Invalid column count for list');
					exit;
			}

			$records = array();
			while ($row = mysql_fetch_row($this->result)) {
				if ($list_type == 'numeric') {
					$records[] = $row[0];
				} else {
					$records[$row[0]] = $row[1];
				}
			}
			$this->error = DB_NO_ERROR;
		}

		return $records;
	}

	function convert ($conversion, $target, $format = '') {
		$convert = '';

		switch ($conversion) {
			case 'uppercase':
				$convert = 'upper(' . $target . ')';
				break;

			case 'lowercase':
				$convert = 'lower(' . $target . ')';
				break;

			default:
				$convert = $target;
		}

		return $convert;
	}

	function last_insert_id () {
		return mysql_insert_id($this->db);
	}

	function escape ($string = '') {
		return mysql_escape_string($string);
	}

  public function catch_error ($error_num = false, $error_string = false) {
    if ($error_num === false ) {
      $error_num = $this->_error_num();
    }

    if ($error_string === false ) {
      $error_string = $this->_error();
    }

    unset($this->error_string);

    if ($error_num !== false) {
      $this->error_string = 'Error number: ' . $error_num;
    }

    if ($error_string !== false) {
      if (isset($this->error_string)) {
        $this->error_string .= ' - ';
      }
      $this->error_string .= $error_string;
    }

    throw new \Exception("Query: " . $this->query . "\n\n" . $this->error_string);
  }

	function _error () {
		return mysql_error($this->db);
	}

	function _error_num () {
		return mysql_errno($this->db);
	}

	function commit () {
		return true;
	}

	function rollback () {
		return false;
	}

}

class MU_STH_MySQL extends MU_STH {

	function MU_STH_MySQL (&$dbh, $statement = '') {
		$this->dbh = $dbh;
		$this->auto_commit = $dbh->auto_commit;
		$this->statement = $statement;
	}

	function execute () {
		$param_count = func_num_args();
		$parameters = array();

		for ($i = 0; $i < $param_count; $i++) {
			$parameter = func_get_arg($i);
			if (is_array($parameter)) {
				$parameters = $parameter;
				break;
			} else {
				$parameters[] = $parameter;
			}
		}

		$this->query = $this->dbh->_parse_placeholders($this->statement, $parameters);

		$this->result = mysql_query($this->query, $this->dbh->db);

		$this->dbh->_track_statement($this->query);

		if ($this->result !== false) {
			$this->current_row = 1;
			if ($this->result !== true) {
				$this->column_count = mysql_num_fields($this->result);
			}
			$success = true;
		} else {
			$success = false;
			$this->catch_error();
		}

		$this->error = $success ? DB_NO_ERROR : DB_ERROR;

		return $success;
	}

	function commit () {
		return true;
	}

	function rollback () {
		return false;
	}

	function fetchrow_assoc () {
		$result = mysql_fetch_assoc($this->result);
		if ($result !== false) {
			$this->current_row++;
		} else {
			$this->current_row = false;
		}
		return is_array($result) ? array_change_key_case($result, CASE_LOWER) : $result;
	}

	function fetchrow_array () {
		$result = mysql_fetch_row($this->result);
		if ($result !== false) {
			$this->current_row++;
		} else {
			$this->current_row = false;
		}
		return $result;
	}

	function last_insert_id () {
		return mysql_insert_id($this->dbh);
	}

	function row_count () {
		return mysql_num_rows($this->result);
	}

	function free () {
		return mysql_free_result($this->result);
	}

  function catch_error ($error_num = false, $error_string = false) {
    if ($error_num === false ) {
      $error_num = $this->_error_num();
    }

    if ($error_string === false ) {
      $error_string = $this->_error();
    }

    unset($this->error_string);

    if ($error_num !== false) {
      $this->error_string = 'Error number: ' . $error_num;
    }

    if ($error_string !== false) {
      if (isset($this->error_string)) {
        $this->error_string .= ' - ';
      }
      $this->error_string .= $error_string;
    }

    if ($this->mu_trigger_error) {
      print "Query: " . $this->query . "\n\n";
      mu_trigger_error($this->error_string);
      exit;
    }
  }

  function _error () {
		return mysql_error($this->dbh->db);
	}

	function _error_num () {
		return mysql_errno($this->dbh->db);
	}

}

?>
