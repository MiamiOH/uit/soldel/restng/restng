+++
title = "REST Client"
weight = 50
+++

RESTng provides a REST client factory which can be injected into your service and used to retrieve a GuzzleHTTP client. Using a factory for this enables injection during testing.

In your resource configuration, define the service with a setter or param dependency of 'RESTClientFactory':

{{< highlight php "linenos=table" >}}
<?php
...
$app->addService(array(
   'name' => 'ServiceName',
   'class' => 'MiamiOH\RESTng\Service\Service\ServiceName',
   'set' => array(
      'restClientFactory' => array('name' => 'RESTClientFactory'),
    ));
{{< / highlight >}} 

Within your business service, capture the injected object for later use:

{{< highlight php "linenos=table" >}}
<?php
...
public function setRestClientFactory($factory) {
    $this->factory = $factory;
}
{{< / highlight >}} 

You can then have the factory create a new client with the appropriate base uri:

{{< highlight php "linenos=table" >}}
<?php
...
/** @var \GuzzleHttp\Client $restClient */
$restClient = $this->restClientFactory->newClient(['base_uri' =>$this->wsUrl]);

$response = $this->restClient->get('/some/url');
{{< / highlight >}} 
