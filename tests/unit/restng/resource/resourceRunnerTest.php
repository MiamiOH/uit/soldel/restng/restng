<?php

class resourceRunnerTest extends \MiamiOH\RESTng\Testing\TestCase
{
  private $resourceRunner;
  private $restngApp;
  private $service;
  /** @var \MiamiOH\RESTng\Service\Apm\Transaction */
  private $transaction;
  /** @var \MiamiOH\RESTng\Util\User */
  private $user;

  /** @var \Illuminate\Cache\Repository|\PHPUnit\Framework\MockObject\MockObject */
  private $cache;

  private $config = array();
  private $routeParams = array();
  private $isOptions = false;
  private $params = array();
  private $body = array();

  private $serviceExpected;

  public function setUp(): void
  {
    parent::setUp();

    $this->config = array(
      'action' => '',
      'name' => '',
      'service' => '',
      'method' => '',
      'xOrgSec' => array(),
    );
    $this->routeParams = array();
    $this->isOptions = false;
    $this->params = array();
    $this->body = array();

    $this->serviceExpected = '';

    $this->renderSubject = '';

    $this->transaction = $this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class);
    $this->user = $this->createMock(\MiamiOH\RESTng\Util\User::class);

    $this->service = $this->getMockBuilder('MiamiOH\RESTng\Service')
          ->setMethods(array('newRequest', 'businessMethod', 'setRequest'))
          ->getMock();

    $this->apiRequest = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
          ->setMethods(array('setResourceConfig', 'setResourceParams', 'extractRequestOptions',
            'setResourceName', 'setData', 'setRequest'))
          ->getMock();

    $this->apiResponse = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
          ->getMock();

    $this->route = $this->getMockBuilder('MiamiOH\RESTng\Slim\Route\Mock')
          ->setMethods(array('getParams', 'getName', 'getPattern'))
          ->getMock();

    $this->router = $this->getMockBuilder('MiamiOH\RESTng\Slim\Router\Mock')
          ->setMethods(array('getCurrentRoute'))
          ->getMock();

    $this->restngApp = $this->getMockBuilder('MiamiOH\RESTng\App')
          ->setMethods(array('newRequest', 'request', 'router', 'urlFor', 'getService', 'apiRender'))
          ->getMock();

    $this->restngApp->request = $this->getMockBuilder('MiamiOH\RESTng\Slim\Request\Mock')
          ->setMethods(array('isOptions', 'params', 'getBody'))
          ->getMock();

    $this->restngApp->expects($this->any())->method('newRequest')
      ->with($this->callback(array($this, 'newRequestWith')))
      ->will($this->returnCallback(array($this, 'newRequestMock')));
    $this->service->expects($this->any())->method('setRequest')
      ->with($this->callback(array($this, 'setRequestWith')));
    $this->service->expects($this->any())->method('businessMethod')
      ->will($this->returnCallback(array($this, 'businessMethodMock')));

    $this->apiRequest->expects($this->any())->method('setResourceConfig')
      ->with($this->callback(array($this, 'setResourceConfigWith')));
    $this->apiRequest->expects($this->any())->method('setResourceParams')->willReturn(true);
    $this->apiRequest->expects($this->any())->method('extractRequestOptions')
      ->with($this->callback(array($this, 'extractRequestOptionsWith')));
    $this->apiRequest->expects($this->any())->method('setResourceName')
      ->with($this->callback(array($this, 'setResourceNameWith')));
    $this->apiRequest->method('setData')
      ->with($this->callback(array($this, 'setDataWith')));

    $this->route->expects($this->any())->method('getParams')
      ->will($this->returnCallback(array($this, 'getParamsMock')));
    $this->route->expects($this->any())->method('getName')
      ->will($this->returnCallback(array($this, 'getNameMock')));

    $this->router->expects($this->any())->method('getCurrentRoute')->willReturn($this->route);

    $this->restngApp->request->expects($this->any())->method('isOptions')
      ->will($this->returnCallback(array($this, 'isOptionsMock')));
    $this->restngApp->request->expects($this->any())->method('params')
      ->will($this->returnCallback(array($this, 'paramsMock')));
    $this->restngApp->request->method('getBody')
      ->will($this->returnCallback(array($this, 'getBodyMock')));

    $this->restngApp->method('request')->willReturn($this->restngApp->request);
    $this->restngApp->method('router')->willReturn($this->router);
    $this->restngApp->expects($this->any())->method('getService')
      ->with($this->callback(array($this, 'getServiceWith')))
      ->will($this->returnCallback(array($this, 'getServiceMock')));
    $this->restngApp->expects($this->any())->method('apiRender')
      ->with($this->callback(array($this, 'apiRenderWith')));

    $this->restngApp->environment = array();

    $this->cache = $this->createMock(\Illuminate\Cache\Repository::class);

    $this->resourceRunner = new \MiamiOH\RESTng\Util\ResourceRunner();
    \MiamiOH\RESTng\Util\ResourceRunner::$underTest = true;

    $this->resourceRunner->setAppInstance($this->restngApp);
    $this->resourceRunner->setCache($this->cache);

  }

  public function testRun() {

    $this->assertNotNull($this->resourceRunner->getAppInstance());

    $this->config['action'] = 'read';
    $this->config['name'] = 'test.resource';
    $this->config['service'] = 'TestService';
    $this->config['method'] = 'businessMethod';
    $this->config['pattern'] = '/some/path';

    $this->resourceRunner->run($this->config);

  }

  public function testRunUnknownMethod() {

    $this->assertNotNull($this->resourceRunner->getAppInstance());

    $this->config['action'] = 'read';
    $this->config['name'] = 'test.resource';
    $this->config['service'] = 'TestService';
    $this->config['method'] = 'businessMethodMissing';
    $this->config['pattern'] = 'test/resource';

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('does not have a method businessMethodMissing');

      $this->resourceRunner->run($this->config);

  }

  public function isOptionsMock() {
    return $this->isOptions;
  }

  public function getServiceWith($subject) {
    $this->serviceExpected = $subject;

    // Skip checking the dependency services
    if (in_array($subject, ['ApmTransaction', 'APIUser', 'APICache'])) {
        return true;
    }

    $this->assertEquals($this->config['service'], $subject);

    return true;
  }

  public function getServiceMock() {
    if ($this->serviceExpected === 'ApmTransaction') {
        return $this->transaction;
    }

    if ($this->serviceExpected === 'APIUser') {
        return $this->user;
    }

    return $this->service;
  }

  public function newRequestWith($subject) {
    $this->assertEquals($this->config['name'], $subject);
    return true;
  }

  public function newRequestMock() {
    return $this->apiRequest;
  }

  public function setResourceConfigWith($subject) {
    $this->assertEquals($this->config, $subject);
    return true;
  }

  public function getParamsMock() {
    return $this->routeParams;
  }

  public function getNameMock() {
    return $this->config['name'];
  }

  public function paramsMock() {
    return $this->params;
  }

  public function extractRequestOptionsWith($subject) {
    $this->assertEquals($this->params, $subject);
    return true;
  }

  public function setResourceNameWith($subject) {
    $this->assertEquals($this->config['name'], $subject);
    return true;
  }

  public function getBodyMock() {
    return $this->body;
  }

  public function setDataWith($subject) {
    $this->assertEquals($this->body, $subject);
    return true;
  }

  public function setRequestWith($subject) {
    $this->assertEquals($this->apiRequest, $subject);
    return true;
  }

  public function businessMethodMock() {
    return $this->apiResponse;
  }

  public function apiRenderWith($subject) {
    $this->assertEquals($this->apiResponse, $subject);
    return true;
  }

}
