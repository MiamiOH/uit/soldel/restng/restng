# Test the basic crud functions
Feature: Create, Read, Update and Delete objects
 As a developer using the RESTng API
 I want to perform create, read, update and delete actions
 In order to manage business objects

 Background:
   Given an empty book table

 Scenario: Create a new book object using JSON data
   Given a REST client
   And that a book record with id <id> does not exist
   When I have a book with the title "<title>"
   And I have a book with the author "<author>"
   And I have a book with the publishDate "<publishDate>"
   And I have a book with the id "<id>"
   And I make a POST request for /citest/book
   Then the HTTP status code is 201
   And a book record with id <id> does exist
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element id equal to "<id>"
   And the subject has an element title equal to "<title>"
   And the subject has an element author equal to "<author>"
   And the subject has an element publishDate equal to "<publishDate>"
   Examples:
      | id | title            | author       | publishDate |
      | 1  | My Book Title    | Joan Smith   | 2014-03-11  |
      | 2  | My Book is Great | Bill Johnson | 2014-06-21  |
      | 3  | Your Book is Not | Frank Howard | 2013-12-21  |

 Scenario: Delete a book given a book id
   Given a REST client
   And a book record with the title "My Book"
   And a book record with the author "Bob Jones"
   And a book record with the publish_date "12-NOV-2013"
   And a book record with the id "1"
   And that the book record has been added
   And that a book record with id 1 does exist
   When I make a DELETE request for /citest/book/1
   Then the HTTP status code is 200
   And a book record with id 1 does not exist

 Scenario: Delete a collection of books
   Given a REST client
   And a book record with the title "My Book"
   And a book record with the author "Bob Jones"
   And a book record with the publish_date "12-NOV-2013"
   And a book record with the id "1"
   And that the book record has been added
   And that a book record with id 1 does exist
   And a book record with the title "My Other Book"
   And a book record with the author "Alice Smith"
   And a book record with the publish_date "12-MAY-2014"
   And a book record with the id "2"
   And that the book record has been added
   And that a book record with id 2 does exist
   When the request body is a collection
   And the request body contains an object with an id of "1"
   And the request body contains an object with an id of "2"
   And I make a DELETE request for /citest/book
   Then the HTTP status code is 200
   And a book record with id 1 does not exist
   And a book record with id 2 does not exist

  Scenario: Using an empty body results in an appropriate error
    Given a REST client
    When the request body is a collection
    And I make a POST request for /citest/book
    Then the HTTP status code is 400
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash

