+++
title = "Content Types"
weight = 20
+++

Successfully exchanging data with a REST resource requires that both parties agree on the data format being used. JSON has quickly become the defacto data format for REST APIs. In many cases, JavaScript is used to consume APIs, either on the client browser or on the server through NodeJS. JSON is a native format for JavaScript and this relationship has accelerated it's adoption. Any modern language or framework will have some means of converting to/from JSON.

RESTng assumes JSON is the data format for both responses is produces and requests it receives unless the consumer indicates otherwise. In addition to JSON, RESTng does offer basic support for XML, but there are limitations on the current data rendering in XML. The preferred data format is JSON.

The only supported method for indicating data format is the use of HTTP headers. The HTTP specification includes headers for both request payload and consumer preferred response payload format. This gives the consumer well defined mechanisms to articulate what format is being sent and should be returned.

**Note:** There is some support for specifying format using extensions in the URL in RESTng. However, this feature is deprecated and will be removed. [https://wiki.miamioh.edu/jira/browse/RESTNG-30]

## Response Data Format

GET and POST transactions with an API result in data being sent back in the response. RESTng will determine the format to use by looking at the HTTP Accept header. The value of the header is a comma separated list of MIME types in order of preference. The server (RESTng) will format the response using the first supported MIME type it finds in the list.

RESTng currently supports two formats, JSON and XML. The MIME types are:

Format  | MIME Type
--------|----------
JSON    | application/json
XML     | application/xml

The default format JSON will be used if no supported MIME type is found.

If you use Chrome to access a RESTng resource, the response will contain XML. The reason is the Accept header value Chrome sends with the request:

    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8

## Request Data Format

Create (POST) and update (PUT) actions require that the consumer include data in the request body. RESTng uses the Content-Type header to determine the format of the incoming data. The Content-Type header should be a single MIME type corresponding to the format of the data in the request body.

RESTng currently supports JSON and XML for incoming request data, using the same MIME types described for the respond data format.

RESTng will assume the data format is JSON if no Content-Type is provided, but it is best practice to always include the Content-Type header for clarity.