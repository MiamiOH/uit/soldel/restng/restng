+++
title = "Create a Provider"
weight = 20
+++

RESTng will call one or more ResourceProvider implementations in your project. ResourceProvider classes are where you register the resources, services and other components your project provides.

## Resource Provider

Create a new class which extends the `\MiamiOH\RESTng\Util\ResourceProvider` abstract class. You must implement each of the required methods, though you are not required to register components in each.

{{< highlight php "linenos=table" >}}
<?php

namespace MiamiOH\RESTng\MyProject;


class MyResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    public function registerDefinitions(): void
    {
        // ...
    }

    public function registerServices(): void
    {
        // ...
    }

    public function registerResources(): void
    {
        // ...
    }
}
{{< / highlight >}} 

We will cover registering resources and services in the next sections.

## Resource Configuration

RESTng requires a single integration point which will describe the mapping of resource requests (HTTP or callResource) to providers which can handle the request. You should create a single file in your project which returns an appropriate configuration array. The convention is to name this file `resources.php` and place it at the root level of your project. Example:

{{< highlight php "linenos=table" >}}
<?php

return [
    'resources' => [
        'myresource' => [
            \MiamiOH\RESTng\MyProject\MyResourceProvider::class,
        ],
    ]
];
{{< / highlight >}}

This will cause RESTng to create an instance of the MyResourceProvider class when a request for ```/myresource``` is made. 
