<?php

namespace MiamiOH\RESTng\Connector;

/**
 * @codeCoverageIgnore
 *
 * This just returns GuzzleHttp objects
 */
class RESTFactory
{

    /**
     * @param array $options
     * @return \GuzzleHttp\Client
     */
    public function newClient($options = array())
    {

        return new \GuzzleHttp\Client($options);

    }

}
