+++
title = "Body Content"
weight = 100
+++

REST resources frequently accept incoming data in the HTTP request body. RESTng provides two resource configuration options to specify the form of the body content.

## Data Representation

The data representation tells RESTng how to handle the body content. The default representation is 'array', which provides the traditional data to array handling provided by RESTng. The body is parsed from the incoming format (json or xml) and set in the Request object as a plain array. The data representation is set using the 'dataRepresentation' configuration key, for example:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'create',
    'name' => 'citest.book.create',
    'description' => 'Creates a new book object.',
    'pattern' => '/citest/book',
    'service' => 'CITestBook',
    'method' => 'addBook',
    'dataRepresentation' => 'array',
));
{{< / highlight >}} 

### File Data

Specifying 'file' as the data representation type will cause RESTng to assume the HTTP request body content represents the content of a file. The content will be set on the Request object without modification.

## Body Schema

The body schema defines details of the object represented in the HTTP request. The schema communicates expectations to the resource consumer. You can describe the expected body using the **body** configuration item:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'create',
    'name' => 'citest.book.create',
    'description' => 'Creates a new book object.',
    'pattern' => '/citest/book',
    'service' => 'CITestBook',
    'method' => 'addBook',
    'body' => array(
        'description' => 'A book object',
        'required' => true,
        'schema' => array(
            '$ref' => '#/definitions/CITest.Book'
        )
    ),
));
{{< / highlight >}} 

The value of the body configuration item is an array containing a description, required indicator and a schema definition. The schema definition should be a '$ref' to a previously defined model or collection. See the section on Data Models for more information.
