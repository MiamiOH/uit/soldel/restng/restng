+++
title = "Feature Testing"
weight = 6
+++

RESTng 2 introduces a new feature testing framework which enables full bootstrapping and request/response processing. Feature tests are **not** integration tests and do not operate on a running web server. However, each test does create and execute a fully operational RESTng application allowing the test to create a suitable request and examine the HTTP response object.

{{% alert theme="warning" %}}**Note** Feature testing is a new capability and the implementation is subject to change.{{% /alert %}}

## Defining Resources

The RESTng bootstrap process must be able to resolve the resources you are testing. This can be done using your project's resources.php file, but you must tell the RESTng loader where to find it. The RESTng loader uses an environment variable RESOURCE_PROVIDER_CONFIG to locate the list of resource providers to load. You can control the value in the PHPUnit configuration (phpunit.xml), for example:

    <env name="RESOURCE_PROVIDER_CONFIG" value="tests/resource-providers.yaml"/>

The content of that yaml file will tell RESTng how to find your resource configuration:

{{< highlight yaml "linenos=table" >}}
---
resources:
  - ../resources.php
{{< / highlight >}} 

## Create a Base Test Case

While optional, you should take the time to create a base test class and ensure all of your tests extend it. A base class is very handy for containing common fixtures and factories used throughout your tests.

{{< highlight php "linenos=table" >}}
<?php
namespace Tests;

abstract class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{

}
{{< / highlight >}} 

## Create a Feature Test

Feature tests must extend the `\MiamiOH\RESTng\Testing\TestCase` class. This class implements the application bootstrapping required for feature tests to run and provides numerous helper methods for executing tests. The RESTng feature testing framework is based on Illuminate (used in Laravel), so should be familiar to many people. The following is a simply, but complete, feature test.

{{< highlight php "linenos=table" >}}
<?php

namespace Tests\Feature;

use Tests\TestCase;

class RandomTest extends TestCase
{
    public function testCanGetRandomNumber(): void
    {
        $response = $this->getJson('/random/string');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'string'
                ],
            ]);

    }
}
{{< / highlight >}} 

Feature tests are run using PHPUnit. You can use the test selection features of PHPUnit, just as when running unit tests:

    vendor/bin/phpunit tests/Feature

## Organizing Tests

Feature tests should be automatible and run during the build process. However, unit and feature tests should be segregated and run separately. During the build we will want to generate code coverage for unit tests, but not enable coverage reports during feature tests for example. You can use PHPUnit testsuites, or rely on directory segregation.

## Using Mock Services

Unit tests are focused on a class and where appropriate, will provide mocks for objects outside of the class. Feature tests are testing the running application, but in many cases, mocks of components are still required. For example, feature tests should not communicate directly with a live database or other external resources. A well designed service can utilize mocks for these components during feature tests by manipulating the service container. 

The following example demonstrates the creation of a mock for a database and injecting it into the service container. The useService method will replace any existing implementation of the named service.

{{< highlight php "linenos=table" >}}
<?php
...
$ds = $this->getMockBuilder(\MiamiOH\RESTng\Connector\DatabaseFactory::class)
    ->setMethods(['getHandle'])
    ->getMock();

$this->dbh = $this->getMockBuilder(\MiamiOH\RESTng\Legacy\DB\DBH\OCI8::class)
    ->setMethods(['queryfirstcolumn'])
    ->getMock();

$ds->method('getHandle')->willReturn($this->dbh);

$this->app->useService([
    'name' => 'APIDatabaseFactory',
    'object' => $ds,
    'description' => 'Mock database factory',
]);
{{< / highlight >}} 

You will find that well designed classes which adhere to good design principles are much easier to test.
