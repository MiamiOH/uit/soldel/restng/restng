<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\RouteMiddleware\Authentication;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\Data\Table\Student;
use Tests\FeatureTestCase;
use MiamiOH\RESTng\Testing\TestResponse;

class ResourceAuthenticationTest extends FeatureTestCase
{
    use UsesDatasource;
    use UsesTestData;
    use UsesAuthentication;
    use UsesAuthorization;

    public function setUp(): void
    {
        parent::setUp();

        $db = $this->createSqlite3DatabaseHandle();

        $this->useData()
            ->withSqlite($db->getDbConnection())
            ->withTable(Student::class)
            ->populate();

        $this->installDatasourceService($db);

        $this->showExceptions();
    }

    public function testAuthenticatedResourceIsNotAuthorizedWithoutToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/citest/student/all/100574');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAuthenticatedResourceIsAuthorizedWithValidTokenInQueryString(): void
    {
        $this->withToken('abc123')->withoutAuthorizationHeader()->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/citest/student/all/100574?token=abc123');

        $this->assertRequestIsAuthorized($response);
    }

    public function testAuthenticatedResourceIsAuthorizedWithValidTokenInAuthorizationHeader(): void
    {
        $this->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/citest/student/all/100574');

        $this->assertRequestIsAuthorized($response);
    }

    /**
     * @dataProvider headerFormatChecks
     */
    public function testTokenIsExtractedFromSupportedAuthorizationHeaderFormats(string $format): void
    {
        $this->withAuthorizationHeaderFormat($format);
        $this->willAuthenticateUser();
        $this->willAuthorizeUser();

        $response = $this->getJson('/citest/student/all/100574');

        $this->assertRequestIsAuthorized($response);
    }

    public function headerFormatChecks(): array
    {
        return [
            'MU Token' => ['Token token=%s'],
            'Bearer' => ['Bearer %s'],
        ];
    }

    public function assertRequestIsAuthorized(TestResponse $response): void
    {
        $response->assertSuccessful();
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}
