+++
title = "Services"
weight = 50
+++

RESTng uses a service container to provide dependencies at run time. Core services have a default implementation of services which is suitable to start with. In some cases, you may need to add a new service or provide an alternate implementation of a service. You can do this in the services configuration.

The service configuration is found at:

```bash
config/services.yaml
```

The contents of the service configuration is a hash describing the services. The following yaml creates a service entry for the `Authentication` service implemented by the `AuthenticationWS` class.

```yaml
---
services:
  Authentication:
    class: \MiamiOH\RESTng\Service\Authentication\AuthenticationWS
    description: Provides authentication services using REST.
    set:
      restService:
        type: service
        name: RESTClientFactory
```

You can find a comprehensive example of service configuration in the RESTng core services file:

https://gitlab.com/MiamiOH/uit/soldel/restng/restng/-/blob/master/src/api-services.yaml
