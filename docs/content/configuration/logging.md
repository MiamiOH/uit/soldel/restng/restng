+++
title = "Logging"
weight = 60
+++

RESTng uses the Log4PHP library. Log4PHP allows a lot of control over message output and log levels. We use the standard PHP configuration.

The logging configuration is found at:

```bash
config/log4php.conf.php
```

One of the more useful aspects of the log configuration is the ability to change the log level for an individual logger. Because RESTng creates a logger for each service class, you can easily change the log level for any class which extends `Miamioh\RESTng\Service`, as shown in the following example.

{{< highlight php "linenos=table" >}}
<?php

Logger::configure(array(
    'rootLogger' => array(
        'appenders' => array('apiLogFile'),
        'level' => 'warn',
    ),

    'loggers' => array(
        'RESTng\Service\CITest\CITest' => array(
            'appenders' => array('apiLogFile'),
            'level' => 'debug',
        ),

    ),

    'appenders' => array(
        'apiLogFile' => array(
            'class' => 'LoggerAppenderFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%level %date [%logger] %message%newline%ex'
                )
            ),
            'params' => array(
                'file' => '/var/log/restng/api.log'
            )
        ),
    )
));
{{< / highlight >}} 
