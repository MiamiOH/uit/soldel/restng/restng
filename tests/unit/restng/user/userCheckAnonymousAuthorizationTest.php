<?php

class userCheckAnonymousAuthorizationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\User */
    private $user;

    /** @var \MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    /** @var \MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $authorizationValidator;

    /** @var \MiamiOH\RESTng\App|PHPUnit_Framework_MockObject_MockObject */
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->credentialValidator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => true, 'token' => 'abc123', 'username' => 'doej']);

        $this->authorizationValidator = $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        // This is ugly as all get out and needs refactored in the class.
        $this->restngApp = $this->getMockBuilder(\MiamiOH\RESTng\App::class)
            ->setMethods(['router', 'getCurrentRoute', 'getParams'])
            ->getMock();
        $this->restngApp->method('router')->willReturnSelf();
        $this->restngApp->method('getCurrentRoute')->willReturnSelf();

        $this->user = new \MiamiOH\RESTng\Util\User();

        $this->user->setApp($this->restngApp);
        $this->user->setCredentialValidator($this->credentialValidator);
        $this->user->setAuthorizationValidator($this->authorizationValidator);
        $this->user->setCache(new \Illuminate\Cache\Repository(new \Illuminate\Cache\ArrayStore()));
        $this->user->setApmTransaction($this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class));

    }

    public function testCheckAuthorizationAnonymous() {
        $this->authorizationValidator->expects($this->never())
            ->method('validateAuthorizationForKey');

        $this->restngApp->expects($this->never())->method('getParams');

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'anonymous',
            ],
        ]);

        $this->assertTrue($auth);

    }

    public function testCheckAuthorizationAnonymousWithUser() {
        $this->authorizationValidator->expects($this->once())
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $this->user->addUserByToken('abc123');

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'anonymous'
            ],
            [
                'application' => 'TestApplication',
                'module' => 'TestModule',
                'key' => 'TestKey',
            ]
        ]);

        $this->assertTrue($auth);

    }

}
