+++
title = "PHP Class"
weight = 10
+++

Your class must extend the \MiamiOH\RESTng\Service class to be considered a valid service.  A constructor is not required, but you may choose to use one for dependency injection.  There is no constructor in the \MiamiOH\RESTng\Service class, so you do not need to call any parent constructor.

Your class source file must begin with the opening PHP tag ‘<?php’ and should not include a closing tag. 

A complete starting example is:

{{< highlight php "linenos=table" >}}
<?php

namespace MiamiOH\RESTng\Service\MyProject;

class MyClass extends \MiamiOH\RESTng\Service {
    public function getStuff () {
        $request = $this->getRequest();
        $response = $this->getResponse();
        
        // Work is done here.
        $records = array();
        
        $response->setStatus(\MiamiOH\RESTng\API_OK);
        $response->setPayload($records);

        return $response;
    }
}
{{< / highlight >}} 

**Important!** RESTng uses a dependency injection container which lazy creates single objects. Your class must operate correctly under the singleton pattern.

## Namespaces and Autoloading

The source file organization and namespace of your project is up to you. RESTng >=1.2 has no expectations on that. However, you must use a proper composer autoloader configuration to resolve your classes. Your project will be bundled into a deployment package as a composer dependency and the final autoloader will be used by RESTng during runtime.

## Base Class

RESTng provides a base service class, \MiamiOH\RESTng\Service which you must extend when creating a service.  RESTng does not enforce this class inheritance. We expect to enforce behaviors by requiring the service class to implement a specific interface, but this is not in place at this time.  Any class which extends the \MiamiOH\RESTng\Service class will inherit the interface implementation.
