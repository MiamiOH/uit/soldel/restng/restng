<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/4/18
 * Time: 2:28 PM
 */

namespace MiamiOH\RESTng\Util;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Collection;
use MiamiOH\RESTng\App;

class ResourceLoader
{

    /**
     * @var array
     */
    private $resources;

    /** @var App */
    private $app;

    private $loaded = [];

    public function __construct(array $resources)
    {
        $this->resources = $resources;
    }

    public static function fromArray(array $config): ResourceLoader
    {

        $resources = $config['resources'] ?? [];

        return new ResourceLoader($resources);
    }

    public function loadResourceConfig(string $name): bool
    {
        if ($name && !isset($this->resources[$name])) {
            return false;
        }

        $toLoad = $name ? [$name] : array_keys($this->resources);

        foreach ($toLoad as $key) {
            if (isset($this->loaded[$key]) && $this->loaded[$key]) {
                continue;
            }

            foreach ($this->resources[$key] as $providerClass) {
                /** @var ResourceProviderInterface $provider */
                $provider = new $providerClass($this->app);
                $provider->registerServices();
                $provider->registerResources();
                $provider->registerDefinitions();
            }

            $this->loaded[$key] = true;
        }

        return true;
    }

    public function useApp(App $app): void
    {
        $this->app = $app;
    }

}