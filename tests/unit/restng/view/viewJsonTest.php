<?php

include_once('abstract.php');

class viewJsonTest extends viewAbstract {

  private $callback = '';

  public function setUp(): void
  {

    parent::setUp();

    $this->callback = '';

    $this->view = new \MiamiOH\RESTng\View\JSON();

    \MiamiOH\RESTng\View\JSON::$stopAfterRender = false;

    $this->view->setAppInstance($this->app);

    $this->objects = array(
        array('id' => 1, 'name' => 'test name'),
        array('id' => 2, 'name' => 'test name'),
        array('id' => 3, 'name' => 'test name'),
        array('id' => 4, 'name' => 'test name'),
        array('id' => 5, 'name' => 'test name'),
        array('id' => 6, 'name' => 'test name'),
        array('id' => 7, 'name' => 'test name'),
        array('id' => 8, 'name' => 'test name'),
        array('id' => 9, 'name' => 'test name'),
        array('id' => 10, 'name' => 'test name'),
      );
    $this->resourceName = 'test.resource';
    $this->status = \MiamiOH\RESTng\App::API_OK;

  }

  public function testViewJson() {

    $this->callback = '';

    $this->response->method('getStatus')->willReturn($this->status);
    $this->response->method('getResponseCallback')->willReturn($this->callback);

    $this->view->replace(array($this->objectName => $this->objects));

    $body = $this->view->apiRender($this->response);

    $bodyObj = json_decode($body, TRUE);

    $this->assertTrue(is_array($bodyObj));

  }

  public function testViewJsonP() {

    $this->callback = 'jsonpFunction';

    $this->response->method('getStatus')->willReturn($this->status);
    $this->response->method('getResponseCallback')->willReturn($this->callback);

    $this->view->replace(array($this->objectName => $this->objects));

    $body = $this->view->apiRender($this->response);

    $this->assertTrue(strpos($body, $this->callback) === 0);

  }

}
