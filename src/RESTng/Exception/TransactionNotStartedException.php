<?php

namespace MiamiOH\RESTng\Exception;

class TransactionNotStartedException extends ApmException
{
}
