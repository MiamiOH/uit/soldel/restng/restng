<?php

class mwContentFormatDeleteTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testMwContentSetupDeleteDefault() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertNull($this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeJsonAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeXmlAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionJsonAndContentTypeXmlAndAcceptHeaderXml() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.json',
                'HTTP_ACCEPT' => 'application/xml',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupDeleteExtensionXmlAndContentTypeJsonAndAcceptHeaderJson() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'DELETE',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service.xml',
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

}
