<?php

namespace MiamiOH\RESTng\View;

use MiamiOH\RESTng\Service\Apm\Transaction;

class Base extends \Slim\View
{
    // Our views must sub-class this for rendering

    public static $stopAfterRender = true;

    protected $app;

    protected $contentType = 'text/plain';

    /** @var \MiamiOH\RESTng\Util|Response $apiResponse */
    protected $apiResponse = '';

    protected $responseBody = array();

    protected $log = '';

    /**
     * @param $app
     */
    public function setAppInstance($app)
    {
        $this->app = $app->getInstance();
    }

    /**
     * @return \MiamiOH\RESTng\Util\AppInstance
     */
    public function getAppInstance()
    {
        return $this->app;
    }

    /**
     * @return array
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * @param $apiResponse
     * @return mixed
     */
    public function apiRender($apiResponse)
    {
        /** @var Transaction $transaction */
        $transaction = $this->app->getService('ApmTransaction');
        $span = $transaction->startSpan('Render', $this->contentType);

        $this->log = \Logger::getLogger(__CLASS__);

        /** @var \MiamiOH\RESTng\Util\Response $apiResponse */
        $this->apiResponse = $apiResponse;

        $status = (int) $apiResponse->getStatus();

        $this->app->response()->setStatus($status);
        $this->app->response()->header('Content-Type', $this->contentType);

        $this->responseBody = $this->all();

        //append status code
        $this->responseBody['status'] = $status;

        //append error bool
        if ($this->has('error') || in_array($status, array(\MiamiOH\RESTng\App::API_FAILED))) {
            $this->responseBody['error'] = true;
        } else {
            $this->responseBody['error'] = false;
        }

        //add flash messages
        // We are not currently using flash messages in the RESTng framework
        if (isset($this->data->flash) && is_object($this->data->flash)) {
            $flash = $this->data->flash->getMessages();
            if (count($flash)) {
                $this->responseBody['flash'] = $flash;
            } else {
                unset($this->responseBody['flash']);
            }
        }

        // If the request/response is paged, inject all of the page links to be rendered.
        if ($apiResponse->isPaged()) {
            // Check that totalObjects has been set, throw error if not
            //   null means not set, 0 means zero objects

            // Get the resource info from the response, which may have mutated from what is
            // in the request.
            $respResourceOptions = array();
            foreach ($apiResponse->getResourceOptions() as $name => $value) {
                $respResourceOptions[] = $name . '=' . urlencode(is_array($value) ? implode(',', $value) : $value);
            }

            if ($apiResponse->isPartial())
            {
                $respResourceOptions[] = 'fields=' . urlencode($apiResponse->getPartialFieldsSpec());
            }

            $respResourceName = $apiResponse->getResourceName();

            $respLimit = $apiResponse->getResponseLimit();
            $respOffset = $apiResponse->getResponseOffset();
            $respTotal = $apiResponse->getTotalObjects();

            $resourceUrl = $this->app->urlFor($respResourceName, $apiResponse->getResourceParams());
            $currentUrl = $resourceUrl . '?' . implode('&', array_merge($respResourceOptions,
                    array('limit=' . $respLimit, 'offset=' . $respOffset)));

            $this->responseBody['total'] = $respTotal;
            $this->responseBody['currentUrl'] = $currentUrl;

            $this->responseBody['firstUrl'] = $resourceUrl . '?' . implode('&', array_merge($respResourceOptions,
                    array('limit=' . $respLimit, 'offset=1')));

            $lastOffset = ($respTotal - $respLimit > 0) ? ($respTotal - $respLimit) + 1 : 1;

            $this->responseBody['lastUrl'] = $resourceUrl . '?' . implode('&', array_merge($respResourceOptions,
                    array('limit=' . $respLimit, 'offset=' . $lastOffset)));

            if (($respOffset - 1) + $respLimit < $respTotal) {
                $this->responseBody['nextUrl'] = $resourceUrl . '?' . implode('&', array_merge($respResourceOptions,
                        array('limit=' . $respLimit, 'offset=' . ($respOffset + $respLimit))));
            }

            if ($respOffset > 1) {
                $this->responseBody['prevUrl'] = $resourceUrl . '?' . implode('&', array_merge($respResourceOptions,
                        array('limit=' . $respLimit, 'offset=' . ($respOffset - $respLimit > 0 ? $respOffset - $respLimit : 1))));
            }

            /*
                Link header:
                <https://miamioh.test.instructure.com/api/v1/courses?page=1&per_page=10>; rel="current",
                <https://miamioh.test.instructure.com/api/v1/courses?page=1&per_page=10>; rel="first",
                <https://miamioh.test.instructure.com/api/v1/courses?page=1&per_page=10>; rel="last"

                Body content:

                totalObjects: n
                offset: n
                limit: n

                Add next and prev?
            */
        }

        // Handle the specific data format
        $body = $this->processBody($this->responseBody);

        $span->stop();

        if (self::$stopAfterRender) {
            // Calling this causes an exception to be thrown which prevents testing
            // @codeCoverageIgnoreStart
            $this->app->stop();
            // @codeCoverageIgnoreEnd
        } else {
            return $body;
        }
    }

    // The processBody method will be overridden in the specific implmentation
    /**
     * @param $response
     * @return mixed
     */
    protected function processbody($response)
    {
        return $this->app->response()->body(serialize($response));
    }

}
