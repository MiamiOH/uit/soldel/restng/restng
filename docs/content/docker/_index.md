+++
title = "Docker for Development"
weight = 35
+++

We can leverage Docker containers for RESTng core development as well as when developing service projects. The RESTng project contains appropriate build files for both cases.
