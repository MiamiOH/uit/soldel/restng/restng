<?php

class slimApiRenderTest extends \MiamiOH\RESTng\Testing\TestCase
{
  private $restngApp;

  public function setUp(): void
  {
    parent::setUp();

    $this->restngApp = new \MiamiOH\RESTng\App();

  }

  public function testApiRender() {

    $apiResponse = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
          ->setMethods(array('getObjectName', 'getPayload'))
          ->getMock();

    $apiResponse->method('getObjectName')->willReturn('');
    $apiResponse->method('getPayload')->willReturn(array('id' => 1, 'description' => 'Some description text'));

    $view = $this->getMockBuilder('MiamiOH\RESTng\View\Base')
          ->setMethods(array('apiRender', 'replace'))
          ->getMock();

    $view->expects($this->once())->method('apiRender')->with($this->identicalTo($apiResponse));
    $view->expects($this->once())->method('replace')->with($this->callback(function($subject){
                          $this->assertEquals(1, count(array_keys($subject)));
                          $this->assertTrue(array_key_exists('data', $subject));
                          return true;
                        }));

    $this->restngApp->view = $view;

    $this->restngApp->apiRender($apiResponse);

  }

  public function testApiRenderObjectLabel() {

    $apiResponse = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
          ->setMethods(array('getObjectName', 'getPayload'))
          ->getMock();

    $apiResponse->method('getObjectName')->willReturn('myObject');
    $apiResponse->method('getPayload')->willReturn(array('id' => 1, 'description' => 'Some description text'));

    $view = $this->getMockBuilder('MiamiOH\RESTng\View\Base')
          ->setMethods(array('apiRender', 'replace'))
          ->getMock();

    $view->expects($this->once())->method('apiRender')->with($this->identicalTo($apiResponse));
    $view->expects($this->once())->method('replace')->with($this->callback(function($subject){
                          $this->assertEquals(1, count(array_keys($subject)));
                          $this->assertTrue(array_key_exists('myObject', $subject));
                          return true;
                        }));

    $this->restngApp->view = $view;

    // The apiRender method does not return, so we are testing that it is calling
    // the correct methods with the expected arguments.
    $this->restngApp->apiRender($apiResponse);

  }

}
