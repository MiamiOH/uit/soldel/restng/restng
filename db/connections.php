<?php

/*
 * Connections defined here are used by Capsule for Eloquent.
 */

$connections = [
    'default' => 'default',
    'connections' => [
        'default' => [
            'driver'   => 'sqlite',
            'database' => ':memory:',
        ]
    ]
];

$mysqlCaCertPath = env('MARIADB_CA_CERT_PATH');

/** @var \MiamiOH\RESTng\Connector\DataSourceFactory $dataSourceFactory */
/** @var \MiamiOH\RESTng\Connector\DataSource $dataSource */
foreach ($dataSourceFactory->getDataSources() as $dataSource) {
    if (!in_array($dataSource->getType(), ['OCI8', 'MySQL'])) {
        continue;
    }

    switch ($dataSource->getType()) {
        case 'OCI8':
            $connections['connections'][$dataSource->getName()] = [
                'driver'       => 'oracle',
                'host'         => $dataSource->getHost(),
                'port'         => $dataSource->getPort(),
                'database'     => $dataSource->getDatabase(),
                'tns'          => $dataSource->getDatabase(),
                'username'     => $dataSource->getUser(),
                'password'     => $dataSource->getPassword(),
                'charset'      => 'AL32UTF8',
                'prefix'       => '',
                'connectType'  => $dataSource->getConnectType(),
                'options'      => $dataSource->getOptions(),
            ];
            break;

        case 'MySQL':
            $connections['connections'][$dataSource->getName()] = [
                'driver'    => 'mysql',
                'host'      => $dataSource->getHost(),
                'port'      => $dataSource->getPort(),
                'database'  => $dataSource->getDatabase(),
                'username'  => $dataSource->getUser(),
                'password'  => $dataSource->getPassword(),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'connectType'  => $dataSource->getConnectType(),
                'options'      => array_merge($dataSource->getOptions(), ['ca_file' => $mysqlCaCertPath]),
            ];
            break;
    }
}

return $connections;
