<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/7/18
 * Time: 7:10 AM
 */

namespace Tests\unit\restng\connector;

use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Exception\InvalidArgumentException;
use Tests\UnitTestCase;

class DataSourceUnitTest extends UnitTestCase
{
    public function testCanBeCreatedFromArray(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData());

        $this->assertInstanceOf(DataSource::class, $datasource);
    }

    public function testNameCanNotBeEmpty(): void
    {
        $this->expectException(InvalidArgumentException::class);

        DataSource::fromArray($this->makeDataSourceData(['name' => '']));
    }

    public function testTypeCanNotBeEmpty(): void
    {
        $this->expectException(InvalidArgumentException::class);

        DataSource::fromArray($this->makeDataSourceData(['type' => '']));
    }

    public function testTypeMustBeAllowedValue(): void
    {
        $this->expectException(InvalidArgumentException::class);

        DataSource::fromArray($this->makeDataSourceData(['type' => 'bob']));
    }

    // Get each attribute
    public function testCanGetName(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['name' => 'test_connection']));

        $this->assertEquals('test_connection', $datasource->getName());
    }

    public function testCanGetType(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['type' => 'OCI8']));

        $this->assertEquals('OCI8', $datasource->getType());
    }

    public function testCanGetUser(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['user' => 'test_user']));

        $this->assertEquals('test_user', $datasource->getUser());
    }

    public function testCanGetPassword(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['password' => 'secr3t']));

        $this->assertEquals('secr3t', $datasource->getPassword());
    }

    public function testCanGetHost(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['host' => 'test_host']));

        $this->assertEquals('test_host', $datasource->getHost());
    }

    public function testCanGetDatabase(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['database' => 'DB1']));

        $this->assertEquals('DB1', $datasource->getDatabase());
    }

    public function testCanGetPort(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['port' => 1234]));

        $this->assertEquals(1234, $datasource->getPort());
    }

    public function testCanGetDefaultConnectType(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData());

        $this->assertEquals(DataSource::NEW_CONNECTION_TYPE, $datasource->getConnectType());
    }

    public function testCanGetConnectType(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['connectType' => 'persistent']));

        $this->assertEquals(DataSource::PERSISTENT_CONNECTION_TYPE, $datasource->getConnectType());
    }

    public function testRequiresSupportedConnectType(): void
    {
        $this->expectException(InvalidArgumentException::class);

        DataSource::fromArray($this->makeDataSourceData(['connectType' => 'other']));
    }

    public function testCanGetDefaultOptions(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData());

        $this->assertEquals([], $datasource->getOptions());
    }

    public function testCanGetOptions(): void
    {
        $datasource = DataSource::fromArray($this->makeDataSourceData(['options' => ['test_option' => 'value']]));

        $this->assertEquals(['test_option' => 'value'], $datasource->getOptions());
    }

    private function makeDataSourceData(array $overrides = []): array
    {
        $data = [
            'name' => 'test',
            'type' => 'Other',
            'user' => '',
            'password' => '',
            'host' => '',
        ];

        return array_merge($data, $overrides);
    }
}
