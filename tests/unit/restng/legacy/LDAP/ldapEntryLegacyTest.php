<?php

include_once('ldapMocks.php');

class ldapEntryLegacyTest extends \PHPUnit\Framework\TestCase {

  private $entry;

  private $ldapDs;

  public function setUp(): void
  {
      parent::setUp();

      \MiamiOH\RESTng\Legacy\LdapMockData::reset();

      $this->ldapDs = new StdClass();
  }

  public function testLdap() {

    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResponse', true);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResultCount', 1);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResultData', array(
        array(
          'uid' => 'bob',
          'cn' => 'Bob Jones',
          'mailalternateAddress' => 'bob@example.com',
          'ou' => 'dc=example,dc=com',
          'sn' => 'Jones',
          'givenname' => 'Bob'
        )
      ));
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockLdapDn', 'uid=bob,dc=example,dc=com');

    $mockEntry = new StdClass();
    $this->entry = new \MiamiOH\RESTng\Legacy\LDAP\Entry($this->ldapDs, $mockEntry);

    $this->assertTrue($this->entry !== null);

  }

}
