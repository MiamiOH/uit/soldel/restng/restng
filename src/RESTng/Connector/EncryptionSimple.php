<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/17/18
 * Time: 9:23 PM
 */

namespace MiamiOH\RESTng\Connector;


class EncryptionSimple implements EncryptionInterface
{

    public function decrypt(string $encryptedString): string
    {
        return str_rot13($encryptedString);
    }

    public function encrypt(string $plainTextString): string
    {
        return str_rot13($plainTextString);
    }
}