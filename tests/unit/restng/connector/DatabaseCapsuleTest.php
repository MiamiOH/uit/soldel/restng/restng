<?php

namespace Tests\unit\restng\connector;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use MiamiOH\RESTng\Connector\DatabaseCapsule;
use PDO;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\UnitTestCase;

class DatabaseCapsuleTest extends UnitTestCase
{
    /** @var DatabaseCapsule */
    private $databaseCapsule;

    /** @var Manager|MockObject */
    private $capsule;
    /** @var DatabaseManager|MockObject */
    private $databaseManager;

    public function setUp(): void
    {
        parent::setUp();

        $dispatcher = $this->createMock(Dispatcher::class);
        $this->databaseManager = $this->createMock(DatabaseManager::class);
        $this->capsule = $this->createMock(Manager::class);
        $this->capsule->method('getDatabaseManager')->willReturn($this->databaseManager);

        $this->databaseCapsule = new DatabaseCapsule($dispatcher, $this->capsule);
    }

    public function testExtendsOracleDriverWhenRegistering(): void
    {
        $this->databaseManager->expects($this->once())->method('extend')
            ->with($this->equalTo('oracle'), $this->anything());

        $this->databaseCapsule->register();
    }

    public function testAddsConnectionsWhenRegistering(): void
    {
        $connections = [
            'foo' => [
                'host' => 'baz.example.com',
                'user' => 'bob',
            ],
            'bar' => [
                'host' => 'baz.example.com',
                'user' => 'alice',
            ],
        ];

        $this->capsule->expects($this->exactly(2))->method('addConnection')
            ->withConsecutive(
                [$this->callback(function (array $connection) {
                    $this->assertEquals('bob', $connection['user']);
                    return true;
                }), 'foo'],
                [$this->callback(function (array $connection) {
                    $this->assertEquals('alice', $connection['user']);
                    return true;
                }), 'bar'],
            );

        $this->databaseCapsule->register([
            'connections' => $connections,
        ]);
    }

    public function testBootsEloquentWhenRegistering(): void
    {
        $this->capsule->expects($this->once())->method('bootEloquent');

        $this->databaseCapsule->register();
    }

    public function testSetsAsGlobalWhenRegistering(): void
    {
        $this->capsule->expects($this->once())->method('setAsGlobal');

        $this->databaseCapsule->register();
    }

    public function testAddsConnectionToCapsuleWithDefaultName(): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->anything(), 'default');

        $this->databaseCapsule->addConnection(['foo' => 'bar']);
    }

    public function testAddsConnectionToCapsuleWithGivenName(): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->anything(), 'foobar');

        $this->databaseCapsule->addConnection(['foo' => 'bar'], 'foobar');
    }

    /**
     * @param string $connectType
     * @param bool $expected
     *
     * @dataProvider connectTypeChecks
     */
    public function testSetsConnectTypeOptionOnConnection(string $connectType, bool $expected): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) use ($expected) {
                $this->assertEquals($expected, $connection['options'][PDO::ATTR_PERSISTENT]);
                return true;
            }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'connectType' => $connectType], 'foobar');
    }

    public function connectTypeChecks(): array
    {
        return [
            'new' => ['new', false],
            'persistent' => ['persistent', true],
        ];
    }

    public function testSetsSessionVariablesOnConnection(): void
    {
        $sessionVars = ['key' => 'value'];

        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) use ($sessionVars) {
                $this->assertEquals($sessionVars, $connection['session_variables']);
                return true;
            }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'options' => ['session_variables' => $sessionVars]], 'foobar');
    }

    public function testRemovesSessionVariablesFromOptionsOnConnection(): void
    {
        $sessionVars = ['key' => 'value'];

        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) use ($sessionVars) {
                $this->assertArrayNotHasKey('session_variables', $connection['options']);
                return true;
            }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'options' => ['session_variables' => $sessionVars]], 'foobar');
    }

    public function testSetsSslCaOnConnection(): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) {
                $this->assertEquals('/foo/bar.pem', $connection['options'][PDO::MYSQL_ATTR_SSL_CA]);
                return true;
            }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'options' => ['ca_file' => '/foo/bar.pem']], 'foobar');
    }

    public function testRemovesSslCaFromOptionsOnConnection(): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) {
                    $this->assertArrayNotHasKey('ca_file', $connection['options']);
                    return true;
                }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'options' => ['ca_file' => '/foo/bar.pem']], 'foobar');
    }

    public function testPassesOptionsOnConnection(): void
    {
        $this->capsule->expects($this->once())->method('addConnection')
            ->with($this->callback(function (array $connection) {
                    $this->assertEquals('bar', $connection['options']['foo']);
                    return true;
                }),
                'foobar'
            );

        $this->databaseCapsule->addConnection(['foo' => 'bar', 'options' => ['foo' => 'bar']], 'foobar');
    }
}

/*

PDO::MYSQL_ATTR_SSL_CA   => env('DB_SSL_CA_PATH')
PDO::ATTR_PERSISTENT => true,

(
    [driver] => mysql
    [host] => mariadb
    [port] =>
    [database] => restng
    [username] => restng
    [password] => Hello123
    [charset] => utf8
    [collation] => utf8_unicode_ci
    [connectType] => new
    [options] => Array
        (
            [cert_path] => /foo/bar/baz
            [session_variables] => Array
                (
                    [NLS_TIME_FORMAT] => HH24:MI:SS
                    [NLS_DATE_FORMAT] => YYYY-MM-DD HH24:MI:SS
                    [NLS_TIMESTAMP_FORMAT] => YYYY-MM-DD HH24:MI:SS
                    [NLS_TIMESTAMP_TZ_FORMAT] => YYYY-MM-DD HH24:MI:SS TZH:TZM
                    [NLS_NUMERIC_CHARACTERS] => .,
                )

        )

)

 */
