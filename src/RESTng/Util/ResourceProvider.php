<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:34 AM
 */

namespace MiamiOH\RESTng\Util;


use MiamiOH\RESTng\App;

abstract class ResourceProvider implements ResourceProviderInterface
{

    /**
     * @var App
     */
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    protected function addTag(array $config): void
    {
        $this->app->addTag($config);
    }

    protected function addDefinition(array $config): void
    {
        $this->app->addDefinition($config);
    }

    protected function addService(array $config): void
    {
        $this->app->addService($config);
    }

    protected function addResource(array $config): void
    {
        $this->app->addResource($config);
    }
}