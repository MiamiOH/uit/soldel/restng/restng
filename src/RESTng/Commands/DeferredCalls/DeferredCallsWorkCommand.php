<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use MiamiOH\RESTng\Util\DeferredCallManager;
use MiamiOH\RESTng\Util\DeferredCallProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeferredCallsWorkCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsWorkCommand extends Command
{
    private const POLL_DELAY = 30;

    protected static $defaultName = 'deferred-calls:work';

    /** @var DeferredCallManager */
    private $callManager;
    /** @var DeferredCallProcessor */
    private $callProcessor;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
        $this->callProcessor = $app->getService('APIDeferredCallProcessor');
    }

    protected function configure(): void
    {
        $this->addOption(
            'once',
            null,
            InputOption::VALUE_NONE,
            'Run once and exit'
        );

        $this->addOption(
            'wait-time',
            null,
            InputOption::VALUE_OPTIONAL,
            'Seconds to wait for polling',
            self::POLL_DELAY
        );

        $this->addOption(
            'tries',
            null,
            InputOption::VALUE_OPTIONAL,
            'Number of tries per job',
            DeferredCallProcessor::MAX_FAILURES
        );

        $this->addOption(
            'retry-delay',
            null,
            InputOption::VALUE_OPTIONAL,
            'Seconds to delay retrying failed job',
            DeferredCallProcessor::RETRY_DELAY
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \MiamiOH\RESTng\Exception\DeferredCallException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $once = $input->getOption('once');
        $pollDelay = $input->getOption('wait-time');

        $this->callProcessor->setMaxFailures($input->getOption('tries'));
        $this->callProcessor->setRetryDelay($input->getOption('retry-delay'));

        while (true) {
            $call = $this->callManager->getNextCall();

            if ($call !== null) {
                $this->callProcessor->processCall($call);

                // Break if only running once
                if ($once) {
                    break;
                }

                // Otherwise continue the loop to check for another job without delay
                continue;
            }

            // Ensure we only run once even if no job was processed
            if ($once) {
                break;
            }

            sleep($pollDelay);
        }

        return Command::SUCCESS;
    }

}
