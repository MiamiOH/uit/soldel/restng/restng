<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/17/18
 * Time: 9:23 PM
 */

namespace MiamiOH\RESTng\Connector;


class EncryptionNull implements EncryptionInterface
{

    public function decrypt(string $encryptedString): string
    {
        return $encryptedString;
    }

    public function encrypt(string $plainTextString): string
    {
        return $plainTextString;
    }
}