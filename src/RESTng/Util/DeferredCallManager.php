<?php

namespace MiamiOH\RESTng\Util;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\RESTng\Exception\DeferredCallException;

class DeferredCallManager
{
    /** @var \MiamiOH\RESTng\App $app */
    private $app;

    /** @var \MiamiOH\RESTng\Util\User $apiUser */
    private $apiUser;

    /** @var DeferredCallFactory */
    private $factory;

    public function setApp($app)
    {
        /** @var \MiamiOH\RESTng\App $app */
        $this->app = $app;
    }

    public function setApiUser($apiUser)
    {
        /** @var \MiamiOH\RESTng\Util\User $apiUser */
        $this->apiUser = $apiUser;
    }

    public function setDeferredCallFactory(DeferredCallFactory $factory)
    {
        $this->factory = $factory;
    }

    public function addDeferredCall($resourceName, $params, $options): Response
    {
        $call = $this->factory->make(array_merge($options, [
            'resourceName' => $resourceName,
            'resourceParams' => $params,
            'apiUser' => $this->apiUser,
        ]));

        $response = $this->app->newResponse();
        $response->setPayload(array(
            'id' => $call->id(),
            'key' => $call->key(),
        ));
        $response->setStatus(\MiamiOH\RESTng\App::API_ACCEPTED);

        // Set the new response as the initial state
        $call->response = $response;

        return $response;
    }

    /**
     * @param $id
     * @return DeferredCall
     * @throws DeferredCallException
     */
    public function getCallById($id): DeferredCall
    {
        try {
            return DeferredCallModel::where('id', '=', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new DeferredCallException(sprintf('Deferred call id %s not found', $id));
        }
    }

    /**
     * @param $key
     * @return DeferredCall
     * @throws DeferredCallException
     */
    public function getCallByKey($key): DeferredCall
    {
        try {
            return DeferredCallModel::where('key', '=', $key)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new DeferredCallException(sprintf('Deferred call key \'%s\' not found', $key));
        }
    }

    public function getNextCall(): ?DeferredCall
    {
        return DeferredCallModel::where('status', '=', DeferredCall::STATUS_PENDING)
            ->where('scheduled_time', '<=', Carbon::now())
            ->orderBy('scheduled_time')
            ->first();
    }

    public function getCalls(?Carbon $since = null, ?string $status = null): Collection
    {
        $query = DeferredCallModel::query();

        if (null !== $since) {
            $query->where('updated_at', '>=', $since);
        }

        if (null !== $status) {
            $query->where('status', '=', $status);
        }

        return $query->orderBy('updated_at')->get();
    }

    public function getFailedCalls(Carbon $since = null): Collection
    {
        $query = DeferredCallModel::where('status', '=', DeferredCall::STATUS_FAILED);

        if (null !== $since) {
            $query->where('updated_at', '>=', $since);
        }

        return $query->orderBy('updated_at')->get();
    }

    public function resetFailedCalls(Carbon $since = null): Collection
    {
        $calls = $this->getFailedCalls($since);

        $calls->each(function (DeferredCall $call) {
            $call->reset();
            $call->save();
        });

        return $calls;
    }

    public function purge(Carbon $before, bool $includeFailed = false): int
    {
        $deleteStatuses = [DeferredCall::STATUS_COMPLETE, DeferredCall::STATUS_CANCELLED];

        if ($includeFailed) {
            $deleteStatuses[] = DeferredCall::STATUS_FAILED;
        }

        $query = DeferredCallModel::whereIn('status', $deleteStatuses)
            ->where('updated_at', '<=', $before);

        $count = $query->count();

        $query->delete();

        return $count;
    }
}
