# Test that use of callResource operates as expected
Feature: The callResource method correctly handles paging, fields and other options
 As a developer using the RESTng API
 I want to assure that the callResource method handles options
 In order to take advantage of paging, fields and other request options

 Scenario: Using call resource to request a paged response
   Given a REST client
   When  I make a GET request for /citest/callResource/paged/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And I get the enrollments_1 element from the subject
   And the subject contains 5 entries
   And I get entry 1 from the subject
   And the subject has an element studentId equal to "104837"
   And the subject has an element enrollmentId equal to "57428075"
   And I get the previous subject
   And I get the previous subject
   And I get the enrollments_6 element from the subject
   And the subject contains 5 entries
   And I get entry 1 from the subject
   And the subject has an element studentId equal to "136461"
   And the subject has an element enrollmentId equal to "57428080"
   And I get the previous subject
   And I get the previous subject
   And I get the enrollments_11 element from the subject
   And the subject contains 4 entries
   And I get entry 1 from the subject
   And the subject has an element studentId equal to "180125"
   And the subject has an element enrollmentId equal to "57428085"

  Scenario: Using call resource to request a paged response with default page limit
    Given a REST client
    When I make a GET request for /citest/callResource/pagedDefaultLimit/20559-03FA
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And I get the enrollments element from the subject
    And the subject contains 7 entries
    And I get entry 1 from the subject
    And the subject has an element studentId equal to "104837"
    And the subject has an element enrollmentId equal to "57428075"

  Scenario: Using call resource to request fields list as an array works
    Given a REST client
    When I make a GET request for /citest/callResource/fieldsArray/20559-03FA
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And I get the course element from the subject
    And the subject has an element courseSubject equal to "COM"
    And the subject has an element courseNumber equal to "135"
    And the subject has an element apiLocation equal to "http://ws/api/citest/course/20559"
    And the subject does not contain an element technical
    And the subject does not contain an element courseId
    And the subject does not contain an element credits
    And the subject does not contain an element description
    And the subject does not contain an element mpCategory

  Scenario: Using call resource with an array as a list option works
    Given a REST client
    When I make a GET request for /citest/callResource/options
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And I get the color element from the subject
    And the subject contains 2 entries

  Scenario: Using call resource with a composed resource works
    Given a REST client
    When I make a GET request for /citest/callResource/compose
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And the subject has an element isComposed equal to "true"
    And I get the composedObjects element from the subject
    And the subject is a hash
    And the subject does contain an element enrollments
    And the subject does contain an element course
    And I get the data element from the response object
    And the subject has an element isPartial equal to "true"
    And I get the partialRequestFields element from the subject
    And the subject is an array
    And the subject contains 3 entries
    And a value in the subject is "id"
    And a value in the subject is "string"
    And a value in the subject is "description"

  Scenario: Using call resource with an array data representation
    Given a REST client
    When I make a GET request for /citest/callResource/arrayBody
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash

