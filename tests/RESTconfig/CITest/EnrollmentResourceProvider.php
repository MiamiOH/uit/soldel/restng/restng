<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class EnrollmentResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Enrollment',
            'type' => 'object',
            'properties' => array(
                'enrollmentId' => array(
                    'type' => 'string',
                ),
                'classId' => array(
                    'type' => 'string',
                ),
                'studentId' => array(
                    'type' => 'string',
                ),
                'midTermGrade' => array(
                    'type' => 'string',
                ),
                'finalGrade' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Enrollment.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Enrollment'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestEnrollment',
            'class' => 'Tests\Service\CITest\Enrollment',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.enrollment.all',
            'description' => 'Pageable collection of enrollment records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/enrollment',
            'service' => 'CITestEnrollment',
            'method' => 'getEnrollment',
            'isPageable' => true,
            'options' => array(
                'classId' => array('type' => 'list', 'description' => 'A test enrollment classId'),
                'studentId' => array('type' => 'list', 'description' => 'A test enrollment studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of enrollments',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Enrollment.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.enrollmentPaged.all',
            'description' => 'Pageable collection of enrollment records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/enrollmentPaged',
            'service' => 'CITestEnrollment',
            'method' => 'getEnrollment',
            'isPageable' => true,
            'defaultPageLimit' => 7,
            'options' => array(
                'classId' => array('type' => 'list', 'description' => 'A test enrollment classId'),
                'studentId' => array('type' => 'list', 'description' => 'A test enrollment studentId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of enrollments',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Enrollment.Collection',
                    )
                ),
            )
        ));
    }
}