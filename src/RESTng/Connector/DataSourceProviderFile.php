<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/7/18
 * Time: 7:06 AM
 */

namespace MiamiOH\RESTng\Connector;


use MiamiOH\RESTng\Exception\DataSourceNotFound;

class DataSourceProviderFile implements DataSourceProviderInterface
{
    private $dataSources = [];

    public function __construct(string $file = null)
    {
        $this->loadDataSourcesFromFile($file);
    }

    private function loadDataSourcesFromFile(string $file): void
    {
        $sourceConfigs = yaml_parse(file_get_contents($file));

        $this->dataSources = array_reduce(
            array_keys($sourceConfigs),
            function (array $c, string $key) use ($sourceConfigs) {
                /*
                 * Puppet managed datasources.yaml may have ':undef' in some fields. This causes
                 * issues with the port as it is expected to be an int. The array of data read
                 * from the yaml file will be scrubbed before passing it to the DataSource for
                 * construction.
                 */
                $c[$key] = DataSource::fromArray(array_merge(['name' => $key], $this->sanitizePort($sourceConfigs[$key])));
                return $c;
            },
            []
        );
    }

    /**
     * @return array
     */
    public function getDataSources(): array
    {
        return $this->dataSources;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasDataSource(string $name): bool
    {
        if (!is_array($this->dataSources)) {
            return false;
        }

        return array_key_exists($name, $this->dataSources);
    }

    /**
     * @param string $name
     * @return DataSource
     * @throws DataSourceNotFound
     */
    public function getDataSource(string $name): DataSource
    {
        if (!$this->hasDataSource($name)) {
            throw new DataSourceNotFound(sprintf('DataSource with name %s not found', $name));
        }

        return $this->dataSources[$name];
    }

    private function sanitizePort(array $dataSource): array
    {
        if (!array_key_exists('port', $dataSource)) {
            $dataSource['port'] = null;
        }

        if (!(null === $dataSource['port'] || is_int($dataSource['port']))) {
            $dataSource['port'] = null;
        }

        return $dataSource;
    }
}