<?php


namespace MiamiOH\RESTng\Connector;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use PDO;
use Yajra\Oci8\Connectors\OracleConnector;
use Yajra\Oci8\Oci8Connection;

class DatabaseCapsule
{
    /** @var Dispatcher */
    private $events;

    /** @var Capsule */
    private $capsule;

    public function __construct(Dispatcher $events, Capsule $capsule = null)
    {
        $this->events = $events;
        $this->capsule = $capsule ?? new Capsule();
    }

    public function register(array $config = null): void
    {
        $manager = $this->capsule->getDatabaseManager();

        $manager->extend('oracle', function($config) {
            $connector = new OracleConnector();
            $connection = $connector->connect($config);
            $db = new Oci8Connection($connection, $config['database'], $config['prefix'], $config);

            // set oracle session variables
            // TODO merge session vars from config
            $sessionVars = [
                'NLS_TIME_FORMAT'         => 'HH24:MI:SS',
                'NLS_DATE_FORMAT'         => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_FORMAT'    => 'YYYY-MM-DD HH24:MI:SS',
                'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                'NLS_NUMERIC_CHARACTERS'  => '.,',
            ];

            // Like Postgres, Oracle allows the concept of "schema"
            if (isset($config['schema'])) {
                $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
            }

            $db->setSessionVars($sessionVars);

            return $db;
        });

        $default = $config['default'] ?? 'default';

        if (null !== $config['connections']) {
            foreach ($config['connections'] as $name => $connection) {
                $this->addConnection(array_merge(['name' => $name], $connection), $default === $name ? 'default' : $name);
            }
        }

        $this->capsule->setEventDispatcher($this->events);

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
    }

    public function addConnection(array $connection, string $name = 'default'): void
    {
        // Convert connectType to the PDO constant key
        $connectType = $connection['connectType'] ?? DataSource::NEW_CONNECTION_TYPE;
        $connection['options'][PDO::ATTR_PERSISTENT] = $connectType === DataSource::PERSISTENT_CONNECTION_TYPE;

        // Extract session variables from options into connection
        $connection['session_variables'] = $connection['options']['session_variables'] ?? [];
        // Remove session variables from options
        unset($connection['options']['session_variables']);

        // Convert ca file to PDO constant if present
        if (!empty($connection['options']['ca_file'])) {
            $connection['options'][PDO::MYSQL_ATTR_SSL_CA] = $connection['options']['ca_file'];
        }
        // Ensure cert path is absent from options
        unset($connection['options']['ca_file']);

        $this->capsule->addConnection($connection, $name);
    }
}
