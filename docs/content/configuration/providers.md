+++
title = "Providers"
weight = 30
+++

RESTng projects must include a provider file which declares the top level path(s) the project's resources occupy and the associated `ResourceProvider` implementations to be loaded. (See the "Provider" section in the Quick Start guide for more information.) The provider configuration is used to tell RESTng where all of the individual project provider files can be found.

The provider configuration is found at:

```bash
config/providers.yaml
```

The contents of this file should be a `resources` key, of which the value is an array of absolute paths to resources files within projects.

```yaml
---
resources:
  - /var/www/restng/service/file-transfer/resources.php
```
