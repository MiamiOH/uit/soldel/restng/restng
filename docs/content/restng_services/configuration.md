+++
title = "Configuration"
weight = 30
+++

RESTng provides a configuration service which can be injected into your service and used to access Miami ConfigMgr values.

In your resource configuration, define the service with a setter or param dependency of 'APIConfiguration':

{{< highlight php "linenos=table" >}}
<?php
...
$app->addService(array(
   'name' => 'ServiceName',
   'class' => 'MiamiOH\RESTng\Service\Service\ServiceName',
   'set' => array(
      'configuration' => array('name' => 'APIConfiguration'),
    ));
{{< / highlight >}} 

Within your business service, capture the injected object for later use:

{{< highlight php "linenos=table" >}}
<?php
...
public function setConfiguration($configuration) {
    $this->configuration = $configuration;
}
{{< / highlight >}} 

The configuration object is an instance of `MiamiOH\RESTng\Util\Configuration` and provides a `getConfiguration()` method to fetch categories from ConfigMgr.

{{< highlight php "linenos=table" >}}
<?php
...
$config = $this->configuration->getConfiguration('MyApplication', 'TheCategory');

$value = $config['the_value'];
{{< / highlight >}} 
