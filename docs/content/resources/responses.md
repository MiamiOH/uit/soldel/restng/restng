+++
title = "Responses"
weight = 70
+++

Every REST resource returns a response of some type. When you document the responses your resource returns under normal circumstances, consumers can plan accordingly. Response are defined for HTTP response codes (using RESTng constants). You should define a response for each normal status produced by your API. You do not need to document a 500 (MiamiOH\RESTng\API_FAILED) as this should only be used for non-normal situations.

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'description' => 'A specific weekday.',
    'name' => 'WeekDay.day',
    'pattern' => '/WeekDay/day',
    'service' => 'WeekDayService',
    'method' => 'getWeekDay',
    'responses' => array(
        \RESTng\API_OK => array(
            'description' => 'A weekday',
            'returns' => array(
                'type' => 'model',
                '$ref' => '#/definitions/WeekDay',
            )
        ),
        \RESTng\API_NOTFOUND => array(
            'description' => 'Requested weekday was not found',
            'returns' => array(
                'type' => 'model',
            )
        ),
    )
));
{{< / highlight >}} 

Each response must have a description entry.

The **returns** entry describes the expected content and must have a 'type'. The preferred method for describing the content of the response is to use a '$ref' to a previously defined model or collection.