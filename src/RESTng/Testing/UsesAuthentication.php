<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 10:06 AM
 */

namespace MiamiOH\RESTng\Testing;


use MiamiOH\RESTng\RouteMiddleware\Authentication;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;

/**
 * Trait UsesAuthentication
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
trait UsesAuthentication
{
    /** @var CredentialValidatorInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    private $token = 'xyz';
    private $useAuthorizationHeader = true;
    private $authorizationHeaderFormat = null;

    public function willNotAuthenticateUser(): void
    {
        $this->installAuthenticationValidator();

        if ($this->useAuthorizationHeader) {
            $this->addAuthorizationHeader($this->token);
        }

        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => false]);
    }

    public function willAuthenticateUser(string $username = null): void
    {
        $this->installAuthenticationValidator();

        if ($this->useAuthorizationHeader) {
            $this->addAuthorizationHeader($this->token);
        }

        $token = $this->token;

        $this->credentialValidator->method('validateToken')
            ->with($this->callback(function ($passedToken) use ($token) {
                $this->assertEquals($token, $passedToken);
                return true;
            }))
            ->willReturn(['valid' => true, 'username' => $username]);
    }

    public function withToken(string $token = null): self
    {
        $this->token = $token;

        return $this;
    }

    public function withoutAuthorizationHeader(): self
    {
        $this->useAuthorizationHeader = false;

        return $this;
    }

    public function withAuthorizationHeaderFormat(string $format = null): void
    {
        $this->authorizationHeaderFormat = $format;
    }

    private function installAuthenticationValidator(): void
    {
        $this->credentialValidator =
            $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->app->useService([
            'name' => 'CredentialValidator',
            'object' => $this->credentialValidator,
            'description' => 'Mock credential validator',
        ]);
    }

    private function addAuthorizationHeader(string $token): void
    {
        $format = $this->authorizationHeaderFormat ?? Authentication::DEFAULT_AUTH_HEADER_FORMAT;
        $this->withHeader('authorization', sprintf($format, $token));
    }
}
