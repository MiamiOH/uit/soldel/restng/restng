# Test the basic api by querying api resources and services
Feature: Get API information from the API itself
 As a developer using the RESTng API
 I want to get information about the API
 In order to understand how to call resources

 Scenario: Get a collection in json
   Given a REST client
   When I make a GET request for /citest/record
   Then the HTTP status code is 200
   Then the HTTP Content-Type header is "application/json"

 Scenario: Get a model in json
   Given a REST client
   When I make a GET request for /citest/record/1
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element id equal to "1"

