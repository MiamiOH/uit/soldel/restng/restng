<?php

namespace MiamiOH\RESTng\Legacy\DB\STH;

use MiamiOH\RESTng\Service\Apm\QuerySpan;

class OCI8 extends \MiamiOH\RESTng\Legacy\DB\STH
{

    private $db;

    private $bind_variables;
    private $has_cursor = false;
    private $cursor_placeholder = '';

    private $returnLobs = false;

    /**
     * @param $dbh
     * @param $result
     * @param string $statement
     */
    function __construct($dbh, $result, $statement = '')
    {
        /** @var \MiamiOH\RESTng\Legacy\DB\DBH\OCI8 $dbh */
        $this->dbh = $dbh;
        $this->db = $dbh->getDbConnection();

        $this->auto_commit = $dbh->getAutoCommit();
        $this->result = $result;
        $this->statement = $statement;
    }

    /**
     * @param $setting
     */
    public function setReturnLobs($setting)
    {
        $this->returnLobs = $setting;
    }

    /**
     * @return bool
     */
    function execute()
    {
        $param_count = func_num_args();
        $parameters = array();

        for ($i = 0; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $this->query = $this->dbh->_parse_placeholders($this->statement, $parameters);

        $this->result = oci_parse($this->db, $this->query);

        $success = true;
        if ($this->result !== false) {
            $cursor = null;
            if ($this->has_cursor == true) {
                $cursor = oci_new_cursor($this->db);

                oci_bind_by_name($this->result, $this->cursor_placeholder, $cursor, -1, OCI_B_CURSOR);
            }

            if (is_array($this->bind_variables)) {
                foreach ($this->bind_variables as $ph_name => $var_info) {
                    if ($var_info['type']) {
                        oci_bind_by_name($this->result, $ph_name, $var_info['variable'],
                            $var_info['length'], $var_info['type']);
                    } else {
                        oci_bind_by_name($this->result, $ph_name, $var_info['variable'], $var_info['length']);
                    }
                }
            }

            if ($this->auto_commit === false) {
                $mode = \OCI_DEFAULT;
            } else {
                $mode = \OCI_COMMIT_ON_SUCCESS;
            }

            $span = $this->transaction->startQuerySpan(QuerySpan::getQueryName($this->statement, 'OCI DB Query'));
            $span->setStatement($this->statement);
            $span->setSubType('oracle');

            $execute = oci_execute($this->result, $mode);

            if ($execute && $this->has_cursor && $cursor) {
                $execute = oci_execute($cursor);
                $this->result = $cursor;
            }

            if ($execute) {
                $this->current_row = 1;
                $this->column_count = oci_num_fields($this->result);
            } else {
                $success = false;
                $this->catch_error(oci_error($this->result));
            }

            $span->stop();
        } else {
            $success = false;
            $this->catch_error(oci_error($this->db));
        }

        $this->error = $success ? DB_NO_ERROR : DB_ERROR;

        return $success;
    }

    function fetchrow_assoc()
    {
        $row = oci_fetch_array($this->result, OCI_ASSOC + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0));

        if ($row !== false) {
            $this->current_row++;
        } else {
            $this->current_row = false;
        }

        return is_array($row) ? array_change_key_case($row, CASE_LOWER) : $row;
    }

    function fetchrow_array()
    {
        $row = oci_fetch_array($this->result, OCI_NUM + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0));

        if ($row !== false) {
            $this->current_row++;
        } else {
            $this->current_row = false;
        }

        return $row;
    }

    function bind_by_name($ph_name, &$variable, $length = -1, $type = '')
    {
        if (substr_count($this->statement, '?')) {
            throw new \Exception('Attempt to mix placeholder replacement and bind by name in query');
        }

        $this->bind_variables[$ph_name] = array('variable' => &$variable,
            'length' => $length, 'type' => $type);
    }

    function new_descriptor($type)
    {
        return oci_new_descriptor($this->db, $type);
    }

    function define_cursor($ph_name)
    {
        $this->has_cursor = true;
        $this->cursor_placeholder = $ph_name;
    }

    function row_count()
    {
        throw new \Exception('Method "row_count" is not supported by the ' . $this->type . ' driver.');
    }

    function free()
    {
        return oci_free_statement($this->result);
    }

    function catch_error($error_array = '')
    {
        if (!is_array($error_array)) {
            $error_num = -1;
            $error_string = 'Unknown error';
        } else {
            $error_num = $error_array['code'];
            $error_string = $error_array['message'];
        }

        unset($this->error_string);

        if ($error_num !== false) {
            $this->error_string = 'Error number: ' . $error_num;
        }

        if ($error_string !== false) {
            if (isset($this->error_string)) {
                $this->error_string .= ' - ';
            }
            $this->error_string .= $error_string;
        }

        throw new \Exception($this->error_string);
    }

}
