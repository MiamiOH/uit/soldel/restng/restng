<?php

namespace MiamiOH\RESTng\Service\Apm;

use Illuminate\Database\Connection;
use Illuminate\Database\Events\QueryExecuted;
use MiamiOH\RESTng\App;

class QueryListener
{
    use CreatesDatabaseQuerySpans;
    /**
     * @var App
     */
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function register(): void
    {
        $this->app->listen(QueryExecuted::class, function (\Illuminate\Database\Events\QueryExecuted $query) {
            $this->recordQuerySpan($query);
        });
    }

    public function recordQuerySpan(QueryExecuted $query): void
    {
        /** @var Transaction $transaction */
        $transaction = $this->app->getDiContainer()->get('ApmTransaction');

        $queryStart = microtime(true) - $transaction->startTime() - $query->time / 1000;
        $queryEnd = $queryStart + $query->time / 1000;

        $data = [
            'label' => self::getQueryName($query->sql),
            'start' => self::toMilliseconds($queryStart),
            'duration' => self::toMilliseconds($queryEnd - $queryStart),
            'type' => sprintf('db.%s.query', $this->connectionDatabaseType($query->connection)),
            'action' => 'query',
            'context' => [
                'db' => [
                    'statement' => $query->sql,
                    'type' => 'sql',
                    'instance' => $query->connection->getDatabaseName(),
                ],
            ],
        ];

        $event = $transaction->createEventSpan($data['label']);
        $event->setType($data['type']);
        $event->setAction($data['action']);
        $event->setCustomContext($data['context']);
        $event->setStartOffset($data['start']);
        $event->setDuration($data['duration']);

        $transaction->putEventSpan($event);
    }

    private function connectionDatabaseType(Connection $connection): string
    {
        switch ($connection->getDriverName()) {
            case 'mysql':
            case 'oracle':
                return $connection->getDriverName();

            default:
                return 'unknown';
        }
    }
}
