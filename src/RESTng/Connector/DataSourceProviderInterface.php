<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/7/18
 * Time: 7:05 AM
 */

namespace MiamiOH\RESTng\Connector;


interface DataSourceProviderInterface
{
    public function hasDataSource(string $name): bool;
    
    public function getDataSource(string $name): DataSource;

    public function getDataSources(): array;
}