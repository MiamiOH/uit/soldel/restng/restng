<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-02-11
 * Time: 20:28
 */

namespace Tests\unit\restng\service\Apm;

use MiamiOH\RESTng\Service\Apm\QuerySpan;
use Nipwaayoni\Events\Transaction;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\UnitTestCase;

class QuerySpanUnitTest extends UnitTestCase
{

    /** @var QuerySpan */
    private $queryLog;

    /** @var Transaction|MockObject  */
    private $parent;

    public function setUp(): void
    {
        parent::setUp();

        $this->parent = $this->createMock(Transaction::class);
        $this->parent->method('getId')->willReturn('123');
        $this->parent->method('getTraceId')->willReturn('abc');

        $this->queryLog = new QuerySpan('Query name', $this->parent);
    }

    public function testCanBeCreatedWithName(): void
    {
        $this->assertInstanceOf(QuerySpan::class, $this->queryLog);
        $this->assertEquals('Query name', $this->queryLog->name());
    }

    public function testCanSetContextInstance(): void
    {
        $this->queryLog->setInstance('test');

        $this->assertEquals('test', $this->queryLog->instance());
    }

    public function testCanSetContextStatement(): void
    {
        $this->queryLog->setStatement('test');

        $this->assertEquals('test', $this->queryLog->statement());
    }

    public function testCanSetContextUser(): void
    {
        $this->queryLog->setUser('test');

        $this->assertEquals('test', $this->queryLog->user());
    }
}
