+++
title = "RESTng Services"
weight = 90
+++

RESTng provides a number of core services that you can inject into your classes. The services provided by the platform will continue grow as we identify opportunities to create common solutions.

Using services and dependency injection in this manner allows us to decouple the implementation from the consumption. This makes changing the service easier and also allows you to unit test your code by providing mock dependencies.

Feel free to suggest new services that could be provided by RESTng. Keep in mind that candidates would have broad applicability and not be specific to a project.