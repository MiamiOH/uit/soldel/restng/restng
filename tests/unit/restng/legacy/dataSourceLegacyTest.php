<?php

use org\bovigo\vfs\vfsStream,
    org\bovigo\vfs\vfsStreamDirectory;

class dataSourceLegacyTest extends \PHPUnit\Framework\TestCase
{

    private $dataSource;

    private $vfs;

    public function setUp(): void
    {
        parent::setUp();

        $single = [
            'ds_access' => [
                'type' => 'Other',
                'user' => 'restng',
                'password' => 'Hello123',
                'host' => '',
                'database' => 'XE',
                'port' => '',
                'connect_type' => '',
            ]
        ];

        $multi= [
            'ds_access' => [
                'type' => 'Other',
                'user' => 'restng',
                'password' => 'Hello123',
                'host' => '',
                'database' => 'XE',
                'port' => '',
                'connect_type' => '',
            ],
            'ds_access2' => [
                'type' => 'Other',
                'user' => 'restng',
                'password' => 'Hello123',
                'host' => '',
                'database' => 'XE',
                'port' => '',
                'connect_type' => '',
            ]
        ];

        $fileStructure = [
            'datasources.yaml' => yaml_emit($single),
            'datasources-multi.yaml' => yaml_emit($multi),
        ];

        $this->vfs = vfsStream::setup('RESTng', null, $fileStructure);

        $this->dataSource = new \MiamiOH\RESTng\Legacy\DataSource();

        $this->dataSource->setDataSourceFileName(vfsStream::url('RESTng/datasources.yaml'));
        $this->dataSource->setEncryption(new \MiamiOH\RESTng\Connector\EncryptionNull());

    }

    public function testDataSource()
    {

        $datasourceFile = 'DataSources.yaml';
        $this->dataSource->setDataSourceFileName($datasourceFile);

        $this->assertEquals($datasourceFile, $this->dataSource->getDataSourceFileName());

    }

    public function testDataSourceFactoryXmlSingle()
    {
        $this->dataSource->setDataSourceFileName(vfsStream::url('RESTng/datasources.yaml'));

        $ds = $this->dataSource->Factory('ds_access');

        $this->assertTrue($this->dataSource !== null);

    }

    public function testDataSourceFactoryXmlMulti()
    {

        $this->dataSource->setDataSourceFileName(vfsStream::url('RESTng/datasources-multi.yaml'));

        $ds = $this->dataSource->Factory('ds_access');

        $this->assertTrue($this->dataSource !== null);

    }

}
