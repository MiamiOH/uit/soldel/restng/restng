<?php

namespace Tests\unit\restng\Util;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Util\DeferredCall;
use MiamiOH\RESTng\Util\DeferredCallFactory;
use MiamiOH\RESTng\Util\DeferredCallManager;
use MiamiOH\RESTng\Util\DeferredCallModel;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;
use MiamiOH\RESTng\Testing\RefreshDatabase;
use Tests\UnitTestCase;

class DeferredCallManagerTest extends UnitTestCase
{
    use RefreshDatabase;

    protected $databaseConnections = ['restng'];

    /** @var DeferredCallManager */
    private $manager;

    /** @var App|MockObject */
    private $restngApp;
    /** @var User|MockObject */
    private $user;
    /** @var DeferredCallFactory|MockObject */
    private $factory;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = $this->createMock(App::class);
        $this->user = $this->createMock(User::class);
        $this->factory = $this->createMock(DeferredCallFactory::class);

        $this->manager = new DeferredCallManager();

        $this->manager->setApp($this->restngApp);
        $this->manager->setApiUser($this->user);
        $this->manager->setDeferredCallFactory($this->factory);
    }

    public function testResponseIdAndKeyMatchNewDeferredCall(): void
    {
        $this->prepareMocks();

        $response = $this->manager->addDeferredCall('test.resource', [], []);

        $payload = $response->getPayload();

        $this->assertEquals(123, $payload['id']);
        $this->assertEquals('abc123', $payload['key']);
    }

    public function testCreatesDeferredCallWithRequestData(): void
    {
        $makeWith = function (array $options) {
                $this->assertEquals('test.resource', $options['resourceName']);
                $this->assertEquals(['color' => 'red'], $options['resourceParams']);
                $this->assertSame($this->user, $options['apiUser']);
                return true;
            };

        $this->prepareMocks(['makeWith' => $makeWith]);

        $this->manager->addDeferredCall('test.resource', ['color' => 'red'], []);
    }

    public function testCanGetCallById(): void
    {
        $call = $this->manager->getCallById(1);

        $this->assertEquals('test.resource.1', $call->resourceName());
    }

    public function testThrowsExceptionWhenIdNotFound(): void
    {
        $this->expectException(DeferredCallException::class);
        $this->expectExceptionMessage('Deferred call id 123 not found');

        $this->manager->getCallById(123);
    }

    public function testCanGetCallByKey(): void
    {
        $call = $this->manager->getCallByKey('fourth');

        $this->assertEquals('test.resource.1', $call->resourceName());
    }

    public function testThrowsExceptionWhenKeyNotFound(): void
    {
        $this->expectException(DeferredCallException::class);
        $this->expectExceptionMessage('Deferred call key \'xyz789\' not found');

        $this->manager->getCallByKey('xyz789');
    }

    public function testGetsNextScheduledCall(): void
    {
        $call = $this->manager->getNextCall();

        $this->assertEquals('first', $call->key());
    }

    public function testDoesNotIncludeFutureScheduledCallsInNext(): void
    {
        $this->clearPendingCalls();

        $this->assertCount(1, $this->getPendingFutureCalls());
        $this->assertNull($this->manager->getNextCall());
    }

    public function testCanGetAllCalls(): void
    {
        $calls = $this->manager->getCalls();

        $this->assertCount(12, $calls);
    }

    public function testCanGetAllCallsWithGivenStatus(): void
    {
        $calls = $this->manager->getCalls(null, DeferredCall::STATUS_COMPLETE);

        $this->assertEquals(2, $calls->count());
    }

    public function testCanGetAllCallsUpdatedSinceGivenTime(): void
    {
        $calls = $this->manager->getCalls(Carbon::now()->subMinutes(65));

        $this->assertEquals(6, $calls->count());
    }

    public function testCanGetAllCallsUpdatedSinceGivenTimeWithGivenStatus(): void
    {
        $calls = $this->manager->getCalls(Carbon::now()->subMinutes(65), DeferredCall::STATUS_COMPLETE);

        $this->assertEquals(1, $calls->count());
    }

    public function testCanGetFailedCalls(): void
    {
        $calls = $this->manager->getFailedCalls();

        $this->assertEquals(2, $calls->count());
    }

    public function testCanGetFailedCallsUpdatedSinceGivenTime(): void
    {
        $calls = $this->manager->getFailedCalls(Carbon::now()->subMinutes(65));

        $this->assertEquals(1, $calls->count());
    }

    public function testCanResetFailedCalls(): void
    {
        $failedCalls = $this->manager->getFailedCalls();

        $this->manager->resetFailedCalls();

        $failedCalls->each(function (DeferredCall $failedCall) {
            $call = $this->manager->getCallById($failedCall->id());

            $this->assertTrue($call->isPending());
            $this->assertEquals(0, $call->attemptCount());
            $this->assertEquals(0, $call->errorCount());
        });
    }

    public function testCanResetFailedCallsUpdatedSinceGivenTime(): void
    {
        $failedCalls = $this->manager->getFailedCalls();

        $cutOffTime = Carbon::now()->subMinutes(65);
        $this->manager->resetFailedCalls($cutOffTime);

        $failedCalls->each(function (DeferredCall $failedCall) use ($cutOffTime) {
            $call = $this->manager->getCallById($failedCall->id());

            if ($failedCall->updatedAt()->lt($cutOffTime)) {
                $this->assertTrue($call->isFailed());
                return;
            }

            $this->assertTrue($call->isPending());
            $this->assertEquals(0, $call->errorCount());
        });
    }

    public function testPurgesOnlyCompleteAndCancelledCallsBeforeGivenTime(): void
    {
        $allCalls = $this->manager->getCalls();

        $cutOffTime = Carbon::now()->subMinutes(65);
        $this->manager->purge($cutOffTime);

        $allCalls->each(function (DeferredCall $call) use ($cutOffTime) {
            if ($call->updatedAt()->lt($cutOffTime) && in_array($call->status(), [DeferredCall::STATUS_COMPLETE, DeferredCall::STATUS_CANCELLED])) {
                try {
                    $this->manager->getCallById($call->id());
                } catch (DeferredCallException $e) {
                    $this->assertStringContainsString(sprintf('Deferred call id %s not found', $call->id()), $e->getMessage());
                    return;
                }

                $this->fail(sprintf('Call with status %s present after purge', $call->status()));
            }

            $verifyCall = $this->manager->getCallById($call->id());

            $this->assertEquals($call->key(), $verifyCall->key());
        });
    }

    public function testPurgesFailedCallsBeforeGivenTime(): void
    {
        $allCalls = $this->manager->getCalls();

        $cutOffTime = Carbon::now()->subMinutes(65);
        $this->manager->purge($cutOffTime, true);

        $allCalls->each(function (DeferredCall $call) use ($cutOffTime) {
            $purgeStatuses = [DeferredCall::STATUS_COMPLETE, DeferredCall::STATUS_CANCELLED,DeferredCall::STATUS_FAILED];
            if ($call->updatedAt()->lt($cutOffTime) && in_array($call->status(), $purgeStatuses)) {
                try {
                    $this->manager->getCallById($call->id());
                } catch (DeferredCallException $e) {
                    $this->assertStringContainsString(sprintf('Deferred call id %s not found', $call->id()), $e->getMessage());
                    return;
                }

                $this->fail(sprintf('Call with status %s present after purge', $call->status()));
            }

            $verifyCall = $this->manager->getCallById($call->id());

            $this->assertEquals($call->key(), $verifyCall->key());
        });
    }

    public function testPurgeReturnsNumberOfCallsDeleted(): void
    {
        $before = $this->manager->getCalls()->count();

        $cutOffTime = Carbon::now()->subMinutes(65);
        $purged = $this->manager->purge($cutOffTime);

        $after = $this->manager->getCalls()->count();

        $this->assertEquals($before - $after, $purged);

    }

    protected function prepareMocks(array $overrides = []): void
    {
        $values = array_merge([
            'id' => 123,
            'key' => 'abc123',
            'resourceName' => 'test.resource',
        ], $overrides);

        $makeWith = $values['makeWith'] ?? function (array $options) use ($values) {
            $this->assertEquals($values['resourceName'], $options['resourceName']);
            return true;
        };

        $model = $this->createMock(DeferredCallModel::class);
        $model->expects($this->once())->method('id')->willReturn($values['id']);
        $model->expects($this->once())->method('key')->willReturn($values['key']);

        $this->factory->expects($this->once())->method('make')
            ->with($this->callback($makeWith))
            ->willReturn($model);

        $this->restngApp->expects($this->once())->method('newResponse')->willReturn(new Response());
    }

    protected function clearPendingCalls(): void
    {
        /** @var Collection $pending */
        $pending = DeferredCallModel::where('status', '=', DeferredCall::STATUS_PENDING)
            ->where('scheduled_time', '<=', Carbon::now())
            ->orderBy('scheduled_time')
            ->get();

        $pending->each(function (DeferredCall $call) {
            $call->markComplete();
            $call->save();
        });
    }

    protected function getPendingFutureCalls(): Collection
    {
        return DeferredCallModel::where('status', '=', DeferredCall::STATUS_PENDING)
            ->where('scheduled_time', '>', Carbon::now())
            ->orderBy('scheduled_time')
            ->get();
    }
}
