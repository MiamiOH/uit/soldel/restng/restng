<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use MiamiOH\RESTng\Util\DeferredCallManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DeferredCallsShowCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsShowCommand extends Command
{
    protected static $defaultName = 'deferred-calls:show';

    /** @var DeferredCallManager */
    private $callManager;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
    }

    protected function configure(): void
    {
        $this->addArgument(
            'job-id',
            InputArgument::REQUIRED,
            'The job id to run'
        );

        $this->addOption(
            'show-response',
            'null',
            InputOption::VALUE_OPTIONAL,
            'Include the complete response if present'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $callId = $input->getArgument('job-id');
        $includeResponse = $input->getOption('show-response');

        $call = $this->callManager->getCallById($callId);

        $data = [
            ['id', $call->id()],
            ['key', $call->key()],
            ['status', $call->status()],
            ['created', $call->createdAt()],
            ['updated', $call->updatedAt()],
            ['scheduled', $call->scheduledTime()],
            ['attempts', $call->attemptCount()],
            ['errors', $call->errorCount()],
            ['resource', $call->resourceName()],
            ['arguments', print_r($call->resourceParams(), true)],
        ];

        if ($call->hasResponse()) {
            $data[] = ['response status', $call->responseStatus()];

            if ($includeResponse) {
                $data[] = ['response', print_r($call->response(), true)];
            }
        }

        $outputStyle->table(['Key', 'Value'], $data);

        return Command::SUCCESS;
    }
}
