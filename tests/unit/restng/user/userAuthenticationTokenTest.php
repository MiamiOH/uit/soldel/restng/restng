<?php

class userAuthenticationTokenTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /** @var \MiamiOH\RESTng\Util\User */
    private $user;

    /** @var \MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->credentialValidator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->user = new \MiamiOH\RESTng\Util\User();

        $this->user->setCredentialValidator($this->credentialValidator);
    }

    public function testAddUserByToken() {
        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => true, 'token' => 'abc123', 'username' => 'doej']);

        $userKey = $this->user->addUserByToken('abc123');

        // result is false on failure or user key on success
        $this->assertTrue($userKey !== false);
        $this->assertTrue($this->user->isAuthenticated());

    }

    public function testAddUserByTokenFail() {
        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => false, 'token' => null, 'username' => null]);

        $userKey = $this->user->addUserByToken('abc123');

        $this->assertFalse($userKey);
        $this->assertFalse($this->user->isAuthenticated());
        $this->assertEmpty($this->user->getUsername());

    }

    public function testAddUserByTokenSecondUser() {
        $this->credentialValidator->method('validateToken')
            ->will($this->onConsecutiveCalls(
                ['valid' => true, 'token' => 'abc123', 'username' => 'doej'],
                ['valid' => true, 'token' => 'xyz456', 'username' => 'smitha']
            ));

        $firstUserKey = $this->user->addUserByToken('abc123');

        $this->assertTrue($firstUserKey !== false);
        $this->assertTrue($this->user->isAuthenticated());
        $this->assertEquals('doej', $this->user->getUsername());

        $secondUserKey = $this->user->addUserByToken('xyz456');

        $this->assertTrue($secondUserKey !== false);
        $this->assertTrue($this->user->isAuthenticated());
        $this->assertEquals('smitha', $this->user->getUsername());

        $this->user->setCurrentUser($firstUserKey);
        $this->assertTrue($this->user->isAuthenticated());
        $this->assertEquals('doej', $this->user->getUsername());

    }

    public function testAddUserByTokenRemoveSecondUser() {
        $this->credentialValidator->method('validateToken')
            ->will($this->onConsecutiveCalls(
                ['valid' => true, 'token' => 'abc123', 'username' => 'doej'],
                ['valid' => true, 'token' => 'xyz456', 'username' => 'smitha']
            ));

        $this->user->addUserByToken('abc123');

        $this->assertEquals('doej', $this->user->getUsername());

        $secondUserkey = $this->user->addUserByToken('xyz456');

        $this->assertEquals('smitha', $this->user->getUsername());

        $this->user->removeUser($secondUserkey);

        // We should be back at the default first user now
        $this->assertTrue($this->user->isAuthenticated());
        $this->assertEquals('doej', $this->user->getUsername());

    }

    public function testPackUser() {
        $this->credentialValidator->method('validateToken')
            ->will($this->onConsecutiveCalls(
                ['valid' => true, 'token' => 'abc123', 'username' => 'doej'],
                ['valid' => true, 'token' => 'xyz456', 'username' => 'smitha']
            ));

        $firstUserKey = $this->user->addUserByToken('abc123');

        $secondUserKey = $this->user->addUserByToken('xyz456');

        $packedUser = $this->user->pack();

        $this->assertEquals($firstUserKey, $packedUser['defaultUser']);
        $this->assertEquals($secondUserKey, $packedUser['currentUser']);
        $this->assertEquals(2, count($packedUser['users']));

    }

    public function callResourceWithName($subject){
        $this->callResourceName = $subject;
        return true;
    }

    public function callResourceWithParams($subject){
        $this->callResourceParams = $subject;
        return true;
    }

    public function callResourceMock() {
        if (isset($this->callResourceResponses[$this->callResourceName]))
        {
            return $this->callResourceResponses[$this->callResourceName];
        }

        return null;
    }

    public function getStatusMock()
    {
        return $this->resourceResponseStatuses[$this->callResourceName];
    }

    public function getPayloadMock()
    {
        return $this->resourcePayloads[$this->callResourceName];
    }
}
