<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 10:47 AM
 */

namespace MiamiOH\RESTng\Testing;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Legacy\DB\DBH\OCI8;
use MiamiOH\RESTng\Legacy\DB\DBH\Sqlite3;
use MiamiOH\RESTng\App;

/**
 * Trait UsesDatasource
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
trait UsesDatasource
{

    /** @var App */
    protected $app;

    protected function installDatasourceService(DBH $dbh = null): DatabaseFactory
    {
        if (null === $dbh) {
            $dbh = $this->createSqlite3DatabaseHandle();
        }

        /** @var DatabaseFactory|\PHPUnit_Framework_MockObject_MockObject $ds */
        $ds = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $ds->method('getHandle')->willReturn($dbh);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $ds,
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
        ]);

        return $ds;
    }

    protected function createOciDatabaseHandle(): OCI8
    {
        return $this->createMock(\MiamiOH\RESTng\Legacy\DB\DBH\OCI8::class);
    }

    protected function createSqlite3DatabaseHandle(string $location = ':memory:'): Sqlite3
    {
        return new \MiamiOH\RESTng\Legacy\DB\DBH\Sqlite3('', '', $location);
    }
}