<?php

namespace MiamiOH\RESTng\Util;

class CacheKeyString implements CacheKey
{
    public const MAX_KEY_LENGTH = 512;
    /**
     * @var string
     */
    private $format;
    /**
     * @var array
     */
    private $replacements;

    /**
     * @var string
     */
    private $key;

    public function __construct(string $format, array $replacements)
    {
        $this->format = $format;
        $this->replacements = $replacements;

        $this->render();
    }

    public function isValid(): bool
    {
        if (empty($this->key)) {
            return false;
        }

        if (strlen($this->key) > self::MAX_KEY_LENGTH) {
            return false;
        }

        return true;
    }

    public function isNotValid(): bool
    {
        return !$this->isValid();
    }

    public function format(): string
    {
        return $this->format;
    }

    public function replacements(): array
    {
        return $this->replacements;
    }

    public function key(): string
    {
        return $this->key;
    }

    private function render(): void
    {
        $this->key = str_replace($this->placeholders(), $this->values(), $this->format);
    }

    private function placeholders(): array
    {
        return array_map(function (string $placeholder) {
            return sprintf('{%s}', $placeholder);
        }, array_keys($this->replacements));
    }

    private function values(): array
    {
        return array_values($this->replacements);
    }
}
