<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-01-26
 * Time: 10:11
 */

namespace MiamiOH\RESTng\Service\Apm;


use MiamiOH\RESTng\Exception\TransactionSpanExistsException;
use MiamiOH\RESTng\Exception\TransactionSpanNotFoundException;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\User;
use Nipwaayoni\ApmAgent;

class Transaction
{
    /** @var ApmAgent */
    private $agent;

    /** @var \Nipwaayoni\Events\Transaction  */
    private $transaction;

    /**
     * @var float
     */
    private $startTime;

    /** @var \Logger  */
    private $logger;

    private $name;
    private $metadata = [ 'type' => 'HTTP' ];
    private $userContext = [];
    private $requestContext = [];
    /** @var Span[] */
    private $spans = [];

    public function __construct(ApmAgent $agent, $name = 'RESTng Request', float $startTime = null)
    {
        $this->name = $name;
        $this->agent = $agent;

        $this->transaction = $agent->startTransaction($name);

        $this->logger = \Logger::getLogger(__CLASS__);

        $this->startTime = $startTime ?? microtime(true);
    }

    public function setName(string $name): void
    {
        $this->transaction->setTransactionName($name);
    }

    public function name(): ?string
    {
        return $this->transaction->getTransactionName();
    }

    public function startTime(): float
    {
        return $this->startTime;
    }

    public function metadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): void
    {
        $this->metadata = array_merge(['type' => 'HTTP'], $metadata);
    }

    public function userContext(): array
    {
        return $this->userContext;
    }

    public function setUserContext(User $apiUser): void
    {
        if (!$apiUser->isAuthenticated()) {
            return;
        }

        $this->userContext = [
            'id' => $apiUser->getUsername()
        ];
    }

    public function requestContext(): array
    {
        return $this->requestContext;
    }

    public function setRequestContext(Request $request): void
    {
        $this->requestContext = [
            'params' => $request->getResourceParams(),
            'options' => $request->getOptions(),
        ];
    }

    public function setRequestContextFromArray(array $context): void
    {
        $this->requestContext = $context;
    }

    public function spans(): array
    {
        return $this->spans;
    }

    public function startSpan(string $name): Span
    {
        $span = new Span($name, $this->transaction);

        $this->spans[] = $span;

        return $span;
    }

    public function startQuerySpan(string $name, string $queryType = 'sql'): QuerySpan
    {
        $span = new QuerySpan($name, $this->transaction, $queryType);

        $this->spans[] = $span;

        return $span;
    }

    public function createEventSpan(string $name): \Nipwaayoni\Events\Span
    {
        return $this->agent->factory()->newSpan($name, $this->transaction);
    }

    public function putEventSpan(\Nipwaayoni\Events\Span $span): void
    {
        $this->agent->putEvent($span);
    }

    public function captureThrowable(\Throwable $thrown, array $context = []): void
    {
        $this->agent->captureThrowable($thrown, $context);

        $this->agentSend();
    }

    public function send(): void
    {
        $this->transaction->stop();

        /** @var Span $span */
        foreach ($this->spans as $span) {
            $this->agent->putEvent($span->event());
        }

        if (!empty($this->userContext)) {
            $this->transaction->setUserContext($this->userContext);
        }

        if (!empty($this->requestContext)) {
            $this->transaction->setCustomContext($this->requestContext);
        }

        $this->transaction->setMeta($this->metadata);

        $this->agentSend();
    }

    private function agentSend(): void
    {
        try {
            $this->agent->send();
            $this->logger->debug('Send to APM server complete');
        } catch (\Exception $e) {
            $this->logger->error('Send to APM server failed', $e);
        }
    }

    public function restart(): void
    {
        // Call Agent::send() safely clears the agent if not active. This is used only during tests
        $this->agent->send();

        $this->userContext = [];
        $this->requestContext = [];
        $this->spans = [];

        $this->transaction = $this->agent->startTransaction($this->name);
    }
}
