+++
title = "Error Handling"
weight = 110
+++

Your service should make no assumptions about the context it is being called from. You have only two means of communication to the caller. Your method must return the valid response object or you may throw an exception. RESTng will catch exceptions and attempt to produce a correct response for the current context.

## What is an Error?

When is an error not an exception? An "exception" refers to a state in which the currently executing process cannot continue. Looking up a requested ID and not finding a record may be considered an "error", but is not an exception. Not finding the requested object should result in a normal response with a status of MiamiOH\RESTng\API_NOTFOUND (HTTP 404). This is an expected response and should be documented as such.

An exception, on the other hand, would occur if a provided option value falls outside an established bound and is therefore illegal. A provided ID which cannot be sanitized, for example alpha characters are given for numeric identifier, could be considered an exception.

The more consistent and predictable you make your service, the easier it will be for others to consume it.

## Throwing an Exception

We don't need to recreate documentation for PHP Exceptions, so refer to http://php.net/manual/en/language.exceptions.php while we focus on RESTng specifics.

## Bad Request

The most common type of exception situation is bad incoming data. It could be an option or something in the POST/PUT payload, but your data validation fails and needs to tell the consumer. RESTng has a custom exception for this case:

{{< highlight php "linenos=table" >}}
<?php
...
if (!$username) {
    throw new \MiamiOH\RESTng\Exception\BadRequest('No username found in request');
}
{{< / highlight >}}

RESTng will catch exceptions of this type and produce a response with status \MiamiOH\RESTng\API_BADREQUEST (HTTP 400), the accepted status code for a "bad request".

## Authorization Failure

While RESTng provides a mechanism to declare and enforce authorization at the REST level, there are circumstances in which the service must enforce or delegate authorization. In the event of an authorization failure, the service should throw an UnAuthorized exception:

{{< highlight php "linenos=table" >}}
<?php
...
if (!$authorized) {
    throw new \MiamiOH\RESTng\Exception\UnAuthorized('User is not authorized to access this service');
}
{{< / highlight >}} 

RESTng will catch exceptions of this type and produce a response with status MiamiOH\RESTng\API_UNAUTHORIZED (HTTP 401).

## Resource Not Found

It becomes more difficult to easily respond when a resource cannot be found as the fetching of resource objects is further separated from the REST layer. In the event that a service cannot find the primary resource, or a required sub-resource, the service should throw a ResourceNotFound exception:

{{< highlight php "linenos=table" >}}
<?php
...
if ($notFound) {
    throw new \MiamiOH\RESTng\Exception\ResourceNotFound("The requested resource $id could not be found");
}
{{< / highlight >}} 

RESTng will catch exceptions of this type and produce a response with status MiamiOH\RESTng\API_NOTFOUND (HTTP 404).

## Failure

In some cases, your service may fail due to a downstream failure. For example, your attempt to connect to a database or open a file for ready may fail. In these situations, you can throw a base PHP exception:

{{< highlight php "linenos=table" >}}
<?php
...
throw new \Exception("Cannot connect to database: $error");
{{< / highlight >}} 

Notice the backslash preceding the Exception class name. This is needed so PHP can find the root Exception class.

**Caution:** Be careful not to reveal sensitive data in the exception message.

## Catching Exceptions

Your service may call other services which themselves may throw exceptions. You mays also find it useful to build create or update resources that operate on collections, but an individual failure should not cause all or part of the request to fail. You should use a try/catch block in these situations and be ready to handle the exception yourself.

Remember, if you don't handle the exception, it will bubble up to RESTng to be handled. You can leave it to RESTng, but if you want more control, a try/catch block will do it.

Calling another resource is a good place to handle exceptions. 

{{< highlight php "linenos=table" >}}
<?php
...
try {
    $enrollmentResponse = $this->callResource('citest.enrollment.all', array(
                'options' => array('classId' => $classId)));
} catch (\Exception $e) {
    // do something with the exception
}
{{< / highlight >}} 

If you have a create or update resource which accepts a collection, you can process each entry and gracefully handle the case of one in the middle failing. 

{{< highlight php "linenos=table" >}}
<?php
...
$results = array();
for ($i = 0; $i < count($data); $i++) {
  try {
    $this->updateWaiver($data[$i]);
    $results[$data[$i]['id']] = array('code' => \RESTng\API_OK, 'message' => '');
  } catch(\Exception $e) {
    $results[$data[$i]['id']] = array('code' => \RESTng\API_FAILED, 'message' => $e->getMessage());
  }
}

$response->setPayload($results);
{{< / highlight >}} 

Remember, do not leave the consumer wondering what happened to the entries *after* the one that failed.