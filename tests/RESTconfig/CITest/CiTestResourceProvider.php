<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;

use MiamiOH\RESTng\Util\ResourceProvider;

class CiTestResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'CITest',
            'description' => 'Resources for supporting Continuous Integration tests'
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Record',
            'type' => 'object',
            'required' => [ 'id', 'string' ],
            'properties' => array(
                'id' => array(
                    'type' => 'integer',
                    'format' => 'int64',
                ),
                'string' => array(
                    'type' => 'string',
                ),
                'color' => array(
                    'type' => 'string',
                    'enum' => ['blue', 'green', 'red', 'yellow', 'orange'],
                ),
                'type' => array(
                    'type' => 'string',
                    'enum' => ['inside', 'outside', 'middle', 'under', 'over'],
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Record.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Record'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITest',
            'class' => 'Tests\Service\CITest\CITest',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
        ));

        $this->addService(array(
            'name' => 'CITestValidation',
            'class' => 'Tests\Service\CITest\CITestValidation',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'validator' => array('type' => 'service', 'name' => 'APIValidationFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.filter',
            'description' => 'A pageable collection of test records. Filter using options.',
            'tags' => array('CITest'),
            'pattern' => '/citest/filter',
            'service' => 'CITest',
            'method' => 'getFilteredExample',
            'isPageable' => true,
            'options' => array(
                'type' => array('required' => true, 'default' => '',
                    'enum' => ['inside', 'outside', 'middle', 'under', 'over'],
                    'description' => 'A type to fetch'),
                'color' => array('type' => 'list', 'default' => '',
                    'enum' => ['blue', 'green', 'red', 'yellow', 'orange'],
                    'description' => 'A list of allowed colors'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of records',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Record.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.reflect.options',
            'description' => 'A resource which simply reflects back the options passed in.',
            'tags' => array('CITest'),
            'pattern' => '/citest/reflect/options',
            'service' => 'CITest',
            'method' => 'getReflectedResponse',
            'options' => array(
                'type' => array('description' => 'Test option'),
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.reflect.parameter',
            'description' => 'A resource which simply reflects back the single paramter passed in.',
            'tags' => array('CITest'),
            'pattern' => '/citest/reflect/parameter/:id',
            'service' => 'CITest',
            'method' => 'getReflectedParameterResponse',
            'params' => array(
                'id' => array('description' => 'A test record id'),
            ),
            'options' => array(
                'type' => array('description' => 'Test option'),
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.reflect.parameter_key',
            'description' => 'A resource which simply reflects back the single paramter alternate key passed in.',
            'tags' => array('CITest'),
            'pattern' => '/citest/reflect/parameter_key/:id',
            'service' => 'CITest',
            'method' => 'getReflectedParameterResponse',
            'params' => array(
                'id' => array('description' => 'A test record id', 'alternateKeys' => ['sid', 'uid']),
            ),
            'options' => array(
                'type' => array('description' => 'Test option'),
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.reflect.parameters',
            'description' => 'A resource which simply reflects back the two parameters passed in.',
            'tags' => array('CITest'),
            'pattern' => '/citest/reflect/parameters/:group/:id',
            'service' => 'CITest',
            'method' => 'getReflectedParameterResponse',
            'params' => array(
                'group' => array('description' => 'A group name'),
                'id' => array('description' => 'A test record id'),
            ),
            'options' => array(
                'type' => array('description' => 'Test option'),
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.reflect.body',
            'description' => 'A resource which simply reflects back the body passed in.',
            'tags' => array('CITest'),
            'pattern' => '/citest/reflect/body',
            'service' => 'CITest',
            'method' => 'getReflectedResponseBody',
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.partial.reflect',
            'description' => 'A resource which simply reflects back the partial response options.',
            'tags' => array('CITest'),
            'pattern' => '/citest/partial/reflect',
            'service' => 'CITest',
            'method' => 'getReflectedPartialResponse',
            'isPartialable' => true,
            'options' => array(
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.compose.reflect',
            'description' => 'A resource which simply reflects back the compose response options.',
            'tags' => array('CITest'),
            'pattern' => '/citest/compose/reflect',
            'service' => 'CITest',
            'method' => 'getReflectedComposeResponse',
            'isComposable' => true,
            'isPartialable' => true,
            'options' => array(
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.subobject.reflect',
            'description' => 'A resource which simply reflects back the subobject response options.',
            'tags' => array('CITest'),
            'pattern' => '/citest/subobject/reflect',
            'service' => 'CITest',
            'method' => 'getReflectedSubObjectResponse',
            'isPartialable' => true,
            'options' => array(
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.partialCombined.reflect',
            'tags' => array('CITest'),
            'description' => 'A resource which simply reflects back the partial and subobject response options.',
            'pattern' => '/citest/partialCombined/reflect',
            'service' => 'CITest',
            'method' => 'getReflectedPartialCombinedResponse',
            'isPartialable' => true,
            'options' => array(
                'color' => array('type' => 'list', 'description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.deferred.create',
            'tags' => array('CITest'),
            'description' => 'A resource which uses callResource to defer the actual execution.',
            'pattern' => '/citest/deferred',
            'service' => 'CITest',
            'method' => 'addDeferredCall',
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.deferred.create.record',
            'description' => 'The target of a deferred call',
            'tags' => array('CITest'),
            'pattern' => '/citest/deferred/record',
            'service' => 'CITest',
            'method' => 'runDeferredCall',
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.validation.basic',
            'description' => 'A resource which performs a simple validation.',
            'tags' => array('CITest'),
            'pattern' => '/citest/validation/basic',
            'service' => 'CITestValidation',
            'method' => 'getValidatedResult',
            'options' => array(
                'color' => array('description' => 'Test option'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.paged.classId',
            'description' => 'Use callResource to get a paged response',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/paged/:classId',
            'service' => 'CITestClass',
            'method' => 'getClassPagedEnrollments',
            'params' => array(
                'classId' => array('description' => 'A class id')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.pagedDefaultLimit.classId',
            'description' => 'Use callResource to get a paged response',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/pagedDefaultLimit/:classId',
            'service' => 'CITestClass',
            'method' => 'getClassPagedEnrollmentsDefaultLimit',
            'params' => array(
                'classId' => array('description' => 'A class id')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.fieldsArray.classId',
            'description' => 'Use callResource to get fields through callResource',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/fieldsArray/:classId',
            'service' => 'CITestClass',
            'method' => 'getClassFieldsArray',
            'params' => array(
                'classId' => array('description' => 'A class id')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.options',
            'description' => 'Use callResource with various options',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/options',
            'service' => 'CITestClass',
            'method' => 'getResourceOptions',
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.arrayBody',
            'description' => 'Use callResource with array data representation',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/arrayBody',
            'service' => 'CITestClass',
            'method' => 'getResourceArrayData',
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.callResource.compose',
            'description' => 'Use callResource with compose',
            'tags' => array('CITest'),
            'pattern' => '/citest/callResource/compose',
            'service' => 'CITestClass',
            'method' => 'getResourceCompose',
            'isComposable' => true,
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.locationFor',
            'description' => 'Use locationFor another resource',
            'tags' => array('CITest'),
            'pattern' => '/citest/locationFor',
            'service' => 'CITest',
            'method' => 'getLocationForResource',
            'options' => array(
              'resource' => array('type' => 'list', 'description' => 'Resource name'),
            ),
        ));
    }
}
