+++
title = "Service Development"
weight = 30
+++

You can use the docker images with docker-compose in your service project. The following example shows how you might do this. You would add this as a `docker-compose.yml` file in the root of your project.

```yaml
version: '3'

services:
  php-restng:
    image: miamioh/service:restng-php-7.3
    volumes:
      - .:/var/www/restng/service/my_project
      - ./docker/config:/var/www/restng/config
      - ./docker/logs/restng:/var/log/restng

  nginx:
    image: miamioh/service:restng-nginx
    volumes:
      - ./docker/logs/nginx:/var/log/nginx
    ports:
      - "80:80"
      - "443:443"
    depends_on:
      - php-restng
```

The first entry under the `volumes` key is important. This will be the path which RESTng will need to load your project from. You can use any path you like, but remember it for later.

Run `docker-compose up -d` from the root of your project. Once the containers are running, you should be able to visit https://localhost/swagger-ui/ and see the default RESTng resource list. (Note that you will not see your project resources yet.)

Once the containers are running, add your project resources. The example above maps a `docker/config` directory in your project root to the expected RESTng configuration location. Create `docker/config` and then follow the directions in the RESTng documentation (https://localhost/docs/quick_start/deploy/) to add your autoloaders and providers. Use the location you specified for mounting your project as the base path. You can add other RESTng configurations, such as datasources, the same way.

You should explicitly exclude the configuration and log files from git, though you may want to use a `.gitkeep` strategy to cause the empty directories to be created upon checkout. For example, include the following in your `.gitignore`:

```text
docker/config/*
!docker/config/.gitkeep

docker/logs/*
!docker/logs/.gitkeep
```

Create the `config` and `log` directories and add a `.gitkeep` file to each.

## Working with Multiple Projects

The previous instructions describe how to run a single RESTng service project in a Docker container. The necessary configuration can be included in your project, thereby providing future developers an easy startup path. There are two cases in which this approach is not sufficient. 

### Running Multiple, Independent Projects 

You can run multiple projects in a single container and interact with them (via Swagger UI, PostMan, etc) for development purposes. To do this, simply create a `docker-compose.yml` file in a directory which contains all of your service projects. Mount that directory into the container using `.:/var/www/restng/service`. This would allow you to add autoloaders and resource providers using `/var/www/restng/service/{project}/...`.

### Running Resource Dependencies

RESTng services can easily call resources provided by other projects using the `callResource` method. In these cases, you should use a git submodule to make the development requirement explicit.
