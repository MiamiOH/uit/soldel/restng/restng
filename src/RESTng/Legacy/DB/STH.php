<?php

namespace MiamiOH\RESTng\Legacy\DB;
use MiamiOH\RESTng\Service\Apm\Transaction;

class STH
{

    /** @var Transaction */
    protected $transaction;

    protected $dbh;
    protected $statement;
    protected $result;
    protected $bind_columns;
    protected $current_row = 0;
    protected $query;
    protected $auto_commit = true;

    //Error related members
    protected $error = DB_NO_ERROR;
    protected $error_string;
    protected $error_num;

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        throw new \Exception('Method "execute" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function fetchrow_assoc()
    {
        throw new \Exception('Method "fetchrow_assoc" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function fetchrow_array()
    {
        throw new \Exception('Method "fetchrow_array" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @param $ph_name
     * @param $variable
     * @param int $length
     * @param string $type
     * @throws \Exception
     */
    public function bind_by_name($ph_name, &$variable, $length = -1, $type = '')
    {
        throw new \Exception('Method "bind_by_name" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @param $row_number
     * @return bool|void
     * @throws \Exception
     */
    public function seek_forward($row_number)
    {
        $found = true;

        while ($found && $this->current_row < $row_number) {
            $found = $this->fetchrow_array();
        }

        return $found;
    }

    /**
     * @throws \Exception
     */
    public function error()
    {
        throw new \Exception('Method "error" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function row_count()
    {
        throw new \Exception('Method "row_count" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function free()
    {
        throw new \Exception('Method "free" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @param string $value
     * @return bool
     */
    public function auto_commit($value = '')
    {
        if ($value === true || $value === false) {
            $this->auto_commit = $value;
        }

        return $this->auto_commit;
    }

    /**
     * @throws \Exception
     */
    public function commit()
    {
        throw new \Exception('Method "commit" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function rollback()
    {
        throw new \Exception('Method "rollback" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function _error()
    {
        throw new \Exception('Method "_error" is not supported by the ' . $this->type . ' driver.');
    }

    /**
     * @throws \Exception
     */
    public function _error_num()
    {
        throw new \Exception('Method "_error_num" is not supported by the ' . $this->type . ' driver.');
    }

}
