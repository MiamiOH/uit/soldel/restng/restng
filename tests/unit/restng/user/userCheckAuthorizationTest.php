<?php

class userCheckAuthorizationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\User */
    private $user;

    /** @var \MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    /** @var \MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $authorizationValidator;

    /** @var \MiamiOH\RESTng\App|PHPUnit_Framework_MockObject_MockObject */
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->credentialValidator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => true, 'token' => 'abc123', 'username' => 'doej']);

        $this->authorizationValidator = $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        // This is ugly as all get out and needs refactored in the class.
        $this->restngApp = $this->getMockBuilder(\MiamiOH\RESTng\App::class)
            ->setMethods(['router', 'getCurrentRoute', 'getParams'])
            ->getMock();
        $this->restngApp->method('router')->willReturnSelf();
        $this->restngApp->method('getCurrentRoute')->willReturnSelf();

        $this->user = new \MiamiOH\RESTng\Util\User();

        $this->user->setApp($this->restngApp);
        $this->user->setCredentialValidator($this->credentialValidator);
        $this->user->setAuthorizationValidator($this->authorizationValidator);
        $this->user->setCache(new \Illuminate\Cache\Repository(new \Illuminate\Cache\ArrayStore()));
        $this->user->setApmTransaction($this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class));

        $this->user->addUserByToken('abc123');
    }

    public function testCheckAuthorizationAuthManBasic() {
        $this->authorizationValidator->expects($this->once())
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $auth = $this->user->checkAuthorization([
            'application' => 'TestApplication',
            'module' => 'TestModule',
            'key' => 'TestKey',
        ]);

        $this->assertTrue($auth);

    }

    public function testCheckAuthorizationAuthManArray() {
        $this->authorizationValidator->expects($this->once())
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'authMan',
                'application' => 'TestApplication',
                'module' => 'TestModule',
                'key' => 'TestKey',
            ],
            [
                'type' => 'self',
                'param' => 'studentId',
            ],
        ]);

        $this->assertTrue($auth);

    }

    public function testCheckAuthorizationSelf() {

        $this->authorizationValidator->expects($this->never())
            ->method('validateAuthorizationForKey');

        $this->restngApp->method('getParams')->willReturn(['studentId' => 'doej']);

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'self',
                'param' => 'studentId',
            ],
        ]);

        $this->assertTrue($auth);

    }

    public function testCheckAuthorizationSelfCaseMixed() {
        $this->authorizationValidator->expects($this->never())
            ->method('validateAuthorizationForKey');

        $this->restngApp->method('getParams')->willReturn(['studentId' => 'DOEJ']);

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'self',
                'param' => 'studentId',
            ],
        ]);

        $this->assertTrue($auth);

    }

    public function testCheckAuthorizationNotSelf() {
        $this->authorizationValidator->expects($this->never())
            ->method('validateAuthorizationForKey');

        $this->restngApp->method('getParams')->willReturn(['studentId' => 'smitha']);

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'self',
                'param' => 'studentId',
            ],
        ]);

        $this->assertFalse($auth);

    }

    public function testCheckAuthorizationNoRouteParam() {
        $this->authorizationValidator->expects($this->never())
            ->method('validateAuthorizationForKey');

        $this->restngApp->method('getParams')->willReturn([]);

        $auth = $this->user->checkAuthorization([
            [
                'type' => 'self',
                'param' => 'studentId',
            ],
        ]);

        $this->assertFalse($auth);

    }

}
