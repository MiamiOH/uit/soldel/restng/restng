<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:57 PM
 */

namespace Tests\Data\Table;


use MiamiOH\RESTng\Testing\Data\Table;

class Book extends Table
{

    public function create(): void
    {
        $this->drop();

        $this->database->exec('
            create table if not exists book (
                id int primary key,
                title text,
                publish_date date,
                author text
            )
        ');
    }

    public function drop(): void
    {
        $this->database->exec('drop table if exists book');
    }

    public function populate(): void
    {
        $this->addFakeBooks();
        $this->addRequestedBooks();
    }

    private function addFakeBooks(): void
    {
        if (!isset($this->options['totalRecords'])) {
            return;
        }

        $faker = \Faker\Factory::create();

        for($i = 1, $iMax = $this->options['totalRecords']; $i <= $iMax; $i++) {
            $insert = sprintf(
                "insert into book (id, title, publish_date, author) values ('%s', '%s', '%s', '%s')",
                $i,
                \SQLite3::escapeString($faker->sentence(5, true)),
                \SQLite3::escapeString($faker->date($format = 'Y-m-d', $max = 'now')),
                \SQLite3::escapeString($faker->name())
            );
            $this->database->exec($insert);
        }
    }
    private function addRequestedBooks(): void
    {
        if (!isset($this->options['withBooks'])) {
            return;
        }

        $books = $this->testBooks();

        foreach ($this->options['withBooks'] as $id) {
            $insert = sprintf(
                "insert into book (id, title, publish_date, author) values ('%s', '%s', '%s', '%s')",
                $id,
                $books[$id]['title'],
                $books[$id]['publishDate'],
                $books[$id]['author']
            );
            $this->database->exec($insert);
        }
    }

    private function testBooks(): array
    {
        return [
            1 => [
                'title' => 'The Story of John Smith',
                'author' => 'Smith, Sally',
                'publishDate' => '2017-08-14',
            ]
        ];
    }
}