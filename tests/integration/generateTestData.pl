#!/usr/bin/perl

use strict;
use Getopt::Long;

my $dbType = 'mysql';

my $result = GetOptions('type|t=s' => \$dbType);

unless (grep { $_ eq $dbType } qw (mysql oracle)) {
	die "DB type must be either mysql or oracle";
}

open SQL, '>testTable.sql' or die "Couldn't open testTable.sql: $!";

my $varcharType = $dbType eq 'mysql' ? 'varchar' : 'varchar2';
my $dropTableTemplate = '';
my $todayFunction = '';
my $sixMonthFunction = '';

if ($dbType eq 'mysql') {
	$dropTableTemplate = 'drop table if exists {tablename};';
    $todayFunction = 'current_date';
    $sixMonthFunction = 'date_add(current_date, interval 180 day)'
} elsif ($dbType eq 'oracle') {
	$dropTableTemplate = qq#\n/\nBEGIN
   EXECUTE IMMEDIATE 'DROP TABLE {tablename}';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;\n/\n#;
    $todayFunction = 'sysdate';
    $sixMonthFunction = 'sysdate + 180';
}

my %drops;
foreach my $tableName (qw( course class enrollment major student book testdata 
		ws_authentication_local_users ws_authentication_token )) {
	$drops{$tableName} = $dropTableTemplate;
	$drops{$tableName} =~ s/\{tablename\}/$tableName/g;
}

if ($dbType eq 'oracle') {
	print SQL "set define off;\n";
}

print SQL qq{
$drops{'course'}
create table course (
	course_id int,
	course_subject ${varcharType}(3),
	course_number ${varcharType}(6),
	description ${varcharType}(128),
	credits int,
	mp_category ${varcharType}(4),
	technical int
);

create index course_course_id on course (course_id);
create index course_course_subject on course (course_subject);
create index course_mp_category on course (mp_category);
create index course_technical on course (technical);

$drops{'class'}
create table class (
	class_id ${varcharType}(16),
	course_id int,
	semester ${varcharType}(8),
	section ${varcharType}(4),
	instructor ${varcharType}(64),
	location ${varcharType}(16),
	meeting_time ${varcharType}(16),
	max_enrollment int
);

create index class_class_id on class (class_id);
create index class_course_id on class (course_id);
create index class_semester on class (semester);

$drops{'enrollment'}
create table enrollment (
	enrollment_id int,
	class_id ${varcharType}(16),
	student_id int,
	mid_term_grade ${varcharType}(2),
	final_grade ${varcharType}(2)
);

create index enrollment_enrollment_id on enrollment (enrollment_id);
create index enrollment_class_id on enrollment (class_id);
create index enrollment_student_id on enrollment (student_id);

$drops{'major'}
create table major (
	major_id ${varcharType}(3),
	major_dept ${varcharType}(16),
	advisor ${varcharType}(32)
);

create index major_major_id on major (major_id);
create index major_major_dept on major (major_dept);

$drops{'student'}
create table student (
	student_id int,
	last_name ${varcharType}(16),
	first_name ${varcharType}(16),
	gender ${varcharType}(1),
	birthday date,
	entry_year int,
	us_resident int,
	state ${varcharType}(2),
	major_id ${varcharType}(3)
);

create index student_student_id on student (student_id);
create index student_gender on student (gender);
create index student_entry_year on student (entry_year);
create index student_us_resident on student (us_resident);
create index student_state on student (state);
create index student_major_id on student (major_id);

$drops{'book'}
create table book (
	id int,
	title ${varcharType}(128),
	publish_date date,
	author ${varcharType}(128)
);

$drops{'testdata'}
create table testdata (
  id int,
  string ${varcharType}(256),
  color ${varcharType}(16),
  type ${varcharType}(32)
);

create index testdata_id on testdata (id);

$drops{'ws_authentication_local_users'}
create table ws_authentication_local_users (
  username ${varcharType}(100),
  algorithm ${varcharType}(5),
  credential_hash ${varcharType}(100),
  create_date date,
  create_username ${varcharType}(50),
  expiration_date date,
  comments ${varcharType}(1000)
);

$drops{'ws_authentication_token'}
create table ws_authentication_token (
  token ${varcharType}(100),
  username ${varcharType}(100),
  application_name ${varcharType}(100),
  issued_time date,
  expiration_time date,
  credential_source ${varcharType}(20)
);

};

open DATA, 'course.txt' or die "Couldn't open course.txt: $!";
<DATA>; # Read the header row
while (<DATA>) {
	chomp;

	my @course = split("\t");

	print SQL qq{
insert into course (course_id, course_subject, course_number, description, credits, mp_category, technical)
	values ($course[0], '$course[1]', '$course[2]', '$course[3]', $course[4], '$course[5]', $course[6]);
};
}
close DATA;

open DATA, 'class.txt' or die "Couldn't open class.txt: $!";
<DATA>; # Read the header row
while (<DATA>) {
	chomp;

	my @class = split("\t");
	
	print SQL qq{
insert into class (class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment)
	values ('$class[0]', $class[1], '$class[2]', '$class[3]', '$class[4]', '$class[5]', '$class[6]', $class[7]);
};
}
close DATA;

open DATA, 'enrollment.txt' or die "Couldn't open enrollment.txt: $!";
<DATA>; # Read the header row
while (<DATA>) {
	chomp;

	my @enrollment = split("\t");
	
	print SQL qq{
insert into enrollment (enrollment_id, class_id, student_id, mid_term_grade, final_grade)
	values ($enrollment[0], '$enrollment[1]', $enrollment[2], '$enrollment[3]', '$enrollment[4]');
};
}
close DATA;

open DATA, 'major.txt' or die "Couldn't open major.txt: $!";
<DATA>; # Read the header row
while (<DATA>) {
	chomp;

	my @major = split("\t");
	
	print SQL qq{
insert into major (major_id, major_dept, advisor)
	values ('$major[0]', '$major[1]', '$major[2]');
};
}
close DATA;

open DATA, 'student.txt' or die "Couldn't open student.txt: $!";
<DATA>; # Read the header row
while (<DATA>) {
	chomp;

	my @student = split("\t");
	my $birthdate = $dbType eq 'oracle' ? "to_date('$student[4]', 'MM/DD/YYYY')" : "'$student[4]'";
	print SQL qq{
insert into student (student_id, last_name, first_name, gender, birthday, entry_year, us_resident, state, major_id)
	values ($student[0], '$student[1]', '$student[2]', '$student[3]', $birthdate,
			$student[5], $student[6], '$student[7]', '$student[8]');
};
}
close DATA;

my @colors = qw( blue red yellow green orange );
my @types = qw( over under middle outside inside );


open DATA, 'testData.txt' or die "Couldn't open testData.txt: $!";

my $id = 1;
while (<DATA>) {
	chomp;

	my $color = $colors[int(rand(scalar(@colors)))];
	my $type = $types[int(rand(scalar(@types)))];

	print SQL qq{
insert into testdata (id, string, color, type)
	values ($id, '$_', '$color', '$type');
};

	$id++;
}

print SQL qq{
insert into ws_authentication_local_users (username, algorithm, credential_hash, create_date, create_username, expiration_date)
    values ('doej', '1', 'd0aabe9a362cb2712ee90e04810902f3', $todayFunction, 'joeadmin', $sixMonthFunction);
};
