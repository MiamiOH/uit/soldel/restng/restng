+++
title = "Actions"
weight = 10
+++

Each resource has a single action which equates directly to one of the REST HTTP methods. The combination of action and pattern is used to match a resource, so it is common to see the same pattern used for multiple actions.  The action does influence the acceptable response status, however.

Action  | HTTP Method | Success Response Code
--------|-------------|--------------
read    | GET         | 200 (MiamiOH\RESTNG\API_OK)
create  | POST        | 201 (MiamiOH\RESTNG\API_CREATED)
update  | PUT         | 204 (MiamiOH\RESTNG\API_NOCONTENT)
delete  | DELETE      | 200 (MiamiOH\RESTNG\API_OK)

**Note:** The default status for a response object is MiamiOH\RESTNG\API_OK. It is your responsiblity to set the appropriate response code based on the action your service is implementing.