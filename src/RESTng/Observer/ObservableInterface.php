<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/8/18
 * Time: 10:28 AM
 */

namespace MiamiOH\RESTng\Observer;


interface ObservableInterface
{
    public function attach(Observer $observer): void;
}