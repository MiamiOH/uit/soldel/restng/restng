+++
title = "Unit Testing"
weight = 7
+++

Every project should include unit tests covering resource and service classes. (Preferrably, Test Driven Development will be used to ensure proper code coverage and improve design.) RESTng includes tests written in [PHPUnit](https://phpunit.de/) and provides some features to make testing service classes easier.

## Test File Layout

We recommend creating a separate directory for unit tests. They should be kept separate from feature tests and other supporting classes. For example:

    tests/unit

You can add an `autoload-dev` entry in your composer.json file to map test classes to this location. For example:

```json
  "autoload-dev": {
    "psr-4": {
      "MiamiOH\\MyProject\\Tests\\": "tests/"
    }
  }
```

## RESTng as a Development Dependency

By including RESTng as a composer development dependency, all classes within RESTng will be available in development installations. This should include the CI test environment.

Please ensure that `miamioh/restng` is only included as a `require-dev` dependency in your `composer.json` file.