<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 9:18 PM
 */

namespace MiamiOH\RESTng\Service\AuthorizationValidator;


interface AuthorizationValidatorInterface
{
    public function validateAuthorizationForKey(AuthorizationSpecification $authSpec, string $username): bool;
}