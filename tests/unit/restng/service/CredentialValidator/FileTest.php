<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/5/18
 * Time: 6:59 PM
 */

namespace Tests\unit\restng\service\CredentialValidator;

use MiamiOH\RESTng\Service\CredentialValidator\File;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    /** @var vfsStream */
    private static $vfs;

    /** @var File */
    private $validator;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $credentials = [
            'users' => [
                ['username' => 'bob', 'password' => 'Hello123'],
                ['username' => 'sally', 'password' => 'secr3t'],
            ],
            'tokens' => [
                ['token' => 'abc123', 'username' => 'bob'],
                ['token' => 'def456', 'username' => 'sally'],
                ['token' => 'xyz987', 'username' => 'joe'],
            ]
        ];

        $fileStructure = [
            'credentials.yaml' => yaml_emit($credentials),
        ];

        self::$vfs = vfsStream::setup('RESTng', null, $fileStructure);
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->validator = new File();
        $this->validator->setCredentialFile(vfsStream::url('RESTng/credentials.yaml'));
    }

    public function testCanBeCreatedWithFile(): void
    {
        $this->assertInstanceOf(File::class, $this->validator);
    }

    public function testReturnsUsernameForValidToken(): void
    {
        $result = $this->validator->validateToken('abc123');

        $this->assertTrue($result['valid']);
        $this->assertEquals('bob', $result['username']);
    }

    public function testReturnsNotValidForInvalidToken(): void
    {
        $result = $this->validator->validateToken('lmn543');

        $this->assertFalse($result['valid']);
    }

    public function testReturnsTokenForValidUsernamePassword(): void
    {
        $result = $this->validator->validateUsernamePassword('bob', 'Hello123');

        $this->assertTrue($result['valid']);
        $this->assertEquals('bob', $result['username']);
        $this->assertEquals('abc123', $result['token']);
    }
}
