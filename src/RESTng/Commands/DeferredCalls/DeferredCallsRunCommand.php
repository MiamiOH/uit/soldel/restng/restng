<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use Logger;
use MiamiOH\RESTng\Util\DeferredCallManager;
use MiamiOH\RESTng\Util\DeferredCallProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeferredCallsRunCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsRunCommand extends Command
{
    protected static $defaultName = 'deferred-calls:run';

    /** @var Logger  */
    private $logger;
    /** @var DeferredCallManager */
    private $callManager;
    /** @var DeferredCallProcessor */
    private $callProcessor;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->logger = Logger::getLogger('cli');
        $this->callManager = $app->getService('APIDeferredCallManager');
        $this->callProcessor = $app->getService('APIDeferredCallProcessor');
    }

    protected function configure(): void
    {
        $this->addArgument(
            'job-id',
            InputArgument::REQUIRED,
            'The job id to run'
        );

        $this->addOption(
            'force',
            null,
            InputOption::VALUE_NONE,
            'Force to run if failed'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \MiamiOH\RESTng\Exception\DeferredCallException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $callId = $input->getArgument('job-id');
        $force = $input->getOption('force');

        $call = $this->callManager->getCallById($callId);

        if ($call->isFailed()) {
            if (!$force) {
                $this->logger->info('Deferred call failed and --force was not used');
                $output->writeln('<info>Deferred call failed and force was not used</info>');
                return Command::FAILURE;
            }

            $call->reset();
            $this->logger->info('Reset failed call because --force was used');
            $output->writeln('<info>Reset failed call because --force was used</info>');
        }

        $this->callProcessor->processCall($call);

        return Command::SUCCESS;
    }
}
