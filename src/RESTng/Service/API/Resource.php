<?php

namespace MiamiOH\RESTng\Service\API;

class Resource extends \MiamiOH\RESTng\Service
{

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getAPI()
    {
        $response = $this->getResponse();

        $api = array(
            'resources' => $this->app->getResources(),
            'services' => $this->app->getServices(),
        );

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($api);

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getResources()
    {
        $response = $this->getResponse();

        $this->app->loadAllConfigs();

        $api = $this->app->getResources();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($api);

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getServices()
    {
        $response = $this->getResponse();

        $this->app->loadAllConfigs();

        $api = $this->app->getServices();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($api);

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getResource()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $resourceName = $request->getResourceParam('name');

        $this->app->loadConfigForResourceName($resourceName);

        $resource = $this->app->getResourceByName($resourceName);

        if (!$resource) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($resource);
        }

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getService()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $serviceName = $request->getResourceParam('name');

        // We can't determine where a service config comes from, so we have
        // to load them all for now.
        $this->app->loadAllConfigs();

        $service = $this->app->getServiceByName($serviceName);

        if (!$service) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($service);
        }

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getTags()
    {
        $response = $this->getResponse();

        $this->app->loadAllConfigs();

        $api = $this->app->getTags();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($api);

        return $response;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getDefinitions()
    {
        $response = $this->getResponse();

        $this->app->loadAllConfigs();

        $api = $this->app->getDefinitions();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($api);

        return $response;
    }

}
