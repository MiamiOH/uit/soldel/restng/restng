<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use Carbon\Carbon;
use MiamiOH\RESTng\Util\DeferredCallManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeferredCallsPurgeCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsPurgeCommand extends Command
{
    protected static $defaultName = 'deferred-calls:purge';

    /** @var DeferredCallManager */
    private $callManager;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
    }

    protected function configure(): void
    {
        $this->addOption(
            'before',
            null,
            InputOption::VALUE_OPTIONAL,
            'Before relative time (ex "2 hours")',
            '1 day'
        );

        $this->addOption(
            'failed',
            null,
            InputOption::VALUE_NONE,
            'Also purge failed jobs'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $before = Carbon::now()->subtract($input->getOption('before'));

        $deleted = $this->callManager->purge($before, $input->getOption('failed'));

        $output->writeln(sprintf('<info>Purged %s deferred call records older than %s</info>', $deleted, $before));

        return Command::SUCCESS;
    }
}
