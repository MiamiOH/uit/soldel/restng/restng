<?php

namespace Tests\Service\CITest;

class Course extends \MiamiOH\RESTng\Service {

  private $database = '';
  private $dbh = '';

  public function setDatabase ($database) {
    $this->database = $database;

    $this->dbh = $this->database->getHandle('authorize');
    $this->dbh->mu_trigger_error = false;
  }

  public function getCourse () {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $id = $request->getResourceParam('courseId');

    $options = $request->getOptions();

    $fieldList = array();
    if ($request->isPartial()) {
      $fieldList = $request->getPartialRequestFields();
    }

    if ($id) {

      $record = $this->dbh->queryfirstrow_assoc('
          select course_id, course_subject, course_number, description,
              credits, mp_category, technical
            from course
            where course_id = ?
        ', $id);

      if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      } else {
        $record = $this->camelCaseKeys($record);

        // Save the apiLocation in case courseId or semester are not in the requested list.
        // Add the apiLocation after filtering the response.
        $apiLocation = $this->locationFor('citest.course.courseId',
          array('courseId' => $record['courseId']));

        if ($fieldList) {
          foreach (array_keys($record) as $field) {
            if (!in_array($field, $fieldList)) {
              unset($record[$field]);
            }
          };
        }

        $record['apiLocation'] = $apiLocation;

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);
      }
    } else {

      $query = '
            select course_id, course_subject, course_number, description,
                credits, mp_category, technical
              from course
              order by course_id
          ';
      $sth = '';

      $records = array();

      if ($request->isPaged()) {
        $minRowToFetch = $request->getOffset();
        $maxRowToFetch = $minRowToFetch + $request->getLimit() - 1;

        // paged queries should include the total number of records
        $totalRecords = $this->dbh->queryfirstcolumn('
            select count(*)
              from course
          ');

        $response->setTotalObjects($totalRecords);

        if ($this->dbh->getType() == 'MySQL') {
          $query .= ' limit ' . ($request->getOffset() - 1) . ', ' . $request->getLimit();
          $sth = $this->dbh->prepare($query);
        } else {
          $query = "
            select course_id, course_subject, course_number, description,
               credits, mp_category, technical
              from ( select /*+ FIRST_ROWS(n) */
                a.*, ROWNUM rnum
                  from ( $query ) a
                  where ROWNUM <= :MAX_ROW_TO_FETCH )
            where rnum  >= :MIN_ROW_TO_FETCH";

          $sth = $this->dbh->prepare($query);

          $sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
          $sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
        }

      } else {
        $sth = $this->dbh->prepare($query);
      }

      $sth->execute();

      while ($record = $sth->fetchrow_assoc()) {
        $record = $this->camelCaseKeys($record);

        // Save the apiLocation in case courseId or semester are not in the requested list.
        // Add the apiLocation after filtering the response.
        $apiLocation = $this->locationFor('citest.course.courseId',
          array('courseId' => $record['courseId']));

        if ($fieldList) {
          foreach (array_keys($record) as $field) {
            if (!in_array($field, $fieldList)) {
              unset($record[$field]);
            }
          };
        }

        $record['apiLocation'] = $apiLocation;

        $records[] = $record;
      }

      $response->setStatus(\MiamiOH\RESTng\App::API_OK);
      $response->setPayload($records);
    }

    return $response;
  }

}
