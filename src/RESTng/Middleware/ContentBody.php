<?php

namespace MiamiOH\RESTng\Middleware;

class ContentBody extends \Slim\Middleware
{
    /**
     * @var array
     */
    protected $contentTypes;
    protected $logger;

    /**
     * Constructor
     * @param array $settings
     */
    public function __construct($settings = array())
    {
        $defaults = array(
            'application/json' => array($this, 'parseJson'),
            'application/xml' => array($this, 'parseXml'),
            'text/xml' => array($this, 'parseXml'),
            'text/csv' => array($this, 'parseCsv'),
            'text/plain' => array($this, 'parseFile'),
            'application/octet-stream' => array($this, 'parseFile'),
        );
        $this->contentTypes = array_merge($defaults, $settings);
        $this->logger = \Logger::getLogger(__CLASS__);
    }

    /**
     * Most of the set up for this class is done in the parent and
     * by the Slim run time process. We need to provide a handful of
     * methods to facilitate unit testing by injecting our objects
     * and getting access to protected data.
     *
     * @return array
     */
    public function getContentTypes()
    {
        return $this->contentTypes;
    }

    /**
     * @param $type
     * @return bool
     */
    public function checkCallable($type)
    {
        return is_callable($this->contentTypes[$type]);
    }

    /**
     * @param $app
     */
    public function setAppMock($app)
    {
        $this->app = $app;
    }

    /**
     * @param $next
     */
    public function setNextMock($next)
    {
        $this->next = $next;
    }

    /**
     *
     */
    public function call()
    {
        $mediaType = $this->app->request()->getMediaType();
        if ($mediaType) {
            $env = $this->app->environment();
            $env['slim.input_original'] = $env['slim.input'];
            if ($env['slim.input'] !== '') {
                $env['slim.input'] = $this->parse($env['slim.input'], $mediaType);
            }
        }

        $this->next->call();
    }

    /**
     * Parse input
     *
     * This method will attempt to parse the request body
     * based on its content type if available.
     *
     * @param  string $input
     * @param  string $contentType
     * @return mixed
     */
    protected function parse($input, $contentType)
    {
        if (isset($this->contentTypes[$contentType]) && is_callable($this->contentTypes[$contentType])) {
            return call_user_func($this->contentTypes[$contentType], $input);
        }

        return $input;
    }

    /**
     * Parse JSON
     *
     * This method converts the raw JSON input
     * into an associative array.
     *
     * @param  string $input
     * @return array|string
     */
    protected function parseJson($input)
    {
        if (function_exists('json_decode')) {
            $result = json_decode($input, true);

            $jsonError = $this->checkJsonError();
            if ($jsonError) {
                throw new \Exception('JSON decode error: ' . $jsonError);
            }

            return $result;
        } else {
            throw new \Exception('Missing function "json_decode"');
        }
    }

    /**
     * @return string
     */
    protected function checkJsonError()
    {
        $error = '';
        switch (json_last_error()) {
            // We only test a couple of easily produced errors
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;

            // @codeCoverageIgnoreStart
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $error = 'Unknown error';
                break;
            // @codeCoverageIgnoreEnd
        }

        return $error;
    }

    /**
     * Parse XML
     *
     * This method creates a SimpleXMLElement
     * based upon the XML input. If the SimpleXML
     * extension is not available, the raw input
     * will be returned unchanged.
     *
     * @param  string $input
     * @return \SimpleXMLElement|string
     */

    protected function parseXml($input)
    {
        if (class_exists('SimpleXMLElement')) {
            try {
                $backup = libxml_disable_entity_loader(true);
                $result = json_decode((string)json_encode(simplexml_load_string($input)), TRUE);
                libxml_disable_entity_loader($backup);
                return $result;
            } catch (\Exception $e) {
                // Rethrow any failures as an Exception type that we know how to handle.
                throw new \Exception($e->getMessage());
            }
        } else {
            throw new \Exception('Class SimpleXMLElement is not available');
        }

    }

    /**
     * Parse CSV
     *
     * This method parses CSV content into a numeric array
     * containing an array of data for each CSV line.
     *
     * @param  string $input
     * @return array
     *
     * @codeCoverageIgnore
     */
    protected function parseCsv($input)
    {
        $temp = fopen('php://memory', 'rw');
        fwrite($temp, $input);
        fseek($temp, 0);
        $res = array();
        while (($data = fgetcsv($temp)) !== false) {
            $res[] = $data;
        }
        fclose($temp);

        return $res;
    }

    /**
     * Parse File
     *
     * This method handles an uploaded file by simply returning it.
     *
     * @param  string $input
     * @return array
     *
     * @codeCoverageIgnore
     */
    protected function parseFile($input)
    {
        /*
         * This will return the input directly, assuming it is an uploaded file.
         * We can use the Slim request to extract some other data about the file
         * and set env vars. The RESTng Request can then use those values when
         * setting the file as data.
         */
        $env = $this->app->environment();
        $env['slim.input.type'] = $this->app->request()->getMediaType();

        return $input;
    }

}
