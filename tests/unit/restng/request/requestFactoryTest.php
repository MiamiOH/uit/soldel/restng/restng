<?php

class requestFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $requestFactory;

  public function setUp(): void
  {
    parent::setUp();
    $this->requestFactory = new \MiamiOH\RESTng\Util\RequestFactory();

  }

  public function testNewRequest() {

    $request = $this->requestFactory->newRequest();

    $this->assertEquals('MiamiOH\RESTng\Util\Request', get_class($request));

  }

}
