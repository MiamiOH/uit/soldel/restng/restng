<?php

class ldapFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $ldapFactory;

  private $dataSource;

  private $factoryArgument;
  private $ldapHandle;

  public function setUp(): void
  {
    parent::setUp();

    $this->factoryArgument = '';
    $this->ldapHandle = new StdClass();

    $this->dataSource = $this->getMockBuilder('\RESTng\Legacy\DataSource')
          ->setMethods(array('Factory'))
          ->getMock();

    $this->ldapFactory = new \MiamiOH\RESTng\Connector\LDAPFactory();

    $this->ldapFactory->setDataSource($this->dataSource);
  }

  public function testGetHandle() {
    $this->dataSource->expects($this->once())->method('Factory')
      ->with($this->callback(array($this, 'factoryWith')))
      ->will($this->returnCallback(array($this, 'factoryMock')));

    $handle = $this->ldapFactory->getHandle('myDataSource');

    $this->assertSame($this->ldapHandle, $handle);

  }

  public function testGetHandleNotFound() {
    $this->dataSource->expects($this->once())->method('Factory')
      ->with($this->callback(array($this, 'factoryWith')))
      ->will($this->returnCallback(array($this, 'factoryMock')));

    $handle = $this->ldapFactory->getHandle('myDataSourceNotFound');

    $this->assertEquals(null, $handle);

  }

  public function factoryWith($subject) {
    $this->factoryArgument = $subject;
    return true;
  }

  public function factoryMock() {
    if ($this->factoryArgument === 'myDataSource') {
      return $this->ldapHandle;
    }

    return null;
  }

}
