# Test the CORS response
Feature: Exercise CORS options and verify headers
 As a developer using the RESTng API
 I want to get the correct CORS behavior
 In order to allow use of the API from different domains

Scenario: Make a GET request with an origin
   Given a REST client
   When I give the HTTP Origin header with the value "http://example.com/"
   And I make a GET request for /citest/student/public/101480
   Then the HTTP status code is 200
   And the HTTP Access-Control-Allow-Credentials header is "true"
   And the HTTP Access-Control-Allow-Origin header is "http://example.com/"
   And the HTTP Access-Control-Max-Age header is "3600"

Scenario: Make a GET request with an origin to an authenticated resource
   Given a REST client
   And a token for the RESTngTest user
   When I give the HTTP Origin header with the value "http://example.com/"
   And I make a GET request for /citest/student/101017
   Then the HTTP status code is 200
   And the HTTP Access-Control-Allow-Credentials header is "true"
   And the HTTP Access-Control-Allow-Origin header is "http://example.com/"
   And the HTTP Access-Control-Max-Age header is "3600"

Scenario: Make an OPTIONS preflight request
   Given a REST client
   When I give the HTTP Access-Control-Request-Method header with the value "POST"
   And I give the HTTP Access-Control-Request-Headers header with the value "X-Our-Header"
   And I make a OPTIONS request for /citest/student/public
   Then the HTTP status code is 200
   And the HTTP Access-Control-Allow-Methods header contains "POST"
   And the HTTP Access-Control-Allow-Headers header is "X-Our-Header"

Scenario: Make an OPTIONS preflight request to an authenticated resource
   Given a REST client
   And a token for the RESTngTest user
   When I give the HTTP Access-Control-Request-Method header with the value "POST"
   And I give the HTTP Access-Control-Request-Headers header with the value "X-Our-Header"
   And I make a OPTIONS request for /citest/student
   Then the HTTP status code is 200
   And the HTTP Access-Control-Allow-Methods header contains "POST"
   And the HTTP Access-Control-Allow-Headers header is "X-Our-Header"
