<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:24 AM
 */

return [
    'resources' => [
        'citest' => [
            \Tests\RESTconfig\CITest\BookResourceProvider::class,
            \Tests\RESTconfig\CITest\CiTestResourceProvider::class,
            \Tests\RESTconfig\CITest\ClassResourceProvider::class,
            \Tests\RESTconfig\CITest\CourseResourceProvider::class,
            \Tests\RESTconfig\CITest\EnrollmentResourceProvider::class,
            \Tests\RESTconfig\CITest\MajorResourceProvider::class,
            \Tests\RESTconfig\CITest\StudentResourceProvider::class,
        ],
        'time' => [
            \Tests\RESTconfig\Time\TimeResourceProvider::class,
        ],
        'date' => [
            \Tests\RESTconfig\Date\DateResourceProvider::class,
        ],
    ]
];
