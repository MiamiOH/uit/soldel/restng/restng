<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\Data\Table\Book;
use Tests\FeatureTestCase;
use MiamiOH\RESTng\Testing\TestResponse;

class ResourceMethodsTest extends FeatureTestCase
{
    use UsesDatasource;
    use UsesTestData;

    private $lastBookId = 0;

    public function setUp(): void
    {
        parent::setUp();

        $db = $this->createSqlite3DatabaseHandle();

        $this->useData()
            ->withSqlite($db->getDbConnection())
            ->withTable(Book::class)
            ->generate();

        $this->installDatasourceService($db);
    }

    public function testCanCreateBook(): void
    {
        $this->assertBookHasBeenCreated();
        $this->assertAction('create');
    }

    public function testCanGetBook(): void
    {
        $bookData = [
            'id' => 2,
            'title' => 'My Test Book',
        ];

        $this->assertBookHasBeenCreated($bookData);

        $response = $this->getJson('/citest/book/2');

        $this->assertBookResponseMatches($response, $bookData);
    }

    public function testCanDeleteBook(): void
    {
        $bookData = [
            'id' => 2,
            'title' => 'My Test Book',
        ];

        $this->assertBookHasBeenCreated($bookData);

        $response = $this->getJson('/citest/book/2');

        $this->assertBookResponseMatches($response, $bookData);
    }

    private function assertBookHasBeenCreated(array $bookData = []): void
    {
        $response = $this->callCreateBook($bookData);

        $response->assertStatus(App::API_CREATED);
    }

    private function assertBookResponseMatches(TestResponse $response, array $bookData): void
    {
        $response->assertSuccessful();
        $response->assertJson(['data' => $bookData]);
    }

    private function callCreateBook(array $overrides = []): TestResponse
    {
        $id = ++$this->lastBookId;

        if (isset($overrides['id'])) {
            $this->lastBookId = $id;
        }

        $bookData = array_merge([
            'id' => $id,
            'title' => 'The Story of John Smith',
            'author' => 'Smith, Sally',
            'publishDate' => '2017-08-14',
        ], $overrides);

        return $this->postJson('/citest/book', $bookData);
    }

}
