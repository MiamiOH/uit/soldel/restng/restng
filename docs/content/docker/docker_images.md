+++
title = "Building Images"
weight = 20
+++

RESTng service projects cannot run outside of the RESTng framework. We can build Docker images that can be used in those service projects to easily enable local development. We produce both an nginx and php-fpm image, which helps validate the separation of those concerns during development.

The php-fpm image uses our customized development fpm image as a starting point, simply adding the RESTng source. This allows us to easily build for any supported version of PHP.

Images should be built from a clean clone of the RESTng project.

```bash
git clone git@gitlab.com:MiamiOH/uit/soldel/restng/restng.git restng-build

cd restng-build

composer install --no-dev

docker build -t miamioh/service:restng-php-7.3 -f Dockerfile-php --build-arg PHP_VERSION=7.3 .
docker push miamioh/service:restng-php-7.3
docker build -t miamioh/service:restng-nginx -f Dockerfile-nginx .
docker push miamioh/service:restng-nginx
```

