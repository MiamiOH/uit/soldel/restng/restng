<?php

header('Content-Type', 'application/x-yaml');
header("Access-Control-Allow-Origin: *");

if (file_exists('../../config/autoloaders.yaml')) {
    $autoloaders = yaml_parse_file('../../config/autoloaders.yaml');
    if (!isset($autoloaders['autoload'])) {
        throw new \Exception('Missing "autoload" key in autoloaders.yaml');
    }
    foreach ($autoloaders['autoload'] as $autoloader) {
        require($autoloader);
    }
}

require('../../vendor/autoload.php');

ini_set( "display_errors", "on" );
error_reporting( E_ALL | E_STRICT );

/** @var \MiamiOH\RESTng\App $app */
$app = require('../../bootstrap.php');

/** @var \MiamiOH\RESTng\Util\OpenApiSpec $spec */
$spec = $app->getService('OpenApiSpec');

print $spec->makeOpenApiSpec();
