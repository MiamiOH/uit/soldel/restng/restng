<?php

namespace Tests\Service;

use Carbon\Carbon;

class Time extends \MiamiOH\RESTng\Service
{

    public function getTime()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $now = Carbon::now();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload([
          'time_string' => $now->toDateTimeString()
        ]);

        return $response;
    }
}
