<?php

class mwContentFormatTypesTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testContentSetupJsonOut() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\JSON', get_class($this->app->view()));
    }

    public function testContentSetupXmlOut() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('MiamiOH\RESTng\View\XML', get_class($this->app->view()));
    }

    public function testContentSetupJsonIn() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/json',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/json', $this->app->environment['CONTENT_TYPE']);
    }

    public function testContentSetupXmlIn() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);
    }

    public function testContentSetupFileIn() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/octet-stream',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/octet-stream', $this->app->environment['CONTENT_TYPE']);
    }

    public function testContentSetupUnknownIn() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/bob',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unsupported content-type: application/bob');

        $this->mw->call();

    }

    public function testContentSetupUnknownOut() {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'HTTP_ACCEPT' => 'application/bob',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unsupported accept type: application/bob');

        $this->mw->call();

    }

}
