<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class ClassResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Class',
            'type' => 'object',
            'properties' => array(
                'classId' => array(
                    'type' => 'string',
                ),
                'courseId' => array(
                    'type' => 'string',
                ),
                'semester' => array(
                    'type' => 'string',
                ),
                'section' => array(
                    'type' => 'string',
                ),
                'instructor' => array(
                    'type' => 'string',
                ),
                'location' => array(
                    'type' => 'string',
                ),
                'meetingTime' => array(
                    'type' => 'string',
                ),
                'maxEnrollment' => array(
                    'type' => 'integer',
                    'format' => 'int64',
                ),
                'course' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/CITest.Course',
                ),
                'enrollments' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/CITest.Enrollment.Collection',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Class.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Class'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestClass',
            'class' => 'Tests\Service\CITest\ClassSection',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.class.all',
            'description' => 'Pageable collection of class records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/class',
            'service' => 'CITestClass',
            'method' => 'getClass',
            'isPartialable' => true,
            'isPageable' => true,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of classes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Class.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.class.classId',
            'description' => 'A single test class.',
            'tags' => array('CITest'),
            'pattern' => '/citest/class/:classId',
            'service' => 'CITestClass',
            'method' => 'getClass',
            'isPartialable' => true,
            'isComposable' => true,
            'params' => array(
                'classId' => array('description' => 'A test class classId'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A class object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Class',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Class Id not found'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.classPaged.all',
            'description' => 'Paged by default collection of class records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/classPaged',
            'service' => 'CITestClass',
            'method' => 'getClass',
            'isPartialable' => true,
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 2400,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of classes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Class.Collection',
                    )
                ),
            )
        ));
    }
}