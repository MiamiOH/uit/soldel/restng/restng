<?php


namespace MiamiOH\RESTng\Util;


use Carbon\Carbon;

interface DeferredCall
{
    public const STATUS_IN_PROGRESS = 'i';
    public const STATUS_PENDING = 'p';
    public const STATUS_COMPLETE = 'c';
    public const STATUS_FAILED = 'f';
    public const STATUS_CANCELLED = 'x';

    // Accessor methods
    public function id(): int;

    public function key(): string;

    public function status(): string;

    public function hash(): string;

    public function scheduledTime(): Carbon;

    public function attemptCount(): int;

    public function errorCount(): int;

    public function apiUser(): ?User;

    public function resourceName(): string;

    public function resourceParams(): array;

    public function responseStatus(): int;

    public function response(): Response;

    public function createdAt(): Carbon;

    public function updatedAt(): Carbon;

    public function isPending(): bool;

    public function isNotPending(): bool;

    public function isFailed(): bool;

    // Mutator methods
    public function scheduleAt(Carbon $scheduleAt) : void;

    public function hasResponse(): bool;

    public function addResponse(Response $response): void;

    public function markInProgress(): void;

    public function markPending(): void;

    public function markComplete(): void;

    public function markFailed(): void;

    public function markCancelled(): void;

    public function incrementAttemptCount(): void;

    public function incrementErrorCount(): void;

    public function save(array $options = []): bool;

    public function reset(): void;

    // Utility methods
    public function verifyCallHash(): bool;

    public function hasApiUser(): bool;
}