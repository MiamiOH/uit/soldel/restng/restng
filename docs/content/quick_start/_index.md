+++
title = "Quick Start"
weight = 30
+++

The best way to explain RESTng is through an example. We'll start with a GET resource that simply returns static data and work up to including some basic features.

RESTng data is represented by either a model or a collection. A model is a defined representation of a single data object, such as a person or class. A collection is an array of models. A RESTng resource should accept and return either a model or a collection. Since REST resources frequently use JSON for data representation, we will do the same here

RESTng provides a separation of concerns between interface implementation (resources) and business logic (services). Resources define how RESTng will implement the REST endpoint. Services follow a specific pattern to accept a request from a resource and return a response.

## Data Model

Since REST deals with data objects, a good place to start designing your service is with your data. We will build a weekday information resource for our tutorial. A weekday object might include the name, abbreviation, type and next occurrence.

```json
{
    "id": 123,
    "name": "Monday",
    "abbreviation": "Mon",
    "dayOfWeek": 2,
    "type": "weekday"
}
```

A collection of weekday objects would be represented as an array.

```json
[
    {
        "id": 123,
        "name": "Monday",
        "abbreviation": "Mon",
        "dayOfWeek": 2,
        "type": "weekday"
    },
    {
        "id": 130,
        "name": "Saturday",
        "abbreviation": "Sat",
        "dayOfWeek": 6,
        "type": "weekend"
    }
]
```
