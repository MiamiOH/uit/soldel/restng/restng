<?php

namespace MiamiOH\RESTng\Middleware;

class ContentFormat extends \Slim\Middleware
{

    /**
     * @param $app
     */
    public function setAppMock($app)
    {
        $this->app = $app;
    }

    /**
     * @param $next
     */
    public function setNextMock($next)
    {
        $this->next = $next;
    }

    public function call()
    {
        $this->parseFormat($this->app);
        $this->next->call();
    }

    public function parseFormat($app)
    {
        $env = $app->environment;

        // Incoming payload format and outgoing rendering format

        /*
          The Slim ContentTypes middleware will be used to get the incoming
          payload.  It uses the request's media type, which is the
          content-type header.  We'll make sure it's set using:

            1. content-type header
            2. file extension
            3. default (json)

          The content type of incoming data is only relevant for certain actions.
          The action must be one of POST, PUT or PATCH for the implicit default
          to be applied, otherwise, do not set a CONTENT_TYPE.

          The outgoing format uses a similar process to determine the type:

            1. accept header
            2. file extension
            3. default (incomingFormat || json)

          If there is a defined incoming format, and not explicitly set outgoing
          format, we will use the incoming format. Since some operations (ex GET)
          do not set an implicit incoming format, it is possible to have no
          incoming format and use the default outgoing format.
        */

        // Explicitly set
        $fileExt = pathinfo(strtolower($env['PATH_INFO']), PATHINFO_EXTENSION);

        $incomingFormat = '';

        $contentMimeType = strpos($env['CONTENT_TYPE'], ';') !== false
            ? substr_replace($env['CONTENT_TYPE'], '', strpos($env['CONTENT_TYPE'], ';'))
            : $env['CONTENT_TYPE'];

        switch ($contentMimeType) {
            case 'application/json':
            case 'text/json':
                $incomingFormat = 'json';
                break;

            case 'application/xml':
            case 'text/xml':
                $incomingFormat = 'xml';
                break;

            case 'application/octet-stream':
            case 'text/plain':
                $incomingFormat = 'file';
                break;

            default:
                if ($env['CONTENT_TYPE']) {
                    throw new \Exception('Unsupported content-type: ' . $env['CONTENT_TYPE'] . ' (' . $contentMimeType . ')');
                }
        }

        $supportedExtensions = array('xml', 'json');

        if (!$incomingFormat && $fileExt && in_array($fileExt, $supportedExtensions)) {
            $incomingFormat = $fileExt;
        }

        // Only use the default if the method will have data.
        if (!$incomingFormat && in_array($env['REQUEST_METHOD'], array('POST', 'PUT', 'PATCH'))) {
            $incomingFormat = 'json';
        }

        // Make sure to remove any extension that we key on or the Slim dispatcher will require
        // a route that includes it.  Don't touch other '.somethings' since that can be a valid
        // part of a resource pattern or param.
        if ($fileExt && in_array($fileExt, $supportedExtensions)) {
            $env['PATH_INFO'] = str_ireplace('.' . $fileExt, '', $env['PATH_INFO']);
        }

        // Determine the outgoing format. There must always be an outgoing format set.
        $outgoingFormat = '';

        foreach (preg_split('/, */', $env['HTTP_ACCEPT']) as $mimeType) {
            $acceptMimeType = strpos($mimeType, ';') !== false
                ? substr_replace($mimeType, '', strpos($mimeType, ';'))
                : $mimeType;

            switch ($acceptMimeType) {
                case 'application/json':
                case 'text/json':
                case '*/*':
                    $outgoingFormat = 'json';
                    break;

                case 'application/xml':
                case 'text/xml':
                    $outgoingFormat = 'xml';
                    break;
            }

            if ($outgoingFormat) {
                break;
            }
        }

        if ($env['HTTP_ACCEPT'] && !$outgoingFormat) {
            throw new \Exception('Unsupported accept type: ' . $env['HTTP_ACCEPT']);
        }

        if (!$outgoingFormat && $fileExt && in_array($fileExt, $supportedExtensions)) {
            $outgoingFormat = $fileExt;
        }

        // Assume json for outgoing format of files if not specified
        if (!$outgoingFormat && $incomingFormat === 'file') {
            $outgoingFormat = 'json';
        }

        if (!$outgoingFormat) {
            $outgoingFormat = $incomingFormat ? $incomingFormat : 'json';
        }

        // Once we know the incoming and outgoing formats, we will set the
        // necessary render pieces and make sure the content-type is correct
        // so the content negotiation works.
        switch ($incomingFormat) {
            case 'json':
                $env['CONTENT_TYPE'] = 'application/json';
                break;

            case 'xml':
                $env['CONTENT_TYPE'] = 'application/xml';
                break;

            case 'file':
                $env['CONTENT_TYPE'] = 'application/octet-stream';
                break;

        }

        switch ($outgoingFormat) {
            case 'json':
                $app->view(new \MiamiOH\RESTng\View\JSON());
                break;

            case 'xml':
                $app->view(new \MiamiOH\RESTng\View\XML());
                break;

        }
    }
}