<?php

namespace MiamiOH\RESTng\View;

class XML extends Base
{

    protected $contentType = 'application/xml';

    /**
     * @param $responseBody
     * @return mixed
     */
    public function processBody($responseBody)
    {

        $xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><root></root>");

        array_to_xml($responseBody, $xml);

        $body = $this->app->response()->body($xml->asXML());

        return $body;
    }

}

/**
 * @param $student_info
 * @param $xml_student_info
 */
function array_to_xml($student_info, &$xml_student_info)
{
    foreach ($student_info as $key => $value) {
        if (is_array($value)) {
            if (!is_numeric($key)) {
                $subnode = $xml_student_info->addChild("$key");
                array_to_xml($value, $subnode);
            } else {
                $subnode = $xml_student_info->addChild("item$key");
                array_to_xml($value, $subnode);
            }
        } else {
            $xml_student_info->addChild("$key", htmlspecialchars("$value"));
        }
    }
}
