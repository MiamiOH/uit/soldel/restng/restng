+++
title = "Tags"
weight = 40
+++

Tags are a feature of the Swagger Spec and provide a method for grouping related resources. Swagger UI will render all groups with the same tag under a single section. The tag can provide an overall description of the resources.

You define tags in your ResourceProvider::registerDefinitions method:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addTag(array(
    'name' => 'Weekday',
    'description' => 'Resources for getting weekday data objects'
));
{{< / highlight >}} 

The **name** must be unique across the RESTng space. The **description** should be plain text and brief.

## Apply a Tag to a Resource

Once a tag has been defined, you apply it to a resource in the configuration array:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'description' => 'A collection of weekdays.',
    'name' => 'WeekDay.all',
    'pattern' => '/WeekDay',
    'service' => 'WeekDayService',
    'method' => 'getWeekDays',
    'tags' => array('Weekday'),
));
{{< / highlight >}} 

Notice that the **tags** value is an array. At this time, Swagger UI only appears to display the resources under a single entry. For now, you must use the array syntax, but include only a single tag.