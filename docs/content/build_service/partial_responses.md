+++
title = "Partial Responses"
weight = 70
+++

Business objects can often contain data that is not useful in every context.  A consumer may specify that only certain fields from an object be returned.  Your service method should support these partial response requests by creating an object representation containing only the requested fields.

Your resource must be configured with 'isPartialable' set to true in order for partial requests to be processed.

A partial REST request will take the form of:

    /course/section/201510?fields=crn,subject,number

RESTng will extract the fields list from the query string and provide it to you in the request object.
 
In your service method, you can determine if a request is for partial fields and get the field list by asking the request:

    $isPartial = $request->isPartial();
    $partialFields = $request->getPartialRequestFields();

The actual implementation of the partial result is up to you.  It is recommended that you use partial response to conditionally include costly resources only when needed.

## Composing Other Objects

Complex objects can be created by composing other objects. This technique provides an easy way to reuse resources. A consumer can request these sub-objects using the fields option. Refer to the section on Consuming Resources for details.

Use the same isPartial method to determine if the request is partial. To get the requested sub-objects:

    $subObjects = $request->getSubObjects();

This will result in an associative array where the keys are the names of the objects and the values are the requested properties. You can use this information to construct the requested objects.