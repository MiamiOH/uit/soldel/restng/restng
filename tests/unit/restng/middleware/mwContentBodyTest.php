<?php

class mwContentBodyTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $mw;

  static public $contentIn = '';

  public function setUp(): void
  {
    parent::setUp();

    mwContentBodyTest::$contentIn = '';

    $this->app = new \MiamiOH\RESTng\App();

    $this->next = $this->getMockBuilder('stdClass')
          ->setMethods(array('call'))
          ->getMock();

    $this->mw = new \MiamiOH\RESTng\Middleware\ContentBody();

  }

  public function testMwConstruct() {

    $this->assertNotNull($this->mw);
    $contentTypeMap = $this->mw->getContentTypes();

    $this->assertTrue(is_array($contentTypeMap));
    $this->assertEquals(6, count(array_keys($contentTypeMap)));
    $this->assertTrue(array_key_exists('application/json', $contentTypeMap));
    $this->assertTrue(array_key_exists('application/xml', $contentTypeMap));
    $this->assertTrue(array_key_exists('text/xml', $contentTypeMap));
    $this->assertTrue(array_key_exists('text/csv', $contentTypeMap));
    $this->assertTrue(array_key_exists('text/plain', $contentTypeMap));
    $this->assertTrue(array_key_exists('application/octet-stream', $contentTypeMap));
    $this->assertTrue($this->mw->checkCallable('application/json'));
    $this->assertTrue($this->mw->checkCallable('application/xml'));
    $this->assertTrue($this->mw->checkCallable('text/xml'));
    $this->assertTrue($this->mw->checkCallable('text/csv'));
    $this->assertTrue($this->mw->checkCallable('text/plain'));
    $this->assertTrue($this->mw->checkCallable('application/octet-stream'));

  }

  public function testMwParseXmlApplication() {
    mwContentBodyTest::$contentIn = '<xml><name>test name</name></xml>';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'application/xml',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->next->expects($this->once())->method('call')
        ->willReturn(true);

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

    $this->mw->call();

    $contentOut = $this->app->environment['slim.input'];

    $this->assertEquals(mwContentBodyTest::$contentIn, $this->app->environment['slim.input_original']);
    $this->assertTrue(is_array($contentOut));
    $this->assertEquals(1, count(array_keys($contentOut)));
    $this->assertTrue(array_key_exists('name', $contentOut));

  }

  public function testMwParseXmlText() {
    mwContentBodyTest::$contentIn = '<xml><name>test name</name></xml>';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'text/xml',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->next->expects($this->once())->method('call')
        ->willReturn(true);

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

    $this->mw->call();

    $contentOut = $this->app->environment['slim.input'];

    $this->assertEquals(mwContentBodyTest::$contentIn, $this->app->environment['slim.input_original']);
    $this->assertTrue(is_array($contentOut));
    $this->assertEquals(1, count(array_keys($contentOut)));
    $this->assertTrue(array_key_exists('name', $contentOut));

  }

  public function testMwParseJson() {
    mwContentBodyTest::$contentIn = '{ "name": "test name" }';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'application/json',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->next->expects($this->once())->method('call')
        ->willReturn(true);

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

    $this->mw->call();

    $contentOut = $this->app->environment['slim.input'];

    $this->assertEquals(mwContentBodyTest::$contentIn, $this->app->environment['slim.input_original']);
    $this->assertTrue(is_array($contentOut));
    $this->assertEquals(1, count(array_keys($contentOut)));
    $this->assertTrue(array_key_exists('name', $contentOut));

  }

  public function testMwUnknownType() {
    mwContentBodyTest::$contentIn = 'name=Test%20Name&type=user';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'text/html',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->next->expects($this->once())->method('call')
        ->willReturn(true);

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

    $this->mw->call();

    $contentOut = $this->app->environment['slim.input'];

    $this->assertEquals(mwContentBodyTest::$contentIn, $contentOut);

  }

  public function testMwParseBadJson() {
    mwContentBodyTest::$contentIn = '{ "name" "test name" }';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'application/json',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('JSON decode error: Syntax error, malformed JSON');

      $this->mw->call();

  }

   public function testMwParseBadXml() {
    mwContentBodyTest::$contentIn = '<xml><name>test name<name></xml>';

    $this->app->container->singleton('environment', function ($c) {
      $defaults = array(
          'REQUEST_METHOD' => 'GET',
          'SCRIPT_NAME' => '/api',
          'PATH_INFO' => '/test/service',
          'CONTENT_TYPE' => 'application/xml',
          'slim.input' => mwContentBodyTest::$contentIn,
      );

      return \Slim\Environment::mock($defaults);
    });

    $this->mw->setAppMock($this->app);
    $this->mw->setNextMock($this->next);

       $this->expectException(\Exception::class);
       $this->expectExceptionMessage('simplexml_load_string(): Entity: line 1: parser error : Opening and ending tag mismatch: name line 1 and xml');

       $this->mw->call();

  }

    public function testMwParseFile() {
        mwContentBodyTest::$contentIn = 'This is file content';

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/octet-stream',
                'slim.input' => mwContentBodyTest::$contentIn,
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals(mwContentBodyTest::$contentIn, $this->app->environment['slim.input_original']);
        $this->assertEquals(mwContentBodyTest::$contentIn, $this->app->environment['slim.input']);
        $this->assertEquals('application/octet-stream', $this->app->environment['slim.input.type']);

    }

}
