<?php

namespace Tests\Service\CITest;

class ClassSection extends \MiamiOH\RESTng\Service
{

    private $database = '';
    private $dbh = '';

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('authorize');
    }

    public function getClass()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $classId = $request->getResourceParam('classId');

        $options = $request->getOptions();

        /*
          The implementation of partial field lists will be up to the service to handle in the
          most efficient way possible. In this case, since we have potential sub-objects keyed
          off of fields in the main table, if the consumer does not request the key field, but
          does request the sub-object, our response will be incorrect. It is actually more
          efficient in this case to select all of the fields and then remove the ones not
          requested.
        */
        $fieldList = array();
        if ($request->isPartial()) {
            $fieldList = $request->getPartialRequestFields();
        }

        $subObjects = $request->getSubObjects();

        if ($classId) {

            $record = $this->dbh->queryfirstrow_assoc("
          select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
            from class
            where class_id = ?
        ", $classId);

            if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            } else {
                $record = $this->camelCaseKeys($record);

                // Save the apiLocation in case courseId is not in the requested list.
                // Add the apiLocation after filtering the response.
                $apiLocation = $this->locationFor('citest.class.classId',
                    array('classId' => $record['classId']));

                $classId = $record['classId'];
                $courseId = $record['courseId'];

                if ($fieldList) {
                    foreach (array_keys($record) as $field) {
                        if (!in_array($field, $fieldList)) {
                            unset($record[$field]);
                        }
                    };
                }

                $record['apiLocation'] = $apiLocation;

                if (is_array($subObjects) && array_key_exists('course', $subObjects)) {
                    $courseResponse = $this->callResource('citest.course.courseId', array(
                            'params' => array('courseId' => $courseId),
                            'options' => array(
                                'fields' => implode(',', $subObjects['course'])
                            )
                        )
                    );

                    $record['course'] = $this->camelCaseKeys($courseResponse->getPayload());
                }

                if (is_array($subObjects) && array_key_exists('enrollments', $subObjects)) {
                    $enrollmentResponse = $this->callResource('citest.enrollment.all', array(
                        'options' => array('classId' => $classId)
                    ));

                    $record['enrollments'] = $this->camelCaseKeys($enrollmentResponse->getPayload());

                    for ($i = 0; $i < count($record['enrollments']); $i++) {
                        $studentResponse = $this->callResource('citest.student.public.studentId', array(
                            'params' => array('studentId' => $record['enrollments'][$i]['studentId'])
                        ));

                        $studentInfo = $this->camelCaseKeys($studentResponse->getPayload());

                        $record['enrollments'][$i]['firstName'] = $studentInfo['firstName'];
                        $record['enrollments'][$i]['lastName'] = $studentInfo['lastName'];
                        $record['enrollments'][$i]['entryYear'] = $studentInfo['entryYear'];
                        $record['enrollments'][$i]['majorId'] = $studentInfo['majorId'];
                    }
                }

                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $response->setPayload($record);
            }
        } else {

            $query = "
            select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
              from class
              order by semester, course_id
          ";
            $sth = '';

            $records = array();

            if ($request->isPaged()) {
                $minRowToFetch = $request->getOffset();
                $maxRowToFetch = $minRowToFetch + $request->getLimit() - 1;

                // paged queries should include the total number of records
                $totalRecords = $this->dbh->queryfirstcolumn('
            select count(*)
              from class
          ');

                $response->setTotalObjects($totalRecords);

                if ($this->dbh->getType() == 'MySQL') {
                    $query .= ' limit ' . ($request->getOffset() - 1) . ', ' . $request->getLimit();
                    $sth = $this->dbh->prepare($query);
                } else {
                    $query = "
            select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
              from ( select /*+ FIRST_ROWS(n) */
                a.*, ROWNUM rnum
                  from ( $query ) a
                  where ROWNUM <= :MAX_ROW_TO_FETCH )
            where rnum  >= :MIN_ROW_TO_FETCH";

                    $sth = $this->dbh->prepare($query);

                    $sth->bind_by_name('MAX_ROW_TO_FETCH', $maxRowToFetch);
                    $sth->bind_by_name('MIN_ROW_TO_FETCH', $minRowToFetch);
                }

            } else {
                $sth = $this->dbh->prepare($query);
            }

            $sth->execute();

            while ($record = $sth->fetchrow_assoc()) {
                $record = $this->camelCaseKeys($record);

                // Save the apiLocation in case classId or semester are not in the requested list.
                // Add the apiLocation after filtering the response.
                $apiLocation = $this->locationFor('citest.class.classId',
                    array('classId' => $record['classId']));

                $classId = $record['classId'];
                $courseId = $record['courseId'];

                if ($fieldList) {
                    foreach (array_keys($record) as $field) {
                        if (!in_array($field, $fieldList)) {
                            unset($record[$field]);
                        }
                    };
                }

                $record['apiLocation'] = $apiLocation;

                if (is_array($subObjects) && array_key_exists('course', $subObjects)) {
                    $courseResponse = $this->callResource('citest.course.courseId', array(
                            'params' => array('courseId' => $courseId),
                            'options' => array(
                                'fields' => $subObjects['course']
                            )
                        )
                    );

                    $record['course'] = $this->camelCaseKeys($courseResponse->getPayload());
                }

                if (is_array($subObjects) && array_key_exists('enrollments', $subObjects)) {
                    $enrollmentResponse = $this->callResource('citest.enrollment.all', array(
                        'options' => array('classId' => $classId)
                    ));

                    $record['enrollments'] = $this->camelCaseKeys($enrollmentResponse->getPayload());

                    for ($i = 0; $i < count($record['enrollments']); $i++) {
                        $studentResponse = $this->callResource('citest.student.studentId', array(
                            'params' => array('studentId' => $record['enrollments'][$i]['studentId'])
                        ));

                        $studentInfo = $this->camelCaseKeys($studentResponse->getPayload());

                        $record['enrollments'][$i]['firstName'] = $studentInfo['firstName'];
                        $record['enrollments'][$i]['lastName'] = $studentInfo['lastName'];
                        $record['enrollments'][$i]['entryYear'] = $studentInfo['entryYear'];
                        $record['enrollments'][$i]['majorId'] = $studentInfo['majorId'];
                    }
                }

                $records[] = $record;
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($records);
        }

        return $response;
    }

    public function getClassPagedEnrollments()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $classId = $request->getResourceParam('classId');

        $options = $request->getOptions();

        $record = $this->dbh->queryfirstrow_assoc("
        select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
          from class
          where class_id = ?
      ", $classId);

        if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $record = $this->camelCaseKeys($record);

            $apiLocation = $this->locationFor('citest.class.classId',
                array('classId' => $record['classId']));

            $classId = $record['classId'];
            $courseId = $record['courseId'];

            $record['apiLocation'] = $apiLocation;

            $courseResponse = $this->callResource('citest.course.courseId', array(
                'params' => array('courseId' => $courseId)
            ));

            $record['course'] = $this->camelCaseKeys($courseResponse->getPayload());

            foreach ([1, 6, 11] as $offset) {
                $enrollmentResponse = $this->callResource('citest.enrollment.all', array(
                        'options' => array(
                            'classId' => $classId,
                            'limit' => 5,
                            'offset' => $offset,
                        )
                    )
                );

                $record['enrollments_' . $offset] = $this->camelCaseKeys($enrollmentResponse->getPayload());

                for ($i = 0; $i < count($record['enrollments_' . $offset]); $i++) {
                    $studentResponse = $this->callResource('citest.student.public.studentId', array(
                        'params' => array('studentId' => $record['enrollments_' . $offset][$i]['studentId'])
                    ));

                    $studentInfo = $this->camelCaseKeys($studentResponse->getPayload());

                    $record['enrollments_' . $offset][$i]['firstName'] = $studentInfo['firstName'];
                    $record['enrollments_' . $offset][$i]['lastName'] = $studentInfo['lastName'];
                    $record['enrollments_' . $offset][$i]['entryYear'] = $studentInfo['entryYear'];
                    $record['enrollments_' . $offset][$i]['majorId'] = $studentInfo['majorId'];
                }
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($record);
        }

        return $response;
    }

    public function getClassPagedEnrollmentsDefaultLimit()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $classId = $request->getResourceParam('classId');

        $options = $request->getOptions();

        $record = $this->dbh->queryfirstrow_assoc("
        select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
          from class
          where class_id = ?
      ", $classId);

        if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $record = $this->camelCaseKeys($record);

            $apiLocation = $this->locationFor('citest.class.classId',
                array('classId' => $record['classId']));

            $classId = $record['classId'];
            $courseId = $record['courseId'];

            $record['apiLocation'] = $apiLocation;

            $courseResponse = $this->callResource('citest.course.courseId', array(
                'params' => array('courseId' => $courseId)
            ));

            $record['course'] = $this->camelCaseKeys($courseResponse->getPayload());

            $enrollmentResponse = $this->callResource('citest.enrollmentPaged.all', array(
                    'options' => array(
                        'classId' => $classId,
                    )
                )
            );

            $record['enrollments'] = $this->camelCaseKeys($enrollmentResponse->getPayload());

            for ($i = 0; $i < count($record['enrollments']); $i++) {
                $studentResponse = $this->callResource('citest.student.public.studentId', array(
                    'params' => array('studentId' => $record['enrollments'][$i]['studentId'])
                ));

                $studentInfo = $this->camelCaseKeys($studentResponse->getPayload());

                $record['enrollments'][$i]['firstName'] = $studentInfo['firstName'];
                $record['enrollments'][$i]['lastName'] = $studentInfo['lastName'];
                $record['enrollments'][$i]['entryYear'] = $studentInfo['entryYear'];
                $record['enrollments'][$i]['majorId'] = $studentInfo['majorId'];
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($record);
        }

        return $response;
    }

    public function getClassFieldsArray()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $classId = $request->getResourceParam('classId');

        $options = $request->getOptions();

        $record = $this->dbh->queryfirstrow_assoc("
        select class_id, course_id, semester, section, instructor, location, meeting_time, max_enrollment
          from class
          where class_id = ?
      ", $classId);

        if ($record === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } else {
            $record = $this->camelCaseKeys($record);

            // Save the apiLocation in case courseId is not in the requested list.
            // Add the apiLocation after filtering the response.
            $apiLocation = $this->locationFor('citest.class.classId',
                array('classId' => $record['classId']));

            $classId = $record['classId'];
            $courseId = $record['courseId'];

            $record['apiLocation'] = $apiLocation;

            $courseResponse = $this->callResource('citest.course.courseId', array(
                    'params' => array('courseId' => $courseId),
                    'options' => array(
                        'fields' => ['courseSubject', 'courseNumber']
                    )
                )
            );

            $record['course'] = $this->camelCaseKeys($courseResponse->getPayload());

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($record);
        }

        return $response;
    }

    public function getResourceOptions()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $subResponse = $this->callResource('citest.reflect', array(
                'options' => array(
                    'color' => ['red', 'green'],
                    'type' => 'under',
                )
            )
        );

        $record = $subResponse->getPayload();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);

        return $response;
    }

    public function getResourceArrayData()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $subResponse = $this->callResource('citest.reflect.body', array(
                'data' => array(
                    'color' => ['red', 'green'],
                    'type' => 'under',
                )
            )
        );

        $record = $subResponse->getPayload();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);

        return $response;
    }

    public function getResourceCompose()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $subResponse = $this->callResource('citest.compose.reflect', array(
                'options' => array(
                    'compose' => ['course(courseId,courseSubject)', 'enrollments'],
                    'fields' => ['id', 'string', 'description'],
                )
            )
        );

        $record = $subResponse->getPayload();

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($record);

        return $response;
    }

}
