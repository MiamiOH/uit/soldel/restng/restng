<?php

namespace MiamiOH\RESTng\Util;

use Illuminate\Cache\ArrayStore;
use Illuminate\Cache\DatabaseStore;
use Illuminate\Cache\FileStore;
use Illuminate\Cache\NullStore;
use Illuminate\Cache\Repository;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Filesystem\Filesystem;
use MiamiOH\RESTng\Exception\InvalidArgumentException;

class CacheManager
{
    /**
     * @var string|null
     */
    private $driver;
    /**
     * @var array
     */
    private $options;

    public function __construct(?string $driver = 'array', array $options = [])
    {
        $this->driver = $driver;
        $this->options = $options;
    }

    /**
     * @return Repository
     * @throws InvalidArgumentException
     */
    public function make(): Repository
    {
        return new Repository($this->makeCacheStore($this->driver));
    }

    private function makeCacheStore(?string $driver): Store
    {
        switch ($driver) {
            case null:
                return new NullStore();

            case 'array':
                return new ArrayStore();

            case 'file':
                $path = $this->options['path'] ?? '/tmp';
                return new FileStore(new Filesystem(), $path);

            case 'database':
                $connection = $this->options['connection'] ?? 'restng';
                $table = $this->options['table'] ?? 'cache';
                return new DatabaseStore(Manager::connection($connection), $table);

            default:
                throw new InvalidArgumentException(sprintf('Unknown cache driver %s', $driver));
        }
    }
}
