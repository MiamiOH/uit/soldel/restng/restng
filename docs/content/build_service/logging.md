+++
title = "Logging"
weight = 100
+++

Classes extending the MiamiOH\RESTng\Service class will have access to the MiamiOH\RESTng\Service::log member, which is itself an instance of the log4php Logger class (http://logging.apache.org/log4php/). Within your business service methods, use the log methods appropriate for your logging needs (http://logging.apache.org/log4php/docs/introduction.html).

RESTng will log a fatal message for any exception thrown from a business service. This behavior may be expanded in the future.

Within your business service method, you can log messages through the log object:

    $this->log->debug(‘Something is happening’);

**Warning!** Every call to a log method does consuming processing time, even if the logger is not currently running at that level. Dozens of unused debug calls can have a measurable impact on performance. Use your log powers wisely.

## Logger Configuration

The log4php configuration is found in the config/log4php.conf.php file. We opted to use the PHP style configuration rather than XML in order to benefit from PHP code acceleration. This can make configuration slightly more challenging, but basic logger configuration is straightforward.

The log configuration is not under source code control. Operational changes will be made to the test and production log configuration as warranted by support needs.

**Caution for shared environments** We ask that you only modify logger configurations that you are responsible for. The log configuration is shared among all developers and coordination of changes is important. Also, do not keep the configuration file open in your editor. If two developers open the file for editing, the last one to save will overwrite the changes of the other. The shorter the duration of editing, the less likely a conflict will occur. We may be able to decentralize the logger configuration in the future.

The root logger is configured with a level of WARN and with a file appender. All other loggers will inherit from this logger.  If you need to see more detailed messages for a class, such as DEBUG or INFO, you should create a logger for the class rather than modifying the root logger.

### Create a New Logger

To create a new logger, add a new array entry to the ‘loggers’ key in the configuration array. The key should be the fully qualified name of the class you are working on. The value is an array containing logger options.

Given the following class definition:

{{< highlight php "linenos=table" >}}
<?php
...
namespace MiamiOH\RESTng\Service\Course;

class Section extends \MiamiOH\RESTng\Service {
    ...
{{< / highlight >}} 

A logger can be defined using:

{{< highlight php "linenos=table" >}}
<?php
...
'loggers' => array(
        ...
        'MiamiOH\RESTng\Service\Course\Section' => array(
            'appenders' => array('myLogFile'),
            'level' => 'debug',
        ),
        ...
{{< / highlight >}} 

The log file 'myLogFile' would be found in the http server log location.