<?php

class resourceRunnerCorsTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $resourceRunner;
  private $restngApp;

  private $config = array();
  private $isOptions = false;

  private $responseHeaders = array();
  private $lastHeaderSet = '';

  private $expectedHeaders = array(
      'Access-Control-Allow-Origin',
      'Access-Control-Allow-Credentials',
      'Access-Control-Max-Age',
      'Access-Control-Allow-Methods',
      'Access-Control-Allow-Headers',
    );

  public function setUp(): void
  {
    parent::setUp();

    $this->config = array(
      'action' => '',
      'name' => '',
      'service' => '',
      'method' => '',
      'xOrgSec' => array(
          'allow-origin' => 'any',
          'allow-credentials' => true,
          'max-age' => 3600,
          'allow-methods' => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'),
          'allow-headers' => '',
        ),
    );
    $this->isOptions = false;

    $this->responseHeaders = array();
    $this->lastHeaderSet = '';

    $this->restngApp = $this->getMockBuilder('MiamiOH\RESTng\Slim\Mock')
          ->setMethods(array('response', 'request', 'router', 'urlFor', 'getService', 'apiRender'))
          ->getMock();

    $this->restngApp->request = $this->getMockBuilder('MiamiOH\RESTng\Slim\Request\Mock')
          ->setMethods(array('isOptions'))
          ->getMock();

    $this->restngApp->response = $this->getMockBuilder('MiamiOH\RESTng\Slim\Response\Mock')
          ->setMethods(array('header'))
          ->getMock();

    $this->restngApp->method('response')->willReturn($this->restngApp->response);

    $this->restngApp->request->expects($this->once())->method('isOptions')
      ->will($this->returnCallback(array($this, 'isOptionsMock')));

    $this->restngApp->response->method('header')
      ->with($this->callback(array($this, 'headerNameWith')),
        $this->callback(array($this, 'headerValueWith')));

    $this->restngApp->environment = array();

    $this->resourceRunner = new \MiamiOH\RESTng\Util\ResourceRunner();
    \MiamiOH\RESTng\Util\ResourceRunner::$underTest = true;

    $this->resourceRunner->setAppInstance($this->restngApp);

  }

  public function testRunPreflight() {

    $this->assertNotNull($this->resourceRunner->getAppInstance());

    $this->config['action'] = 'read';
    $this->config['name'] = 'test.resource';
    $this->config['service'] = 'TestService';
    $this->config['method'] = 'businessMethod';

    $this->isOptions = true;
    $this->restngApp->environment['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] = 'POST';
    $this->restngApp->environment['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'] = 'x-my-header';

    $this->resourceRunner->handleCors($this->config);

    $this->assertTrue(is_array($this->responseHeaders));
    $this->assertEquals(2, count($this->responseHeaders));
    $this->assertEquals('GET, POST, PUT, DELETE, OPTIONS', $this->responseHeaders['Access-Control-Allow-Methods']);
    $this->assertEquals('x-my-header', $this->responseHeaders['Access-Control-Allow-Headers']);

  }

  public function testRunOptions() {

    $this->assertNotNull($this->resourceRunner->getAppInstance());

    $this->config['action'] = 'read';
    $this->config['name'] = 'test.resource';
    $this->config['service'] = 'TestService';
    $this->config['method'] = 'businessMethod';

    $this->restngApp->environment['HTTP_ORIGIN'] = 'http://example.com';

    $this->resourceRunner->handleCors($this->config);

    $this->assertTrue(is_array($this->responseHeaders));
    $this->assertEquals(3, count($this->responseHeaders));
    $this->assertEquals('http://example.com', $this->responseHeaders['Access-Control-Allow-Origin']);
    $this->assertEquals('true', $this->responseHeaders['Access-Control-Allow-Credentials']);
    $this->assertEquals('3600', $this->responseHeaders['Access-Control-Max-Age']);

  }

  public function isOptionsMock() {
    return $this->isOptions;
  }

  public function headerNameWith($subject) {
    $this->lastHeaderSet = $subject;

    $this->assertFalse(array_key_exists($subject, $this->responseHeaders));
    $this->assertTrue(in_array($subject, $this->expectedHeaders));

    $this->responseHeaders[$subject] = '';

    return true;
  }

  public function headerValueWith($subject) {
    $this->assertNotEquals('', $this->lastHeaderSet);
    $this->assertTrue(isset($this->responseHeaders[$this->lastHeaderSet]));

    $this->responseHeaders[$this->lastHeaderSet] = $subject;

    $this->lastHeaderSet = '';

    return true;
  }

}
