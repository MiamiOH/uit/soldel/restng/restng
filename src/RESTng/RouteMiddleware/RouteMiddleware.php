<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 12:30 PM
 */

namespace MiamiOH\RESTng\RouteMiddleware;


use MiamiOH\RESTng\App;

abstract class RouteMiddleware implements RouteMiddlewareInterface
{
    public function install(App $app, array $options): callable
    {
        $mw = $this;
        return function ($route) use ($mw, $app, $options) {
            $mw->call($app, $options);
        };

    }
}