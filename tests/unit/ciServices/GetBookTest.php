<?php

class GetBookTest extends \MiamiOH\RESTng\Testing\TestCase
{

  protected $bookObj;

  protected $testBooks = [];

  public function setUp(): void
  {
    parent::setUp();

    $this->initializeData();

    /*
        The API service handles a lot of the coordination between the consumer
        interface layer and the business service layer. We mock that here
        so that we have complete control of what happens.
    */
    $app = $this->getMockBuilder('\MiamiOH\RESTng\App')
          ->setMethods(array('newResponse', 'callResource', 'locationFor'))
          ->getMock();

    /*
        We will use the real Response class in this case. If we wanted to enforce
        specific behaviors of the response, we could mock it as well.
    */
    $app->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

    /*
        Create a real instance of our subject under test.
    */
    $this->bookObj = new Tests\Service\CITest\Book();

    /*
        Inject the mock API service using the RESTng\Service::setApi method. Since
        all business services are supposed to inherit from that class, this method
        should exist.
    */
    $this->bookObj->setApp($app);

  }

  public function testGetBook() {

    /*
        Create a mock database handle. This will be used to respond to any queries
        coming from the subject under test.
    */
    $dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc'))
            ->getMock();

    $dbh->method('queryfirstrow_assoc')->willReturn($this->testBooks[0]);

    /*
        Create a mock database service. The database service is normally injected
        by the framework when a service asks for it. We will inject the mock so
        we can return our mock database handle.
    */
    $db = $this->getMockBuilder('MiamiOH\RESTng\Connector\DatabaseFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($dbh);

    /*
        Inject the mock Database service. The subject under test declares the setter
        method in this case.
    */
    $this->bookObj->setDatabase($db);

    /*
        In normal operation, the framework constructs and provides the Request
        object. For our test, we want to mock the Request so we can apply very
        specific values.
    */
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    /*
        The getResourceParam and getOptions methods are called within the
        business service. Using our mock Request object, we can respond with
        exactly the data we need for our test.
    */
    $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn(1);
    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->bookObj->setRequest($request);

    $resp = $this->bookObj->getBook();

    $payload = $resp->getPayload();

    $this->assertArrayHasKey('id', $payload);
    $this->assertArrayHasKey('title', $payload);

    $this->assertEquals(1, $payload['id']);

  }

  public function testGetBookNotFound() {

    $dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc'))
            ->getMock();

    $dbh->method('queryfirstrow_assoc')->willReturn(array());

    $db = $this->getMockBuilder('MiamiOH\RESTng\Connector\DatabaseFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($dbh);

    $this->bookObj->setDatabase($db);

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn(2);
    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->bookObj->setRequest($request);

    $resp = $this->bookObj->getBook();

    $payload = $resp->getPayload();

    $this->assertArrayNotHasKey('id', $payload);
    $this->assertArrayNotHasKey('title', $payload);

  }

  public function testGetBookList() {

    $dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

    $dbh->method('queryall_array')->willReturn($this->testBooks);

    $db = $this->getMockBuilder('MiamiOH\RESTng\Connector\DatabaseFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($dbh);

    $this->bookObj->setDatabase($db);

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn('');
    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->bookObj->setRequest($request);

    $resp = $this->bookObj->getBook();

    $payload = $resp->getPayload();

    $this->assertCount(count($this->testBooks), $payload);
  }

  public function testGetBookListPaged() {

    $dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryfirstcolumn'))
            ->getMock();

    $dbh->method('queryall_array')->willReturn($this->testBooks);
    $dbh->method('queryfirstcolumn')->willReturn(count($this->testBooks));

    $db = $this->getMockBuilder('MiamiOH\RESTng\Connector\DatabaseFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($dbh);

    $this->bookObj->setDatabase($db);

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'isPaged', 'getOffset', 'getLimit'))
            ->getMock();

    $offset = 1;
    $limit = 10;

    $request->expects($this->once())->method('getResourceParam')->with($this->equalTo('id'))->willReturn('');
    $request->expects($this->once())->method('getOptions')->willReturn(array());
    $request->expects($this->once())->method('isPaged')->willReturn(true);
    $request->expects($this->once())->method('getOffset')->willReturn($offset);
    $request->expects($this->once())->method('getLimit')->willReturn($limit);

    $this->bookObj->setRequest($request);

    $resp = $this->bookObj->getBook();

    $payload = $resp->getPayload();

    $this->assertCount($limit, $payload);
    $this->assertEquals(count($this->testBooks), $resp->getTotalObjects());
  }

  private function initializeData() {
    $this->testBooks = [];

    for ($i = 1; $i <= 100; $i++) {
      $this->testBooks[] = array('id' => $i, 'title' => 'My Book Title ' . $i);
    }
    }

}
