<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/21/18
 * Time: 1:13 PM
 */

namespace MiamiOH\RESTng\Testing;

use Illuminate\Support\Str;
use MiamiOH\RESTng\RouteMiddleware\Authentication;
use MiamiOH\RESTng\RouteMiddleware\Authorization;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Util\ResourceRunner;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait MakesHttpResourceRequests
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
trait MakesHttpResourceRequests
{
    /** @var \MiamiOH\RESTng\App $app */
    protected $app;

    private $middlewareCount = 0;

    private $showExceptions = false;

    private $services = [];
    private $observations = [];
    protected $defaultHeaders = [];
    protected $defaultCookies = [];
    protected $cookies = [];

    public function showExceptions(): void
    {
        $this->showExceptions = true;
    }

    public function hideExceptions(): void
    {
        $this->showExceptions = false;
    }

    public function withMiddleware()
    {
        // Not implemented
    }

    public function withoutMiddleware()
    {
        // Not implemented
    }

    public function useService(array $service): void
    {
        $this->services[] = $service;
    }

    private function parseUri(string $uri): array
    {
        $parts = explode('?', $uri);

        return [$parts[0], $parts[1] ?? ''];
    }

    public function withHeaders(array $headers)
    {
        $this->defaultHeaders = array_merge($this->defaultHeaders, $headers);

        return $this;
    }

    public function withHeader(string $name, string $value)
    {
        $this->defaultHeaders[$name] = $value;

        return $this;
    }

    public function flushHeaders()
    {
        $this->defaultHeaders = [];

        return $this;
    }

    public function get($uri, array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('GET', $uri, [], $cookies, [], $server);
    }

    public function getJson($uri, array $headers = []): TestResponse
    {
        return $this->json('GET', $uri, [], $headers);
    }

    public function post($uri, array $data = [], array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('POST', $uri, $data, $cookies, [], $server);
    }

    public function postJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->json('POST', $uri, $data, $headers);
    }

    public function put($uri, array $data = [], array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('PUT', $uri, $data, $cookies, [], $server);
    }

    public function putJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->json('PUT', $uri, $data, $headers);
    }

    public function patch($uri, array $data = [], array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('PATCH', $uri, $data, $cookies, [], $server);
    }

    public function patchJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->json('PATCH', $uri, $data, $headers);
    }

    public function delete($uri, array $data = [], array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('DELETE', $uri, $data, $cookies, [], $server);
    }

    public function deleteJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->json('DELETE', $uri, $data, $headers);
    }

    public function options($uri, array $data = [], array $headers = []): TestResponse
    {
        $server = $this->transformHeadersToServerVars($headers);
        $cookies = $this->prepareCookiesForRequest();

        return $this->call('OPTIONS', $uri, $data, $cookies, [], $server);
    }

    public function optionsJson($uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->json('OPTIONS', $uri, $data, $headers);
    }

    public function json($method, $uri, array $data = [], array $headers = []): TestResponse
    {
        $files = $this->extractFilesFromDataArray($data);

        $content = json_encode($data);

        $headers = array_merge([
            'CONTENT_LENGTH' => mb_strlen($content, '8bit'),
            'CONTENT_TYPE' => 'application/json',
            'Accept' => 'application/json',
        ], $headers);

        return $this->call(
            $method, $uri, [], [], $files, $this->transformHeadersToServerVars($headers), $content
        );
    }

    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null): TestResponse
    {
        list($uri, $queryString) = $this->parseUri($uri);

        $env = [
            'REQUEST_METHOD' => $method,
            'SCRIPT_NAME' => '/api',
            'PATH_INFO' => $uri,
            'QUERY_STRING' => $queryString,
            'CONTENT_TYPE' => 'application/json',
        ];

        $env = $this->addServerVarsToEnv($env, $server);
        $env = $this->addContentToEnv($env, $content);

        /** @var Transaction $transaction */
        $transaction = $this->app->getService('ApmTransaction');
        // Ensure the ApmTransaction is started. Some tests make multiple transactions.
        $transaction->restart();

        $this->app->environment = \Slim\Environment::mock($env);
        $this->app->request = new \Slim\Http\Request($this->app->environment);
        $this->app->response = new \Slim\Http\Response();

        $this->app->addRouteMiddleware('authenticate', new Authentication(), 1);
        $this->app->addRouteMiddleware('authorize', new Authorization(), 2);

        $this->addMiddleware(new \MiamiOH\RESTng\Middleware\ContentBody());
        $this->addMiddleware(new \MiamiOH\RESTng\Middleware\ContentFormat());

        // Override the normal behavior so PHPUnit will get the exception
        if ($this->showExceptions) {
            $this->app->error(function (\Exception $e) {
                throw $e;
            });
        }

        $this->app->loadConfigForResourcePattern($this->app->environment['PATH_INFO']);

        foreach ($this->services as $service) {
            $this->app->useService($service);
        }
        $this->services = [];

        \MiamiOH\RESTng\View\Base::$stopAfterRender = false;

        list($status, $headers, $body) = $this->app->pseudoRun();

        $this->observations = $this->app->collectObservations();

        $this->reset();

        return $this->createTestResponse(new Response($body, $status, $headers->all()));
    }

    protected function createTestResponse($response): TestResponse
    {
        return TestResponse::fromBaseResponse($response);
    }

    protected function transformHeadersToServerVars(array $headers): array
    {
        return collect(array_merge($this->defaultHeaders, $headers))->mapWithKeys(function ($value, $name) {
            $name = strtr(strtoupper($name), '-', '_');

            return [$this->formatServerHeaderKey($name) => $value];
        })->all();
    }

    protected function formatServerHeaderKey($name)
    {
        if (! Str::startsWith($name, 'HTTP_') && $name !== 'CONTENT_TYPE' && $name !== 'REMOTE_ADDR') {
            return 'HTTP_'.$name;
        }

        return $name;
    }

    protected function prepareCookiesForRequest(): array
    {
        return array_merge($this->defaultCookies, $this->cookies);
    }

    protected function extractFilesFromDataArray(&$data): array
    {
        $files = [];

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $files[$key] = $this->extractFilesFromDataArray($value);

                $data[$key] = $value;
            }
        }

        return $files;
    }

    private function addServerVarsToEnv(array $env, array $server): array
    {
        return array_merge($env, $server);
    }

    private function addContentToEnv(array $env, $content = null): array
    {
        if (!empty($content)) {
            $env['slim.input'] = $content;
        }

        return $env;
    }

    private function addMiddleware(\Slim\Middleware $mw): void
    {
        $this->app->add($mw);
        $this->middlewareCount++;
    }

    private function reset(): void
    {
        while ($this->middlewareCount > 0) {
            $this->app->removeLastMiddleware();
            $this->middlewareCount--;
        }

        $this->app->clearObservations();
    }

    public function assertAction(string $action): void
    {
        $runnerFacts = $this->findRunnerFacts();

        $this->assertEquals($action, $runnerFacts['action']);
    }

    public function assertMethod(string $method): void
    {
        $runnerFacts = $this->findRunnerFacts();

        $this->assertEquals($method, $runnerFacts['method']);
    }

    public function assertServiceClass(string $class): void
    {
        $runnerFacts = $this->findRunnerFacts();

        $this->assertEquals($class, $runnerFacts['serviceClass']);
    }

    public function assertRouteName(string $name): void
    {
        $runnerFacts = $this->findRunnerFacts();

        $this->assertEquals($name, $runnerFacts['routeName']);
    }

    public function assertRoutePattern(string $pattern): void
    {
        $runnerFacts = $this->findRunnerFacts();

        $this->assertEquals($pattern, $runnerFacts['routePattern']);
    }

    private function findRunnerFacts(): array
    {
        $runnerFacts = array_filter(
            $this->observations,
            function ($entry) {
                return $entry['class'] === ResourceRunner::class;
            }
        );

        if (empty($runnerFacts)) {
            throw new \Exception('Runner facts not found');
        }

        return array_shift($runnerFacts);
    }
}
