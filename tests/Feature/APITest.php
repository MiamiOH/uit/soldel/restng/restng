<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use Tests\FeatureTestCase;

class APITest extends FeatureTestCase
{

    public function setUp(): void
    {
        parent::setUp();

        // This is the minimum mock needed to get the class working
        $ds = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $dbh = $this->createMock(\MiamiOH\RESTng\Legacy\DB\DBH\OCI8::class);

        $ds->method('getHandle')->willReturn($dbh);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $ds,
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
        ]);
    }

    public function testEnforcesRequiredOption(): void
    {
        $response = $this->getJson('/citest/filter');

        $response
            ->assertStatus(500)
            ->assertJson([
                'data' => [
                    'message' => 'Missing required option type for resource citest.filter'
                    ],
            ]);
    }

    public function testCanParseParamValueAsList(): void
    {
        $response = $this->getJson('/citest/reflect/options?color=blue,yellow,green');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'color' => ['blue', 'yellow', 'green']
                    ],
            ]);
    }

    public function testCanParseSingleParamValueAsList(): void
    {
        $response = $this->getJson('/citest/reflect/options?color=blue');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'color' => ['blue']
                    ],
            ]);
    }

    public function testSupportsProviderResourceResolution(): void
    {
        $response = $this->getJson('/citest/locationFor?resource=citest.reflect.options');

        $response->assertStatus(200);
    }

    public function testSupportsProviderResourceResolutionInDifferentNamespace(): void
    {
        $response = $this->getJson('/citest/locationFor?resource=time.get');

        $response->assertStatus(200);
    }

    public function testSupportsProviderResourceResolutionInMultipleDifferentNamespace(): void
    {
        $response = $this->getJson('/citest/locationFor?resource=time.get,date.get');

        $response->assertStatus(200);
    }
}
