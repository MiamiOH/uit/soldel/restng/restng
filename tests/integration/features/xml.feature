# Test the content type handling by using extensions and headers
Feature: Specify incoming and outgoing content types using extensions and headers
 As a developer using the RESTng API
 I want to specify content type by extension and header
 In order to have control of the data format

 Scenario: Get a model in xml by specifying the extension
   Given a REST client
   When I make a GET request for /citest/record/1.xml
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"
