<?php

include_once('dbOciMocks.php');

class oci8SthLegacyTest extends \PHPUnit\Framework\TestCase
{

    private $sth;

    /**
     * @var \MiamiOH\RESTng\Service\Apm\Transaction|\PHPUnit\Framework\MockObject\MockObject
     */
    private $transaction;

    private $dbh;
    private $dbConnection;

    private $sthResult = '';
    private $query = '';

    public function setUp(): void
    {
        parent::setUp();

        $this->sthResult = '';
        $this->query = '';

        $this->transaction = $this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class);

        $this->dbConnection = new StdClass();

        $this->dbh = $this->getMockBuilder('MiamiOH\RESTng\Legacy\DB\DBH\OCI8')
            ->setMethods(array('getDbConnection'))
            ->getMock();

        $this->dbh->expects($this->once())->method('getDbConnection')
            ->willReturn($this->dbConnection);

        $this->sth = '';

    }

    public function testSth()
    {

        $this->query = 'select first, last from table where status = ?';

        $this->sth = new \MiamiOH\RESTng\Legacy\DB\STH\OCI8($this->dbh, $this->sthResult, $this->query);

        $this->assertTrue($this->sth !== null);

    }

    public function testOciSthExecute()
    {

        $preparedStatement = new StdClass();
        $executeResult = new StdClass();

        \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockParseResult', $preparedStatement);
        \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockExecuteResult', $executeResult);
        \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockNumFields', 2);

        $this->query = 'select first, last from table where status = ?';

        $this->sth = new \MiamiOH\RESTng\Legacy\DB\STH\OCI8($this->dbh, $this->sthResult, $this->query);
        $this->sth->setApmTransaction($this->transaction);

        $result = $this->sth->execute(1);

        $this->assertTrue($result);

        $this->assertNotEmpty(\MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('executeStatement'));

    }

}
