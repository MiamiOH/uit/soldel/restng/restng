<?php

namespace MiamiOH\RESTng\Service\Apm;

use Jasny\DB\MySQL\QuerySplitter;

trait CreatesDatabaseQuerySpans
{
    public static function toMilliseconds(float $time): float
    {
        return round($time * 1000, 3);
    }

    public static function getQueryName(string $sql, string $fallback = 'Database Query'): string
    {
        try {
            $query_type = QuerySplitter::getQueryType($sql);
            $tables = QuerySplitter::splitTables($sql);

            if (isset($query_type) && is_array($tables)) {
                // Query type and tables
                return $query_type . ' ' . implode(', ', array_values($tables));
            }

            return $fallback;
        } catch (\Exception $e) {
            return $fallback;
        }
    }
}
