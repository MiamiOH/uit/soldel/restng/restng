<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 2:19 PM
 */

namespace MiamiOH\RESTng\Testing\Data;

/**
 * Trait UsesTestData
 * @package MiamiOH\RESTng\Testing\Data
 *
 * @codeCoverageIgnore 
 */
trait UsesTestData
{
    /** @var Factory */
    private $factory;

    protected function useData(): \MiamiOH\RESTng\Testing\Data\Factory
    {
        if ($this->factory === null) {
            $this->factory = new Factory();
        }

        return $this->factory;
    }
}