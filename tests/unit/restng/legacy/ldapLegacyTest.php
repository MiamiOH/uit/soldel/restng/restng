<?php

include_once('ldapMocks.php');

class ldapLegacyTest extends \PHPUnit\Framework\TestCase {

  private $ldap;

  public function setUp(): void
  {
      parent::setUp();

      \MiamiOH\RESTng\Legacy\LdapMockData::reset();

      $this->ldap = new \MiamiOH\RESTng\Legacy\LDAP();

  }

  public function testLdap() {

    $ldap = new \MiamiOH\RESTng\Legacy\LDAP('ldap.miamioh.edu');

    $this->assertTrue($ldap !== null);
    $this->assertTrue($ldap->connected);

  }

  public function testLdapConnect() {

    $this->ldap->connect('ldap.miamioh.edu');

    $this->assertTrue($this->ldap !== null);
    $this->assertEquals('ldap.miamioh.edu', \MiamiOH\RESTng\Legacy\LdapMockData::get('server'));
    $this->assertEquals('389', \MiamiOH\RESTng\Legacy\LdapMockData::get('port'));
    $this->assertTrue($this->ldap->connected);

  }

  public function testLdapPort() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);
    $this->assertEquals('ldap.miamioh.edu', \MiamiOH\RESTng\Legacy\LdapMockData::get('server'));
    $this->assertEquals(636, \MiamiOH\RESTng\Legacy\LdapMockData::get('port'));
    $this->assertTrue($this->ldap->connected);

  }

  public function testLdapBind() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);

    $result = $this->ldap->bind('uid=bob,dc=example,dc=com', 's3kret');

    $this->assertTrue($result);

  }

  public function testLdapBindFail() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);

    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockLoginResult', false);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockErrorNo', 1);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockError', 'Authentication failed');
    $result = $this->ldap->bind('uid=bob,dc=example,dc=com', 's3kret');

    $this->assertFalse($result);
    $this->assertEquals(1, $this->ldap->error_num);
    $this->assertEquals('Authentication failed', $this->ldap->error);

  }

  public function testLdapAuthenticateMissingArgs() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);

    $result = $this->ldap->authenticate();

    $this->assertEquals(-1, $result);
    $this->assertEquals(-2, $this->ldap->error_num);
    $this->assertEquals('Missing credentials', $this->ldap->error);

  }

  public function testLdapAuthenticateNotFound() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);

    $result = $this->ldap->authenticate('uid=bob,dc=example,dc=com', 's3kret');

    $this->assertEquals(0, $result);
    $this->assertEquals(-3, $this->ldap->error_num);
    $this->assertEquals('ID not found', $this->ldap->error);

  }

  public function testLdapAuthenticate() {

    $this->ldap->connect('ldap.miamioh.edu', 636);

    $this->assertTrue($this->ldap !== null);

    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResponse', true);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResultCount', 1);
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockSearchResultData', array(
        array(
          'uid' => 'bob',
          'cn' => 'Bob Jones',
          'mailalternateAddress' => 'bob@example.com',
          'ou' => 'dc=example,dc=com',
          'sn' => 'Jones',
          'givenname' => 'Bob'
        )
      ));
    \MiamiOH\RESTng\Legacy\LdapMockData::set('mockLdapDn', 'uid=bob,dc=example,dc=com');
    $result = $this->ldap->authenticate('uid=bob,dc=example,dc=com', 's3kret');

    $this->assertTrue($result);

  }

}
