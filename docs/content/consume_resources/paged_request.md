+++
title = "Paged Requests"
weight = 40
+++

REST resources which return large numbers of objects for read (HTTP GET) requests should support paging. (The resource developer must implement paging in the service, so make sure the resource is declared as pageable.) Paging allows the consumer to request a subset of objects and "page" through them in an ordered fashion.

To request a paged resource, use the following options in your query string:

Option | Description
-------|------------
limit  | (Integer) The maximum number of objects to return.
offset | (Integer) [Optional] The starting object number within the set, inclusive. If not provided, the offset will be 1.

An example request would be:

    https://ws.miamioh.edu/api/citest/record?limit=10&offset=1

## Paged Response

A paged response contains meta-data about the data set. These elements are present in the top level of the response payload object.

Name        | Description
------------|--------------
total       | (Integer) The total number of objects in the data set.
currentUrl  | (URL) The URL to the current page of objects.
firstUrl    | (URL) The URL to the first page of objects.
lastUrl     | (URL) The URL to the last page of objects.
nextUrl     | (URL) The URL to the next page of objects. Only present when there are subsequent pages.
prevUrl     | (URL) The URL to the previous page of objects. Only present when there are previous pages.

