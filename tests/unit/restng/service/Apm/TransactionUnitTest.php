<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-01-26
 * Time: 10:11
 */

namespace Tests\unit\restng\service\Apm;

use MiamiOH\RESTng\Exception\TransactionSpanNotFoundException;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\User;
use Nipwaayoni\Agent;
use Nipwaayoni\Config;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\UnitTestCase;

class TransactionUnitTest extends UnitTestCase
{

    /** @var Transaction */
    private $transaction;

    /** @var Agent|MockObject */
    private $agent;

    /** @var \Nipwaayoni\Events\Transaction|MockObject  */
    private $apmTransaction;

    public function setUp(): void
    {
        parent::setUp();

        $this->apmTransaction = $this->createMock(\Nipwaayoni\Events\Transaction::class);
        $this->apmTransaction->method('getId')->willReturn('123');
        $this->apmTransaction->method('getTraceId')->willReturn('abcd');

        $this->agent = $this->createMock(Agent::class);
        $this->agent->method('startTransaction')->willReturn($this->apmTransaction);

        $this->transaction = new Transaction($this->agent);
    }

    public function testCanSetMetadata(): void
    {
        $this->transaction->setMetadata(['result' => 200]);

        $metadata = $this->transaction->metadata();
        $this->assertEquals(200, $metadata['result']);
    }

    public function testMetadataTypeIsHttpByDefault(): void
    {
        $this->transaction->setMetadata(['result' => 200]);

        $metadata = $this->transaction->metadata();
        $this->assertEquals('HTTP', $metadata['type']);
    }

    public function testCanOverrideDefaultMetadataType(): void
    {
        $this->transaction->setMetadata(['type' => 'Other', 'result' => 200]);

        $metadata = $this->transaction->metadata();
        $this->assertEquals(200, $metadata['result']);
        $this->assertEquals('Other', $metadata['type']);
    }

    public function testCanSetUserContext(): void
    {
        /** @var User|MockObject $user */
        $user = $this->createMock(User::class);
        $user->method('isAuthenticated')->willReturn(true);
        $user->method('getUsername')->willReturn('bob');

        $this->transaction->setUserContext($user);

        $this->assertEquals(['id' => 'bob'], $this->transaction->userContext());
    }

    public function testCanSetRequestContext(): void
    {
        $context = [
            'params' => ['id' => 1],
            'options' => ['color' => 'green'],
        ];

        /** @var Request|MockObject $request */
        $request = $this->createMock(Request::class);
        $request->method('getResourceParams')->willReturn($context['params']);
        $request->method('getOptions')->willReturn($context['options']);

        $this->transaction->setRequestContext($request);

        $this->assertEquals($context, $this->transaction->requestContext());
    }

    public function testCanStartSpan(): void
    {
        $this->assertCount(0, $this->transaction->spans());

        $this->transaction->startSpan('span-name');

        $this->assertCount(1, $this->transaction->spans());
    }

    public function testCanStartQuerySpan(): void
    {
        $this->assertCount(0, $this->transaction->spans());

        $this->transaction->startQuerySpan('span-name', 'sql');

        $this->assertCount(1, $this->transaction->spans());
    }

    public function testCanSendToApm(): void
    {
        $this->agent->expects($this->once())->method('send');

        $this->transaction->send();
    }

    public function testAddsSpansToTransaction(): void
    {
        $this->agent->expects($this->once())->method('send');

        $this->transaction->setName('transact1');
        $span = $this->transaction->startSpan('span-name');
        $span->stop();

        $this->transaction->send();
    }

    public function testCatchesAndSendsException(): void
    {
        $this->agent->expects($this->once())->method('captureThrowable');
        $this->agent->expects($this->once())->method('send');

        $this->transaction->captureThrowable(new \Exception());
    }
}
