<?php

namespace RESTng;

const MU_API_URL = 'https://wsdev.miamioh.edu';
const DATASOURCE_FILE = 'DataSources.xml';
const DATASOURCE_DECRYPT = '\RESTng\_MU_DS_PASSWORD_DECRYPT';

// Errors will not be reported back in the response if display errors is off
ini_set( "display_errors", "on" );
error_reporting( E_ALL | E_STRICT );

function _MU_DS_PASSWORD_DECRYPT ($password) {
  return $password;
}

