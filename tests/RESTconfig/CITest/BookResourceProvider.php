<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class BookResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Book',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'integer',
                    'format' => 'int64',
                ),
                'name' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Book.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Book'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestBook',
            'class' => 'Tests\Service\CITest\Book',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'validator' => array('type' => 'service', 'name' => 'APIValidationFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.book.all',
            'description' => 'A pageable collection of book records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book',
            'service' => 'CITestBook',
            'method' => 'getBook',
            'isPageable' => true,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of books',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Book.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.book.all.paged',
            'description' => 'A pageable collection of book records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/bookPaged',
            'service' => 'CITestBook',
            'method' => 'getBook',
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of books',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Book.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.book.id',
            'description' => 'A single test book.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book/:id',
            'service' => 'CITestBook',
            'method' => 'getBook',
            'params' => array(
                'id' => array('description' => 'A test book id'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A book',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Book',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Book not found'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.book.create',
            'description' => 'Creates a new book object.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book',
            'service' => 'CITestBook',
            'method' => 'addBook',
            'middleware' => array(),
            'body' => array(
                'description' => 'A book object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/CITest.Book'
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'The newly created book',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Book',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'citest.book.update',
            'description' => 'Updates the given book object.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book/:id',
            'service' => 'CITestBook',
            'method' => 'updateBook',
            'params' => array(
                'id' => array('description' => 'A book id'),
            ),
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'citest.book.delete',
            'description' => 'Deletes the given book object.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book/:id',
            'service' => 'CITestBook',
            'method' => 'deleteBook',
            'params' => array(
                'id' => array('description' => 'A book id'),
            ),
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'citest.book.delete.collection',
            'description' => 'Deletes the books contained in the provided collection.',
            'tags' => array('CITest'),
            'pattern' => '/citest/book',
            'service' => 'CITestBook',
            'method' => 'deleteBooks',
            'middleware' => array(),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'citest.validated.book.create',
            'description' => 'Creates a new book object.',
            'tags' => array('CITest'),
            'pattern' => '/citest/validated/book',
            'service' => 'CITestBook',
            'method' => 'addBookValidated',
            'middleware' => array(),
            'body' => array(
                'description' => 'A book object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/CITest.Book'
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'The newly created book',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Book',
                    )
                ),
            )
        ));

    }
}