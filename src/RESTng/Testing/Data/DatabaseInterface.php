<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/1/18
 * Time: 1:58 PM
 */

namespace MiamiOH\RESTng\Testing\Data;


interface DatabaseInterface
{
    public function query(string $query);
    public function exec(string $query): bool;
}