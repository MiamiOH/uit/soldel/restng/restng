<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\CITest;


use MiamiOH\RESTng\Util\ResourceProvider;

class CourseResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CITest.Course',
            'type' => 'object',
            'properties' => array(
                'courseId' => array(
                    'type' => 'string',
                ),
                'courseSubject' => array(
                    'type' => 'string',
                ),
                'courseNumber' => array(
                    'type' => 'string',
                ),
                'description' => array(
                    'type' => 'string',
                ),
                'credits' => array(
                    'type' => 'integer',
                    'format' => 'int64',
                ),
                'mpCategory' => array(
                    'type' => 'string',
                ),
                'technical' => array(
                    'type' => 'boolean',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'CITest.Course.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CITest.Course'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CITestCourse',
            'class' => 'Tests\Service\CITest\Course',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.course.all',
            'description' => 'Pageable collection of course records.',
            'tags' => array('CITest'),
            'pattern' => '/citest/course',
            'service' => 'CITestCourse',
            'method' => 'getCourse',
            'isPageable' => true,
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A collection of courses',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CITest.Course.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'citest.course.courseId',
            'description' => 'A single test course.',
            'tags' => array('CITest'),
            'pattern' => '/citest/course/:courseId',
            'service' => 'CITestCourse',
            'method' => 'getCourse',
            'isPartialable' => true,
            'params' => array(
                'courseId' => array('description' => 'A test course id'),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A course object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/CITest.Course',
                    )
                ),
                \MiamiOH\RESTng\App::API_NOTFOUND => array(
                    'description' => 'Course Id not found'
                )
            )
        ));
    }
}