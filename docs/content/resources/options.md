+++
title = "Resource Options"
weight = 60
+++

Resource options are provided as part of the request in addition to resource parameters. Options can influence the response of the resource by specifying filters or other resource specific behaviors.  Resource consumers specify options in the HTTP query string (after the '?') or as a parameter to the callResource method.

Options must be defined in the resource configuration. If a request contains options which are not defined in the configuration or middleware, RESTng will throw an exception. This requirement allows options to be easily discoverable by consumers and helps prevent unexpected problems caused by incorrect option names. Options are specified in the ‘options’ key of the resource configuration array where the key is the option name and the value is an array:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    ...
    'options' => array(
            'type' => array('required' => true, 'default' => '',
                'enum' => ['inside', 'outside', 'middle', 'under', 'over'],
                'description' => 'A type to fetch'),
            'color' => array('type' => 'list', 'default' => '',
                'enum' => ['blue', 'green', 'red', 'yellow', 'orange'],
                'description' => 'A list of allowed colors'),
        ),
));
{{< / highlight >}} 

Name        | Example   | Description
------------|-----------|--------------
type        | 'list     | One of 'list' or 'single', defaults to 'single'. A 'single' option will always be a single scalar value. A 'list' option will always be an array if it was present in the request.
required    | true      | A boolean value indicating if the option is required.
default     | (string)  | A default value to use for the option.
enum        | ['red', 'blue'] | An array of allowed values for the option.
description | (string)  | A text description of the option.

Defining an option does not force it to be present in the request, unless it is required.  If the consumer does not provide an option, the key will not be present in the request options array.  The service implementation **must** check for the presence of the option prior to using it.

When consumed via HTTP, a list is defined as a comma separated string. RESTng will automatically explode the value into an array. The framework provides the \MiamiOH\RESTng\API_LIST_SEPARATOR constant to represent the separator. When consumed via callResource, you must provide the value as an array, even if passing a single value.

Resource options coexist with some RESTng features, such as pagination and partial responses.  Those features are also implemented through query string values in the request.  The request object will remove any RESTng entries prior to providing the options array to your service. Certain keywords, such as 'token', 'limit', 'offset', and 'fields' are reserved. RESTng will throw an exception if you try to define a reserved option name. 

