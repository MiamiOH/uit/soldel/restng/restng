<?php

namespace MiamiOH\RESTng\Util;

interface CacheKey
{
    public function isValid(): bool;

    public function isNotValid(): bool;

    public function format(): string;

    public function replacements(): array;

    public function key(): string;
}
