<?php
namespace Tests;

use Helmich\JsonAssert\JsonAssertions;

abstract class UnitTestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    use JsonAssertions;
}
