<?php

namespace MiamiOH\RESTng\Util;

class ValidationFactory
{

    /**
     * @return \Respect\Validation\Validator
     */
    public function newValidator()
    {
        return new \Respect\Validation\Validator();
    }
}
