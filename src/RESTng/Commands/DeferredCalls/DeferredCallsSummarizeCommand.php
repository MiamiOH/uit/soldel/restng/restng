<?php


namespace MiamiOH\RESTng\Commands\DeferredCalls;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use MiamiOH\RESTng\Util\DeferredCall;
use MiamiOH\RESTng\Util\DeferredCallManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DeferredCallsSummarizeCommand
 * @package MiamiOH\RESTng\Commands\DeferredCalls
 *
 * @codeCoverageIgnore
 */
class DeferredCallsSummarizeCommand extends Command
{
    protected static $defaultName = 'deferred-calls:summary';

    /** @var DeferredCallManager */
    private $callManager;

    public function __construct(\MiamiOH\RESTng\App $app)
    {
        parent::__construct();

        $this->callManager = $app->getService('APIDeferredCallManager');
    }

    protected function configure(): void
    {
        $this->addOption(
            'status',
            null,
            InputOption::VALUE_OPTIONAL,
            'List only status (p, c, f)'
        );

        $this->addOption(
            'within',
            null,
            InputOption::VALUE_OPTIONAL,
            'Within timeframe (ex "2 hours")',
            '12 hours'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new SymfonyStyle($input, $output);

        $status = $input->getOption('status');
        $within = $input->getOption('within');
        $since = Carbon::now()->subtract($within);

        $calls = $this->callManager->getCalls($since, $status);

        $groups = $calls->groupBy(function (DeferredCall $call) {
            return $call->status();
        })
        ->map(function (Collection $items, string $status) {
            $count = $items->count();
            $minDate = $items->min(function (DeferredCall $call) {
                return $call->updatedAt();
            });
            $maxDate = $items->max(function (DeferredCall $call) {
                return $call->updatedAt();
            });
            return [$status, $count, $minDate, $maxDate];
        })
        ->toArray();

        $output->writeln(sprintf('<info>Deferred calls in the previous %s</info>', $within));
        $outputStyle->table(['Status', 'Count', 'Oldest', 'Newest'], $groups);

        return Command::SUCCESS;
    }
}
