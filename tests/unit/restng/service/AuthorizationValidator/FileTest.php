<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/5/18
 * Time: 6:59 PM
 */

namespace Tests\unit\restng\service\AuthorizationValidator;

use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;
use MiamiOH\RESTng\Service\AuthorizationValidator\File;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    /** @var vfsStream */
    private static $vfs;

    /** @var File */
    private $validator;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $authorizations = [
            'Bookstore' => [
                'Sales Counter' => [
                    'checkout' => ['bob', 'sally'],
                    'override' => ['sally'],
                ],
                'Inventory' => [
                    'new order' => ['bob', 'sally'],
                    'purchase order' => ['joe'],
                ]
            ]
        ];

        $fileStructure = [
            'authorizations.yaml' => yaml_emit($authorizations),
        ];

        self::$vfs = vfsStream::setup('RESTng', null, $fileStructure);
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->validator = new File();
        $this->validator->setAuthorizationFile(vfsStream::url('RESTng/authorizations.yaml'));
    }

    public function testCanBeCreatedWithFile(): void
    {
        $this->assertInstanceOf(File::class, $this->validator);
    }

    public function testCanValidateAuthorizedUser(): void
    {
        $authSpec = AuthorizationSpecification::fromValues('Bookstore', 'Sales Counter', 'checkout');

        $this->assertTrue($this->validator->validateAuthorizationForKey($authSpec, 'bob'));
    }

    public function testRejectsUnauthorizedUser(): void
    {
        $authSpec = AuthorizationSpecification::fromValues('Bookstore', 'Sales Counter', 'checkout');

        $this->assertFalse($this->validator->validateAuthorizationForKey($authSpec, 'frank'));
    }
}
