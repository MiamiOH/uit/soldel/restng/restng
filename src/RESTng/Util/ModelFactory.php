<?php

namespace MiamiOH\RESTng\Util;

class ModelFactory
{
    // a map of model names to factory closures
    protected $map = array();

    protected $app;

    /**
     * @param array $map
     * @throws \Exception
     */
    public function __construct($map = array())
    {
        if ($map) {
            $this->setMap($map);
        }
    }

    /**
     * @param $app
     */
    public function setAppInstance($app)
    {
        $this->app = $app->getInstance();
    }

    /**
     * @param $map
     * @throws \Exception
     */
    public function setMap($map)
    {
        if (!is_array($map)) {
            throw new \Exception('Map argument for ' . __CLASS__ . '::setMap must be an array');
        }

        $this->map = array();

        $di = $this->app->getDiContainer();

        foreach ($map as $key => $value) {
            $this->map[$key] = $di->newFactory($value);
        }

    }

    /**
     * @param $modelName
     * @return mixed
     */
    public function newInstance($modelName)
    {
        $factory = $this->map[$modelName];
        $model = $factory();
        return $model;
    }
}
