<?php


namespace MiamiOH\RESTng\Util;


use Illuminate\Database\Eloquent\Collection;
use MiamiOH\RESTng\Exception\DeferredCallException;

class DeferredCallFactory
{
    /**
     * @param array $options
     * @return DeferredCall
     * @throws DeferredCallException
     * @throws \MiamiOH\RESTng\Exception\InvalidArgumentException
     */
    public function make(array $options): DeferredCall
    {
        if (empty($options['key'])) {
            $options['key'] = uniqid('', true);
        }

        $this->checkKey($options['key']);

        $call = new DeferredCallModel([
            'key' => $options['key'],
            'resource_name' => $options['resourceName'],
            'resource_params' => $options['resourceParams'],
            'api_user' => $options['apiUser'],
            'scheduled_time' => $options['scheduledTimestamp'] ?? null,
        ]);

        $call->save();

        return $call;
    }

    /**
     * @param string $key
     * @throws DeferredCallException
     */
    private function checkKey(string $key): void
    {
        /** @var Collection $matches */
        $matches = DeferredCallModel::where('key', '=', $key)->get();

        if ($matches->isEmpty()) {
            return;
        }

        throw new DeferredCallException(sprintf('Duplicate key: %s', $key));
    }
}