+++
title = "Databases"
weight = 10
+++

One of the most common things we need in a service is a database connection. RESTng provides a database connection factory service that you can inject into your service.

In your resource configuration, define the service with a setter or param dependency of 'APIDatabaseFactory':

{{< highlight php "linenos=table" >}}
<?php
...
$this->addService(array(
   'name' => 'ServiceName',
   'class' => 'MiamiOH\RESTng\Service\Service\ServiceName',
   'set' => array(
      'database' => array('name' => 'APIDatabaseFactory'),
    ));
{{< / highlight >}} 

Within your business service, capture the injected object for later use:

{{< highlight php "linenos=table" >}}
<?php
...
public function setDatabase($database) {
    $this->database = $database;
}
{{< / highlight >}} 

You can then get a database handle from the factory at any time:

{{< highlight php "linenos=table" >}}
<?php
...
$dbh = $this->database->getHandle('datasource_name');
{{< / highlight >}} 

The resulting database handle class is closely related to the legacy Marmot database handle class and implements the same methods. The class has been cleaned up to throw exceptions instead of triggering errors.