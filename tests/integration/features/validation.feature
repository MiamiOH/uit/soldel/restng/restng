# Test that validation is correctly supported
Feature: RESTng provides a validation service
  As a developer using the RESTng API
  I want to validate data using easy to implement rules
  In order to consistently enforce data integrity

  Background:
    Given an empty book table

  Scenario: Validate an option is alpha between 4 and 10
    Given a REST client
    When I make a GET request for /citest/validation/basic?color=blue
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And the subject has an element color equal to "blue"
    And the subject has an element passes equal to "true"

  Scenario: Validate an option is alpha between 4 and 10 with bad data
    Given a REST client
    When I make a GET request for /citest/validation/basic?color=red
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And the subject has an element color equal to "red"
    And the subject has an element passes equal to "false"

  Scenario: Create a new book object using JSON data
    Given a REST client
    And that a book record with id <id> does not exist
    When I have a book with the title "<title>"
    And I have a book with the author "<author>"
    And I have a book with the publishDate "<publishDate>"
    And I have a book with the id "<id>"
    And I make a POST request for /citest/validated/book
    Then the HTTP status code is <code>
  Examples:
  | id | title            | author       | publishDate | code |
  | 1  | My Book Title    | Joan Smith   | 2014-03-11  | 201  |
  | 2  | My Book is Great | Bill Johnson | 2014-06-21  | 201  |
  | a  | Your Book is Not | Frank Howard | 2013-12-21  | 400  |
  | 3  | Your Book is Not | Frank Howard | 2013-15-21  | 400  |

