<?php

class mwContentFormatTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*
        There are a number of factors that impact the incoming and outgoing
        content format. Each combination must be tested for complete coverage.

        request methods
          GET
          POST
          PUT
          DELETE
          PATCH (not supported yet, no routes will match)

        content settings (tested for xml and json)
          extension ( ), content-type ( ), accept ( )
          extension (x), content-type ( ), accept ( )
          extension ( ), content-type (x), accept ( )
          extension ( ), content-type ( ), accept (x)
          extension (x), content-type (x), accept ( )
          extension (x), content-type (y), accept ( )
          extension (x), content-type ( ), accept (x)
          extension (x), content-type ( ), accept (y)
          extension ( ), content-type (x), accept (x)
          extension ( ), content-type (x), accept (y)
          extension (x), content-type (x), accept (x)
          extension (x), content-type (y), accept (x)
          extension (x), content-type (x), accept (y)
          extension (y), content-type (x), accept (x)
    */

    // Asserting
    //  $this->app->environment['CONTENT_TYPE'], should equal mime type
    //  $this->app->view() return view object, check class

    // Mocking Slim environment:

    // key REQUEST_METHOD: GET
    // key SCRIPT_NAME: /api
    // key PATH_INFO: /citest/class/20559-03FA
    // key QUERY_STRING: fields=section,instructor,location,course(courseSubject,courseNumber),enrollments()
    // key SERVER_NAME: localhost
    // key SERVER_PORT: 18080
    // key ACCEPT:
    // key ACCEPT_LANGUAGE:
    // key ACCEPT_CHARSET:
    // key USER_AGENT:
    // key REMOTE_ADDR: 10.0.2.2
    // key slim.url_scheme: http
    // key slim.input:
    // key slim.errors:

    private $mw;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = new \MiamiOH\RESTng\App();

        $this->next = $this->getMockBuilder('stdClass')
            ->setMethods(array('call'))
            ->getMock();

        $this->mw = new \MiamiOH\RESTng\Middleware\ContentFormat();

    }

    public function testConstruct()
    {

        \Slim\Environment::mock();
        $this->assertNotNull($this->mw);

        $this->assertEquals('GET', $this->app->environment->offsetGet('REQUEST_METHOD'));

    }

    public function testMwContentTypeXml()
    {

        $this->app->container->singleton('environment', function ($c) {
            $defaults = array(
                'REQUEST_METHOD' => 'GET',
                'SCRIPT_NAME' => '/api',
                'PATH_INFO' => '/test/service',
                'CONTENT_TYPE' => 'application/xml',
            );

            return \Slim\Environment::mock($defaults);
        });

        $this->next->expects($this->once())->method('call')
            ->willReturn(true);

        $this->mw->setAppMock($this->app);
        $this->mw->setNextMock($this->next);

        $this->mw->call();

        $this->assertEquals('application/xml', $this->app->environment['CONTENT_TYPE']);

    }

}
