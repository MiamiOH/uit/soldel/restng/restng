# Test that request paging is correctly supported
Feature: Request options for paged results are correctly handled
 As a developer using the RESTng API
 I want to specify paging (limit and offset) request options
 In order to retrieve pages of objects from a collection

 Scenario: Requesting offset and limit result in the correct response elements
   Given a REST client
   When I set an offset of 1
   And I set a limit of 10
   And I make a GET request for /citest/record
   Then the HTTP status code is 200
   And the response can be parsed
   And the subject has an element currentUrl matching the pattern "limit=10&offset=1"
   And the subject has an element firstUrl matching the pattern "limit=10&offset=1"
   And the subject has an element lastUrl matching the pattern "limit=10&offset=991"
   And the subject has an element nextUrl matching the pattern "limit=10&offset=11"
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 10 entries

 Scenario: Requesting offset and limit result in the correct records
   Given a REST client
   When I set an offset of <offset>
   And I set a limit of <limit>
   And I make a GET request for /citest/record
   Then the HTTP status code is 200
   And the response can be parsed
   And the subject has an element total matching the pattern "1000"
   And the response contains records <firstRecord> to <lastRecord>
   Examples:
      | offset | limit | firstRecord | lastRecord |
      | 1      | 10    | 1           | 10         |
      | 11     | 10    | 11          | 20         |
      | 45     | 20    | 45          | 64         |
      | 450    | 50    | 450         | 499        |

 Scenario: Requesting a resource with options includes options in constructed urls
   Given a REST client
   When I set an offset of 1
   And I set a limit of 10
   And I make a GET request for /citest/filter?color=green,red&type=outside
   Then the HTTP status code is 200
   And the response can be parsed
   And the subject has an element currentUrl matching the pattern "limit=10&offset=1"
   And the subject has an element firstUrl matching the pattern "limit=10&offset=1"
   And the subject has an element lastUrl matching the pattern "limit=10&offset=67"
   And the subject has an element nextUrl matching the pattern "limit=10&offset=11"
   And the subject has an element currentUrl matching the pattern "color=green%2Cred&type=outside"
   And the subject has an element firstUrl matching the pattern "color=green%2Cred&type=outside"
   And the subject has an element lastUrl matching the pattern "color=green%2Cred&type=outside"
   And the subject has an element nextUrl matching the pattern "color=green%2Cred&type=outside"
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 10 entries

 Scenario: Requesting a resource with options includes fields in constructed urls
   Given a REST client
   When I set an offset of 1
   And I set a limit of 10
   And I make a GET request for /citest/class?fields=classid,semester,section
   Then the HTTP status code is 200
   And the response can be parsed
   And the subject has an element currentUrl matching the pattern "limit=10&offset=1"
   And the subject has an element firstUrl matching the pattern "limit=10&offset=1"
   And the subject has an element lastUrl matching the pattern "limit=10&offset=2751"
   And the subject has an element nextUrl matching the pattern "limit=10&offset=11"
   And the subject has an element currentUrl matching the pattern "fields=classid%2Csemester%2Csection"
   And the subject has an element firstUrl matching the pattern "fields=classid%2Csemester%2Csection"
   And the subject has an element lastUrl matching the pattern "fields=classid%2Csemester%2Csection"
   And the subject has an element nextUrl matching the pattern "fields=classid%2Csemester%2Csection"
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 10 entries

 Scenario: Requesting a pageable resource with no limit returns all records
   Given a REST client
   When I make a GET request for /citest/class
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 2760 entries

 Scenario: Requesting a resource with default paging returns a paged response
   Given a REST client
   When I make a GET request for /citest/classPaged
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 20 entries

 Scenario: Requesting a resource with default paging and a limit returns the limit records
   Given a REST client
   When I make a GET request for /citest/classPaged?limit=25
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a array
   And the subject contains 25 entries

 Scenario: Requesting a resource with max page limit less than limit is an error
   Given a REST client
   When I make a GET request for /citest/classPaged?limit=2500
   Then the HTTP status code is 400

  # The course table has 138 records. Use it to test additional scenarios
  Scenario: Paged response data includes correct relative URLs
    Given a REST client
    When I set an offset of <offset>
    And I set a limit of <limit>
    And I make a GET request for /citest/course
    Then the HTTP status code is 200
    And the response can be parsed
    And the subject has an element total equal to "138"
    And the subject has an element currentUrl matching the pattern "limit=<limit>&offset=<offset>"
    And the subject has an element firstUrl matching the pattern "limit=<limit>&offset=1"
    And the subject has an element lastUrl matching the pattern "limit=<limit>&offset=<lastOffset>"
    And the subject has an element nextUrl matching the pattern "limit=<limit>&offset=<nextOffset>"
    And I get the data element from the response object
    And the subject is a array
    And the subject contains <expectedCount> entries
    Examples:
      | offset | limit | nextOffset | lastOffset | expectedCount |
      | 1      | 10    | 11         | 129        | 10            |
      | 11     | 10    | 21         | 129        | 10            |
      | 45     | 20    | 65         | 119        | 20            |

  Scenario: Requesting a offset greater than the total records yields correct relative URLs
    Given a REST client
    When I set an offset of 140
    And I set a limit of 10
    And I make a GET request for /citest/course
    Then the HTTP status code is 200
    And the response can be parsed
    And the subject has an element total equal to "138"
    And the subject has an element currentUrl matching the pattern "limit=10&offset=140"
    And the subject has an element firstUrl matching the pattern "limit=10&offset=1"
    And the subject has an element lastUrl matching the pattern "limit=10&offset=129"
    And the subject has an element prevUrl matching the pattern "limit=10&offset=130"
    And the subject does not contain an element nextUrl
    And I get the data element from the response object
    And the subject is a array
    And the subject contains 0 entries

  Scenario: Requesting a limit greater than the total records yields correct relative URLs
    Given a REST client
    When I set an offset of 1
    And I set a limit of 140
    And I make a GET request for /citest/course
    Then the HTTP status code is 200
    And the response can be parsed
    And the subject has an element total equal to "138"
    And the subject has an element currentUrl matching the pattern "limit=140&offset=1"
    And the subject has an element firstUrl matching the pattern "limit=140&offset=1"
    And the subject has an element lastUrl matching the pattern "limit=140&offset=1"
    And the subject does not contain an element prevUrl
    And the subject does not contain an element nextUrl
    And I get the data element from the response object
    And the subject is a array
    And the subject contains 138 entries

  Scenario: Requesting an offset and limit greater than the total records yields correct relative URLs
    Given a REST client
    When I set an offset of 150
    And I set a limit of 200
    And I make a GET request for /citest/course
    Then the HTTP status code is 200
    And the response can be parsed
    And the subject has an element total equal to "138"
    And the subject has an element currentUrl matching the pattern "limit=200&offset=150"
    And the subject has an element firstUrl matching the pattern "limit=200&offset=1"
    And the subject has an element lastUrl matching the pattern "limit=200&offset=1"
    And the subject has an element prevUrl matching the pattern "limit=200&offset=1"
    And the subject does not contain an element nextUrl
    And I get the data element from the response object
    And the subject is a array
    And the subject contains 0 entries

  Scenario: Requesting a limit of total records - 1 yields correct relative URLs
    Given a REST client
    When I set an offset of 1
    And I set a limit of 137
    And I make a GET request for /citest/course
    Then the HTTP status code is 200
    And the response can be parsed
    And the subject has an element total equal to "138"
    And the subject has an element currentUrl matching the pattern "limit=137&offset=1"
    And the subject has an element firstUrl matching the pattern "limit=137&offset=1"
    And the subject has an element lastUrl matching the pattern "limit=137&offset=2"
    And the subject has an element nextUrl matching the pattern "limit=137&offset=138"
    And the subject does not contain an element prevUrl
    And I get the data element from the response object
    And the subject is a array
    And the subject contains 137 entries
