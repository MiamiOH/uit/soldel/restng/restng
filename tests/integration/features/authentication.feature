# Test authentication resources for tokens and local users
Feature: Create and validate tokens for local users
 As a developer needing to authenticate access to resources
 I want to authenticate users and generate a token
 In order to limit access to my resources

 Scenario: Validate a an expired or missing token
   Given a REST client
   When I make a GET request for /authentication/v1/missingtoken
   Then the HTTP status code is 404

Scenario: Create a new token using JSON data
   Given a REST client
   When I have a authenticationRequest with the username "RESTUSER"
   And I have a authenticationRequest with the password "bubba"
   And I have a authenticationRequest with the applicationName "Test Application"
   And I have a authenticationRequest with the type "usernamePassword"
   And I give the HTTP Content-type header with the value "application/json"
   And I make a POST request for /authentication/v1
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element username equal to "RESTUSER"

Scenario: Require authentication for a protected resource
   Given a REST client
   When I make a GET request for /random/string/authn
   Then the HTTP status code is 401

Scenario: Allow access to a protected resource with a valid token
   Given a REST client
   And a token for the RESTngTest user
   When I make a GET request for /random/string/authn
   Then the HTTP status code is 200

Scenario: Deny access to a protected resource with an invalid token
   Given a REST client
   And a invalid token for a user
   When I make a GET request for /random/string/authn
   Then the HTTP status code is 401

Scenario: Allow access to a protected resource with a valid Authorization header
   Given a REST client
   And a token for the RESTngTest user in the Authorization header
   When I make a GET request for /random/string/authn
   Then the HTTP status code is 200

Scenario: Allow anonymous access for a resource which also allows authentication
   Given a REST client
   When I make a GET request for /random/string/authn/anon
   Then the HTTP status code is 200

Scenario: Allow access to a resource supporting anonymous with a valid token
   Given a REST client
   And a token for the RESTngTest user
   When I make a GET request for /random/string/authn/anon
   Then the HTTP status code is 200

Scenario: Allow access to a resource supporting anonymous with a valid Authorization header
   Given a REST client
   And a token for the RESTngTest user in the Authorization header
   When I make a GET request for /random/string/authn/anon
   Then the HTTP status code is 200

