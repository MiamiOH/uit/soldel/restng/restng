<?php

class slimRouteMiddlewareTest extends \MiamiOH\RESTng\Testing\TestCase
{

  /** @var \MiamiOH\RESTng\App */
    private $restngApp;

  /** @var \MiamiOH\RESTng\RouteMiddleware\RouteMiddlewareInterface|PHPUnit_Framework_MockObject_MockObject */
  private $routeMiddleware;

  public function setUp(): void
  {
    parent::setUp();

    $this->routeMiddleware = $this->createMock(\MiamiOH\RESTng\RouteMiddleware\RouteMiddlewareInterface::class);

    $this->restngApp = new \MiamiOH\RESTng\App();
  }

  public function testAddRouteMiddlewareDefaultWeight() {

    $response = $this->restngApp->addRouteMiddleware('testMiddleware', $this->routeMiddleware);

    $this->assertTrue($response);

    $middleWare = $this->restngApp->getRouteMiddleware();
    $middleWareWeights = $this->restngApp->getRouteMiddlewareWeights();

    $this->assertTrue(array_key_exists('testMiddleware', $middleWare));
    $this->assertTrue(array_key_exists('testMiddleware', $middleWareWeights));
    $this->assertEquals(0, $middleWareWeights['testMiddleware']);

  }

  public function testAddRouteMiddlewareWeight() {

    $response = $this->restngApp->addRouteMiddleware('testMiddleware1', $this->routeMiddleware, 2);

    $this->assertTrue($response);

    $response = $this->restngApp->addRouteMiddleware('testMiddleware2', $this->routeMiddleware, -1);

    $this->assertTrue($response);

    $response = $this->restngApp->addRouteMiddleware('testMiddleware3', $this->routeMiddleware, 1);

    $this->assertTrue($response);

    $middleWare = $this->restngApp->getRouteMiddleware();
    $middleWareWeights = $this->restngApp->getRouteMiddlewareWeights();

    $this->assertEquals(3, count($middleWare));

    $this->assertTrue(array_key_exists('testMiddleware1', $middleWare));
    $this->assertTrue(array_key_exists('testMiddleware2', $middleWare));
    $this->assertTrue(array_key_exists('testMiddleware3', $middleWare));

    $this->assertEquals(3, count($middleWareWeights));

    $this->assertTrue(array_key_exists('testMiddleware1', $middleWareWeights));
    $this->assertTrue(array_key_exists('testMiddleware2', $middleWareWeights));
    $this->assertTrue(array_key_exists('testMiddleware3', $middleWareWeights));

    $this->assertEquals(2, $middleWareWeights['testMiddleware1']);
    $this->assertEquals(-1, $middleWareWeights['testMiddleware2']);
    $this->assertEquals(1, $middleWareWeights['testMiddleware3']);

  }

}
