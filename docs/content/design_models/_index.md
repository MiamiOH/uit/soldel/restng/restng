+++
title = "Data Models"
weight = 50
+++

REST (REpresentational State Transfer) APIs typically act on representations of data objects. Spending time designing and clearly documenting the objects your API deals with will help you and consumers, so invest in this activity.

RESTng is utilizing the Swagger Spec (http://swagger.io/) to document APIs. Swagger is a widely accept method for describing APIs and tools are availble to validate, browse and interact with APIs based solely on the specification produced for the resources. RESTng leverages the configuration requirements to make documenting your API a natural part of the development process.

We define only two types of objects for RESTng requests and responses. **Models** represent a single business object. A model may not have one to one relation with persistent storage and may be composed of other models. The value of a model is defined by the business use. **Collections** are arrays (numerically indexed) of models. A collection may contain zero, one or many items. A collection with 0 items must **not** be considered "not found" since an empty collection exists and has meaning.