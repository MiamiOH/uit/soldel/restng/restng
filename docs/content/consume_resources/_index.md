+++
title = "Using Resources"
weight = 80
+++

There are two primary ways that RESTng resources are consumed. We've already discussed the callResource method as part of service development. The second, and most common method, is through HTTP transactions. (A third method, bootstrapping RESTng into a normal PHP applications, is in early development and may or may not be viable.)

Any consumer outside of the local RESTng platform will interact with your resources via HTTP. RESTng provides a number of features to facilitate these transactions and insulate your business service from the interface. However, you have to know how to interact with your resources in order to effectively test them during development.

## REST clients

While REST uses normal HTTP requests and responses, trying to test your API in a standard web browser window will prove difficult. Your web browser will construct the request for a web page, not a REST resource, which can lead to confusing results and suffers substantial lack of control. There are a number REST clients available as browser plugins or stand-alone apps. Try some and ask around for opinions.

