<?php

namespace Tests\unit\restng\Util;

use Carbon\Carbon;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Service\Apm\TransactionFactory;
use MiamiOH\RESTng\Util\DeferredCall;
use MiamiOH\RESTng\Util\DeferredCallProcessor;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\ResponseFactory;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;
use MiamiOH\RESTng\Testing\RefreshDatabase;
use Tests\UnitTestCase;

class DeferredCallProcessorTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @var DeferredCallProcessor  */
    private $processor;

    /** @var App|MockObject */
    private $restngApp;
    /** @var User|MockObject */
    private $user;
    /** @var ResponseFactory|MockObject  */
    private $responseFactory;
    /** @var TransactionFactory|MockObject  */
    private $transactionFactory;
    /** @var Transaction|MockObject  */
    private $transaction;

    /** @var DeferredCall|MockObject  */
    private $call;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = $this->createMock(App::class);
        $this->user = $this->createMock(User::class);
        $this->responseFactory = $this->createMock(ResponseFactory::class);
        $this->transactionFactory = $this->createMock(TransactionFactory::class);
        $this->transaction = $this->createMock(Transaction::class);

        $this->processor = new DeferredCallProcessor();

        $this->processor->setApp($this->restngApp);
        $this->processor->setApiUser($this->user);
        $this->processor->setResponseFactory($this->responseFactory);
        $this->processor->setApmTransactionFactory($this->transactionFactory);
    }

    public function tearDown(): void
    {
        parent::tearDown();;

        Carbon::setTestNow();
    }

    public function testThrowsExceptionIfCallIsNotPending(): void
    {
        $this->prepareMockCall(
            ['status' => DeferredCall::STATUS_COMPLETE],
            ['verifyCallHash' => false, 'apiUser' => false]
        );

        $this->expectException(DeferredCallException::class);
        $this->expectExceptionMessage('Call 123 status is not pending: ' . DeferredCall::STATUS_COMPLETE);

        $this->processor->processCall($this->call);
    }

    public function testThrowsExceptionAndMarksCancelledIfCallFailsValidation(): void
    {
        $this->prepareMockCall(['valid' => false], ['apiUser' => false]);

        $this->call->expects($this->once())->method('markCancelled');
        $this->call->expects($this->once())->method('save');

        $this->expectException(DeferredCallException::class);
        $this->expectExceptionMessage('Attempt to verify call id 123 failed');

        $this->processor->processCall($this->call);
    }

    public function testAssumesApiUserFromCall(): void
    {
        $this->prepareMockCall();

        $this->user->expects($this->once())->method('assumeUser')
            ->with($this->equalTo($this->user));

        $this->processor->processCall($this->call);
    }

    public function testSetsUpNewTransactionForCall(): void
    {
        $callOptions = [
            'resourceName' => 'test.resource.transaction',
            'resourceParams' => ['color' => 'red'],
        ];

        $this->prepareMockCall($callOptions, ['newTransaction' => false]);

        $this->transactionFactory->expects($this->once())->method('newTransaction')
            ->with($this->equalTo($callOptions['resourceName']))
            ->willReturn($this->transaction);

        $this->transaction->expects($this->once())->method('setUserContext')
            ->with($this->equalTo($this->user));
        $this->transaction->expects($this->once())->method('setRequestContextFromArray')
            ->with($this->equalTo($callOptions['resourceParams']));

        $this->restngApp->expects($this->once())->method('useService')
            ->with($this->callback(function (array $serviceOptions) {
                $this->assertEquals('ApmTransaction', $serviceOptions['name']);
                $this->assertEquals($this->transaction, $serviceOptions['object']);
                return true;
            }));

        $this->processor->processCall($this->call);
    }

    public function testMarksCallInProgressWhenExecuting(): void
    {
        $this->prepareMockCall();

        $this->call->expects($this->once())->method('markInProgress');
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testExecutesCallWithOriginalNameAndParameters(): void
    {
        $this->prepareMockCall([
            'resourceName' => 'test.resource.call',
            'resourceParams' => [],
        ], ['callResource' => false]);

        $this->restngApp->expects($this->once())->method('callResource')
            ->with(
                $this->equalTo('test.resource.call'),
                $this->equalTo([]),
            )
            ->willReturn(new Response());

        $this->processor->processCall($this->call);
    }

    public function testCreatesNewResponseWhenCallResultsInException(): void
    {
        $this->prepareMockCall([
            'resourceName' => 'test.resource.call',
            'resourceParams' => [],
        ], ['callResource' => false]);

        $this->restngApp->expects($this->once())->method('callResource')
            ->willThrowException(new BadRequest());

        $response = $this->createMock(Response::class);
        $response->expects($this->once())->method('setStatus')
            ->with($this->equalTo(App::API_FAILED));

        $this->responseFactory->expects($this->once())->method('newResponse')
            ->willReturn($response);

        $this->processor->processCall($this->call);
    }

    public function testIncrementsAttemptsAndAddsResponseToCall(): void
    {
        $this->prepareMockCall();

        $response = new Response();
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('addResponse')
            ->with($this->equalTo($response));
        $this->call->expects($this->once())->method('incrementAttemptCount');

        $this->processor->processCall($this->call);
    }

    public function testAddsMetadataAndSendsTransaction(): void
    {
        $this->prepareMockCall();

        $this->transaction->expects($this->once())->method('setMetadata');
        $this->transaction->expects($this->once())->method('send');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallCompleteOnSuccess(): void
    {
        $this->prepareMockCall();

        $this->call->expects($this->once())->method('markComplete');
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallPendingWithDefaultRetryOnPreconditionFailure(): void
    {
        $testNow = Carbon::create(2020, 12, 12, 14, 32, 45);
        Carbon::setTestNow($testNow);

        $this->prepareMockCall([], ['callResource' => false]);

        $response = new Response();
        $response->setStatus(App::API_PRECONDITIONREQUIRED);
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('markPending');
        $this->call->expects($this->once())->method('scheduleAt')
            ->with($this->equalTo($testNow->copy()->addSeconds(DeferredCallProcessor::RETRY_DELAY)));
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallPendingWithGivenRetryOnPreconditionFailure(): void
    {
        $testNow = Carbon::create(2020, 12, 12, 14, 32, 45);
        Carbon::setTestNow($testNow);

        $this->prepareMockCall([], ['callResource' => false]);

        $response = new Response();
        $response->setStatus(App::API_PRECONDITIONREQUIRED);
        $response->setPayload(['delay' => 500]);
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('markPending');
        $this->call->expects($this->once())->method('scheduleAt')
            ->with($this->equalTo($testNow->copy()->addSeconds(500)));
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallFailedWhenExecuteFails(): void
    {
        $this->prepareMockCall([], ['callResource' => false]);

        $response = new Response();
        $response->setStatus(App::API_BADREQUEST);
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('incrementErrorCount');
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallPendingWhenExecuteFailsUnderLimit(): void
    {
        $testNow = Carbon::create(2020, 12, 12, 14, 32, 45);
        Carbon::setTestNow($testNow);

        $this->prepareMockCall([], ['callResource' => false]);

        $response = new Response();
        $response->setStatus(App::API_BADREQUEST);
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('errorCount')
            ->willReturn(1);
        $this->call->expects($this->once())->method('markPending');
        $this->call->expects($this->once())->method('scheduleAt')
            ->with($this->equalTo($testNow->copy()->addSeconds(DeferredCallProcessor::RETRY_DELAY)));
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    public function testMarksCallFailedWhenExecuteFailsAtLimit(): void
    {
        $testNow = Carbon::create(2020, 12, 12, 14, 32, 45);
        Carbon::setTestNow($testNow);

        $this->prepareMockCall([], ['callResource' => false]);

        $response = new Response();
        $response->setStatus(App::API_BADREQUEST);
        $this->restngApp->expects($this->once())->method('callResource')
            ->willReturn($response);

        $this->call->expects($this->once())->method('errorCount')
            ->willReturn(DeferredCallProcessor::MAX_FAILURES);
        $this->call->expects($this->once())->method('markFailed');
        $this->call->expects($this->exactly(2))->method('save');

        $this->processor->processCall($this->call);
    }

    private function prepareMockCall(array $overrides = [], array $expectOverrides = []): void
    {
        $options = array_merge([
            'id' => 123,
            'status' => DeferredCall::STATUS_PENDING,
            'valid' => true,
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
        ], $overrides);

        $expects = array_merge([
            'verifyCallHash' => true,
            'newTransaction' => true,
            'callResource' => true,
        ], $expectOverrides);

        $this->call = $this->createMock(DeferredCall::class);

        $this->call->method('id')->willReturn($options['id']);
        $this->call->method('status')->willReturn($options['status']);
        $this->call->expects($this->once())->method('isNotPending')
            ->willReturn($options['status'] !== DeferredCall::STATUS_PENDING);
        $this->call->method('resourceName')->willReturn($options['resourceName']);
        $this->call->method('resourceParams')->willReturn($options['resourceParams']);
        $this->call->method('apiUser')->willReturn($options['apiUser']);

        if ($expects['verifyCallHash']) {
            $this->call->expects($this->once())->method('verifyCallHash')->willReturn($options['valid']);
        } else {
            $this->call->expects($this->never())->method('verifyCallHash');
        }

        if ($expects['newTransaction']) {
            $this->transactionFactory->method('newTransaction')->willReturn($this->transaction);
        }

        if ($expects['callResource']) {
            $this->restngApp->method('callResource')->willReturn(new Response());
        }
    }
}
