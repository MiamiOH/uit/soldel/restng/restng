+++
title = "Deferred Call Commands"
weight = 20
+++

The following commands are available to query and manage deferred calls. These commands must be run from the root of the RESTng installation.

Valid call status values are:

| Code | Status      |
|------|-------------|
| p    | Pending     |
| i    | In Progress |
| c    | Complete    |
| f    | Failed      |
| x    | Cancelled   |

Calls are "cancelled" when the recorded has does match the calculated hash for the call. This indicates the resource name, parameters or user associated with the call have been altered from the original values.

Many commands accept a time range which should be given in units of time suitable for `Carbon::sub()`, for example:

* 2 hours
* 1 day
* 30 minutes

## List Deferred Calls

```shell
php console deferred-calls:list"
```

Produces a list of all deferred call records.

The list can be limited by call status and a time range:

```shell
php console deferred-calls:list --status f --within "2 hours"
```

## Show Details About a Deferred Call

```shell
php console deferred-calls:show 123
```

Shows the details of the deferred call for the given id.

## Summarize Deferred Calls

```shell
php console deferred-calls:summary
```

Show a summary of deferred call counts by status.

The summary can be limited by call status and a time range:

```shell
php console deferred-calls:summary --status f --within "2 hours"
```

## Purge Deferred Calls

```shell
php console deferred-calls:purge --before "1 day"
```

Purge completed deferred calls older than the indicated time.

By default, only completed and cancelled calls will be purged. Failed calls may be purged by passing the `--failed` flag (note that completed and cancelled calls will still be purged):

```shell
php console deferred-calls:purge --before "1 day" --failed
```

## Check for Failed Deferred Calls

```shell
php console deferred-calls:check-failures
```

Checks for any failed jobs and exits with a non-zero status if any are found.

The check can be limited by a time range:

```shell
php console deferred-calls:check-failures --within "1 hour"
```

## Reset Failed Deferred Calls

```shell
php console deferred-calls:reset-failed
```

Resets the status of any failed jobs so they will be retried.

The reset can be limited by a time range:

```shell
php console deferred-calls:reset-failed --within "2 hours"
```

## Run a Deferred Call

```shell
php console deferred-calls:run 123
```

Runs the deferred call for the given id.

By default, attempting to run a failed job will result in an exception. You can force a failed job to be reset and run using the `--force` flag:

```shell
php console deferred-calls:run 123 --force
```

## Run the Deferred Call Worker

```shell
php console deferred-calls:work
```

Runs a worker process to poll for and execute deferred call records. The process will continue running until killed.

You can run the worker for a single iteration using the `--once` flag:

```shell
php console deferred-calls:work --once
```

The default polling interval is 30 seconds. You can specify a polling time using the `--wait-time` option:

```shell
php console deferred-calls:work --wait-time 60
```

The default maximum allowed failures is 3 before a job is marked failed. You can specify a maximum threshold using the `--tries` option: 

```shell
php console deferred-calls:work --tries 5
```

The default retry delay after a failure is 300 seconds. You can specify a retry delay using the `--retry-delay` option:

```shell
php console deferred-calls:work --retry-delay 600
```
