# Test that request options are correctly supported
Feature: Request options (query string values) affect the behavior of the resource
 As a developer using the RESTng API
 I want to provide request options
 In order to alter the behavior of the resource

 Scenario: Options defined as required must be present
   Given a REST client
   When I make a GET request for /citest/filter
   Then the HTTP status code is 500
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element message matching the pattern "^Missing required option type for resource"

 Scenario: Unknown request options result in an error
   Given a REST client
   When I make a GET request for /citest/filter?size=big
   Then the HTTP status code is 500
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element message matching the pattern "^No defined option size for resource"

 Scenario: Request options can be declared as a list
   Given a REST client
   When I make a GET request for /citest/reflect?color=blue,yellow,green
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And I get the color element from the subject
   And the subject contains 3 entries
   And a value in the subject is "blue"
   And a value in the subject is "yellow"
   And a value in the subject is "green"

 Scenario: A list option will be an array even if only one value is given
   Given a REST client
   When I make a GET request for /citest/reflect?color=blue
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And I get the color element from the subject
   And the subject contains 1 entry
   And a value in the subject is "blue"

 Scenario: A scalar option will be a string even if a list is given
   Given a REST client
   When I make a GET request for /citest/reflect?type=under,over
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element type equal to "under,over"

 Scenario: Multiple options are correctly provided
   Given a REST client
   When I make a GET request for /citest/reflect?color=blue,red&type=outside
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element type equal to "outside"
   And I get the color element from the subject
   And the subject contains 2 entries
   And a value in the subject is "blue"
   And a value in the subject is "red"

