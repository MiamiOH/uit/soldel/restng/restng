<?php

class sthLegacyTest extends \PHPUnit\Framework\TestCase {

  private $sth;

  public function setUp(): void
  {
      parent::setUp();

      $this->sth = new \MiamiOH\RESTng\Legacy\DB\STH();
  }

  public function testSth() {

    $this->assertTrue($this->sth !== null);

  }

  public function testSthAutoCommit() {

    $result = $this->sth->auto_commit(false);

    $this->assertFalse($result);

  }

  public function testSthAutoCommitReset() {

    $result = $this->sth->auto_commit(false);

    $this->assertFalse($result);

    $result = $this->sth->auto_commit(true);

    $this->assertTrue($result);

  }

}
