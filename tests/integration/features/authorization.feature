# Test authorization to resources
Feature: User various means to authorize access to resources
 As a resource provider
 I want to authorize users to access resources
 In order to limit access and manage activity

Scenario: Verify that authorization is required
   Given a REST client
   When I make a GET request for /citest/student/101017
   Then the HTTP status code is 401

Scenario: Allow access to a protected resource with a valid token
   Given a REST client
   And a token for the RESTngTest user
   When I make a GET request for /citest/student/101017
   Then the HTTP status code is 200

Scenario: Allow access to a protected resource as SELF
   Given a REST client
   And a token for the TestSELF user
   When I make a GET request for /citest/student/self/101017
   Then the HTTP status code is 200

Scenario: Deny access to a protected resource as an unauthorized user
   Given a REST client
   And a token for the TestSELF user
   When I make a GET request for /citest/student/self/101402
   Then the HTTP status code is 401

Scenario: Allow access as anonymous to a mixed authorization resource
   Given a REST client
   When I make a GET request for /citest/student/mixed/101017
   Then the HTTP status code is 200

Scenario: Allow access as authenticated to a mixed authorization resource
   Given a REST client
   And a token for the RESTngTest user
   When I make a GET request for /citest/student/mixed/101017
   Then the HTTP status code is 200

Scenario: Verify AuthMan authorization of mixed authorization resource
   Given a REST client
   And a token for the RESTngTest user
   When I make a GET request for /citest/student/mixed/101017
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element authorized equal to "true"
   And the subject has an element self equal to "false"

Scenario: Verify self authorization of mixed authorization resource
   Given a REST client
   And a token for the TestSELF user
   When I make a GET request for /citest/student/mixed/101017
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element authorized equal to "false"
   And the subject has an element self equal to "true"

