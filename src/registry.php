<?php

return [
    'commands' => [
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsCheckFailuresCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsListCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsPurgeCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsResetFailedCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsRunCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsShowCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsSummarizeCommand::class,
        \MiamiOH\RESTng\Commands\DeferredCalls\DeferredCallsWorkCommand::class,
    ],
];