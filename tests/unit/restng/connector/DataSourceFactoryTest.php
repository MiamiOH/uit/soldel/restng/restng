<?php

use MiamiOH\RESTng\Connector\DataSourceProviderFile;
use org\bovigo\vfs\vfsStream;

class DataSourceFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /** @var \MiamiOH\RESTng\Connector\DataSourceFactory */
    private $factory;

    /** @var \MiamiOH\RESTng\Connector\DataSourceProviderInterface|PHPUnit_Framework_MockObject_MockObject */
    private $provider;

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = $this->createMock(\MiamiOH\RESTng\Connector\DataSourceProviderInterface::class);

        $this->factory = new \MiamiOH\RESTng\Connector\DataSourceFactory();

    }

    public function testCanAddDataSourceProvider(): void
    {
        $this->assertFalse($this->factory->hasProvider());

        $this->factory->addProvider($this->provider);

        $this->assertTrue($this->factory->hasProvider());
    }

    public function testReturnsDataSourceForValidName()
    {
        $this->provider->method('hasDataSource')
            ->willReturn(true);
        $this->provider->method('getDataSource')
            ->willReturn(\MiamiOH\RESTng\Connector\DataSource::fromArray([
                'name' => 'text_connection',
                'type' => 'Other',
                'user' => 'bob'
            ]));
        $this->factory->addProvider($this->provider);

        $ds = $this->factory->getDataSource('test_connection');

        $this->assertInstanceOf(\MiamiOH\RESTng\Connector\DataSource::class, $ds);
        $this->assertEquals('bob', $ds->getUser());
    }

    public function testThrowsExceptionIfNameNotFound()
    {
        $this->provider->method('hasDataSource')
            ->willReturn(false);
        $this->factory->addProvider($this->provider);
        $this->expectException(\MiamiOH\RESTng\Exception\DataSourceNotFound::class);

        $ds = $this->factory->getDataSource('other_connection');
    }

    public function testAssertsIfFactoryHasDataSourceName(): void
    {
        $this->provider->method('hasDataSource')
            ->willReturn(true);
        $this->factory->addProvider($this->provider);

        $this->assertTrue($this->factory->hasDataSource('test'));
    }

    public function testAssertsIfFactoryDoesNotHaveDataSourceName(): void
    {
        $this->provider->method('hasDataSource')
            ->willReturn(false);
        $this->factory->addProvider($this->provider);

        $this->assertFalse($this->factory->hasDataSource('test'));
    }

    public function testReturnsDataSourcesFromAllProviders(): void
    {
        $provider1 = $this->createMock(\MiamiOH\RESTng\Connector\DataSourceProviderInterface::class);
        $provider1->method('getDataSources')
            ->willReturn([
                \MiamiOH\RESTng\Connector\DataSource::fromArray([
                    'name' => 'test_connection1',
                    'type' => 'Other',
                    'user' => 'bob'
                ])
            ]);
        $this->factory->addProvider($provider1);

        $provider2 = $this->createMock(\MiamiOH\RESTng\Connector\DataSourceProviderInterface::class);
        $provider2->method('getDataSources')
            ->willReturn([
                \MiamiOH\RESTng\Connector\DataSource::fromArray([
                    'name' => 'test_connection2',
                    'type' => 'Other',
                    'user' => 'sally'
                ]),
                \MiamiOH\RESTng\Connector\DataSource::fromArray([
                    'name' => 'test_connection3',
                    'type' => 'Other',
                    'user' => 'fred'
                ]),
            ]);
        $this->factory->addProvider($provider2);

        $dataSources = $this->factory->getDataSources();

        $this->assertCount(3, $dataSources);
    }

    public function testReturnsEmptyDataSourcesWhenNoProvidersAreFound(): void
    {
        $dataSources = $this->factory->getDataSources();

        $this->assertCount(0, $dataSources);
    }
}
