+++
title = "DataSources"
weight = 20
+++

Datasources provide a means for services to lookup credential information for direct use. The datasource factory can be injected into a service and used to resolve a named datasource object. The service can then use the datasource values to perform any type of connection or other activity.

In your resource configuration, define the service with a setter or param dependency of 'APIDataSourceFactory':

{{< highlight php "linenos=table" >}}
<?php
...
$this->addService(array(
   'name' => 'ServiceName',
   'class' => 'MiamiOH\RESTng\Service\Service\ServiceName',
   'set' => array(
      'datasourceFactory' => array('name' => 'APIDataSourceFactory'),
    ));
{{< / highlight >}} 

Within your business service, capture the injected object for later use:

{{< highlight php "linenos=table" >}}
<?php
...
public function setDatasourceFactory($factory) {
    $this->factory = $factory;
}
{{< / highlight >}} 

You can then get a datasource object from the factory at any time:

{{< highlight php "linenos=table" >}}
<?php
...
$datasource = $this->factory->getDataSource($name);
{{< / highlight >}} 

The factory returns a `MiamiOH\RESTng\Connector\DataSource` object which provides method to retrieve the various values.
