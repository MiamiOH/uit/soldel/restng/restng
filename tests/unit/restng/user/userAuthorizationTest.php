<?php

use MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationSpecification;

class userAuthorizationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /** @var \MiamiOH\RESTng\Util\User */
    private $user;

    /** @var \MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $credentialValidator;

    /** @var \MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface|PHPUnit_Framework_MockObject_MockObject */
    private $authorizationValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->credentialValidator = $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->credentialValidator->method('validateToken')
            ->willReturn(['valid' => true, 'token' => 'abc123', 'username' => 'doej']);

        $this->authorizationValidator = $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        $app = $this->createMock(\MiamiOH\RESTng\App::class);

        $this->user = new \MiamiOH\RESTng\Util\User();

        $this->user->setCredentialValidator($this->credentialValidator);
        $this->user->setAuthorizationValidator($this->authorizationValidator);
        $this->user->setApp($app);
        $this->user->setCache(new \Illuminate\Cache\Repository(new \Illuminate\Cache\ArrayStore()));
        $this->user->setApmTransaction($this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class));

        $this->user->addUserByToken('abc123');
    }

    public function testIsAuthorized() {
        $this->authorizationValidator->method('validateAuthorizationForKey')
            ->willReturn(true);

        $auth = $this->user->isAuthorized('TestApplication', 'TestModule', 'TestKey');

        $this->assertTrue($auth);
    }

    public function testIsAuthorizedMultiKey() {
        $this->authorizationValidator->method('validateAuthorizationForKey')
            ->willReturn(true);

        $auth = $this->user->isAuthorized('TestApplication', 'TestModule', ['TestKey1', 'TestKey2']);

        $this->assertTrue($auth);
    }

    public function testIsAuthorizedCache()
    {
        $this->authorizationValidator->expects($this->once())
            ->method('validateAuthorizationForKey')
            ->willReturn(true);

        $auth = $this->user->isAuthorized('TestApplication', 'TestModule', 'TestKey');

        $this->assertTrue($auth);

        // Should not trigger another call to callResource
        $auth = $this->user->isAuthorized('TestApplication', 'TestModule', 'TestKey');

        $this->assertTrue($auth);

    }

    public function testIsNotAuthorized() {
        $this->authorizationValidator->method('validateAuthorizationForKey')
            ->willReturn(false);

        $auth = $this->user->isAuthorized('TestApplication', 'TestModule', 'TestKey');

        $this->assertFalse($auth);

    }

}
