<?php

namespace MiamiOH\RESTng;

use Illuminate\Cache\Repository;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;

class Service
{
    /** @var Request */
    private $request;
    /** @var Response */
    private $response;

    /** @var App */
    protected $app;
    /** @var User */
    protected $apiUser;

    /** @var Repository */
    protected $cache;

    /** @var Transaction */
    protected $transaction;

    // Child class log, determined at run time
    /** @var \Logger $log */
    protected $log = '';

    // Log for this class
    /** @var \Logger $serviceLog */
    protected $serviceLog = '';

    public function setLogger()
    {
        // This logger is for the child class.
        if (!$this->log) {
            $this->log = \Logger::getLogger(get_called_class());
        }

        // This logger is for this class.
        if (!$this->serviceLog) {
            $this->serviceLog = \Logger::getLogger(__CLASS__);
        }
    }

    /**
     * @return \Logger
     */
    public function getLogger()
    {
        $this->setLogger();
        return $this->log;
    }

    /**
     * @return \Logger
     */
    public function getServiceLog()
    {
        $this->setLogger();
        return $this->serviceLog;
    }

    public function setApp(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param $response
     * @return bool
     */
    public function setResponse($response)
    {
        // Check this is of class RESTng\Util\Response
        $this->response = $response;

        return true;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function getResponse()
    {
        if ($this->response === null) {
            $this->response = new Response();
        }

        return $this->response;
    }

    /**
     * @param $request
     * @return bool
     */
    public function setRequest($request)
    {
        // Check this is of class RESTng\Util\Request
        $this->request = $request;

        if ($this->response === null) {
            $this->response = new Response();
        }

        $this->response->setRequest($this->request);

        return true;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param $user
     */
    public function setApiUser($user)
    {
        // Make sure the logger is set since we don't know the order
        // of injection.
        $this->setLogger();
        $this->serviceLog->debug('Set API user for ' . get_called_class());
        $this->apiUser = $user;
    }

    /**
     * @return \MiamiOH\RESTng\Util\User
     */
    public function getApiUser()
    {
        // Make sure the logger is set since we don't know the order
        // of injection.
        $this->setLogger();
        $this->serviceLog->debug('Get API user for ' . get_called_class());

        return $this->apiUser;
    }

    public function setCache(Repository $cache): void
    {
        $this->cache = $cache;
    }

    public function setApmTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @param $resource
     * @return \MiamiOH\RESTng\Util\Request
     */
    public function newRequest($resource)
    {
        return $this->app->newRequest($resource);
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function newResponse()
    {
        return $this->app->newResponse();
    }

    /**
     * @param $resourceName
     * @param array $params
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function callResource($resourceName, $params = array(), $options = array())
    {
        return $this->app->callResource($resourceName, $params, $options);
    }

    /**
     * @param $resourceName
     * @param array $resourceParams
     * @return string
     */
    public function locationFor($resourceName, $resourceParams = array())
    {
        $location = $this->app->locationFor($resourceName, $resourceParams);
        return $location;
    }

    /*
        Snake case is easiest to work with in SQL since field names are not case
        sensitive and are converted to lowercase by the db handler in most cases.
        However, we expect camel case keys when responding in JSON.

        The following methods are from:

        https://gist.github.com/goldsky/3372487

        The service classes can use these to convert back and forth.

        One major point is that you can camel case (sort of) keys containing '_#',
        but that will effectively be a one way conversion. There is no pracical way
        to accurately reverse that consistently. For example:

        major_1_code => major1Code => major1_code

        The best recommendation is to avoid digits in key names that will be run
        through these conversions. If you must have digits, consider putting them
        at the end with no intervening underscore. Example:

        major_code1 => majorCode1 => major_code1
    */
    /**
     * Convert under_score type array's keys to camelCase type array's keys
     * @param   array $array array to convert
     * @param   array $arrayHolder parent array holder for recursive array
     * @return  array   camelCase array
     */
    public function camelCaseKeys($array, $arrayHolder = array())
    {
        if (!is_array($array)) {
            throw new \Exception('Argument to camelCaseKeys must be an array');
        }

        $camelCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = $key;
            if (strpos($key, '_') !== false) {
                $newKey = @explode('_', $key);
                for ($i = 1; $i < count($newKey); $i++) {
                    $newKey[$i] = ucwords($newKey[$i]);
                }
                #array_walk($newKey, create_function('&$v', '$v = ucwords($v);'));
                $newKey = @implode('', $newKey);
            }
            #$newKey{0} = strtolower($newKey{0});
            //if (!is_array($val)) {
            $camelCaseArray[$newKey] = $val;
            //} else {
            //    $camelCaseArray[$newKey] = $this->camelCaseKeys($val, $camelCaseArray[$newKey]);
            //}
        }
        return $camelCaseArray;
    }
    /**
     * Convert under_score type array's keys to CamelCap type array's keys
     * @param   array $array array to convert
     * @param   array $arrayHolder parent array holder for recursive array
     * @return  array   CamelCaps array
     */
    public function camelCapsKeys($array, $arrayHolder = array())
    {
        if (!is_array($array)) {
            throw new \Exception('Argument to camelCapsKeys must be an array');
        }

        $CamelCapsArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = ucwords($key);
            if (strpos($key, '_') !== false) {
                $newKey = @explode('_', $key);
                for ($i = 0; $i < count($newKey); $i++) {
                    $newKey[$i] = ucwords($newKey[$i]);
                }
                $newKey = @implode('', $newKey);
            }
            $CamelCapsArray[$newKey] = $val;
        }
        return $CamelCapsArray;
    }

    /**
     * Convert camelCase type array's keys to under_score+lowercase type array's keys
     * @param   array $array array to convert
     * @param   array $arrayHolder parent array holder for recursive array
     * @return  array   under_score array
     */
    public function snakeCaseKeys($array, $arrayHolder = array())
    {
        if (!is_array($array)) {
            throw new \Exception('Argument to snakeCaseKeys must be an array');
        }

        $snakeCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = preg_replace('/[A-Z]/', '_$0', $key);
            $newKey = strtolower($newKey);
            $newKey = ltrim($newKey, '_');
            //if (!is_array($val)) {
            $snakeCaseArray[$newKey] = $val;
            //} else {
            //    $snakeCaseArray[$newKey] = $this->snakeCaseKeys($val, $snakeCaseArray[$newKey]);
            //}
        }
        return $snakeCaseArray;
    }

    /**
     * @param $array
     * @param array $arrayHolder
     * @return array
     * @throws \Exception
     */
    public function snakeCaseValues($array, $arrayHolder = array())
    {
        if (!is_array($array)) {
            throw new \Exception('Argument to snakeCaseValues must be an array');
        }

        $snakeCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            if (!is_array($val)) {
                $newValue = preg_replace('/[A-Z]/', '_$0', $val);
                $newValue = strtolower($newValue);
                $newValue = ltrim($newValue, '_');
                $snakeCaseArray[$key] = $newValue;
            } else {
                $snakeCaseArray[$key] = $this->snakeCaseValues($val, $array[$key]);
            }
        }
        return $snakeCaseArray;
    }

}
