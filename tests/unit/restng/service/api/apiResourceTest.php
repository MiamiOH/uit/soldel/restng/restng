<?php

class apiResourceTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $resource;

  /** @var \MiamiOH\RESTng\App|PHPUnit_Framework_MockObject_MockObject */
  private $restngApp;

  private $testResources = array();
  private $testServices = array();

  private $resourceName = '';
  private $serviceName = '';

  protected function setUp(): void
  {
    parent::setUp();

    $this->resourceName = '';
    $this->serviceName = '';

    $this->testResources = array(
        'test.resource' => array(
                'name' => 'test.resource'
            )
        );

    $this->testServices = array(
            'TestService' => array(
                'name' => 'TestService'
            )
        );

    $this->restngApp = $this->getMockBuilder('MiamiOH\RESTng\App')
          ->setMethods(array('loadAllConfigs', 'loadConfigForResourceName', 'getResourceByName',
            'getResources', 'getServices', 'getServiceByName'))
          ->getMock();

    $this->restngApp->method('getResources')->will($this->returnCallback(array($this, 'getResourcesMock')));
    $this->restngApp->method('getServices')->will($this->returnCallback(array($this, 'getServicesMock')));

    $this->resource = new \MiamiOH\RESTng\Service\API\Resource();

    $this->resource->setApp($this->restngApp);

  }

  public function testGetApi() {

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->resource->setRequest($request);

    $resp = $this->resource->getAPI();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals(2, count(array_keys($payload)));
    $this->assertTrue(array_key_exists('resources', $payload));
    $this->assertTrue(array_key_exists('services', $payload));
    $this->assertEquals($this->testResources, $payload['resources']);
    $this->assertEquals($this->testServices, $payload['services']);

  }

  public function testGetResources() {

    $this->restngApp->expects($this->once())->method('loadAllConfigs')->willReturn(true);

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->resource->setRequest($request);

    $resp = $this->resource->getResources();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals($this->testResources, $payload);

  }

  public function testGetServices() {

    $this->restngApp->expects($this->once())->method('loadAllConfigs')->willReturn(true);

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->resource->setRequest($request);

    $resp = $this->resource->getServices();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals($this->testServices, $payload);

  }

  public function testGetResource() {

    $this->restngApp->expects($this->once())->method('loadConfigForResourceName')
        ->with($this->callback(array($this, 'loadConfigForResourceNameWith')))
        ->willReturn(true);

    $this->restngApp->expects($this->once())->method('getResourceByName')
        ->with($this->callback(array($this, 'getResourceByNameWith')))
        ->will($this->returnCallback(array($this, 'getResourceByNameMock')));

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->resourceName = 'test.resource';

    $request->expects($this->once())->method('getResourceParam')->willReturn($this->resourceName);

    $this->resource->setRequest($request);

    $resp = $this->resource->getResource();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals($this->testResources[$this->resourceName], $payload);

  }

  public function testGetResourceNotFound() {

    $this->restngApp->expects($this->once())->method('loadConfigForResourceName')
        ->with($this->callback(array($this, 'loadConfigForResourceNameWith')))
        ->willReturn(true);

    $this->restngApp->expects($this->once())->method('getResourceByName')
        ->with($this->callback(array($this, 'getResourceByNameWith')))
        ->will($this->returnCallback(array($this, 'getResourceByNameMock')));

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->resourceName = 'test.resource.missing';

    $request->expects($this->once())->method('getResourceParam')->willReturn($this->resourceName);

    $this->resource->setRequest($request);

    $resp = $this->resource->getResource();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

  }

  public function testGetService() {

    $this->restngApp->expects($this->once())->method('loadAllConfigs')->willReturn(true);

    $this->restngApp->expects($this->once())->method('getServiceByName')
        ->with($this->callback(array($this, 'getServiceByNameWith')))
        ->will($this->returnCallback(array($this, 'getServiceByNameMock')));

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->serviceName = 'TestService';

    $request->expects($this->once())->method('getResourceParam')->willReturn($this->serviceName);

    $this->resource->setRequest($request);

    $resp = $this->resource->getService();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals($this->testServices[$this->serviceName], $payload);

  }

  public function testGetServiceNotFound() {

    $this->restngApp->expects($this->once())->method('loadAllConfigs')->willReturn(true);

    $this->restngApp->expects($this->once())->method('getServiceByName')
        ->with($this->callback(array($this, 'getServiceByNameWith')))
        ->will($this->returnCallback(array($this, 'getServiceByNameMock')));

    $request = $this->getMockBuilder('\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

    $this->serviceName = 'TestServiceMissing';

    $request->expects($this->once())->method('getResourceParam')->willReturn($this->serviceName);

    $this->resource->setRequest($request);

    $resp = $this->resource->getService();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

  }

  public function getResourcesMock() {
    return $this->testResources;
  }

  public function getServicesMock() {
    return $this->testServices;
  }

  public function loadConfigForResourceNameWith($subject) {
    $this->assertEquals($this->resourceName, $subject);
    return true;
  }

  public function getResourceByNameWith($subject) {
    $this->assertEquals($this->resourceName, $subject);
    return true;
  }

  public function getResourceByNameMock() {
    if (isset($this->testResources[$this->resourceName])) {
        return $this->testResources[$this->resourceName];
    } else {
        return null;
    }
  }

  public function getServiceByNameWith($subject) {
    $this->assertEquals($this->serviceName, $subject);
    return true;
  }

  public function getServiceByNameMock() {
    if (isset($this->testServices[$this->serviceName])) {
        return $this->testServices[$this->serviceName];
    } else {
        return null;
    }
  }

}
