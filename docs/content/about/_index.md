+++
title = "About RESTng"
weight = 10
+++

The clear trend in application development is to interact with data through a middleware layer, typically referred to as an API.  Using RESTful design provides an easy to use implementation.

## Goals

We have several goals for our API implementation. These are NOT in any particular order.

* Create reusable business logic components
* Sustain acceptable performance levels
* Build on active community projects

## Design Principles

The following design principles, specifically separation of concerns, must be observed.

1. REST service layer
    * REST services must be exposed through a single, common interface
    * The framework must manage all request/response functions in REST layer
    * The framework must catch exceptions from the service layer
2. Service implementation layer
    * The service layer must be agnostic to the calling layer implementation
    * The service layer must throw exceptions for abnormal behavior
    * The service layer must use dependency injection
    * The service layer must register services with the DI container

## Architecture

The following diagram depicts the separation of REST resources and business services within RESTng. Most developers will be working on the Business Services side, creating callable, reusable business method to manipulated defined data objects.

{{< figure src="APIArchitecture.png" title="API Architecture" >}}

## Components

As a general rule, we prefer to implement third party, open source solutions rather than writing our own.

The following libraries and frameworks will be incorporated into our solution.

* PHP library installer
    * Composer (https://getcomposer.org/)
* REST service layer
    * Slim framework (http://www.slimframework.com/)
* Dependency Injection
    * Aura.DI (https://github.com/auraphp/Aura.Di)
* ORM
    * Propel (http://propelorm.org/)
* Logging
    * log4php (http://logging.apache.org/log4php/)
* Unit testing
    * PHPUnit (https://phpunit.de/)
