<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\Date;

use MiamiOH\RESTng\Util\ResourceProvider;

class DateResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Date',
            'description' => 'Resources for supporting Continuous Integration tests'
        ));

        $this->addDefinition(array(
            'name' => 'Date.Record',
            'type' => 'object',
            'properties' => array(
                'date_string' => array(
                    'type' => 'string',
                )
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'DateTest',
            'class' => 'Tests\Service\Date',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'date.get',
            'description' => 'The current date',
            'tags' => array('Date'),
            'pattern' => '/date',
            'service' => 'DateTest',
            'method' => 'getDate',
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'The current date object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Date.Record',
                    )
                ),
            )
        ));
    }
}
