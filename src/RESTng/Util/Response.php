<?php

namespace MiamiOH\RESTng\Util;

class Response implements \Serializable
{
    /** @var \MiamiOH\RESTng\Util\Request $request */
    protected $request = '';

    protected $statusCode = 200;
    protected $payload = array();
    protected $objectName = 'data';

    protected $totalObjects = 0;

    public function serialize() {
        return serialize(array(
            'statusCode' => $this->statusCode,
            'payload' => $this->payload,
            'objectName' => $this->objectName,
            'totalObjects' => $this->totalObjects,
            'request' => serialize($this->request),
        ));
    }

    public function unserialize($data) {
        $dataArray = unserialize($data);

        $this->statusCode = $dataArray['statusCode'];
        $this->payload = $dataArray['payload'];
        $this->objectName = $dataArray['objectName'];
        $this->totalObjects = $dataArray['totalObjects'];
        $this->request = unserialize($dataArray['request']);
    }

    /**
     * @param $request
     */
    public function setRequest($request)
    {
        /** @var \MiamiOH\RESTng\Util\Request $request */
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function setPayload($data)
    {
        if (!is_array($data)) {
            throw new \Exception('Argument to ' . __CLASS__ . '::setPayload() must be an array');
        }

        $this->payload = $data;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param $statusCode
     */
    public function setStatus($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->statusCode;
    }

    public function isSuccess()
    {
        // Assume HTTP status code range for now
        return ($this->statusCode >= 200 && $this->statusCode <= 299);
    }

    /**
     * @param $name
     */
    public function setObjectName($name)
    {
        $this->objectName = $name;
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return $this->objectName;
    }

    /**
     * @param $count
     */
    public function setTotalObjects($count)
    {
        $this->totalObjects = $count;
    }

    /**
     * @return int
     */
    public function getTotalObjects()
    {
        return $this->totalObjects;
    }

    /**
     * @return bool
     */
    public function isPaged()
    {
        return $this->request->isPaged();
    }

    /**
     * @return bool
     */
    public function isPartial()
    {
        return $this->request->isPartial();
    }

    /**
     * @return string
     */
    public function getPartialFieldsSpec()
    {
        return $this->request->getPartialFieldsSpec();
    }

    /**
     * @return array
     */
    public function getResourceParams()
    {
        return $this->request->getResourceParams();
    }

    /**
     * @return array
     */
    public function getResourceOptions()
    {
        return $this->request->getOptions();
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->request->getResourceName();
    }

    /**
     * @return int
     */
    public function getResponseLimit()
    {
        return $this->request->getLimit();
    }

    /**
     * @return int
     */
    public function getResponseOffset()
    {
        return $this->request->getOffset();
    }

    /**
     * @return string
     */
    public function getResponseCallback()
    {
        return $this->request->getCallback();
    }
}
