<?php

class slimDefinitionTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $restngApp;

    public function setUp(): void
    {
        parent::setUp();

        $this->restngApp = new \MiamiOH\RESTng\App();

    }

    public function testAddDefinitionRequiresArray() {

        $definitionConfig = '';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Definition config must be an array for addDefinition');

        $this->restngApp->addDefinition($definitionConfig);
    }

    public function testAddTagRequiresName() {

        $definitionConfig = array();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Name is required when adding a definition');

        $this->restngApp->addDefinition($definitionConfig);
    }

}
