<?php

namespace MiamiOH\RESTng\Util;

class Configuration
{

    private $database = '';
    private $dbh = '';

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param $application
     * @param string $category
     * @return array
     * @throws \Exception
     */
    public function getConfiguration($application, $category = '')
    {

        if (!$application) {
            throw new \Exception('getConfiguration requires a valid application');
        }

        $this->dbh = $this->database->getHandle('authman');

        $query = '
              SELECT config_key, config_value
              FROM cm_config
              WHERE config_application = ?
            ';

        $queryArgs = array($application);

        if ($category) {
            $query .= 'and config_category = ?';
            $queryArgs[] = $category;
        }

        $configs = $this->dbh->queryall_list($query, $queryArgs);

        return $configs;
    }

}
