<?php

namespace MiamiOH\RESTng\Testing;

use Illuminate\Database\Capsule\Manager;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DatabaseCapsule;
use MiamiOH\RESTng\Observer\ResourceRunnerObserver;
use MiamiOH\RESTng\Util\ResourceRunner;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package MiamiOH\RESTng\Testing
 *
 * @codeCoverageIgnore
 */
abstract class TestCase extends BaseTestCase
{
    use MakesHttpResourceRequests;

    /** @var \MiamiOH\RESTng\App $app */
    protected $app;

    /** @var DatabaseCapsule */
    private $capsule;

    protected $databaseConnections = [];

    protected $refreshDatabase = false;
    protected $setUpHasRun = false;

    public function createApplication()
    {
        /** @var App $app */
        $app = require __DIR__ . '/../../../bootstrap.php';

        $app->observeClassWith(ResourceRunner::class, ResourceRunnerObserver::class);

        $env = [
            'REQUEST_METHOD' => 'GET',
            'SCRIPT_NAME' => '/api',
            'PATH_INFO' => '/random/string',
            'CONTENT_TYPE' => 'application/json',
            'QUERY_STRING' => '',
            'HTTP_ACCEPT' => 'application/json',
        ];

        $app->container->singleton('environment', function ($c) use ($env) {
            return \Slim\Environment::mock($env);
        });

        return $app;
    }

    protected function setUp(): void
    {
        if (! $this->app) {
            $this->refreshApplication();
        }

        $this->setUpHasRun = true;
    }

    protected function setUpTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[RefreshDatabase::class])) {
            $this->refreshDatabase();

            /** @var DatabaseCapsule $capsule */
            $capsule = $this->app->getService('APIDatabaseCapsule');

            $pdo = Manager::connection()->getPdo();

            foreach ($this->databaseConnections as $connectionName) {
                $capsule->addConnection([
                    'driver'   => 'sqlite',
                    'database' => ':memory:',
                ], $connectionName);

                Manager::connection($connectionName)->setPdo($pdo);
            }
        }

        return $uses;
    }

    protected function tearDownTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[RefreshDatabase::class])) {
            $this->resetDatabase();
        }

        return $uses;
    }

    protected function refreshApplication()
    {
        $this->app = $this->createApplication();

        $this->setUpTraits();
    }

    protected function tearDown(): void
    {
        $this->tearDownTraits();

        if ($this->app) {
            // Clear the static singleton app instance
            \MiamiOH\RESTng\App::clearInstance();
            $this->app = null;
        }

        $this->setUpHasRun = false;

        parent::tearDown();
    }

    protected function assertHttpStatusWithJsonMessage(TestResponse $response, int $status, string $message): void
    {
        $response->assertStatus($status);
        $response->assertJson([
                'data' => [
                    'message' => $message
                ],
            ]);
    }
}
