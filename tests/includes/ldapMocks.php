<?php

namespace MiamiOH\RESTng\Legacy;

include_once('ldapEntryMocks.php');

class LdapMockData {
  static private $mockData = array();

  static public $currentEntryNum = 0;

  static public $mockAttributeList = array();
  static public $requestedAttributes = array();
  static public $entryList = array();

  static public function reset() {
    self::$mockData = array();

    self::$currentEntryNum = 0;

    self::$mockAttributeList = array();
    self::$requestedAttributes = array();
    self::$entryList = array();
  }

  static public function set($key, $value) {
    self::$mockData[$key] = $value;

    // Expect arrays for some mock data. Make a copy in our local scope
    // so that function mocks can consume them in a loop.
    switch ($key) {
      case 'mockAttributeList':
        if (!is_array($value)) {
          throw new \Exception('Mock value for attributeList must be an array');
        }
        self::$attributeList = $value;
        break;

      case 'requestedAttributes':
        if (!is_array($value)) {
          throw new \Exception('Mock value for requestedAttributes must be an array');
        }
        self::$requestedAttributes = $value;
        break;

      case 'mockSearchResultData':
        if (!is_array($value)) {
          throw new \Exception('Mock value for mockSearchResultData must be an array');
        }
        self::$entryList = $value;
        break;

    }

  }

  static public function keyExists($key) {
    return array_key_exists($key, self::$mockData);
  }

  static public function get($key) {
    if (!isset(self::$mockData[$key])) {
      throw new \Exception('Mock data for ' . $key . ' not found');
    }

    return self::$mockData[$key];
  }
}

function ldap_connect ($server, $port = 389) {
  $ds = new \StdClass();

  LdapMockData::set('ds', $ds);

  LdapMockData::set('server', $server);
  LdapMockData::set('port', $port);

  return $ds;
}

function ldap_bind($ds, $dn, $password) {
  if (LdapMockData::keyExists('mockLoginResult')) {
    return LdapMockData::get('mockLoginResult');
  }

  return true;
}

function ldap_errno() {
  if (LdapMockData::keyExists('mockErrorNo')) {
    return LdapMockData::get('mockErrorNo');
  }

  return null;
}

function ldap_error() {
  if (LdapMockData::keyExists('mockError')) {
    return LdapMockData::get('mockError');
  }

  return null;
}

function ldap_search($ds , $baseDn, $filter, $attributes, $attrsOnly, $sizeLimit, $timeLimit, $deref = LDAP_DEREF_NEVER) {

  LdapMockData::set('requestedAttributes', $attributes);
  LdapMockData::set('searchFilter', $filter);

  if (LdapMockData::keyExists('mockSearchResponse')) {
    return LdapMockData::get('mockSearchResponse');
  }

  return false;
}

function ldap_count_entries($ds, $searchResults) {
  if (LdapMockData::keyExists('mockSearchResultCount')) {
    return LdapMockData::get('mockSearchResultCount');
  }

  return 0;
}

function ldap_first_entry($ds, $searchResults) {
  if (count(LdapMockData::$entryList)) {
    LdapMockData::$currentEntryNum = count(LdapMockData::$entryList);
    return LdapMockData::$currentEntryNum;
  }

  return null;
}

function ldap_next_entry($ds, $currentEntry) {
  if ($currentEntry < count(LdapMockData::$entryList)) {
    LdapMockData::$currentEntryNum++;
    return LdapMockData::$currentEntryNum;
  }

  return null;
}

function ldap_get_dn($ds, $entry) {
  if (LdapMockData::keyExists('mockLdapDn')) {
    return LdapMockData::get('mockLdapDn');
  }

  return '';
}

function ldap_first_attribute($ds, $entry, $berId) {
  return array_shift(LdapMockData::$requestedAttributes);
}

function ldap_next_attribute($ds, $entry, $berId) {
  return array_shift(LdapMockData::$requestedAttributes);
}

function ldap_get_values($ds, $entry, $attribute) {
  if (isset(LdapMockData::$entryList[LdapMockData::$currentEntryNum][$attribute])) {
    $values = $entryList[LdapMockData::$currentEntryNum][$attribute];
    $values['count'] = count($entryList[LdapMockData::$currentEntryNum][$attribute]);

    return $values;
  }

  return null;
}
