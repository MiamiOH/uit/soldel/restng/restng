<?php

class serviceTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /** @var \MiamiOH\RESTng\Service */
    private $service;

  public function setUp(): void
  {
    parent::setUp();

    $this->service = new \MiamiOH\RESTng\Service();

  }

  public function testSetUpService() {
    $response = new \MiamiOH\RESTng\Util\Response();

    $this->service->setResponse($response);

    $this->assertEquals($response, $this->service->getResponse());
    $this->assertNotEquals('', $this->service->getLogger());
    $this->assertNotEquals('', $this->service->getServiceLog());

  }

  public function testSetRequest() {
    $response = $this->getMockBuilder('MiamiOH\RESTng\Util\Response')
          ->setMethods(array('setRequest'))
          ->getMock();

    $response->method('setRequest')->with();

    $request = $this->getMockBuilder('MiamiOH\RESTng\Util\Request')
          ->setMethods(array('setRequest'))
          ->getMock();

    $response->expects($this->once())->method('setRequest')->with($this->identicalTo($request));

    $setResp = $this->service->setResponse($response);

    $this->assertTrue($setResp);

    $setResp = $this->service->setRequest($request);

    $this->assertTrue($setResp);

    $this->assertEquals($request, $this->service->getRequest());
  }

  public function testApiUser() {
    $apiUser = new MiamiOH\RESTng\Util\User();

    $this->assertEquals('', $this->service->getApiUser());

    $this->service->setApiUser($apiUser);

    $this->assertEquals($apiUser, $this->service->getApiUser());
  }

  public function testCallResource() {
    $app = $this->getMockBuilder('MiamiOH\RESTng\App')
          ->setMethods(array('newResponse', 'callResource'))
          ->getMock();

    $options = array('type' => 'new', 'color' => 'blue');

    $response = new MiamiOH\RESTng\Util\Response();
    $app->method('newResponse')->willReturn($response);
    $app->expects($this->once())->method('callResource')
      ->with($this->equalTo('test.service'), $this->equalTo($options))
      ->willReturn($response);

    $setResp = $this->service->setApp($app);

    $callResp = $this->service->callResource('test.service', $options);

    $this->assertEquals($response, $callResp);

  }

  public function testLocationFor() {
    $app = $this->getMockBuilder('MiamiOH\RESTng\App')
          ->setMethods(array('newResponse', 'locationFor'))
          ->getMock();

    $options = array('type' => 'new', 'color' => 'blue');

    $response = new MiamiOH\RESTng\Util\Response();
    $app->method('newResponse')->willReturn($response);
    $app->expects($this->once())->method('locationFor')
      ->with($this->equalTo('test.service'), $this->equalTo($options))
      ->willReturn('http://example.com/api/test/service');

    $setResp = $this->service->setApp($app);

    $callResp = $this->service->locationFor('test.service', $options);

    $this->assertEquals('http://example.com/api/test/service', $callResp);

  }

}
