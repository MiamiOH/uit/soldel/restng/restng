<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 12:33 PM
 */

namespace MiamiOH\RESTng\RouteMiddleware;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\Apm\Transaction;

class Authentication extends RouteMiddleware
{
    public const DEFAULT_AUTH_HEADER_FORMAT = 'Token token=%s';
    public const SUPPORTED_AUTH_HEADER_FORMATS = [
        self::DEFAULT_AUTH_HEADER_FORMAT,
        'Bearer %s',
    ];

    public function call(App $app, array $options): void
    {
        /** @var Transaction $transaction */
        $transaction = $app->getService('ApmTransaction');
        $span = $transaction->startSpan('Authentication', 'middleware');

        /*
         * Loop over each authnConfig entry. Exit the loop after the first successful
         * authentication, except for anonymous which is only flagged as allowed.
         */
        $authenticated = false;
        $allowAnonymous = false;

        /*
         * Grandfather in original config which inferred token auth from an empty array.
         * This will be assumed to be type token and injected as the first option in
         * the array of configs. If the incoming array is not empty, assume it is built
         * correctly.
         */
        if (count($options) === 0) {
            $options[] = [ 'type' => 'token' ];
        }

        foreach ($options as $authnConfig)
        {
            $authType = (isset($authnConfig['type']) && $authnConfig['type']) ? $authnConfig['type'] : 'token';

            if ($authType === 'anonymous') {
                $allowAnonymous = true;
            } elseif ($authType === 'token') {

                $token = $this->extractTokenFromHeader($app->request->headers->get('AUTHORIZATION'));

                // If no token was retrieved from the Authorization header, look for a query
                // string value.
                if (empty($token))
                {
                    $token = $app->request->params('token');

                    // We're dealing with the Slim Request object at this point.
                    // We need to remove any middleware specific request content
                    // to prevent problems later.
                    $app->removeRequestOption('token');

                }

                /** @var \MiamiOH\RESTng\Util\User $apiUser */
                $apiUser = $app->getService('APIUser');

                if ($token && $apiUser->addUserByToken($token)) {
                    $authenticated = true;
                    break;
                }
            }
        }

        $span->stop();

        /*
         * If there was no successful authentication or anonymous
         * access is allowed, reject the request.
         */

        if (!($authenticated || $allowAnonymous)) {
            # TODO - render a suitable response to get proper formatting

            $logger = \Logger::getLogger('api.middleware');
            $logger->debug('origin: ' . $app->environment['HTTP_ORIGIN']);

            // Allow domain for CORS so the consumer can handle the response
            if (isset($app->environment['HTTP_ORIGIN'])) {
                $app->response()->header('Access-Control-Allow-Origin', $app->environment['HTTP_ORIGIN']);
            }

            $transaction->setName('RESTng Authentication Middleware Failed');

            $app->halt(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
        }
    }

    private function extractTokenFromHeader(?string $headerValue): ?string
    {
        if (empty($headerValue)) {
            return null;
        }

        foreach (self::SUPPORTED_AUTH_HEADER_FORMATS as $format) {
            $pattern = '/' . str_replace('%s', '(.*)', $format) . '/';
            if (preg_match($pattern, $headerValue, $matches)) {
                return str_replace('"', '', $matches[1]);
            }
        }

        return null;
    }
}
