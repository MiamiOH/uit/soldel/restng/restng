<?php

namespace MiamiOH\RESTng\Exception;

class TransactionMissingNameException extends ApmException
{
}
