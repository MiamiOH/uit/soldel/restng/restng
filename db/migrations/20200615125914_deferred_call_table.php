<?php

use Phinx\Migration\AbstractMigration;

class DeferredCallTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('deferred_calls')
            ->addColumn('key', 'string', ['limit' => 256, 'null' => false])
            ->addColumn('status', 'string', ['limit' => 25])
            ->addColumn('hash', 'string', ['limit' => 256])
            ->addColumn('scheduled_time', 'datetime')
            ->addColumn('attempt_count', 'integer', ['default' => 0])
            ->addColumn('error_count', 'integer' , ['default' => 0])
            ->addColumn('api_user', 'text', ['null' => true])
            ->addColumn('resource_name', 'string', ['limit' => 256, 'null' => false])
            ->addColumn('resource_params', 'text', ['null' => true])
            ->addColumn('response_status', 'integer', ['null' => true])
            ->addColumn('response', 'text', ['null' => true])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->create();

    }
}
