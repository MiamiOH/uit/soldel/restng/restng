# Test that requests for composed subobjects are supported
Feature: Request options containing composed specfications result in correct response
 As a developer using the RESTng API
 I want to specify what subobjects to compose and return
 In order to efficiently fetch relevant data

 Scenario: Composed subobject specs are passed to the business service
   Given a REST client
   When I want to get composed objects "course,enrollments"
   And I make a GET request for /citest/compose/reflect
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element isComposed equal to "true"
   And I get the composedObjects element from the subject
   And the subject is a hash
   And the subject does contain an element enrollments
   And the subject does contain an element course

  Scenario: Composed subobject specs with fields are passed to the business service
    Given a REST client
    When I want to get composed objects "course(courseId,courseSubject),enrollments"
    And I make a GET request for /citest/compose/reflect
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And the subject has an element isComposed equal to "true"
    And I get the composedObjects element from the subject
    And the subject is a hash
    And the subject does contain an element enrollments
    And the subject does contain an element course
    And I get the course element from the subject
    And a value in the subject is "courseSubject"
    And a value in the subject is "courseId"

  Scenario: Composed subobject can be used with fields for main object
    Given a REST client
    When I want to get composed objects "course,enrollments"
    And I want to get fields "id,string,description"
    And I make a GET request for /citest/compose/reflect
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject is a hash
    And the subject has an element isComposed equal to "true"
    And I get the composedObjects element from the subject
    And the subject is a hash
    And the subject does contain an element enrollments
    And the subject does contain an element course
    And I get the data element from the response object
    And the subject has an element isPartial equal to "true"
    And I get the partialRequestFields element from the subject
    And the subject is an array
    And the subject contains 3 entries
    And a value in the subject is "id"
    And a value in the subject is "string"
    And a value in the subject is "description"

  Scenario: Composed subobject cannot be used with subobjects in fields for main object
    Given a REST client
    When I want to get composed objects "course,enrollments"
    And I want to get fields "id,string,description,course()"
    And I make a GET request for /citest/compose/reflect
    Then the HTTP status code is 400
