<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:40 AM
 */

namespace Tests\RESTconfig\Time;

use MiamiOH\RESTng\Util\ResourceProvider;

class TimeResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Time',
            'description' => 'Resources for supporting Continuous Integration tests'
        ));

        $this->addDefinition(array(
            'name' => 'Time.Record',
            'type' => 'object',
            'properties' => array(
                'time_string' => array(
                    'type' => 'string',
                )
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'TimeTest',
            'class' => 'Tests\Service\Time',
            'description' => 'Services and resources to facilitate integration and regression testing of the framework.',
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'time.get',
            'description' => 'The current time',
            'tags' => array('Time'),
            'pattern' => '/time',
            'service' => 'TimeTest',
            'method' => 'getTime',
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'The current time object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Time.Record',
                    )
                ),
            )
        ));
    }
}
