<?php

namespace MiamiOH\RESTng\Connector;

class LDAPFactory
{

    private $dataSource;

    /**
     * @param $dataSource
     */
    public function setDataSource($dataSource)
    {
        $this->dataSource = $dataSource;
    }

    /**
     * @param $datasourceName
     * @return \MiamiOH\RESTng\Legacy\LDAP
     */
    public function getHandle($datasourceName)
    {
        $ldapHandle = $this->dataSource->Factory($datasourceName);

        return $ldapHandle;
    }

}
