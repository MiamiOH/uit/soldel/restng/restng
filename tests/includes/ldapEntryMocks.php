<?php

namespace MiamiOH\RESTng\Legacy\LDAP;

/*
  These functions support classes in different namespaces, but we
  don't want to repeat all of the mocking. Simply call the higher
  level mocks and return the value.
*/

function ldap_get_dn($ds, $entry) {
  return \MiamiOH\RESTng\Legacy\ldap_get_dn($ds, $entry);
}

function ldap_first_attribute($ds, $entry, $berId) {
  return \MiamiOH\RESTng\Legacy\ldap_first_attribute($ds, $entry, $berId);
}

function ldap_next_attribute($ds, $entry, $berId) {
  return \MiamiOH\RESTng\Legacy\ldap_next_attribute($ds, $entry, $berId);
}

function ldap_get_values($ds, $entry, $berId) {
  return \MiamiOH\RESTng\Legacy\ldap_get_values($ds, $entry, $berId);
}
