<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 5/14/18
 * Time: 7:46 PM
 */

namespace Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesDatasource;
use Tests\Data\Table\Book;
use Tests\FeatureTestCase;
use MiamiOH\RESTng\Testing\TestResponse;

class ContentTypeTest extends FeatureTestCase
{
    use UsesDatasource;
    use UsesTestData;

    /** @var \MiamiOH\RESTng\Legacy\DB\DBH\Sqlite3 */
    private static $handle;

    public function setUp(): void
    {
        parent::setUp();

        if (self::$handle === null) {
            self::$handle = $this->createSqlite3DatabaseHandle();
        }

        $this->useData()
            ->withSqlite(self::$handle->getDbConnection())
            ->withTable(Book::class, ['withBooks' => [1]])
            ->populate();

        $this->installDatasourceService(self::$handle);

    }

    public function testDefaultContentFormatIsJson(): void
    {
        $response = $this->get('/citest/book/1');

        $this->assertResponseIsJson($response);
    }

    public function testCanUseJsonExtensionForAcceptType(): void
    {
        $response = $this->get('/citest/book/1.json');

        $this->assertResponseIsJson($response);
    }

    public function testCanUseAcceptJsonHeaderForAcceptType(): void
    {
        $response = $this->getJson('/citest/book/1');

        $this->assertResponseIsJson($response);
    }

    public function testReturnsJsonWhenBothAcceptHeaderAndExtensionAreSet(): void
    {
        $response = $this->getJson('/citest/book/1.json');

        $this->assertResponseIsJson($response);
    }

    public function testReturnsJsonWhenAcceptHeaderIsJsonAndExtensionIsNot(): void
    {
        $response = $this->getJson('/citest/book/1.xml');

        $this->assertResponseIsJson($response);
    }

    public function testCanUseXmlExtensionForAcceptType(): void
    {
        $response = $this->get('/citest/book/1.xml');

        $this->assertResponseIsXml($response);
    }

    public function testCanUseAcceptXmlHeaderForAcceptType(): void
    {
        $response = $this
            ->withHeader('accept', 'application/xml')
            ->get('/citest/book/1');

        $this->assertResponseIsXml($response);
    }

    public function testReturnsXmlWhenBothAcceptHeaderAndExtensionAreSet(): void
    {
        $response = $this
            ->withHeader('accept', 'application/xml')
            ->get('/citest/book/1.xml');

        $this->assertResponseIsXml($response);
    }

    public function testReturnsXmlWhenAcceptHeaderIsXmlAndExtensionIsNot(): void
    {
        $response = $this
            ->withHeader('accept', 'application/xml')
            ->get('/citest/book/1.json');

        $this->assertResponseIsXml($response);
    }

    private function assertResponseIsJson(TestResponse $response): void
    {
        $response->assertStatus(App::API_OK);
        $response->assertHeader('content-type', 'application/json');
    }

    private function assertResponseIsXml(TestResponse $response): void
    {
        $response->assertStatus(App::API_OK);
        $response->assertHeader('content-type', 'application/xml');
    }
}
