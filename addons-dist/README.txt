You can add any local dependencies here using a composer.json file. It is up to
you to run composer install/update. RESTng will load the vendor/autoload.php if
it exists.