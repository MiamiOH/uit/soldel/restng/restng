# Test the content type handling by using extensions and headers
Feature: Specify incoming and outgoing content types using extensions and headers
 As a developer using the RESTng API
 I want to specify content type by extension and header
 In order to have control of the data format

 # Top level collection
 Scenario: Get a collection in json by specifying the extension
   Given a REST client
   When I make a GET request for /citest/record.json
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a collection in xml by specifying the extension
   Given a REST client
   When I make a GET request for /citest/record.xml
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

 Scenario: Get a collection in json by specifying the Accept header
   Given a REST client
   When I give the HTTP Accept header with the value "application/json"
   And I make a GET request for /citest/record
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a collection in xml by specifying the Accept header
   Given a REST client
   When I give the HTTP Accept header with the value "application/xml"
   And I make a GET request for /citest/record
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

 # Models
 Scenario: Get a model in json by specifying the extension
   Given a REST client
   When I make a GET request for /citest/record/1.json
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a model in xml by specifying the extension
   Given a REST client
   When I make a GET request for /citest/record/1.xml
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

 Scenario: Get a model in json by specifying the Accept header
   Given a REST client
   When I give the HTTP Accept header with the value "application/json"
   And I make a GET request for /citest/record/1
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a model in xml by specifying the Accept header
   Given a REST client
   When I give the HTTP Accept header with the value "application/xml"
   And I make a GET request for /citest/record/1
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

 # Matching header and extension
 Scenario: Get a model in json when both header and extension are given
   Given a REST client
   When I give the HTTP Accept header with the value "application/json"
   And I make a GET request for /citest/record/1.json
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a model in xml when both header and extension are given
   Given a REST client
   When I give the HTTP Accept header with the value "application/xml"
   And I make a GET request for /citest/record/1.xml
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

 # Mismatched header and extension
 Scenario: Get a model in json when header is json and extension is xml
   Given a REST client
   When I give the HTTP Accept header with the value "application/json"
   And I make a GET request for /citest/record/1.xml
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/json"

 Scenario: Get a model in xml when header is xml and extension is json
   Given a REST client
   When I give the HTTP Accept header with the value "application/xml"
   And I make a GET request for /citest/record/1.json
   Then the HTTP status code is 200
   And the HTTP Content-Type header is "application/xml"

