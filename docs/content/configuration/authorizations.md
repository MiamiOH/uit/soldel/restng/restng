+++
title = "Authorization"
weight = 80
+++

RESTng provides the `MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface` as a means to handle authorization when making requests. The default implementation is `MiamiOH\RESTng\Service\AuthorizationValidator\File` which validates against a configuration file rather than an external service. This approach gives you complete control over authorization during development.

The authorization configuration is found at:

```bash
config/authorizations.yaml
```

Authorizations are based on the AuthMan convention of `application`, `module` and `key`. The file will contain objects reflecting that structure and ending in an array of users authorized for that key. In the following example, the application is 'File Transfer', the module is 'SolDel_FileTransfer' and the keys are 'list', 'get', 'create' and 'delete'. For each key, 'bubba' is the only authorized user.

```yaml
---
File Transfer:
  SolDel_FileTransfer:
    list:
      - bubba
    get:
      - bubba
    create:
      - bubba
    delete:
      - bubba
```
