+++
title = "Building a Service"
weight = 70
+++

Business services are classes which implement the methods called when resources are requested. A service class may implement one or several methods called by resources, as well as any other supporting methods. The business service is the embodiment of the business value of the service. It acts on requests to fetch or update data represented by models and returns appropriate models as a response.

The separation of the business logic from the interface layer allows us to focus our development efforts on business value. This implementation encourages testable design and higher service reuse.

## Testing

RESTng 2 services should be well testing with both feature and unit tests. The relevant documentation sections are listed first here to encourage a "test first" approach.

## Defining a Service

Services are defined in the ResourceProvider along with resources and definitions. You use the addService method to do so.

{{< highlight php "linenos=table" >}}
<?php
...
$this->addService(array(
    'name' => 'WeekDayService',
    'class' => 'RESTng\Service\WeekDay',
    'description' => 'Provides weekday data.'
));
{{< / highlight >}} 

The **name** must be unique across the RESTng namespace. We encourage the use of PascalCase or dot separated names to increase readability. Incorporting the project or package in the name is recommended. Use a left to right order of specificity, for example "ProjectPackageService".

The **class** must be the fully qualified name of the class which implements the service.

The **description** is self-explanatory, but is not currently exposed to developers other than by browsing the source code.

## Singleton Pattern

It's important to understand that the RESTng dependency injection container will lazily instantiate your object as a singleton. This means that your object is only created when something asks for it, not when you define it, and that only one instance of your class will be created. Your class must take care not to assume particular state across method calls.

## Changing Implementations

Defining a service and then referencing that service in resources may seem redundant at first, but it enables a very powerful design choice. By using a service in the resource definition, we decouple the resource from the class which implements the service. This allows us to easily change the implementation without impacting resources.

For example, the authentication service on one instance of RESTng may interact directly with a local database to create and validate tokens. An instance of RESTng on another server may not have that same access. By defining the authentication service to use a class that is simply a REST client to the first instance, but provides all of the same exposed methods, the resources that use the authentication service work with no changes.