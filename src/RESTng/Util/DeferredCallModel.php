<?php


namespace MiamiOH\RESTng\Util;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Exception\InvalidArgumentException;

class DeferredCallModel extends Model implements DeferredCall
{
    protected $connection = 'restng';

    protected $table = 'deferred_calls';

    public $timestamps = false;

    protected $fillable = [
        'key',
        'api_user',
        'resource_name',
        'resource_params',
        'scheduled_time',
    ];

    private $required = [
        'key',
        'resource_name',
    ];

    public function fill(array $attributes)
    {
        // Default fillable attributes
        if (empty($attributes['scheduled_time'])) {
            $attributes['scheduled_time'] = Carbon::now();
        }

        // Default non-fillable attributes
        $this->setAttribute('status', self::STATUS_PENDING);
        $this->setAttribute('attempt_count', 0);
        $this->setAttribute('error_count', 0);
        $this->setAttribute('created_at', Carbon::now());
        $this->setAttribute('updated_at', Carbon::now());

        return parent::fill($attributes);
    }

    public function id(): int
    {
        return $this->id;
    }

    public function key(): string
    {
        return $this->key;
    }

    public function resourceName(): string
    {
        return $this->resource_name;
    }

    public function resourceParams(): array
    {
        return $this->resource_params;
    }

    public function hasApiUser(): bool
    {
        return !empty($this->attributes['api_user']);
    }

    public function apiUser(): ?User
    {
        return $this->api_user;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function hash(): string
    {
        return $this->hash;
    }

    public function scheduledTime(): Carbon
    {
        return $this->scheduled_time;
    }

    public function attemptCount(): int
    {
        return $this->attempt_count;
    }

    public function errorCount(): int
    {
        return $this->error_count;
    }

    public function responseStatus(): int
    {
        return $this->response_status;
    }

    public function response(): Response
    {
        return $this->response;
    }

    public function createdAt(): Carbon
    {
        return $this->created_at;
    }

    public function updatedAt(): Carbon
    {
        return $this->updated_at;
    }

    public function isPending(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function isNotPending(): bool
    {
        return !$this->isPending();
    }

    public function isFailed(): bool
    {
        return $this->status === 'f';
    }

    public function scheduleAt(Carbon $scheduleAt): void
    {
        $this->scheduled_time = $scheduleAt;
    }

    public function hasResponse(): bool
    {
        return !empty($this->attributes['response']);
    }

    public function addResponse(Response $response): void
    {
        $this->response = $response;
    }

    public function markInProgress(): void
    {
        $this->status = self::STATUS_IN_PROGRESS;
    }

    public function markPending(): void
    {
        $this->status = self::STATUS_PENDING;
    }

    public function markCancelled(): void
    {
        $this->status = self::STATUS_CANCELLED;
    }

    public function markComplete(): void
    {
        $this->status = self::STATUS_COMPLETE;
    }

    public function markFailed(): void
    {
        $this->status = self::STATUS_FAILED;
    }

    public function incrementAttemptCount(): void
    {
        $this->attempt_count++;
    }

    public function incrementErrorCount(): void
    {
        $this->error_count++;
    }

    public function verifyCallHash(): bool
    {
        return $this->hash === $this->generateCallHash();
    }

    public function save(array $options = []): bool
    {
        foreach ($this->required as $name) {
            if (empty($this->attributes[$name])) {
                throw new InvalidArgumentException(sprintf('A "%s" must be provided to create a DeferredCall.', $name));
            }
        }

        if ($this->isDirty()) {
            $this->updated_at = Carbon::now();
        }

        return parent::save($options);
    }

    public function reset(): void
    {
        $this->status = 'p';
        $this->attempt_count = 0;
        $this->error_count = 0;
        $this->scheduleAt(Carbon::now());
    }


    private function generateCallHash()
    {
        if (!env('HASH_KEY')) {
            throw new \Exception('Hash key not defined');
        }

        $string = implode('', [
            $this->attributes['resource_name'] ?? '',
            $this->attributes['resource_params'] ?? '',
            $this->attributes['api_user'] ?? '',
        ]);

        return base64_encode(hash_hmac('sha256', $string, env('HASH_KEY'), true));
    }

    // Model attribute accessors and mutators
    protected function setKeyAttribute(string $value)
    {
        if (!empty($this->attributes['key'])) {
            throw new DeferredCallException('Call key cannot be changed.');
        }

        $this->attributes['key'] = $value;
    }

    protected function setResourceNameAttribute(string $value)
    {
        if (!empty($this->attributes['resource_name'])) {
            throw new DeferredCallException('Call resource name cannot be changed.');
        }

        $this->attributes['resource_name'] = $value;
        $this->attributes['hash'] = $this->generateCallHash();
    }

    protected function getResourceParamsAttribute($value): array
    {
        return unserialize($value, ['allowed_classes' => false]);
    }

    protected function setResourceParamsAttribute($value)
    {
        if (!empty($this->attributes['resource_params'])) {
            throw new DeferredCallException('Call resource parameters cannot be changed.');
        }

        if (!is_array($value)) {
            throw new InvalidArgumentException('Resource parameters must be an array.');
        }

        $this->attributes['resource_params'] = serialize($value);
        $this->attributes['hash'] = $this->generateCallHash();
    }

    protected function getApiUserAttribute($value)
    {
        if (empty($value)) {
            throw new DeferredCallException('Deferred Call has no associated API User.');
        }

        return unserialize($value, ['allowed_classes' => [User::class]]);
    }

    protected function setApiUserAttribute($value)
    {
        if (!empty($this->attributes['api_user'])) {
            throw new DeferredCallException('Call API user cannot be changed.');
        }

        if (!$value instanceof User) {
            throw new InvalidArgumentException('API User must be of type ' . User::class);
        }

        $this->attributes['api_user'] = serialize($value);
        $this->attributes['hash'] = $this->generateCallHash();
    }

    protected function getScheduledTimeAttribute($value)
    {
        if (empty($value)) {
            throw new DeferredCallException('Empty scheduled time value');
        }

        return Carbon::parse($value);
    }

    protected function getCreatedAtAttribute($value)
    {
        if (empty($value)) {
            throw new DeferredCallException('Empty created at value');
        }

        return Carbon::parse($value);
    }

    protected function getUpdatedAtAttribute($value)
    {
        if (empty($value)) {
            throw new DeferredCallException('Empty updated at value');
        }

        return Carbon::parse($value);
    }

    protected function getResponseStatusAttribute($value)
    {
        if (empty($value)) {
            throw new DeferredCallException('Empty response status value');
        }

        return $value;
    }

    protected function setResponseStatusAttribute(): void
    {
        throw new DeferredCallException('Response status cannot be set directly');
    }

    protected function getResponseAttribute($value): Response
    {
        if (empty($value)) {
            throw new DeferredCallException('Empty response value');
        }

        return unserialize($value, [Response::class]);
    }

    protected function setResponseAttribute(Response $response)
    {
        $this->attributes['response_status'] = $response->getStatus();
        $this->attributes['response'] = serialize($response);
    }
}