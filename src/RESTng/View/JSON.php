<?php

namespace MiamiOH\RESTng\View;

class JSON extends Base
{

    protected $contentType = 'application/json';
    /**
     * Bitmask consisting of <b>JSON_HEX_QUOT</b>,
     * <b>JSON_HEX_TAG</b>,
     * <b>JSON_HEX_AMP</b>,
     * <b>JSON_HEX_APOS</b>,
     * <b>JSON_NUMERIC_CHECK</b>,
     * <b>JSON_PRETTY_PRINT</b>,
     * <b>JSON_UNESCAPED_SLASHES</b>,
     * <b>JSON_FORCE_OBJECT</b>,
     * <b>JSON_UNESCAPED_UNICODE</b>.
     * The behaviour of these constants is described on
     * the JSON constants page.
     * @var int
     */
    public $encodingOptions = 0;

    /**
     * @param $responseBody
     * @return string
     */
    public function processBody($responseBody)
    {

        $jsonp_callback = $this->apiResponse->getResponseCallback();

        $body = '';

        if ($jsonp_callback) {
            $body = $this->app->response()->body($jsonp_callback . '(' . \json_encode($responseBody, $this->encodingOptions) . ')');
        } else {
            $body = $this->app->response()->body(\json_encode($responseBody, $this->encodingOptions));
        }

        return $body;
    }

}
