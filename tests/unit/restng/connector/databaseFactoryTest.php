<?php

class databaseFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{

  private $dbFactory;

  private $dataSource;

  private $factoryArgument;
  private $dbh;

  public function setUp(): void
  {
    parent::setUp();

    $this->factoryArgument = '';
    $this->dbh = new StdClass();

    $this->dataSource = $this->getMockBuilder('\RESTng\Legacy\DataSource')
          ->setMethods(array('Factory'))
          ->getMock();

    $this->dbFactory = new \MiamiOH\RESTng\Connector\DatabaseFactory();

    $this->dbFactory->setDataSource($this->dataSource);
  }

  public function testGetHandle() {
    $this->dataSource->expects($this->once())->method('Factory')
      ->with($this->callback(array($this, 'factoryWith')))
      ->will($this->returnCallback(array($this, 'factoryMock')));

    $handle = $this->dbFactory->getHandle('myDataSource');

    $this->assertSame($this->dbh, $handle);

  }

  public function testGetHandleNotFound() {
    $this->dataSource->expects($this->once())->method('Factory')
      ->with($this->callback(array($this, 'factoryWith')))
      ->will($this->returnCallback(array($this, 'factoryMock')));

    $handle = $this->dbFactory->getHandle('myDataSourceNotFound');

    $this->assertEquals(null, $handle);

  }

  public function factoryWith($subject) {
    $this->factoryArgument = $subject;
    return true;
  }

  public function factoryMock() {
    if ($this->factoryArgument === 'myDataSource') {
      return $this->dbh;
    }

    return null;
  }

}
