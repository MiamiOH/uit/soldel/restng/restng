# Test that requests for partial responses and subobjects are supported
Feature: Request options containing field specfications result in partial response
 As a developer using the RESTng API
 I want to specify what fields and subobjects to return
 In order to efficiently fetch relevant data

 Scenario: Partial field specs are passed to the business service
   Given a REST client
   When I want to get fields "id,string,description"
   And I make a GET request for /citest/partial/reflect
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element isPartial equal to "true"
   And I get the partialRequestFields element from the subject
   And the subject is an array
   And the subject contains 3 entries
   And a value in the subject is "id"
   And a value in the subject is "string"
   And a value in the subject is "description"

 Scenario: Subobject specs are passed to the business service
   Given a REST client
   When I want to get subobjects "numberFact(),another(id,name),anotherNew(id,name)"
   And I make a GET request for /citest/subobject/reflect
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And I get the numberFact element from the subject
   And the subject is an array
   And the subject contains 0 entries
   And I get the previous subject
   And I get the another element from the subject
   And the subject is an array
   And the subject contains 2 entries
   And a value in the subject is "id"
   And a value in the subject is "name"
   And I get the previous subject
   And I get the anotherNew element from the subject
   And the subject is an array
   And the subject contains 2 entries
   And a value in the subject is "id"
   And a value in the subject is "name"

 Scenario: Partial and subobject specs can be combined
   Given a REST client
   When I want to get fields "name,id,numberFact(),another(id,name),anotherNew(id,name),description"
   And I make a GET request for /citest/partialCombined/reflect
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And I get the numberFact element from the subject
   And the subject is an array
   And the subject contains 0 entries
   And I get the previous subject
   And I get the another element from the subject
   And the subject is an array
   And the subject contains 2 entries
   And a value in the subject is "id"
   And a value in the subject is "name"
   And I get the previous subject
   And I get the anotherNew element from the subject
   And the subject is an array
   And the subject contains 2 entries
   And a value in the subject is "id"
   And a value in the subject is "name"
   And I get the previous subject
   And I get the partialRequestFields element from the subject
   And the subject is an array
   And the subject contains 3 entries
   And a value in the subject is "id"
   And a value in the subject is "name"
   And a value in the subject is "description"

 Scenario: Get a complete object when no fields are requested
   Given a REST client
   When  I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element courseId equal to "20559"
   And the subject has an element semester equal to "2003Fa"
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element meetingTime equal to "MWF  10:00am"
   And the subject has an element maxEnrollment equal to "23"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"

 Scenario: Get a partial object when certain fields are requested
   Given a REST client
   When I want to get fields "section,instructor,location"
   And I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"
   And the subject does not contain an element classId
   And the subject does not contain an element courseId
   And the subject does not contain an element semester
   And the subject does not contain an element meetingTime
   And the subject does not contain an element maxEnrollment

 Scenario: Get a subobject with default fields
   Given a REST client
   When I want to get subobjects "course()"
   And I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element courseId equal to "20559"
   And the subject has an element semester equal to "2003Fa"
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element meetingTime equal to "MWF  10:00am"
   And the subject has an element maxEnrollment equal to "23"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"
   And I get the course element from the subject
   And the subject has an element technical equal to "0"
   And the subject has an element courseSubject equal to "COM"
   And the subject has an element apiLocation equal to "http://ws/api/citest/course/20559"
   And the subject has an element courseId equal to "20559"
   And the subject has an element credits equal to "3"
   And the subject has an element description equal to "Intro: Publ Express & Crit Inq "
   And the subject has an element courseNumber equal to "135"
   And the subject has an element mpCategory equal to "II"

 Scenario: Get multiple subobjects with default fields
   Given a REST client
   When I want to get subobjects "course(),enrollments()"
   And I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element courseId equal to "20559"
   And the subject has an element semester equal to "2003Fa"
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element meetingTime equal to "MWF  10:00am"
   And the subject has an element maxEnrollment equal to "23"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"
   And I get the course element from the subject
   And the subject has an element technical equal to "0"
   And the subject has an element courseSubject equal to "COM"
   And the subject has an element apiLocation equal to "http://ws/api/citest/course/20559"
   And the subject has an element courseId equal to "20559"
   And the subject has an element credits equal to "3"
   And the subject has an element description equal to "Intro: Publ Express & Crit Inq "
   And the subject has an element courseNumber equal to "135"
   And the subject has an element mpCategory equal to "II"
   And I get the previous subject
   And I get the enrollments element from the subject
   And the subject contains 14 entries
   And I get entry 1 from the subject
   And the subject has an element majorId equal to "MGT"
   And the subject has an element studentId equal to "104837"
   And the subject has an element lastName equal to "Bonner"
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element finalGrade equal to "B"
   And the subject has an element entryYear equal to "2003"
   And the subject has an element enrollmentId equal to "57428075"
   And the subject has an element midTermGrade equal to "A"
   And the subject has an element firstName equal to "Geoff"

 Scenario: Get subobjects with specific fields
   Given a REST client
   When I want to get subobjects "section,instructor,location,course(),enrollments()"
   And I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"
   And the subject does not contain an element classId
   And the subject does not contain an element courseId
   And the subject does not contain an element semester
   And the subject does not contain an element meetingTime
   And the subject does not contain an element maxEnrollment
   And I get the course element from the subject
   And the subject has an element technical equal to "0"
   And the subject has an element courseSubject equal to "COM"
   And the subject has an element apiLocation equal to "http://ws/api/citest/course/20559"
   And the subject has an element courseId equal to "20559"
   And the subject has an element credits equal to "3"
   And the subject has an element description equal to "Intro: Publ Express & Crit Inq "
   And the subject has an element courseNumber equal to "135"
   And the subject has an element mpCategory equal to "II"
   And I get the previous subject
   And I get the enrollments element from the subject
   And the subject contains 14 entries
   And I get entry 1 from the subject
   And the subject has an element majorId equal to "MGT"
   And the subject has an element studentId equal to "104837"
   And the subject has an element lastName equal to "Bonner"
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element finalGrade equal to "B"
   And the subject has an element entryYear equal to "2003"
   And the subject has an element enrollmentId equal to "57428075"
   And the subject has an element midTermGrade equal to "A"
   And the subject has an element firstName equal to "Geoff"

 Scenario: Get subobjects with specific fields of both the main and subobjects
   Given a REST client
   When I want to get subobjects "section,instructor,location,course(courseSubject,courseNumber),enrollments()"
   And I make a GET request for /citest/class/20559-03FA
   Then the HTTP status code is 200
   And the response can be parsed
   And I get the data element from the response object
   And the subject is a hash
   And the subject has an element section equal to "A"
   And the subject has an element instructor equal to "Kiefer, David"
   And the subject has an element location equal to "BAC 134"
   And the subject has an element apiLocation matching the pattern "/citest/class/20559"
   And the subject does not contain an element classId
   And the subject does not contain an element courseId
   And the subject does not contain an element semester
   And the subject does not contain an element meetingTime
   And the subject does not contain an element maxEnrollment
   And I get the course element from the subject
   And the subject has an element courseSubject equal to "COM"
   And the subject has an element courseNumber equal to "135"
   And the subject has an element apiLocation equal to "http://ws/api/citest/course/20559"
   And the subject does not contain an element technical
   And the subject does not contain an element courseId
   And the subject does not contain an element credits
   And the subject does not contain an element description
   And the subject does not contain an element mpCategory
   And I get the previous subject
   And I get the enrollments element from the subject
   And the subject contains 14 entries
   And I get entry 1 from the subject
   And the subject has an element majorId equal to "MGT"
   And the subject has an element studentId equal to "104837"
   And the subject has an element lastName equal to "Bonner"
   And the subject has an element classId equal to "20559-03FA"
   And the subject has an element finalGrade equal to "B"
   And the subject has an element entryYear equal to "2003"
   And the subject has an element enrollmentId equal to "57428075"
   And the subject has an element midTermGrade equal to "A"
   And the subject has an element firstName equal to "Geoff"
