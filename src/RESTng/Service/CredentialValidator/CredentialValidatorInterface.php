<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 9:16 PM
 */

namespace MiamiOH\RESTng\Service\CredentialValidator;


interface CredentialValidatorInterface
{
    public function validateToken(string $token): array;
    public function validateUsernamePassword(string $username, string $password): array;
}