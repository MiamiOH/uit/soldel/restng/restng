+++
title = "Resource Parameters"
weight = 50
+++

Parameters are variable parts of the pattern (exposed as an HTTP URL) defined for your resource. The value of the corresponding part of the URL will be supplied in the request.

**Note:** Parameters are required by definition. A missing parameter is, in effect, a missing part of the HTTP URL and results in a different resource being matched or a 404 response.

You must declare any parameter values in your pattern using the configuration ‘params’ key. RESTng will use this information to validate actual parameters in a request and to document the usage of your resource. The params value is an associative array where the keys are the param name from the pattern (without the ‘:’) and the value is an array describing the parameter.

For example, for resource using the pattern ‘/course/section/:term/:crn’, we would declare our parameters as:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'name' => 'course.term.crn',
    'pattern' => '/course/section/:term/:crn’,
    'params' => array(
                 'term' => array(
                       'description' => 'A valid term code',
                      ),
                 'crn' => array(
                       'description' => 'A valid CRN',
                      ),

                  ),
    'service' => 'Course.Section',
    'method' => 'getSectionByCrn',
));
{{< / highlight >}} 

Each parameter must be defined and must include a description.

## Alternate Keys

Resource parameters support an alternate key syntax which can be used in the path. This syntax allows the resource to support multiple keys for identifying a resource while using the same pattern. For example:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'name' => 'citest.student.public.studentId',
    'description' => 'A single test student.',
    'pattern' => '/citest/student/public/:studentId',
    'service' => 'CITestStudent',
    'method' => 'getStudent',
    'params' => array(
        'studentId' => array('description' => 'A test studentId', 'alternateKeys' => ['sid', 'uid']),
    ),
));
{{< / highlight >}} 

This resource can accept either *sid* or *uid* as the studentId parameter. Alternate keys take the form of `key=value` in the path. RESTng will parse the component and provide it to the service as part of the request. If no key is provided in the component, the first entry in the list of alternate keys will be used as the default. The following URLs may all identify the same student resource:

    /citest/student/public/101402
    /citest/student/public/sid=101402
    /citest/student/public/uid=wat_wile

RESTng provides the key used in the request object available to your service. You can use the getResourceParamKey method to retrieve the key used and alter behavior accordingly:

{{< highlight php "linenos=table" >}}
<?php
    $studentId = $request->getResourceParam('studentId');
    $keyField = 'student_id';
    switch ($request->getResourceParamKey('studentId')) {
      case 'sid':
            $keyField = 'student_id';
            break;
      case 'uid':
            $keyField = 'unique_id';
            break;
    }
{{< / highlight >}} 

### Restrictions

There are two important restrictions that should be observed when implementing alternate keys. 

First, the use of alternate keys should only affect the identification of the resource and should have no other impact on behavior. Filtering and other behavior changes should be implemented through resource options (in the URL query string).

Second, consistent with the use parameters in general, the parameter value must include a single identifier. Using a list to identify multiple objects in a single resource URL is strongly discouraged.