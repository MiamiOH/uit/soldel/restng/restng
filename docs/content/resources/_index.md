+++
title = "Defining Resources"
weight = 60
+++

Business services are exposed for external consumption through resources. The REST layer implements these resources as HTTP URIs.

Resources are registered in your ResourceProvider by calling the addResource method, passing an array of resource configuration values.  Consider a simple model resource:

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'description' => 'A specific weekday.',
    'name' => 'WeekDay.day',
    'pattern' => '/WeekDay/day',
    'service' => 'WeekDayService',
    'method' => 'getWeekDay',
));
{{< / highlight >}} 

This example shows the minimum configuration required for a resource to function. The following tables summarizes the resource configuration options. Subsequent sections provide more detail where appropriate.

Config  | Description
--------|------------
action  | (Required) The action must be one of ‘create’, ‘read’, ‘update’ or ‘delete’.  The REST layer will map these to the HTTP methods POST, GET, PUT and DELETE.  (PATCH is not yet supported.)
description | (Required) The description should be a consumer friendly, complete description of the resource. It will be included in the Swagger Spec and rendered in Swagger-UI, and can therefore contain GFM syntax (https://help.github.com/articles/github-flavored-markdown/).
name | (Required) The name is optional and will default to the path value if left out.  The name must be unique across all resources, and it is considered best practice to provide a name to facilitate consumption from other services. A naming standard specification will be developed to facilitate resource naming. Services may use this name to acquire a location (a REST URL) from RESTng to represent a resource.
pattern | (Required) A resource pattern is used by RESTng to match incoming HTTP requests.  Patterns must begin with a ‘/’ and must not include a trailing ‘/’.
service | (Required) The service tells RESTng what registered service name to request when running this resource. Note that this is not the class name. The framework will acquire the service from the dependency injection container.
method | (Required) The provided method name will be called on the injected service object. No parameters are passed in the method call.
summary | A plan text, short explanation of the resource. The summary will be displayed in the Swagger UI interface.
tags | Tags are a feature of Swagger which allow grouping of resources. Tags must be added to RESTng before they will be used in the Swagger UI presentation. See the section on tags for details.
isPartialable | (boolean) Indicates that the resource supports partial object requests and/or composition with subobjects. RESTng will provide the partial spec in the request, but your service must implement the logic to construct the object(s) correctly.
isPageable | (boolean) Indicates that the resource supports paged responses. RESTng will provide the page related options and add the page metadata to the response, but your service must implement logic to fetch the appropriate objects.
responses | An array describing the expected responses for the resource. The keys should be one of the defined RESTng API constants. The value for each response code is an array with a description of the response and details of the returned object (using Swagger Spec).
body | The body describes the expected content of an incoming request body for create (POST) and update (PUT) resources. The body value should refer to a defined model or collection.
middleware | Middleware implements functionality for your resource prior to the your service method being invoked. Currently, the two supported middleware services are authentication and authorization. The middle resource configuration allows you to specify the desired behavior.
params | Resource params are variable parts of the URL pattern. The params configuration is required to describe those parts. RESTng requires that you have a param config entry for each parameter in your URL pattern. The key is the value (without the ":") and the value array must contain a "description" at a minimum.
options | Options are key/value pairs in the query string of the URL in a request (the part after the '?'). The options configuration is required to describe those values. RESTng requires that you have an options config entry for each option your resource supports and the value array must contain a "description" at a minimum. RESTng will throw an error if options are found in the request which are not defined for your service.
xOrgSec | RESTng implements Cross-origin resource sharing (CORS - https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) to allow manage access to resources across domains. The default security posture is open, meaning a resource can be run from any other domain (operating within the context of a web browser's same origin policy). You may configure the CORS options for individual resources.

