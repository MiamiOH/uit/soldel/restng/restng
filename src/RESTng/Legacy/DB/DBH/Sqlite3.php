<?php

namespace MiamiOH\RESTng\Legacy\DB\DBH;

class Sqlite3 extends \MiamiOH\RESTng\Legacy\DB\DBH
{

    /** @var \SQLite3 */
    protected $db;

    /** @var \SQLite3Stmt */
    private $statement;

    /** @var \SQLite3Result */
    private $resultSet;

    /**
     * @param string $user
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string $connect_type
     */
    public function __construct($user = '', $password = '', $database = '', $host = '', $connect_type = '')
    {
        $this->connect($user, $password, $database);
    }

    /**
     * @param string $user
     * @param string $password
     * @param string $database
     * @param string $connect_type
     * @return bool
     * @throws \Exception
     */
    public function connect($user = '', $password = '', $database = '', $connect_type = '')
    {
        $this->connected = false;

        if (empty($database)) {
            $database = ':memory:';
        }

        $this->db = new \Sqlite3($database);
        $this->db->enableExceptions(true);

        $this->connected = true;

        return $this->connected;
    }

    /**
     * @param $statement
     * @param string $parameters
     * @return bool
     * @throws \Exception
     */
    public function _run($statement, $parameters = '')
    {

        if (!$statement) {
            throw new \Exception('No SQL statement to run');
        }

        if (!$statement instanceof \SQLite3Stmt) {
            $statement = $this->db->prepare($statement);
        }

        $this->statement = $statement;

        for ($i = 0, $iMax = count($parameters); $i < $iMax; $i++) {
            $this->statement->bindParam($i + 1, $parameters[$i]);
        }

        $this->resultSet = $this->statement->execute();

        return true;
    }

    /**
     * @param $statement
     * @return array|bool|string
     * @throws \Exception
     */
    public function queryfirstrow_assoc($statement, ...$parameters)
    {
        if ($this->_run($statement, $parameters)) {
            $result = $this->resultSet->fetchArray(SQLITE3_ASSOC);
            $this->resultSet = null;
            $this->statement->close();
            return $result;
        }

        return false;
    }

    /**
     * @param $statement
     * @return array|bool
     * @throws \Exception
     */
    public function queryall_array($statement, ...$parameters)
    {
        if ($this->_run($statement, $parameters)) {
            $records = [];
            while ($record = $this->resultSet->fetchArray(SQLITE3_ASSOC)) {
                $records[] = $record;
            }
            $this->resultSet = null;
            $this->statement->close();
            return $records;
        }

        return false;
    }

}
