<?php


namespace MiamiOH\RESTng\Service\Apm;


use Nipwaayoni\ApmAgent;

class TransactionFactory
{
    /** @var ApmAgent */
    private $agent;

    public function __construct(ApmAgent $agent)
    {
        $this->agent = $agent;
    }

    public function newTransaction(string $name = 'RESTng Request'): Transaction
    {
        return new Transaction($this->agent, $name);
    }
}