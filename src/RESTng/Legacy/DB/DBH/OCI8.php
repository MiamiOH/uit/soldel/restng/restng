<?php

namespace MiamiOH\RESTng\Legacy\DB\DBH;

use MiamiOH\RESTng\Service\Apm\QuerySpan;

class OCI8 extends \MiamiOH\RESTng\Legacy\DB\DBH
{

    private $default_date_format = 'DD-MON-YYYY HH24:MI:SS';
    private $returnLobs = false;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $connect_type;

    /**
     * @param string $user
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string $connect_type
     */
    public function __construct($user = '', $password = '', $database = '', $host = '', $connect_type = '')
    {
        $this->type = 'oci8';

        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
        $this->host = $host;
        $this->connect_type = $connect_type;
    }

    /**
     * @param $setting
     */
    public function setReturnLobs($setting)
    {
        $this->returnLobs = $setting;
    }

    private function ensureConnection(): void
    {
        if ($this->connected) {
            return;
        }

        $this->connect($this->user, $this->password, $this->database, $this->connect_type);
    }
    /**
     * @param string $user
     * @param string $password
     * @param string $database
     * @param string $connect_type
     * @return bool
     * @throws \Exception
     */
    public function connect($user = '', $password = '', $database = '', $connect_type = '')
    {
        $this->connected = false;

        if (null !== $this->transaction) {
            $connectSpan = $this->transaction->startSpan(sprintf('Connect to: %s', $database));
        }

        // Must explicitly ask for persistent connections, otherwise we use new.
        if ($connect_type == 'persistent') {
            $this->db = @oci_pconnect($user, $password, $database);
        } else {
            $this->db = @oci_new_connect($user, $password, $database);
        }

        if (null !== $this->transaction && null !== $connectSpan) {
            $connectSpan->stop();
        }

        $this->user = $user;
        $this->database = $database;

        if ($this->db !== false) {
            $this->connected = true;
            $this->user = $user;
            $this->database = $database;
        } else {
            $error = oci_error();

            $this->error = DB_ERROR;
            $this->catch_error($error);

        }

        $this->error = $this->connected ? DB_NO_ERROR : DB_ERROR;

        return $this->connected;
    }

    /**
     * @return bool
     */
    public function disconnect()
    {
        $disconnected = false;
        if ($this->connected) {
            $disconnected = oci_close($this->db);
        } else {
            $disconnected = true;
        }

        $this->error = $disconnected === null ? DB_NO_ERROR : DB_ERROR;

        return $disconnected;
    }

    /**
     * @param $statement
     * @return bool|\RESTng\Legacy\DB\STH\OCI8
     * @throws \Exception
     */
    public function prepare($statement)
    {
        if (!$statement) {
            throw new \Exception('No SQL statement to prepare');
        }

        $this->ensureConnection();

        $result = oci_parse($this->db, $statement);

        $error = oci_error($this->db);

        if ($error === false) {
            $this->error = DB_NO_ERROR;
        } else {
            $this->error = DB_ERROR;
            $this->catch_error($error);
        }

        if ($this->error !== DB_NO_ERROR) {
            return false;
        }

        $sth = new \MiamiOH\RESTng\Legacy\DB\STH\OCI8($this, $result, $statement);
        $sth->setApmTransaction($this->transaction);
        return $sth;
    }

    /**
     * @param $statement
     * @param string $parameters
     * @return bool
     * @throws \Exception
     */
    public function _run($statement, $parameters = '')
    {

        if (!$statement) {
            throw new \Exception('No SQL statement to run');
        }

        $this->ensureConnection();

        $this->query = $this->_parse_placeholders($statement, $parameters);

        $this->result = oci_parse($this->db, $this->query);

        if ($this->result !== false) {
            if ($this->auto_commit === false) {
                $mode = defined(OCI_NO_AUTO_COMMIT) ? OCI_NO_AUTO_COMMIT : OCI_DEFAULT;
            } else {
                $mode = OCI_COMMIT_ON_SUCCESS;
            }
            $span = $this->transaction->startQuerySpan(QuerySpan::getQueryName($statement, 'OCI DB Query'));
            $span->setStatement($statement);
            $span->setSubType('oracle');

            if (@oci_execute($this->result, $mode)) {
                $success = true;
                $this->error = DB_NO_ERROR;
            } else {
                $success = false;
                $this->error = DB_ERROR;
                $this->catch_error(oci_error($this->result));
            }

            $span->stop();
        }

        return $success;
    }

    /**
     * @param $statement
     * @return array|bool|string
     * @throws \Exception
     */
    public function queryfirstrow_array($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $result_row = false;

        if ($this->_run($statement, $parameters)) {
            $result_row = oci_fetch_array($this->result, OCI_NUM + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0));
            if ($result_row === false) {
                $result_row = DB_EMPTY_SET;
            }
            $this->error = DB_NO_ERROR;
        }

        return $result_row;
    }

    /**
     * @param $statement
     * @return array|bool|string
     * @throws \Exception
     */
    public function queryfirstrow_assoc($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $result_row = false;

        if ($this->_run($statement, $parameters)) {
            $result_row = oci_fetch_array($this->result, OCI_ASSOC + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0));
            if ($result_row === false) {
                $result_row = DB_EMPTY_SET;
            }
            $this->error = DB_NO_ERROR;
        }

        return is_array($result_row) ? array_change_key_case($result_row, CASE_LOWER) : $result_row;
    }

    /**
     * @param $statement
     * @return array|bool|string
     * @throws \Exception
     */
    public function queryfirstcolumn($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $result_row = false;

        if ($this->_run($statement, $parameters)) {
            $result_row = oci_fetch_array($this->result, OCI_NUM + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0));
            if ($result_row === false) {
                $result_row = DB_EMPTY_SET;
            }
            $this->error = DB_NO_ERROR;
            oci_free_statement($this->result);
        }

        return is_array($result_row) ? $result_row[0] : $result_row;
    }

    /**
     * @param $statement
     * @return array|bool
     * @throws \Exception
     */
    public function queryall_array($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $records = false;

        if ($this->_run($statement, $parameters)) {
            $records = array();
            $row_num = 0;
            $row = array();
            while ($row = oci_fetch_array($this->result, OCI_ASSOC + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0))) {
                $row = array_change_key_case($row, CASE_LOWER);
                $records[$row_num] = array();
                foreach ($row as $name => $value) {
                    $records[$row_num][$name] = $value;
                }
                $row_num++;
            }
            $this->error = DB_NO_ERROR;
        }

        return $records;
    }

    /**
     * @param $statement
     * @return array|bool
     * @throws \Exception
     */
    public function queryall_assoc($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $records = false;

        if ($this->_run($statement, $parameters)) {
            $records = array();
            $row = array();
            while ($row = oci_fetch_array($this->result, OCI_ASSOC + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0))) {
                $row = array_change_key_case($row, CASE_LOWER);
                $key = array_shift($row);
                $records[$key] = array();
                foreach ($row as $name => $value) {
                    $records[$key][$name] = $value;
                }
            }
            $this->error = DB_NO_ERROR;
        }

        return $records;
    }

    /**
     * @param $statement
     * @return array|bool
     * @throws \Exception
     */
    public function queryall_list($statement)
    {
        $param_count = func_num_args();
        $parameters = array();
        for ($i = 1; $i < $param_count; $i++) {
            $parameter = func_get_arg($i);
            if (is_array($parameter)) {
                $parameters = $parameter;
                break;
            } else {
                $parameters[] = $parameter;
            }
        }

        $records = false;

        if ($this->_run($statement, $parameters)) {
            $field_count = oci_num_fields($this->result);

            switch ($field_count) {
                case (1):
                    $list_type = 'numeric';
                    break;
                case (2):
                    $list_type = 'assoc';
                    break;
                default:
                    throw new \Exception('Invalid column count for list');
            }

            $records = array();
            $row = array();
            while ($row = oci_fetch_array($this->result, OCI_NUM + OCI_RETURN_NULLS + ($this->returnLobs ? OCI_RETURN_LOBS : 0))) {
                if ($list_type == 'numeric') {
                    $records[] = $row[0];
                } else {
                    $records[$row[0]] = $row[1];
                }
            }
            $this->error = DB_NO_ERROR;
        }

        return $records;
    }

    /**
     * @param $conversion
     * @param $target
     * @param string $format
     * @return string
     */
    public function convert($conversion, $target, $format = '')
    {
        $convert = '';

        switch ($conversion) {
            case 'date':
                if ($format == '') {
                    $format = $this->default_date_format;
                } else {
                    $format = $this->db_date_format($format);
                }

                $convert = 'to_date(' . $target . ', \'' . $format . '\')';
                break;

            case 'char_to_date':
                if ($format == '') {
                    $format = $this->default_date_format;
                } else {
                    $format = $this->db_date_format($format);
                }

                $convert = 'to_date(' . $target . ', \'' . $format . '\')';
                break;

            case 'char_to_timestamp':
                if ($format == '') {
                    $format = $this->default_date_format;
                } else {
                    $format = $this->db_date_format($format);
                }

                $convert = 'to_date(' . $target . ', \'' . $format . '\')';
                break;

            case 'char_to_time':
                if ($format == '') {
                    $format = $this->default_date_format;
                } else {
                    $format = $this->db_date_format($format);
                }

                $convert = 'to_date(' . $target . ', \'' . $format . '\')';
                break;

            case 'date_to_char':
                if ($format == '') {
                    $format = $this->default_date_format;
                } else {
                    $format = $this->db_date_format($format);
                }

                $convert = 'to_char(' . $target . ', \'' . $format . '\')';
                break;

            case 'uppercase':
                $convert = 'upper(' . $target . ')';
                break;

            case 'lowercase':
                $convert = 'lower(' . $target . ')';
                break;
        }

        return $convert;
    }

    /**
     * @param $format
     * @return mixed
     */
    public function db_date_format($format)
    {
        $replace = array(
            '%e' => 'DD',
            '%d' => 'DD',
            '%m' => 'MM',
            '%n' => 'MM',
            '%Y' => 'YYYY',
            '%y' => 'YY',
            '%H' => 'HH24',
            '%I' => 'HH12',
            '%M' => 'MI',
            '%S' => 'SS',
            '%p' => 'AM',
        );

        return str_replace(array_keys($replace), array_values($replace), $format);
    }

    /**
     * @param string $string
     * @return mixed
     */
    public function escape($string = '')
    {
        return str_replace('\'', '\'\'', $string);
    }

    /**
     * @param string $error_array
     * @throws \Exception
     */
    public function catch_error($error_array = '')
    {
        // Set some defaults if we can't determine the real error
        $error_num = -1;
        $error_string = 'Unknown error';
        if (is_array($error_array)) {
            $error_num = $error_array['code'];
            $error_string = $error_array['message'];
            $error_string .= "\nQuery: " . $error_array['sqltext'];
            $error_string .= "\nOffset: " . $error_array['offset'];
        }

        unset($this->error_string);

        if ($error_num !== false) {
            $this->error_string = 'Error number: ' . $error_num;
        }

        if ($error_string !== false) {
            if (isset($this->error_string)) {
                $this->error_string .= ' - ';
            }
            $this->error_string .= $error_string;
        }

        throw new \Exception('user: ' . $this->user . "\n" . $error_string);
    }

    /**
     * @return bool
     */
    public function commit()
    {
        $this->ensureConnection();

        if ($this->auto_commit) {
            $commit = true;
        } else {
            $commit = oci_commit($this->db);
        }

        return $commit;
    }

    /**
     * @return bool
     */
    public function rollback()
    {
        $this->ensureConnection();

        if ($this->auto_commit) {
            $rollback = true;
        } else {
            $rollback = oci_rollback($this->db);
        }

        return $rollback;
    }

}
