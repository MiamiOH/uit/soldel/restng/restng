<?php

namespace MiamiOH\RESTng\Util;

class RequestParameter implements \Serializable
{
    private $config = [];
    private $alternateKeys = [];

    private $name = '';
    private $key = '';
    private $value = '';

    /**
     * RequestParameter constructor.
     * @param $config
     */
    public function __construct($config = [])
    {
        if (!is_array($config)) {
            throw new \Exception('Configuration for ' . __CLASS__ . ' must be an array');
        }

        $this->config = $config;

        if (isset($config['alternateKeys'])) {
            $this->setAlternateKeys($config['alternateKeys']);
        }
    }

    /**
     * @param $keys
     * @throws \Exception
     */
    public function setAlternateKeys($keys)
    {
        if (!is_array($keys)) {
            throw new \Exception('Alternate keys must be an array');
        }

        $this->alternateKeys = $keys;
    }

    public function getAlternateKeys()
    {
        return $this->alternateKeys;
    }

    /**
     * @param string $name
     * @param null $value
     * @throws \Exception
     */
    public function setParameter($name = '', $value = null)
    {

        if (!$name) {
            throw new \Exception('Name is required');
        }

        $key = '';

        if (strpos($value, '=') !== false) {
            list($key, $value) = explode('=', $value);
        }

        if (count($this->alternateKeys) > 0) {
            if (!$key) {
                $key = $this->alternateKeys[0];
            }

            if (!in_array($key, $this->alternateKeys)) {
                throw new \Exception('Provided key ' . $key . ' is not in the alternate key list: ' .
                    implode(', ', $this->alternateKeys));
            }

        }

        $this->setName($name);
        $this->setValue($value);
        $this->setKey($key);
    }

    /**
     * @param $name
     * @throws \Exception
     */
    public function setName($name)
    {
        if (!$name) {
            throw new \Exception('A parameter name is required');
        }

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            'name' => $this->getName(),
            'value' => $this->getValue(),
            'key' => $this->getKey(),
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $dataArray = unserialize($serialized);

        $this->setName($dataArray['name']);
        $this->setValue($dataArray['value']);
        $this->setKey($dataArray['key']);
    }

    // The toString magic method is called in limited contexts which renders
    // it less useful than hoped.
    /**
     * @return string
     */
    public function __xtoString()
    {
        return $this->value;
    }
}