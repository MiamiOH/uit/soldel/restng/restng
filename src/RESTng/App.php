<?php

namespace MiamiOH\RESTng;
/*
	Miami specific sub-class of Slim.  This lets us override some methods to
	get our desired behavior (i.e. urlFor) and extend the functions.
*/

use Aura\Di\Container;
use Aura\Di\Factory;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use MiamiOH\RESTng\Connector\DatabaseCapsule;
use MiamiOH\RESTng\Connector\DataSourceProviderFile;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Exception\ResourceNotFound;
use MiamiOH\RESTng\Exception\UnAuthorized;
use MiamiOH\RESTng\Observer\ObservableInterface;
use MiamiOH\RESTng\Observer\ObservationCollector;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\RouteMiddleware\RouteMiddlewareInterface;
use MiamiOH\RESTng\Service\Apm\Transaction;
use MiamiOH\RESTng\Util\CacheManager;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\ResourceLoader;
use MiamiOH\RESTng\Util\ResourceRunner;
use MiamiOH\RESTng\Util\Response;

class App extends \Slim\Slim
{
    public const API_OK =  200;
    public const API_CREATED =  201;
    public const API_ACCEPTED =  202;
    public const API_NOCONTENT = 204;
    public const API_BADREQUEST = 400;
    public const API_UNAUTHORIZED =  401;
    public const API_NOTFOUND =  404;
    public const API_ENTITYALREADYEXISTS =  409;
    public const API_PRECONDITIONREQUIRED = 428;
    public const API_FAILED =  500;
    public const API_NOTIMPLEMENTED =  501;

    public const API_LIST_SEPARATOR =  ',';

    public const API_DEFAULTPAGELIMIT = 10;

    /** @var \Logger $logger */
    protected $logger;

    /** @var \Aura\Di\Container $diContainer */
    protected $diContainer;

    /** @var \Illuminate\Container\Container */
    protected $serviceContainer;

    /** @var ObservationCollector  */
    private $observationCollector;

    private $requestFactory = '';
    private $responseFactory = '';

    protected $services = array();
    protected $resources = array();
    protected $tags = array();
    protected $definitions = array();
    /** @var Collection */
    protected $requiredFileCollection;
    /** @var ResourceLoader[] */
    protected $resourceLoaders = [];
    protected $ormConnections = array();
    protected $routeMiddleware = array();
    protected $routeMiddlewareWeights = array();

    protected $allowedActions = array('create', 'read', 'update', 'delete', 'patch');

    private $reservedOptionNames = array(
            'fields',
            'limit',
            'offset',
            'token',
        );

    private $currentStage = 'development';
    private $stages = array(
        'local' => 2,
        'development' => 5,
        'test' => 10,
        'ci' => 11,
        'production' => 15,
    );

    public static function clearInstance($name = 'default')
    {
        if (isset(static::$apps[$name])) {
            unset(static::$apps[$name]);
        }
    }

    /**
     * @param array $userSettings
     */
    public function __construct(array $userSettings = array())
    {
        $this->logger = \Logger::getLogger(__CLASS__);
        $this->logger->debug('Constructed ' . __CLASS__);

        parent::__construct($userSettings);

        $this->resetDiContainer();

        $this->diContainer->set('App', $this);

        $this->requiredFileCollection = new Collection();

        $this->serviceContainer = new \Illuminate\Container\Container();

        $this->observationCollector = new ObservationCollector();

    }

    public function run(): void
    {
        /** @var Transaction $transaction */
        $transaction = $this->getService('ApmTransaction');
        $span = $transaction->startSpan('Run', 'RESTng');

        parent::run();

        $span->stop();

        $transaction->send();
    }

    public function pseudoRun()
    {
        $this->middleware[0]->call();

        $this->response->finalize();
        $status = $this->response->getStatus();
        $headers = $this->response->headers;
        $body = $this->response->getBody();

        /** @var Transaction $transaction */
        $transaction = $this->getService('ApmTransaction');
        $transaction->send();

        return [$status, $headers, $body];
    }

    public function observeClassWith(string $observedClass, string $observerClass): void
    {
        $this->observationCollector->observeClassWith($observedClass, $observerClass);
    }

    public function attachObservers(ObservableInterface $observable): void
    {
        $this->observationCollector->attachObservers($observable);
    }

    public function collectObservations(): array
    {
        return $this->observationCollector->collectObservations();
    }

    public function clearObservations(): void
    {
        $this->observationCollector->clearObservations();
    }

    public function resetDiContainer()
    {
        $this->diContainer = new Container(new Factory());
    }

    /**
     * @param $stage
     */
    public function setCurrentStage($stage)
    {

        if (!array_key_exists($stage, $this->stages))
        {
            throw new \Exception('Cannot set current stage to ' . $stage . '. Unsupported stage type.');
        }

        $this->currentStage = $stage;
    }

    public function getCurrentStage()
    {
        return $this->currentStage;
    }

    /**
     * @param $stage
     * @return bool
     */
    public function isStagePriorTo ($stage)
    {
        if (!array_key_exists($stage, $this->stages))
        {
            throw new \Exception('Cannot test stage ' . $stage . '. Unsupported stage type.');
        }

        $isPriorTo = $this->stages[$this->currentStage] < $this->stages[$stage];

        return $isPriorTo;
    }

    public function addResourceLoader(ResourceLoader $loader)
    {
        $loader->useApp($this);

        $this->resourceLoaders[] = $loader;
    }

    /**
     * @param $config
     * @throws \Exception
     */
    public function loadConfig($config)
    {
        foreach ($this->resourceLoaders as $loader) {
            $loader->loadResourceConfig($config);
        }
    }

    /**
     * @param $resourceName
     * @throws \Exception
     */
    public function loadConfigForResourceName($resourceName)
    {
        if (strpos($resourceName, '.') !== false) {
            $resourceName = substr_replace($resourceName, '', strpos($resourceName, '.'));
        }

        $this->loadConfig($resourceName);
    }

    /**
     * @param $resourcePattern
     * @throws \Exception
     */
    public function loadConfigForResourcePattern($resourcePattern)
    {
        // Remove any '/' after the first character
        if (strpos($resourcePattern, '/', 1)) {
            $resourcePattern = substr_replace($resourcePattern, '', strpos($resourcePattern, '/', 1));
        }

        // Remove any extension
        if (strpos($resourcePattern, '.', 1)) {
            $resourcePattern = substr_replace($resourcePattern, '', strpos($resourcePattern, '.', 1));
        }

        $name = str_replace('/', '', $resourcePattern);
        $this->loadConfig($name);
    }

    /**
     * @throws \Exception
     */
    public function loadAllConfigs()
    {
        $this->loadConfig('');
    }

    /**
     * @param $connectionName
     * @param string $datasourceName
     * @param string $ormProvider
     *
     * @deprecated
     */
    public function registerOrmConnection($connectionName, $datasourceName = '', $ormProvider = 'Propel')
    {
        $this->logger->info('Registering ORM connections has been deprecated and removed');
    }

    /**
     * @param $diContainer
     */
    public function setDiContainer($diContainer)
    {
        /** @var \Aura\Di\Container $diContainer */
        $this->diContainer = $diContainer;
    }

    public function getDiContainer()
    {
        return $this->diContainer;
    }

    /**
     * @param $serviceName
     * @return mixed
     */
    public function getService($serviceName)
    {
        $service = $this->diContainer->get($serviceName);

        if (is_object($service) && $service instanceof ObservableInterface) {
            $this->attachObservers($service);
        }

        return $service;
    }

    public function makeEventHandler(): Dispatcher
    {
        if ($this->diContainer->has('APIEvents')) {
            return $this->diContainer->get('APIEvents');
        }

        $events = new Dispatcher(\Illuminate\Container\Container::getInstance());

        $this->diContainer->set('APIEvents', function () use ($events) {
            return $events;
        });

        return $events;
    }

    public function listen($event, callable $listener): void
    {
        $dispatcher = $this->makeEventHandler();

        $dispatcher->listen($event, $listener);
    }

    public function makeDatabaseCapsule(): DatabaseCapsule
    {
        $capsule = new DatabaseCapsule($this->makeEventHandler());

        $this->diContainer->set('APIDatabaseCapsule', function () use ($capsule) {
            return $capsule;
        });

        return $capsule;
    }

    public function makeDataSourceFactory(string $sourceFile): DataSourceFactory
    {
        $dataSourceFactory = new DataSourceFactory();

        if (file_exists($sourceFile)) {
            $dataSourceFactory->addProvider(new DataSourceProviderFile($sourceFile));
        }

        $this->diContainer->set('APIDataSourceFactory', function () use ($dataSourceFactory) {
            return $dataSourceFactory;
        });

        return $dataSourceFactory;
    }

    public function makeCache(?string $driver, array $options = []): void
    {
        $this->diContainer->set('APICache', function () use ($driver, $options) {
            return (new CacheManager($driver, $options))->make();
        });
    }

    public function addRegistry(array $registry): void
    {
        $this->diContainer->set('APIRegistry', function () use ($registry) {
            return $registry;
        });
    }

    /**
     * @param $config
     * @throws \Exception
     */
    public function addTag($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Tag config must be an array for addTag');
        }

        if (!isset($config['name']))
        {
            throw new \Exception('Name is required when adding a tag');
        }

        $this->tags[$config['name']] = $config;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param $config
     * @throws \Exception
     */
    public function addDefinition($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Definition config must be an array for addDefinition');
        }

        if (!isset($config['name']))
        {
            throw new \Exception('Name is required when adding a definition');
        }

        $this->definitions[$config['name']] = $config;
    }

    /**
     * @return array
     */
    public function getDefinitions()
    {
        return $this->definitions;
    }

    /**
     * @param $name
     * @return bool
     */
    public function isServiceDefined($name)
    {
        return isset($this->services[$name]);
    }

    public function useService($config)
    {
        if (isset($this->services[$config['name']])) {
            unset($this->services[$config['name']]);
        }

        $this->addService($config);
    }

    public function bind($abstract, $concrete = null, $shared = false)
    {
        $this->serviceContainer->bind($abstract, $concrete, $shared);
    }

    /**
     * @param $config
     * @return bool
     * @throws \Exception
     */
    public function addService($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Service config must be an array for addService');
        }

        if (!(isset($config['name']) && $config['name'])) {
            throw new \Exception('Service name required');
        }

        if (!((isset($config['class']) && $config['class']) || (isset($config['object']) && $config['object']))) {
            throw new \Exception('Service class or object required for ' . $config['name']);
        }

        if (!(isset($config['description']) && $config['description'])) {
            throw new \Exception('Service description required for ' . $config['name']);
        }

        if (isset($this->services[$config['name']])) {
            throw new \Exception('Service with name ' . $config['name'] . ' is already defined');
        }

        if (isset($config['class'])) {
            $this->diContainer->set($config['name'], $this->diContainer->lazyNew($config['class']));
        }

        if (isset($config['object'])) {
            $this->diContainer->set($config['name'], $config['object']);
        }

        /*
            Avoid injecting through constructors of base classes since calling parent constructors
            breaks DI.  Instead, we will use setters and a strict convention to set lazy loaded
            dependencies.

            Specified as 'set' array in $config.  Key is name, which will be capitalized and
            prefixed with 'set' for setter name.  Value is an assoc array where key is one of
            'service', 'scalar' or 'array'.  Service is assumed to be a registered di service
            and will be called with lazyGet.  Scalar is assigned directly and array gets a new
            array.
        */
        if (!isset($config['set'])) {
            $config['set'] = array();
        }

        if (!is_array($config['set'])) {
            throw new \Exception('set option must be an array when adding a service');
        }

        foreach (array_keys($config['set']) as $setName) {
            $config['set'][$setName]['setterMethod'] = 'set' . ucwords($setName);

            if (!(isset($config['set'][$setName]['type']) && $config['set'][$setName]['type'])) {
                $config['set'][$setName]['type'] = 'service';
            }

            switch ($config['set'][$setName]['type']) {
                case 'service':
                    if (!(isset($config['set'][$setName]['name']) && $config['set'][$setName]['name'])) {
                        throw new \Exception('Missing service name for setter dependency: ' . $setName);
                    }

                    $this->diContainer->setter[$config['class']][$config['set'][$setName]['setterMethod']] =
                        $this->diContainer->lazyGet($config['set'][$setName]['name']);
                    break;

                case 'scalar':
                    if (!(isset($config['set'][$setName]['value']) && $config['set'][$setName]['value'])) {
                        throw new \Exception('Missing value for declared set scalar dependency');
                    }

                    $this->diContainer->setter[$config['class']][$config['set'][$setName]['setterMethod']] = $config['set'][$setName]['value'];
                    break;

                case 'array':
                    $this->diContainer->setter[$config['class']][$config['set'][$setName]['setterMethod']] = $config['set'][$setName]['value'];
                    break;

                default:
                    throw new \Exception('Unknown type "' . $config['set'][$setName]['type'] . '" provided for set option');
                    break;
            }
        }

        /*
            Constructor params can still be used for classes which are not subclassed.  In these
            cases, we should avoid type hinting in the constructor as this could interfere with
            injecting mocks for testing.  Constructor parms should be specified in $config
            using the 'params' array.  The keys are the constructor param names and the values
            follow the same rules as described above for setters.
        */

        if (!isset($config['params'])) {
            $config['params'] = array();
        }

        if (!is_array($config['params'])) {
            throw new \Exception('params option must be an array when adding a service');
        }

        foreach (array_keys($config['params']) as $param) {
            if (!(isset($config['params'][$param]['type']) && $config['params'][$param]['type'])) {
                $config['params'][$param]['type'] = 'service';
            }

            switch ($config['params'][$param]['type']) {
                case 'service':
                    if (!(isset($config['params'][$param]['name']) && $config['params'][$param]['name'])) {
                        throw new \Exception('Missing service name for param dependency: ' . $param);
                    }

                    $this->diContainer->params[$config['class']][$param] =
                        $this->diContainer->lazyGet($config['params'][$param]['name']);
                    break;

                case 'scalar':
                    if (!(isset($config['params'][$param]['value']) && $config['params'][$param]['value'])) {
                        throw new \Exception('Missing value for declared param scalar dependency');
                    }

                    $this->diContainer->params[$config['class']][$param] = $config['params'][$param]['value'];
                    break;

                case 'array':
                    $this->diContainer->params[$config['class']][$param] = array();
                    break;

                default:
                    throw new \Exception('Unknown type "' . $config['params'][$param]['type'] . '" provided for param option');
                    break;
            }
        }

        $this->services[$config['name']] = $config;

        return true;
    }

    /**
     * @param $config
     * @return bool
     * @throws \Exception
     */
    public function addResource($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Resource service config must be an array');
        }

        if (!(isset($config['service']) && $config['service'])) {
            throw new \Exception('Resource service required');
        }

        if (!(isset($config['method']) && $config['method'])) {
            throw new \Exception('Resource method required');
        }

        if (!(isset($config['pattern']) && $config['pattern'])) {
            throw new \Exception('Resource pattern required');
        }

        // Make sure there is a name
        if (!(isset($config['name']) && $config['name'])) {
            $config['name'] = $config['pattern'];
        }

        if (!(isset($config['description']) && $config['description'])) {
            throw new \Exception('Resource description required for ' . $config['name']);
        }

        // Validate any params in the pattern are declared
        if (!isset($config['params'])) {
            $config['params'] = array();
        }

        if (!is_array($config['params'])) {
            throw new \Exception('Config option "params" must be an array for addResource');
        }

        // Get params from pattern and verify each is declared
        preg_match_all('/\:(\w+)/', $config['pattern'], $params);
        if (is_array($params[1])) {
            foreach ($params[1] as $param) {
                if (!array_key_exists($param, $config['params'])) {
                    throw new \Exception('Resource param ' . $param . ' not defined for resource ' . $config['name']);
                }

                if (!(isset($config['params'][$param]['description']) && $config['params'][$param]['description'])) {
                    throw new \Exception('No description provided for param ' . $param . ' for resource ' . $config['name']);
                }

                if (isset($config['params'][$param]['alternateKeys']) && !is_array($config['params'][$param]['alternateKeys'])) {
                    throw new \Exception('The alternateKeys value must be an array');
                }
            }
        }

        // Verify that each declared param is in pattern
        $config['conditions'] = array();
        foreach (array_keys($config['params']) as $param) {
            if (!in_array($param, $params[1])) {
                throw new \Exception('Resource param ' . $param . ' not specified in pattern for resource ' . $config['name']);
            }

            if (isset($config['params'][$param]['condition'])) {
                $config['conditions'][$param] = $config['params'][$param]['condition'];
            }
        }

        // Validate the action
        if (!(isset($config['action']) && $config['action'])) {
            $config['action'] = 'read';
        }

        if (!in_array($config['action'], $this->allowedActions)) {
            throw new \Exception('Invalid action (' . $config['action'] . ') Must be one of: ' .
                implode(', ', $this->allowedActions));
        }

        if (!isset($config['isPageable']) || $config['isPageable'] !== true) {
            $config['isPageable'] = false;
        }

        if (isset($config['defaultPageLimit']) && !(is_int($config['defaultPageLimit']) &&
                $config['defaultPageLimit'] > 0)) {
            throw new \Exception("defaultPageLimit must be an integer greater than or equal to 1");
        }

        if (isset($config['maxPageLimit']) && !(is_int($config['maxPageLimit']) &&
                $config['maxPageLimit'] > 0)) {
            throw new \Exception("maxPageLimit must be an integer greater than or equal to 1");
        }

        if (!isset($config['isPartialable']) || $config['isPartialable'] !== true) {
            $config['isPartialable'] = false;
        }

        if (!isset($config['isComposable']) || $config['isComposable'] !== true) {
            $config['isComposable'] = false;
        }

        if (empty($config['dataRepresentation'])) {
            $config['dataRepresentation'] = 'array';
        }

        if (!in_array($config['dataRepresentation'], ['array', 'file'])) {
            throw new \Exception("Invalid dataRepresentation: " . $config['dataRepresentation']);
        }

        // Make sure middleware is an array
        if (!isset($config['middleware'])) {
            $config['middleware'] = array();
        }

        if (!is_array($config['middleware'])) {
            throw new \Exception('addResource "middleware" must be an array');
        }

        if (!isset($config['options'])) {
            $config['options'] = array();
        }

        if (!is_array($config['options'])) {
            throw new \Exception('addResource "options" must be an array');
        }

        foreach (array_keys($config['options']) as $key) {
            if (in_array($key, $this->reservedOptionNames)) {
                throw new \Exception('Reserved option name: ' . $key);
            }

            if (!is_array($config['options'][$key])) {
                throw new \Exception('addResource options key must be an array');
            }

            if (!(isset($config['options'][$key]['description']) && $config['options'][$key]['description'])) {
                throw new \Exception('Description is required for ' . $config['name'] . ' option ' . $key);
            }

            if (!isset($config['options'][$key]['type'])) {
                $config['options'][$key]['type'] = 'single';
            }

            if (!in_array($config['options'][$key]['type'], array('single', 'list'))) {
                throw new \Exception('Type value for resource option must be "single" or "list"');
            }

            if (!isset($config['options'][$key]['required'])) {
                $config['options'][$key]['required'] = false;
            }
        }

        // Setup CORS defaults
        if (!isset($config['xOrgSec'])) {
            $config['xOrgSec'] = array();
        }

        if (!is_array($config['xOrgSec'])) {
            throw new \Exception('addResource xOrgSec must be an array');
        }

        if (!isset($config['xOrgSec']['allow-origin'])) {
            $config['xOrgSec']['allow-origin'] = 'any';
        }

        if (!isset($config['xOrgSec']['allow-credentials'])) {
            $config['xOrgSec']['allow-credentials'] = true;
        }

        if (!isset($config['xOrgSec']['max-age'])) {
            $config['xOrgSec']['max-age'] = 3600;
        }

        if (!isset($config['xOrgSec']['allow-methods'])) {
            $config['xOrgSec']['allow-methods'] = array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS');
        }

        if (!isset($config['xOrgSec']['allow-headers'])) {
            $config['xOrgSec']['allow-headers'] = null;
        }

        /*
            We set up the route here.  This is the heart of the REST framework and allows
            us to consolidate the set up for consistency.  The developer has minimal work
            to do in order set up a functional service.
        */

        $app = $this;

        $route = $this->map($config['pattern'], function () use ($app, $config) {
            $resourceRunner = $app->getService('APIResourceRunner');
            $resourceRunner->setAppInstance($app);
            $resourceRunner->setCache($app->getService('APICache'));
            $resourceRunner->run($config);
        });

        switch ($config['action']) {
            case 'create':
                // create POST route
                $route->via(\Slim\Http\Request::METHOD_POST, \Slim\Http\Request::METHOD_OPTIONS);
                break;

            case 'read':
                // create GET route
                $route->via(\Slim\Http\Request::METHOD_GET, \Slim\Http\Request::METHOD_HEAD);
                break;

            case 'update':
                // create PUT route
                // Support for partial update (PATCH)?
                $route->via(\Slim\Http\Request::METHOD_PUT, \Slim\Http\Request::METHOD_OPTIONS);
                break;

            case 'delete':
                // create DELETE route
                $route->via(\Slim\Http\Request::METHOD_DELETE, \Slim\Http\Request::METHOD_OPTIONS);
                break;

            default:
                $this->halt(HTTP_SERVER_ERROR, 'Unsupported resource action (' . $config['action'] . ') for ' .
                    $config['pattern']);

        }

        $route->name($config['name']);
        if (!$this->router->hasNamedRoute($config['name'])) {
            $this->router->addNamedRoute($config['name'], $route);
        }


        // Conditions are collected from the params spec
        if ($config['conditions']) {
            $route->conditions($config['conditions']);
        }

        // Route middleware is invoked in the order it is defined.  Some of our middleware
        // is sensitive to that order.  For example, authentication must happen before
        // authorization.  A weighting system allows us to manage the order.

        if (isset($config['middleware']['authorize']) && !isset($config['middleware']['authenticate'])) {
            throw new \Exception('The authorize middleware cannot be used without authenticate');
        }
        asort($this->routeMiddlewareWeights);

        foreach (array_keys($this->routeMiddlewareWeights) as $mw) {
            if (isset($config['middleware'][$mw])) {
                $route->setMiddleware($this->routeMiddleware[$mw]->install($this, $config['middleware'][$mw]));
            }
        }

        $id = $config['name'] ? $config['name'] : $config['action'] . '-' . $config['pattern'];

        if (isset($this->resources[$id])) {
            throw new \Exception('Resource with id ' . $id . ' is already defined');
        }

        $this->resources[$id] = $config;

        return true;
    }

    /**
     * @return array
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getResourceByName($name)
    {
        return $this->resources[$name];
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getServiceByName($name)
    {
        return $this->services[$name];
    }

    /**
     * @param $name
     * @param $function
     * @param int $weight
     * @return bool
     * @throws \Exception
     */
    public function addRouteMiddleware($name, RouteMiddlewareInterface $middleware, int $weight = 0)
    {
        if (!$name) {
            throw new \Exception('Route middleware requires a valid name');
        }

        $this->routeMiddleware[$name] = $middleware;
        $this->routeMiddlewareWeights[$name] = $weight;

        // asort sorts by element retaining index associations
        // it sorts the array in place, so do it after adding middleware
        asort($this->routeMiddlewareWeights);

        return true;
    }

    /**
     * @return array
     */
    public function getRouteMiddleware()
    {
        return $this->routeMiddleware;
    }

    /**
     * @return array
     */
    public function getRouteMiddlewareWeights()
    {
        return $this->routeMiddlewareWeights;
    }

    public function removeLastMiddleware()
    {
        if (empty($this->middleware)) {
            return;
        }

        array_shift($this->middleware);
    }

    /*
      Provide a means for middleware to remove query string values
      after processing them. If not removed, they will trigger errors
      when not declared as a param for the resource.
    */
    /**
     * @param $name
     * @return bool
     */
    public function removeRequestOption($name)
    {
        $paramHash = $this->environment->offsetGet('slim.request.query_hash');
        unset($paramHash[$name]);
        $this->environment->offsetSet('slim.request.query_hash', $paramHash);
        return true;
    }

    public function newRequest($resource)
    {
        $request = new Request();

        $request->setResourceConfig($this->getResourceByName($resource));

        return $request;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     */
    public function newResponse()
    {
        return new Response();
    }

    /**
     * @param $resourceName
     * @param array $resourceParams
     * @return string
     */
    public function locationFor($resourceName, $resourceParams = array())
    {
        // The RESTng REST implementation uses Slim.  Other implementations
        // could do something else here and the business layer should not know.

        $this->loadConfigForResourceName($resourceName);
        return $this->urlFor($resourceName, $resourceParams);
    }

    /**
     * @param $resourceName
     * @param array $params
     * @param bool $defer
     * @return Response
     * @throws \Exception
     */
    public function callResource($resourceName, $params = array(), $options = array())
    {

        if (!is_array($options)) {
            throw new \Exception('The options argument for callResource must be an array');
        }

        if (!isset($options['deferred'])) {
            $options['deferred'] = false;
        }

        $this->loadConfigForResourceName($resourceName);

        $config = $this->getResourceByName($resourceName);

        if (!$config) {
            throw new \Exception('No defined resource ' . $resourceName);
        }

        // Run any defined authorization
        if (isset($config['middleware']) && isset($config['middleware']['authorize'])) {
            /** @var \MiamiOH\RESTng\Util\User $apiUser */
            $apiUser = $this->getService('APIUser');
            $authorized = $apiUser->checkAuthorization($config['middleware']['authorize']);

            if (!$authorized) {
                // Create a new response and return it.
                $response = $this->newResponse();
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                return $response;
            }
        }

        if ($options['deferred']) {
            /** @var \MiamiOH\RESTng\Util\DeferredCallManager $dCallMgr */
            $dCallMgr = $this->getService('APIDeferredCallManager');
            return $dCallMgr->addDeferredCall($resourceName, $params, $options);
        } else {
            /** @var Transaction $transaction */
            $transaction = $this->diContainer->get('ApmTransaction');
            $span = $transaction->startSpan('Call resource ' . $resourceName);
            $result = $this->callResourceNow($resourceName, $config, $params);
            $span->stop();

            return $result;
        }
    }

    /**
     * @param $resourceName
     * @param $config
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    private function callResourceNow($resourceName, $config, $params)
    {

        $r = $this->getService($config['service']);

        if (!is_a($r, 'MiamiOH\RESTng\Service')) {
            throw new \Exception('Service object for ' . $config['service'] . ' must be a descendent of MiamiOH\RESTng\Service');
        }

        $request = new Request();

        $request->setResourceConfig($config);

        $request->setResourceName($resourceName);

        if (isset($params['options']) && is_array($params['options'])) {
            $request->extractRequestOptions($params['options']);
        }

        if (isset($params['params']) && is_array($params['params'])) {
            $request->setResourceParams($params['params']);
        }

        if (isset($config['action']) && in_array($config['action'], array('create', 'update', 'delete')) && isset($params['data']) && is_array($params['data'])) {
            $request->setDataAsArray($params['data']);
        }

        if (!isset($config['method'])) {
            throw new \Exception('No method provided for resource ' . $resourceName);
        }

        $method = $config['method'];

        if (!method_exists($r, $method)) {
            throw new \Exception('Service ' . get_class($r) . ' does not have a method ' . $method);
        }

        $r->setApp($this);
        $r->setResponse(new Response());
        $r->setRequest($request);
        $r->setCache($this->diContainer->get('APICache'));
        $r->setApmTransaction($this->diContainer->get('ApmTransaction'));

        return $r->$method();

    }

    public function hash($string)
    {
        if (!env('HASH_KEY')) {
            throw new \Exception('Hash key not defined');
        }

        return base64_encode(hash_hmac('sha256', $string, env('HASH_KEY'), true));
    }

    /**
     * @param $apiResponse
     * @throws \Exception
     */
    public function apiRender($apiResponse)
    {

        if (!is_a($apiResponse, 'MiamiOH\RESTng\Util\Response')) {
            throw new \Exception('API response object must a descendent of class "MiamiOH\RESTng\Util\Response"');
        }

        if (!is_a($this->view, 'MiamiOH\RESTng\View\Base')) {
            throw new \Exception('View must be a descendent of class "MiamiOH\RESTng\View\Base", given (' .
                get_class($this->view) . ')');
        }

        // TODO move most of this into the View Base class
        $objectName = $apiResponse->getObjectName();
        if (!$objectName) {
            $objectName = 'data';
        }

        $this->view->replace(array($objectName => $apiResponse->getPayload()));

        // Inject our application into the view. The current object is the instance
        // of the application that we need.
        $this->view->setAppInstance($this);

        // The view::render method will cause execution to halt. Nothing is returned and
        // no processing can be done after this call.
        $this->view->apiRender($apiResponse);

    }

    public function handleException($e, $returnExceptions = false)
    {
        // Log the error along with the exception
        $logger = \Logger::getLogger('api');
        $logger->fatal('API call failed', $e);

        /** @var Transaction $agent */
        $agent = $this->getService('ApmTransaction');
        $agent->captureThrowable($e);

        // We cannot assume that services have been set up when an error occurs,
        // so we must instanciate the classes ourselves, using the minimum needed.

        // Use the content format middleware to set up our environment.
        $mw = new \MiamiOH\RESTng\Middleware\ContentFormat();
        $mw->parseFormat($this);

        // Use the Resource Runner service to make sure we handle CORS
        // while sending the error back. Default to open.
        // TODO: get the xOrgSec for the current service
        $rr = new \MiamiOH\RESTng\Util\ResourceRunner();
        $rr->setAppInstance($this);
        $xOrgSecOptions = [
            'allow-origin' => 'any',
            'allow-credentials' => true,
            'max-age' => 3600,
            'allow-methods' => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'),
            'allow-headers' => null,
        ];
        $rr->handleCors([ 'xOrgSec' => $xOrgSecOptions]);

        // construct a valid response object to be rendered
        $request = $this->getService('APIServiceRequestFactory')->newRequest();
        $response = $this->getService('APIServiceResponseFactory')->newResponse();
        $response->setRequest($request);

        switch (get_class($e)) {

            case BadRequest::class:
                $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
                break;

            case UnAuthorized::class:
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                break;

            case ResourceNotFound::class:
                $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                break;

            default:
                $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);

        }

        $payload = array(
            'message' => $e->getMessage(),
            'line' => $e->getLine(),
            'file' => $e->getFile(),
        );

        // Only show the stack trace in stages prior to testing. The trace will still be in the
        // log and may expose sensitive information.
        if ($this->isStagePriorTo('test')) {
            $payload['trace'] = $e->getTraceAsString();
        }

        $response->setPayload($payload);

        \MiamiOH\RESTng\View\Base::$stopAfterRender = false;
        $this->apiRender($response);

        // Emulate the Slim::run method
        $status = $this->response->getStatus();
        $headers = $this->response->headers;
        $body = $this->response->getBody();

        if ($returnExceptions) {
            return [$status, $headers, $body];
        }

        //Send headers
        if (headers_sent() === false) {
            //Send status
            header(sprintf('HTTP/%s %s', $this->config('http.version'), \Slim\Http\Response::getMessageForCode($status)));

            //Send headers
            foreach ($headers as $name => $value) {
                $hValues = explode("\n", $value);
                foreach ($hValues as $hVal) {
                    header("$name: $hVal", false);
                }
            }
        }

        echo $body;
        exit;

    }

    /*
        Override functions
    */
    /**
     * @param string $name
     * @param array $params
     * @return string
     */
    public function urlFor($name, $params = array())
    {
        // The Slim\Request::getUrl() method returns scheme://hostname[:port] not the whole request URL
        return $this->request->getUrl() . $this->request->getRootUri() . $this->router->urlFor($name, $params);
    }

}
