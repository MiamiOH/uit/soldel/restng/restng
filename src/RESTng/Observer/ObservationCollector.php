<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/8/18
 * Time: 11:44 AM
 */

namespace MiamiOH\RESTng\Observer;


class ObservationCollector
{
    private $classesToObserve = [];

    /** @var Observer[]  */
    private $observers = [];

    private $objectsUnderObservation = [];

    public function observeClassWith(string $observedClass, string $observerClass): void
    {
        $this->prepareToObserve($observedClass);
        $this->addClassObserver($observedClass, $observerClass);
    }

    public function attachObservers(ObservableInterface $observable): void
    {
        if (!$this->willObserve($observable)) {
            return;
        }

        foreach ($this->getObserversForObject($observable) as $observerClass) {
            if ($this->isObservingObjectWithClass($observable, $observerClass)) {
                continue;
            }

            $this->createObserverForObject($observable, $observerClass);
        }
    }

    public function collectObservations(): array
    {
        $observations = [];

        foreach ($this->observers as $observer) {
            $observations[] = $observer->observations();
        }

        return \array_merge($observations);
    }

    public function clearObservations(): void
    {
        foreach ($this->observers as $observer) {
            $observer->clear();
        }
    }

    private function prepareToObserve(string $class): void
    {
        if (\array_key_exists($class, $this->classesToObserve)) {
            return;
        }

        $this->classesToObserve[$class] = [];
    }

    private function addClassObserver(string $class, string $observerClass): void
    {
        $this->classesToObserve[$class][] = $observerClass;
    }

    private function willObserve(ObservableInterface $observable): bool
    {
        return array_key_exists(\get_class($observable), $this->classesToObserve);
    }

    private function getObserversForObject(ObservableInterface $observable): array
    {
        return $this->classesToObserve[\get_class($observable)];
    }

    private function createObserverForObject(ObservableInterface $observable, string $observerClass): void
    {
        $observer = new $observerClass($observable);
        $observable->attach($observer);
        $this->observers[] = $observer;

        $observableId = spl_object_hash($observable);

        if (!array_key_exists($observableId, $this->objectsUnderObservation)) {
            $this->objectsUnderObservation[$observableId] = [];
        }

        $this->objectsUnderObservation[$observableId][] = $observerClass;
    }

    private function isObservingObjectWithClass(ObservableInterface $observable, string $observerClass): bool
    {
        return (
            array_key_exists(spl_object_hash($observable), $this->objectsUnderObservation)
            &&
            in_array($observerClass, $this->objectsUnderObservation[spl_object_hash($observable)])
        );
    }
}