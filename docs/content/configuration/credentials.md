+++
title = "Credentials"
weight = 70
+++

RESTng provides the `MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface` as a means to handle usernames and password or tokens when making authenticated requests. The default implementation is `MiamiOH\RESTng\Service\CredentialValidator\File` which validates against a configuration file rather than an external service. This approach gives you complete control over authentication during development.

The credential configuration is found at:

```bash
config/credentials.yaml
```

The file must contain two keys `users` and `tokens`. Each key is a array of objects as show below. You may have as many users and tokens as you want.

```yaml
---
users:
  - username: bob
    password: Hello123
tokens:
  - username: bob
    token: abc123
```
