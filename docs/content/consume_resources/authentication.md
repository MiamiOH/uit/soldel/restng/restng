+++
title = "Authentication"
weight = 10
+++

Many RESTng services require the consumer to be authenticated in order to use them. The resource itself does not authenticate user, however. Rather, the consumer authenticates to another service and the provides a token with reach request.  The token represents the authenticated user and RESTng will validate it and get back a valid user.

## Authentication

The only currently supported authentication mechanism is our authentication service. The authentication service is exposed as it's own RESTng service and accepts a username/password for validation. It will return a new token upon successful authentication.

In perl, for example:

```perl
my $ua = LWP::UserAgent->new();

my $request = HTTP::Request->new('POST', $server . '/authentication/v1');
my $data = {
  'username' => $config{'username'},
  'password' => $config{'password'},
  'type' => 'usernamePassword'
};

$request->content(to_json($data));
$request->header('Content-Type', 'application/json');

my $response = $ua->simple_request($request);

my $tokenInfo = extractResponseData('response' => $response);

$token = $tokenInfo->{'token'};
$tokenLastUpdate = time();
```

The extractResponseData function would check for errors in the response, parse the returned JSON and return a hash represent the object.

Tokens have a fixed 1 hour lifetime, so if you have a long running process, keep track of the time and get a new token before it expires.

## Using a Token

Once you have a valid token, you must include it in requests to authenticated resources. There are two ways to do this, in the Authorization header or in the query string.

### Authorization Header

The preferred method is to use the HTTP Authorization header. REST, being built on HTTP, can make use of HTTP headers to communicate additional information about a request or response. There a number of relevant standard headers, plus the HTTP standard allows for the inclusion of custom headers by prefixing them with 'X-'.

The Authorization header should be in the form of:

    Authorization: Token token=12345
    
Using the header avoids logging of the token and, with most REST clients, is simple to implement.

### Query String

You can include the token in the query string by simply add 'token=###' to the query after the '?'. 

    https://ws.miamioh.edu/api/course/section?token=12345

While this is easy, there is a downside. Web servers typically log the entire URL for each request. This means you token is being logged in plain text. The actual risk from this is likely low, but should be considered.

