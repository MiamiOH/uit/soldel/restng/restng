<?php

namespace Tests\unit\restng\Util;

use Carbon\Carbon;
use MiamiOH\RESTng\Exception\DeferredCallException;
use MiamiOH\RESTng\Util\DeferredCallFactory;
use MiamiOH\RESTng\Util\DeferredCallModel;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;
use MiamiOH\RESTng\Testing\RefreshDatabase;
use Tests\UnitTestCase;

class DeferredCallFactoryTest extends UnitTestCase
{
    use RefreshDatabase;

    protected $databaseConnections = ['restng'];

    /** @var DeferredCallFactory */
    private $factory;

    /** @var User|MockObject  */
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = $this->createMock(User::class);

        $this->factory = new DeferredCallFactory();
    }

    public function testCreatesNewDeferredCall(): void
    {
        $call = $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
        ]);

        $this->assertEquals('test.resource', $call->resourceName());

        $records = DeferredCallModel::where('id', '=', $call->id())->get();

        $this->assertCount(1, $records);
    }

    public function testCreatesNewDeferredCallScheduledAtNow(): void
    {
        $testNow = Carbon::create(2020, 12, 12, 14, 32, 45);
        Carbon::setTestNow($testNow);

        $call = $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
        ]);

        $this->assertEquals($testNow, $call->scheduledTime());

        Carbon::setTestNow();
    }

    public function testCreatesNewDeferredCallScheduledAtGivenTime(): void
    {
        $scheduledAt = Carbon::now()->addHour();

        $call = $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
            'scheduledTimestamp' => $scheduledAt,
        ]);

        $this->assertEquals($scheduledAt, $call->scheduledTime());
    }

    public function testCreatesNewDeferredCallWithGivenKey(): void
    {
        $call = $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
            'key' => 'abc-123',
        ]);

        $this->assertEquals('abc-123', $call->key());
    }

    public function testNewDeferredCallKeysMustBeUnique(): void
    {
        $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
            'key' => 'abc-123',
        ]);

        $this->expectException(DeferredCallException::class);

        $this->factory->make([
            'resourceName' => 'test.resource',
            'resourceParams' => [],
            'apiUser' => $this->user,
            'key' => 'abc-123',
        ]);
    }


}
