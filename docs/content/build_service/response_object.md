+++
title = "Response Object"
weight = 40
+++

The response object used by your method will be provided by RESTng.  This object must be returned by your method.  At a minimum, your method is responsible for setting the response status and, if appropriate, the payload. 

You can get the response object by calling:

    $response = $this->getResponse();
    
Your method should end by returning the response object:

    return $response;
    
Under no circumstance should you return anything other than the response object, nor should you print or otherwise affect the output buffer. Please see the section on Error Handling for more information on responding to error conditions.

The response object also provides methods for managing other framework features, such as pagination.  These features will be covered in later sections.

## Response Status

The response status must be one of the supported API values.  These are defined as constants in the RESTng setup and are available to sub-classes.  The values map to the REST HTTP response codes, but RESTng may not adhere to that convention in the future.  Developers must use the defined constants.

Constant                | Value | Purpose
------------------------|-------|-----------
MiamiOH\RESTng\API_OK           | 200   | Normal success response
MiamiOH\RESTng\API_CREATED      | 201   | Successful create (POST) action
MiamiOH\RESTng\API_ACCEPTED     | 202 | The rquest has been accepted but is not completed
MiamiOH\RESTng\API_NOCONTENT    | 204   | Successful update (PUT) action, no body content is returned
MiamiOH\RESTng\API_BADREQUEST   | 400   | The request was incorrectly formed and could not be completed, body should contain additional details
MiamiOH\RESTng\API_UNAUTHORIZED | 401   | The consumer could not be authenticated or is not authorized to access the resource
MiamiOH\RESTng\API_NOTFOUND     | 404   | The requested resource could not be found
MiamiOH\RESTng\API_ENTITYALREADYEXISTS | 409 | Creation was rejected because the entity already exists 
MiamiOH\RESTng\API_PRECONDITIONREQUIRED |428 | A precondition required for the action failed
MiamiOH\RESTng\API_FAILED | 500 | A server side error occurred which prevents successful operation
MiamiOH\RESTng\API_NOTIMPLEMENTED | 501 | The requested resource exists but is not implemented

## Response Payload

The response payload is an array representation of the model or collection of models. A collection is a numeric array containing zero or more models.  A model is an associative array of key/value pairs representing the properties of an object.  The values of a model may be collections or models themselves.

The business service must not assume anything about the format or handling of the payload data. Transformation to the format requested by the consumer will be handled by the framework.
