<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 7/3/18
 * Time: 9:21 PM
 */

namespace MiamiOH\RESTng\Service\AuthorizationValidator;


class File implements AuthorizationValidatorInterface
{
    private $authorizations = [];

    public function setAuthorizationFile(string $filename): void
    {
        if (file_exists($filename)) {
            $this->setAuthorizations(file_get_contents($filename));
        }
    }

    public function setAuthorizations(string $authorizationData): void
    {
        $this->authorizations = yaml_parse($authorizationData);
    }

    public function validateAuthorizationForKey(AuthorizationSpecification $authSpec, string $username): bool
    {
        if (!array_key_exists($authSpec->application(), $this->authorizations)) {
            return false;
        }

        if (!array_key_exists($authSpec->module(), $this->authorizations[$authSpec->application()])) {
            return false;
        }

        if (!array_key_exists($authSpec->key(), $this->authorizations[$authSpec->application()][$authSpec->module()])) {
            return false;
        }

        return in_array($username, $this->authorizations[$authSpec->application()][$authSpec->module()][$authSpec->key()]);
    }
}
