+++
title = "Resource Method Pattern"
weight = 20
+++

Each defined resource must have a service and method. RESTng will instanciate a singleton instance of the service and call the specified method. The method invocation must return a valid response object. Any consumable service method must adhere to the following pattern:

{{< highlight php "linenos=table" >}}
<?php
...
public function getStuff () {
    $request = $this->getRequest();
    $response = $this->getResponse();
    
    // Work is done here.
    $records = array();
    
    $response->setStatus(API_OK);
    $response->setPayload($records);

    return $response;
}
{{< / highlight >}} 

If a method is to be consumed by other services, it must follow this pattern and be registered as a resource. A service may provide methods which do not follow this pattern to other classes within its own “package”.  Such methods should not be used outside the package.
