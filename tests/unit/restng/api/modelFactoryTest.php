<?php

class modelFactoryTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $modelFactory;

    private $restngApp;
    private $diContainer;

    private $classMap = array();
    private $getFactoryClass = '';

    public function setUp(): void
    {
        parent::setUp();

        $this->classMap = array();
        $this->getFactoryClass = '';

        $this->restngApp = $this->getMockBuilder('MiamiOH\RESTng\Util\AppInstance')
            ->setMethods(array('getInstance'))
            ->getMock();

        $this->diContainer = $this->getMockBuilder('\Aura\DI')
            ->setMethods(array('newFactory'))
            ->getMock();

        $this->diContainer->method('newFactory')
            ->with($this->callback(array($this, 'newFactoryWith')))
            ->will($this->returnCallback(array($this, 'newFactoryMock')));

        // The Slim class cannot be mocked, possibly due to static methods.
        $slim = new MiamiOH\RESTng\App();
        $slim->setDiContainer($this->diContainer);

        $this->restngApp->method('getInstance')->willReturn($slim);

        $this->modelFactory = new \MiamiOH\RESTng\Util\ModelFactory();

        $this->modelFactory->setAppInstance($this->restngApp);

    }

    /*
     * This is a real hack. The modelFactory expects the value of the map
     * to be a PHP invokeable. In normal use, this is an Aura.DI factory
     * object which returns a new object of the given class when invoked.
     *
     * For mocking, I'm just using a built in function which takes no
     * arguments and will give a predictable result.
     */
    public function testModelFactory() {
        $this->map = array('test' => 'get_include_path');

        $this->modelFactory->setMap($this->map);

        $obj = $this->modelFactory->newInstance('test');

        $this->assertEquals(get_include_path(), $obj);

    }

    public function newFactoryWith($subject)
    {
        $this->getFactoryClass = $subject;
        return true;
    }

    public function newFactoryMock()
    {
        return $this->getFactoryClass;
    }

}
