+++
title = "Start Your Project"
weight = 10
+++

RESTng service projects will rely on composer. There are a few setup steps you should follow when starting your project.

## Composer Init

Create a new project directory, change into it and run:

```shell
composer init
```

This will help you create a composer.json file for your project.

You will need some Miami custom dependencies for your project. Most of these will come from our satis server (https://satis.itapps.miamioh.edu/) which needs to be added to your composer.json file:

{{< highlight json "linenos=table" >}}
{
    "repositories": [{
      "type": "composer",
      "url": "https://satis.itapps.miamioh.edu/miamioh"
    }]
}
{{< / highlight >}} 

During development, it is also acceptable to use VCS repository types to include non-stable releases of other projects. See https://getcomposer.org/doc/05-repositories.md#vcs for more information.

## Autoloading and Namespaces

Composer provides a robust autoloading framework which we rely on during development and also when deploying service projects in RESTng. You will need to decide on an appropriate namespace for your project classes and add the necessary autoloader. A PSR-4 autoloader is fine for this purpose.

{{< highlight json "linenos=table" >}}
{
  "autoload": {
    "psr-4": {
      "MiamiOH\\RESTng\\MyProject\\" : "src/"
    }
  },
  "autoload-dev": {
    "psr-4": {
      "Tests\\": "tests/"
    }
  },
}
{{< / highlight >}} 

## Require RESTng

Requiring the RESTng project as a dependency in your project will allow the IDE to help you in various ways as well as provide testing capabilities. You should be sure to only require RESTng for development installation. Use composer to require RESTng:

```shell
composer require --dev miamioh/restng
```
If the RESTng project cannot be resolved, you may need to add the dependency manually:

{{< highlight json "linenos=table" >}}
{
    "require-dev": {
      "miamioh/restng": "dev-master"
    }
}
{{< / highlight >}} 

Be sure to update dependencies after making manual changes to the composer.json file:

```shell
composer update
```
