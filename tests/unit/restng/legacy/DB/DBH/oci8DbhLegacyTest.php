<?php

include_once('dbOciMocks.php');

class oci8DbhLegacyTest extends \PHPUnit\Framework\TestCase {

    private $dbh;

    /**
     * @var \MiamiOH\RESTng\Service\Apm\Transaction|\PHPUnit\Framework\MockObject\MockObject
     */
    private $transaction;

    public function setUp(): void
  {
    parent::setUp();

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::reset();

    $this->transaction = $this->createMock(\MiamiOH\RESTng\Service\Apm\Transaction::class);

    $this->dbh = new \MiamiOH\RESTng\Legacy\DB\DBH\OCI8();

    $this->dbh->setApmTransaction($this->transaction);
  }

  public function testOciDbh() {

    $this->assertTrue($this->dbh !== null);

  }

  public function testOciDbhConnectNew() {

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockConnectResult', true);

    $result = $this->dbh->connect('restng', 'Hello123', 'XE');

    $this->assertTrue($result);

    $this->assertEquals('new', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('connectType'));

    $this->assertEquals('restng', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('user'));
    $this->assertEquals('Hello123', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('password'));
    $this->assertEquals('XE', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('database'));

  }

  public function testOciDbhConnectPersistent() {

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockConnectResult', true);

    $result = $this->dbh->connect('restng', 'Hello123', 'XE', 'persistent');

    $this->assertTrue($result);

    $this->assertEquals('persistent', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('connectType'));

    $this->assertEquals('restng', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('user'));
    $this->assertEquals('Hello123', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('password'));
    $this->assertEquals('XE', \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('database'));

  }

  public function testOciDbhConnectError() {

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockConnectResult', false);
    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockOciError',
      array(
        'code' => '123',
        'message' => 'An SQL error occurred',
        'offset' => '10',
        'sqltext' => 'select * from some_table',
      ));

      $this->expectException(\Exception::class);
      $this->expectExceptionMessage('An SQL error occurred');

      $result = $this->dbh->connect('restng', 'Helloabc', 'XE');

    $this->assertTrue($result);

  }

  public function testOciDbhDisconnect() {

    $mockDb = new StdClass();

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockConnectResult', $mockDb);
    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockDisconnectResult', true);

    $result = $this->dbh->connect('restng', 'Hello123', 'XE');

    $this->assertTrue($result);

    $result = $this->dbh->disconnect();

    $this->assertTrue($result);
    $this->assertEquals($mockDb, \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::get('closeResource'));

  }

  public function testOciDbhPrepare() {

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockConnectResult', true);

    $result = $this->dbh->connect('restng', 'Hello123', 'XE');

    $mockDb = new StdClass();

    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockParseResult', $mockDb);
    \MiamiOH\RESTng\Legacy\DB\DBH\dbOciMockData::set('mockOciError', false);

    $query = 'select * from some_table';

    $sth = $this->dbh->prepare($query);

    $this->assertEquals('MiamiOH\RESTng\Legacy\DB\STH\OCI8', get_class($sth));

  }

}
