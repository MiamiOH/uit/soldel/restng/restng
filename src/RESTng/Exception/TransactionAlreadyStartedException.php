<?php

namespace MiamiOH\RESTng\Exception;

class TransactionAlreadyStartedException extends ApmException
{
}
