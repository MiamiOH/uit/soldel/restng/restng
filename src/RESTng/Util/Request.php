<?php

namespace MiamiOH\RESTng\Util;

class Request implements \Serializable
{

    protected $resourceConfig = array();
    protected $resourceConfigReservedKeys = array(
        'offset',
        'limit',
        'fields',
    );

    // Resource params come from pattern (i.e. /resource/:param1/:param2)
    protected $resourceParams = array();
    protected $resourceParamValues = array();
    protected $resourceName = '';

    // Resource options are GET values (i.e. /resource/123/12?offset=0&limit=10&color=blue)
    protected $options = array();

    // Data representing the object for a create or update action
    protected $data = array();

    // Paging options
    protected $paged = false;
    protected $limit = \MiamiOH\RESTng\App::API_DEFAULTPAGELIMIT;
    protected $maxPageLimit = 0; // Default to no limit
    protected $offset = 1; // Default to starting with first record

    // Partial response and sub-objects
    protected $partialFieldSpec = '';
    protected $isPartialRequest = false;
    protected $isComposedRequest = false;
    protected $partialRequestFields = array();
    protected $subObjects = array();

    // Callback name for jsonp, if given
    protected $callback = '';

    public function serialize() {
        return serialize(array(
            'resourceConfig' => $this->resourceConfig,
            'resourceParams' => $this->resourceParams,
            'resourceName' => $this->resourceName,
            'options' => $this->options,
            'data' => $this->data,
            'paged' => $this->paged,
            'limit' => $this->limit,
            'maxPageLimit' => $this->maxPageLimit,
            'offset' => $this->offset,
            'partialFieldSpec' => $this->partialFieldSpec,
            'isPartialRequest' => $this->isPartialRequest,
            'partialRequestFields' => $this->partialRequestFields,
            'subObjects' => $this->subObjects,
        ));
    }

    public function unserialize($data) {
        $dataArray = unserialize($data);

        $this->resourceConfig = $dataArray['resourceConfig'];
        $this->resourceParams = $dataArray['resourceParams'];
        $this->resourceName = $dataArray['resourceName'];
        $this->options = $dataArray['options'];
        $this->data = $dataArray['data'];
        $this->paged = $dataArray['paged'];
        $this->limit = $dataArray['limit'];
        $this->maxPageLimit = $dataArray['maxPageLimit'];
        $this->offset = $dataArray['offset'];
        $this->partialFieldSpec = $dataArray['partialFieldSpec'];
        $this->isPartialRequest = $dataArray['isPartialRequest'];
        $this->partialRequestFields = $dataArray['partialRequestFields'];
        $this->subObjects = $dataArray['subObjects'];
    }

    /**
     * @param string $name
     */
    public function setResourceName($name)
    {
        $this->resourceName = $name;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @param array $config
     * @throws \Exception
     */
    public function setResourceConfig($config)
    {
        if (!is_array($config)) {
            throw new \Exception('Argument for ' . __CLASS__ . '::setResourceConfig must be an array');
        }

        if (!(isset($config['name']) && $config['name'])) {
            throw new \Exception('Config argument for ' . __CLASS__ . '::setResourceConfig must contain a name element');
        }

        $this->setResourceName($config['name']);
        $this->resourceConfig = $config;
    }

    /**
     * @return array
     */
    public function getResourceConfig()
    {
        return $this->resourceConfig;
    }

    /*
      Don't use the word 'route' here, implies interface
    */
    /**
     * @param array $params
     * @throws \Exception
     */
    public function setResourceParams($params)
    {
        if (!is_array($params)) {
            throw new \Exception('Argument for ' . __CLASS__ . '::setRouteParams must be an array');
        }

        $this->resourceParams = [];
        $this->resourceParamValues =[];

        if (isset($this->resourceConfig['params'])) {
            foreach ($this->resourceConfig['params'] as $paramName => $paramConfig) {
                if (!isset($params[$paramName])) {
                    continue;
                }

                $param = new \MiamiOH\RESTng\Util\RequestParameter($paramConfig);
                $param->setParameter($paramName, $params[$paramName]);

                $this->resourceParams[$paramName] = $param;

                $this->resourceParamValues[$paramName] = $param->getValue();

            }
        }
    }

    /**
     * @return array
     */
    public function getResourceParams()
    {
        return $this->resourceParamValues;
    }

    /**
     * @param string $name
     * @param string $default
     * @return string
     */
    public function getResourceParam($name, $default = null)
    {
        if (isset($this->resourceParams[$name])) {
            return $this->resourceParams[$name]->getValue();
        } else {
            return $default;
        }
    }

    public function getResourceParamKey($name)
    {
        if (isset($this->resourceParams[$name])) {
            return $this->resourceParams[$name]->getKey();
        } else {
            return null;
        }
    }


    /**
     * This is a short cut for setDataAsArray and exists for backward compatibility.
     *
     * @param $data
     */
    public function setData($data)
    {
        return $this->setDataAsArray($data);
    }

    public function setDataAsArray($data)
    {
        /*
         * The dataRepresentation should be set in the resource config and never be empty by the
         * time we get here.
         */
        if (empty($this->resourceConfig['dataRepresentation'])) {
            throw new \Exception('Missing dataRepresentation');
        }

        if ($this->resourceConfig['dataRepresentation'] !== 'array') {
            throw new \Exception('Cannot set data as array when dataRepresentation is ' .
                $this->resourceConfig['dataRepresentation']);
        }

        // The data variable should always be set at this point, but if it is a
        // truthy value and not an array, throw an exception.
        if ($data && !is_array($data)) {
            throw new \Exception('Argument for ' . __CLASS__ . '::setDataAsArray must be an array');
        }

        // Getting here means the data is an array or is a falsey value that should
        // properly be an empty array. This may not be correct and should be reconsidered
        // in the future as payload types expand.
        if (!is_array($data)) {
            $data = array();
        }

        $this->data = $data;

    }

    public function setDataAsFile($data)
    {
        /*
         * The dataRepresentation should be set in the resource config and never be empty by the
         * time we get here.
         */
        if (empty($this->resourceConfig['dataRepresentation'])) {
            throw new \Exception('Missing dataRepresentation');
        }

        if ($this->resourceConfig['dataRepresentation'] !== 'file') {
            throw new \Exception('Cannot set data as file when dataRepresentation is ' .
                $this->resourceConfig['dataRepresentation']);
        }

        if ($data && !is_scalar($data)) {
            throw new \Exception('Argument for ' . __CLASS__ . '::setData must be an array');
        }

        $this->data = $data;

    }

    /**
     * @return array
     */
    public function getData()
    {
        if (empty($this->resourceConfig['dataRepresentation'])) {
            throw new \Exception('Missing dataRepresentation');
        }

        if ($this->resourceConfig['dataRepresentation'] !== 'array') {
            throw new \Exception('Cannot get data as array when dataRepresentation is ' .
                $this->resourceConfig['dataRepresentation']);
        }

        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getDataFile()
    {
        if (empty($this->resourceConfig['dataRepresentation'])) {
            throw new \Exception('Missing dataRepresentation');
        }

        if ($this->resourceConfig['dataRepresentation'] !== 'file') {
            throw new \Exception('Cannot get data as file when dataRepresentation is ' .
                $this->resourceConfig['dataRepresentation']);
        }

        return $this->data;
    }

    /**
     * @param $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    /**
     * @return string
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param array $options
     * @throws \Exception
     */
    public function setOptions($options)
    {
        if (!is_array($options)) {
            throw new \Exception('Argument for ' . __CLASS__ . '::setOptions must be an array');
        }

        // Validate options against the declared configuration
        $this->options = array();

        foreach (array_keys($options) as $key) {
            if (in_array($key, $this->resourceConfigReservedKeys)) {
                throw new \Exception('Attempt to use reserved option key ' . $key . ' for resource ' . $this->resourceName);
            }

            if (!isset($this->resourceConfig['options'][$key])) {
                throw new \Exception('No defined option ' . $key . ' for resource ' . $this->resourceName);
            }

            if (isset($this->resourceConfig['options'][$key]['type']) &&
                $this->resourceConfig['options'][$key]['type'] === 'list'
            ) {
                // The callResource method can accept options as an array directly, so check
                // the type before exploding
                if (is_array($options[$key])) {
                    $this->options[$key] = $options[$key];
                } else {
                    $this->options[$key] = explode(\MiamiOH\RESTng\App::API_LIST_SEPARATOR, $options[$key]);
                }
            } else {
                // The value must be a string at this point
                if (!is_scalar($options[$key])) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest('Value for ' . $key . ' must be a scalar');
                }
                
                $this->options[$key] = $options[$key];
            }
        }

        if (isset($this->resourceConfig['options']) && is_array($this->resourceConfig['options'])) {
            // Verify that required options are present
            foreach (array_keys($this->resourceConfig['options']) as $key) {
                if ($this->resourceConfig['options'][$key]['required'] && !isset($this->options[$key])) {
                    throw new \Exception('Missing required option ' . $key . ' for resource ' . $this->resourceName);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $resourceOptions
     * @throws \Exception
     */
    public function extractRequestOptions($resourceOptions)
    {
        // If the resource is pageable and there is a default page limit, then enable paging
        // for the request. I would like to simply enable paging if the resource is pageable,
        // but that would enforce the limit and cause resources to quietly return limited
        // records. We can, however, assume that the presence of a default page limit implies
        // the resource expects to always have a limit the consumer should be aware of.
        if (isset($this->resourceConfig['isPageable']) && $this->resourceConfig['isPageable'] &&
            isset($this->resourceConfig['defaultPageLimit'])) {
            $this->paged = true;
        }

        // Set the limit member if a default is given. Do this directly rather than through the
        // setLimit method to avoid triggering the paged indicator prematurely.
        if (isset($this->resourceConfig['defaultPageLimit'])) {
            $this->limit = $this->resourceConfig['defaultPageLimit'];
        }

        // Set our maxPageLimit before calling setLimit
        $this->maxPageLimit = isset($this->resourceConfig['maxPageLimit']) ? $this->resourceConfig['maxPageLimit'] : 0;

        // Extract any paging indicators
        if (isset($resourceOptions['limit']) && $resourceOptions['limit']) {
            $this->setLimit($resourceOptions['limit']);
            unset($resourceOptions['limit']);
        }

        if (isset($resourceOptions['offset']) && $resourceOptions['offset']) {
            $this->setOffset($resourceOptions['offset']);
            unset($resourceOptions['offset']);
        }

        if (isset($resourceOptions['callback']) && $resourceOptions['callback']) {
            $this->setCallback($resourceOptions['callback']);
            unset($resourceOptions['callback']);
        }

        if ($this->resourceConfig['isComposable'] && isset($resourceOptions['compose'])) {
            // The callResource method will accept the compose option as an array to make
            // it easier to use. Rather than trying to be overly complicated, I'll just
            // implode the array to a string for normal processing.
            if (is_array($resourceOptions['compose'])) {
                $resourceOptions['compose'] = implode(',', $resourceOptions['compose']);
            }

            $composeSpec = $resourceOptions['compose'];

            preg_match_all('/(\w+\([^\)]*\))/', $composeSpec, $subObjects);

            foreach ($subObjects[0] as $subObjectSpec) {
                $composeSpec = str_replace($subObjectSpec, '', $composeSpec);

                preg_match('/(\w+)\(([^\)]*)\)/', $subObjectSpec, $subObjectDef);

                $subObjectName = $subObjectDef[1]; // First captured match
                $subObjectFields = $subObjectDef[2] ? explode(',', $subObjectDef[2]) : array(); // Second captured match

                $this->addSubObject($subObjectName, $subObjectFields);
            }

            // Clean up any remaining commas, collapsing multiple commas to 1 first,
            // then removing leading and trailing commas
            $composeSpec = preg_replace(array('/,{2,}/', '/^,/', '/,$/'), array(',', '', ''), $composeSpec);

            if ($composeSpec) {
                foreach(explode(',', $composeSpec) as $subObjectName) {
                    $this->addSubObject($subObjectName, []);
                }
            }

            //$this->setPartialFieldsSpec($resourceOptions['compose']);

            unset($resourceOptions['compose']);
        }

        if ($this->resourceConfig['isPartialable'] && isset($resourceOptions['fields'])) {
            // The callResource method will accept the fields option as an array to make
            // it easier to use. Rather than trying to be overly complicated, I'll just
            // implode the array to a string for normal processing.
            if (is_array($resourceOptions['fields'])) {
                $resourceOptions['fields'] = implode(',', $resourceOptions['fields']);
            }

            $fieldSpec = $resourceOptions['fields'];

            preg_match_all('/(\w+\([^\)]*\))/', $fieldSpec, $subObjects);

            // Throw an exception if there are subobjects in the fields spec and objects have
            // already been set via compose. Combining the two methods is discouraged.
            if ($subObjects[0] && $this->isComposed()) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Requesting subobjects via fields and compose concurrently is not allowed');
            }
            
            foreach ($subObjects[0] as $subObjectSpec) {
                $fieldSpec = str_replace($subObjectSpec, '', $fieldSpec);

                preg_match('/(\w+)\(([^\)]*)\)/', $subObjectSpec, $subObjectDef);

                $subObjectName = $subObjectDef[1]; // First captured match
                $subObjectFields = $subObjectDef[2] ? explode(',', $subObjectDef[2]) : array(); // Second captured match

                $this->addSubObject($subObjectName, $subObjectFields);
            }

            // Clean up any remaining commas, collapsing multiple commas to 1 first,
            // then removing leading and trailing commas
            $fieldSpec = preg_replace(array('/,{2,}/', '/^,/', '/,$/'), array(',', '', ''), $fieldSpec);

            if ($fieldSpec) {
                $fields = explode(',', $fieldSpec);
                $this->setPartialFields($fields);
            }

            $this->setPartialFieldsSpec($resourceOptions['fields']);

            unset($resourceOptions['fields']);
        }

        // The remaining resource options are made available to the service.
        $this->setOptions($resourceOptions);

    }

    /**
     * @param $spec
     */
    public function setPartialFieldsSpec($spec)
    {
        $this->partialFieldSpec = $spec;
    }

    /**
     * @return string
     */
    public function getPartialFieldsSpec()
    {
        return $this->partialFieldSpec;
    }

    /**
     * @param array $fields
     * @throws \Exception
     */
    public function setPartialFields($fields)
    {
        if (!is_array($fields)) {
            throw new \Exception('Fields must be provided as an array when adding a subObject to a request');
        }

        $this->partialRequestFields = array();

        foreach ($fields as $field) {
            if ($field) {
                $this->partialRequestFields[] = $field;
            }
        }

        if (count($this->partialRequestFields) > 0) {
            $this->setIsPartialRequest(true);
        }
    }

    /**
     * @return array
     */
    public function getPartialRequestFields()
    {
        return $this->partialRequestFields;
    }

    /**
     * @param $isPartial
     */
    public function setIsPartialRequest($isPartial)
    {
        $this->isPartialRequest = $isPartial;
    }

    /**
     * @return bool
     */
    public function isPartial()
    {
        return $this->isPartialRequest;
    }

    /**
     * @param $isComposed
     */
    public function setIsComposedRequest($isComposed)
    {
        $this->isComposedRequest = $isComposed;
    }

    /**
     * @return bool
     */
    public function isComposed()
    {
        return $this->isComposedRequest;
    }

    /**
     * @param $name
     * @param $fields
     * @throws \Exception
     */
    public function addSubObject($name, $fields)
    {
        if (!$name) {
            throw new \Exception('Name is required to add a subObject to a request');
        }

        if (!is_array($fields)) {
            throw new \Exception('Fields must be provided as an array when adding a subObject to a request');
        }

        $this->subObjects[$name] = $fields;
        $this->setIsComposedRequest(true);

    }

    /**
     * @return array
     */
    public function getSubObjects()
    {
        return $this->subObjects;
    }

    /**
     * @return bool
     */
    public function isPaged()
    {
        return $this->paged;
    }

    /**
     * @param $limit
     * @throws \Exception
     */
    public function setLimit($limit)
    {
        if (!is_numeric($limit)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Limit argument must be a numeric value ' . $limit);
        }

        if ((string)(int)$limit !== (string)$limit) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Limit argument must be an integer value ' . $limit);
        }

        if ($limit < 1) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Limit argument must be 1 or higher');
        }

        if ($this->maxPageLimit && $limit > $this->maxPageLimit) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Max page limit for resource is ' . $this->maxPageLimit);
        }

        $this->limit = $limit;
        $this->paged = true;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param $offset
     * @throws \Exception
     */
    public function setOffset($offset)
    {
        if (!is_numeric($offset)) {
            throw new \Exception('Offset argument must be a numeric value ' . $offset);
        }

        if ((string)(int)$offset !== (string)$offset) {
            throw new \Exception('Offset argument must be an integer value ' . $offset);
        }

        if ($offset < 1) {
            throw new \Exception('Offset argument must be 1 or higher');
        }

        $this->offset = $offset;
        $this->paged = true;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

}
