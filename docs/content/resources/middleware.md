+++
title = "Middleware"
weight = 110
+++

Middleware is a means by which common functionality can be applied to a resource before your service method is invoked. At this time, there are two supported middleware features you can configure for your resource, **authentication** and **authorization**.

{{< highlight php "linenos=table" >}}
<?php
...
$this->addResource(array(
    'action' => 'read',
    'name' => 'citest.record.all',
    'description' => 'A pageable collection of test records. No params or options.',
    'tags' => array('CITest'),
    'pattern' => '/citest/record',
    'service' => 'CITest',
    'method' => 'getExample',
    'isPageable' => true,
    'middleware' => array(
        'authenticate' => array(),
        'authorize' => array(
            array(
                'application' => 'RESTng Test App',
                'module' => 'User',
                'key' => 'view'),
        ),
    ),
));
{{< / highlight >}}
 
## Authentication

The authentication middleware uses established methods to authenticate the user making the request. The presence of the **authenticate** middleware configuration triggers authentication. The authenticate configuration must be an array. An empty array will cause the default 'token' authentication method to be used. If present, each value in the configuration array should be an array itself containing at least a 'type' key. The valid types are 'token' and 'anonymous'.

The token type uses Miami's authentication service to validate the provided token and retrieve the associated user.
 
The anonymous type allows a resource to support, but not require, authentication. Note that if you want to do this, you must explicitly declare a token authentication type as well:

{{< highlight php "linenos=table" >}}
<?php
...
'middleware' => array(
    'authenticate' => array(
        array(
            'type' => 'anonymous'
        ),
        array(
            'type' => 'token'
        )
    )
),
{{< / highlight >}}

The current API User object (exposed through the \MiamiOH\RESTng\Service::getApiUser() method) will be populated with the authenticated user information.  A \MiamiOH\RESTng\API_UNAUTHORIZED status will be returned immediately to the consumer if authentication fails, including the case of an invalid token. Refer to the business service section for more details about using the authenticated user in your service.

## Authorization

The authorization middleware will perform authorization of the authenticated user to the resource. The value of the authorize middleware key should be an array of configuration arrays. Each configuration array must meet the format of one of the supported authorization types. All authorizations are performed with the currently authenticated user (with the exception of the anonymous type) and authorization middleware requires authentication middleware be enabled. The callResource method uses this same authorization configuration to authorize internal (service to service) calls.

The **authMan** type is the default type and uses an application and module, with a single grant key or an array of grant keys. These values are used to check the authorization of the current user against our AuthMan service. The check will stop on the first positive match if multiple keys are present.

The **self** type takes a parameter argument which is the name of a resource parameter to treat as the username in the resource path. If the value of this parameter matches the current username, the user is authorized. Both the parameter value and the current username will be lowercased and then checked for an exact match.

The **anonymous** type allows authorization of an unauthenticated user along with the anonymous authentication type. This is useful when a resource supports authorization for additional features, but also needs to provide anonymous access for basic features.

You can specify multiple authorization methods, including more than one of each type. The first successful authorization will stop the process. Authorizations are processed in the order they are defined. The API user object will cache successful authorizations.

The following example uses two different authMan checks (the first with multiple keys) before if the current user is accessing their own resource. Finally, it allows anonymous access.

{{< highlight php "linenos=table" >}}
<?php
...
'authorize' => array(
    array( 'type' => 'authMan',
         'application' => 'Person Service', 
         ‘module’ => 'Read Attributes', 
         'key' => array('view', 'edit')
    ),
    array( 'type' => 'authMan',
         'application' => 'Person Service', 
         ‘module’ => 'Read Attributes', 
         'key' => 'admin',
    ),
    array( 'type' => 'self',
         'param' => 'id', 
    ),
    array( 'type' => 'anonymous',
    ),
),
{{< / highlight >}} 

### Authorization Caching

The RESTng framework provides a caching service which the Authorization middleware uses for all configured `authMan` type authorizations. While this greatly improves the response time for requests using identical authorization parameters, be aware that changes in authorization may not take affect until cached entries expire.
